//
//  MyCalendar.m
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/9/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "MyCalendar.h"
#import <EventKit/EventKit.h>
static EKEventStore *eventStore = nil;
@implementation MyCalendar

+ (void)requestAccess:(void (^)(BOOL granted, NSError *error))callback;
{
    if (eventStore == nil) {
        eventStore = [[EKEventStore alloc] init];
    }
    // request permissions
    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:callback];
}

+ (BOOL)addEventAt:(NSDate*)eventStartDate eventEndDate:(NSDate *)endDate withTitle:(NSString*)title inLocation:(NSString*)location
{
    EKEvent *event = [EKEvent eventWithEventStore:eventStore];
    EKCalendar *calendar = nil;
    NSString *calendarIdentifier = [[NSUserDefaults standardUserDefaults] valueForKey:@"my_calendar_identifier"];
    EKSource* localSource;
    // when identifier exists, my calendar probably already exists
    // note that user can delete my calendar. In that case I have to create it again.
    if (calendarIdentifier) {
        calendar = [eventStore calendarWithIdentifier:calendarIdentifier];
    }
    
    if (eventStartDate != endDate) {
        EKRecurrenceRule *recurrenceRule = [[EKRecurrenceRule alloc]
                                            initRecurrenceWithFrequency:EKRecurrenceFrequencyDaily
                                            interval:1
                                            end:[EKRecurrenceEnd recurrenceEndWithEndDate:endDate]];
        event.recurrenceRules = @[recurrenceRule];

    }
   
    // calendar doesn't exist, create it and save it's identifier
    if (!calendar) {
        // http://stackoverflow.com/questions/7945537/add-a-new-calendar-to-an-ekeventstore-with-eventkit
        calendar = [EKCalendar calendarForEntityType:EKEntityTypeEvent eventStore:eventStore];
        
        // set calendar name. This is what users will see in their Calendar app
        [calendar setTitle:NSLocalizedString(@"APP_NAME", "")];
        
        // find appropriate source type. I'm interested only in local calendars but
        // there are also calendars in iCloud, MS Exchange, ...
        // look for EKSourceType in manual for more options
        
        for (EKSource *source in eventStore.sources)
        {
            if (source.sourceType == EKSourceTypeCalDAV &&
                [source.title isEqualToString:@"iCloud"]) //Couldn't find better way, if there is, then tell me too. :)
            {
                calendar.source  = source;
                break;
            }
        }
        
        if (localSource == nil)
        {
            for (EKSource *source in eventStore.sources)
            {
                if (source.sourceType == EKSourceTypeLocal)
                {
                    calendar.source  = source;
                    break;
                }
            }
        }
        
        // save this in NSUserDefaults data for retrieval later
        NSString *calendarIdentifier = [calendar calendarIdentifier];
        
        NSError *error = nil;
        BOOL saved = [eventStore saveCalendar:calendar commit:YES error:&error];
        if (saved) {
            // http://stackoverflow.com/questions/1731530/whats-the-easiest-way-to-persist-data-in-an-iphone-app
            // saved successfuly, store it's identifier in NSUserDefaults
            [[NSUserDefaults standardUserDefaults] setObject:calendarIdentifier forKey:@"my_calendar_identifier"];
        } else {
            // unable to save calendar
            return NO;
        }
    }
    
    // this shouldn't happen
    if (!calendar) {
        return NO;
    }
    
    // assign basic information to the event; location is optional
    event.calendar = calendar;
    event.location = location;
    event.title = title;
    event.allDay = NO;
    // set the start date to the current date/time and the event duration to two hours
    NSDate *startDate = eventStartDate;
    event.startDate = startDate;
    event.endDate = endDate;
    event.alarms=[NSArray arrayWithObject:[EKAlarm alarmWithRelativeOffset:-3600]];
    NSError *error = nil;
    // save event to the callendar
    BOOL result = [eventStore saveEvent:event span:EKSpanThisEvent commit:YES error:&error];
    if (result) {
        return YES;
    } else {
        // //NSLog(@"Error saving event: %@", error);
        // unable to save event to the calendar
        return NO;
    }
}


/*

+ (BOOL)addEventAt:(NSDate*)eventStartDate eventEndDate:(NSDate *)endDate withTitle:(NSString*)title inLocation:(NSString*)location
{
    EKEvent *event = [EKEvent eventWithEventStore:eventStore];
    EKCalendar *calendar = nil;
    NSString *calendarIdentifier = [[NSUserDefaults standardUserDefaults] valueForKey:@"my_calendar_identifier"];
    
    // when identifier exists, my calendar probably already exists
    // note that user can delete my calendar. In that case I have to create it again.
    if (calendarIdentifier) {
        calendar = [eventStore calendarWithIdentifier:calendarIdentifier];
    }
    
    // calendar doesn't exist, create it and save it's identifier
    if (!calendar) {
        // http://stackoverflow.com/questions/7945537/add-a-new-calendar-to-an-ekeventstore-with-eventkit
        calendar = [EKCalendar calendarForEntityType:EKEntityTypeEvent eventStore:eventStore];
        
        // set calendar name. This is what users will see in their Calendar app
        [calendar setTitle:@"Immigo"];
        
        // find appropriate source type. I'm interested only in local calendars but
        // there are also calendars in iCloud, MS Exchange, ...
        // look for EKSourceType in manual for more options
        for (EKSource *s in eventStore.sources) {
            if (s.sourceType == EKSourceTypeLocal) {
                calendar.source = s;
                break;
            }
        }
        
        // save this in NSUserDefaults data for retrieval later
        NSString *calendarIdentifier = [calendar calendarIdentifier];
        
        NSError *error = nil;
        BOOL saved = [eventStore saveCalendar:calendar commit:YES error:&error];
        if (saved) {
            // http://stackoverflow.com/questions/1731530/whats-the-easiest-way-to-persist-data-in-an-iphone-app
            // saved successfuly, store it's identifier in NSUserDefaults
            [[NSUserDefaults standardUserDefaults] setObject:calendarIdentifier forKey:@"my_calendar_identifier"];
        } else {
            // unable to save calendar
            return NO;
        }
    }
    
    // this shouldn't happen
    if (!calendar) {
        return NO;
    }
    
    // assign basic information to the event; location is optional
    event.calendar = calendar;
    event.location = location;
    event.title = title;
    event.allDay = NO;
    // set the start date to the current date/time and the event duration to two hours
    NSDate *startDate = eventStartDate;
    event.startDate = startDate;
    event.endDate = endDate;
    event.alarms=[NSArray arrayWithObject:[EKAlarm alarmWithRelativeOffset:-3600]];
    NSError *error = nil;
    // save event to the callendar
    BOOL result = [eventStore saveEvent:event span:EKSpanThisEvent commit:YES error:&error];
    if (result) {
        return YES;
    } else {
        // //NSLog(@"Error saving event: %@", error);
        // unable to save event to the calendar
        return NO;
    }
}
*/
@end
