//
//  ImmigoTests.m
//  ImmigoTests
//
//  Created by pradeep ISPV on 2/27/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "EventsModel.h"
#import "NewsModel.h"
#import "PrivacyModel.h"
@interface ImmigoTests : XCTestCase

@end

@implementation ImmigoTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

- (void)testSigletonEventsModel
{
    EventsModel *obj1= [[EventsModel alloc] init];
    EventsModel *obj2= [EventsModel sharedDataSource];
    EventsModel *obj3= [[EventsModel sharedDataSource] copy];
    EventsModel *obj4= [[EventsModel allocWithZone:nil] init];
    EventsModel *obj5= [EventsModel alloc];
    EventsModel *obj6= [obj1 copy];
    XCTAssertEqualObjects(obj1, obj1, @"fail to create single ton object");
    XCTAssertEqualObjects(obj1, obj2, @"fail to create single ton object");
    XCTAssertEqualObjects(obj1, obj3, @"fail to create single ton object");
    XCTAssertEqualObjects(obj1, obj4, @"fail to create single ton object");
    XCTAssertEqualObjects(obj1, obj5, @"fail to create single ton object");
    XCTAssertEqualObjects(obj1, obj6, @"fail to create single ton object");
}

- (void)testNewsModel
{
    NewsModel *obj1= [[NewsModel alloc] init];
    NewsModel *obj2= [NewsModel sharedDataSource];
    NewsModel *obj3= [[NewsModel sharedDataSource] copy];
    NewsModel *obj4= [[NewsModel allocWithZone:nil] init];
    NewsModel *obj5= [NewsModel alloc];
    NewsModel *obj6= [obj1 copy];
    XCTAssertEqualObjects(obj1, obj1, @"fail to create NewsModel single ton object");
    XCTAssertEqualObjects(obj1, obj2, @"fail to create NewsModel single ton object");
    XCTAssertEqualObjects(obj1, obj3, @"fail to create NewsModel single ton object");
    XCTAssertEqualObjects(obj1, obj4, @"fail to create NewsModel single ton object");
    XCTAssertEqualObjects(obj1, obj5, @"fail to create  NewsModel  single ton object");
    XCTAssertEqualObjects(obj1, obj6, @"fail to create NewsModel single ton object");
}

- (void)testPrivacyModel
{
    PrivacyModel *obj1= [[PrivacyModel alloc] init];
    PrivacyModel *obj2= [PrivacyModel sharedDataSource];
    PrivacyModel *obj3= [[PrivacyModel sharedDataSource] copy];
    PrivacyModel *obj4= [[PrivacyModel allocWithZone:nil] init];
    PrivacyModel *obj5= [PrivacyModel alloc];
    PrivacyModel *obj6= [obj1 copy];
    XCTAssertEqualObjects(obj1, obj1, @"fail to create PrivacyModel single ton object");
    XCTAssertEqualObjects(obj1, obj2, @"fail to create PrivacyModel single ton object");
    XCTAssertEqualObjects(obj1, obj3, @"fail to create PrivacyModel single ton object");
    XCTAssertEqualObjects(obj1, obj4, @"fail to create PrivacyModel single ton object");
    XCTAssertEqualObjects(obj1, obj5, @"fail to create  PrivacyModel  single ton object");
    XCTAssertEqualObjects(obj1, obj6, @"fail to create PrivacyModel single ton object");
}

-(void)testGetRequestForAlertsThatNeedToFailWithParams
{
    NSDictionary *dictParams=[[NSDictionary alloc] initWithObjectsAndKeys:
                              @1, @"Param1",
                              @2, @"Param2",
                              @3, @"Param3",
                              nil];
    APIRequest *request=[APIRequest apiRequestWithKey:API_ALERTS_GET];
    [request setRequestDataWithDictionary:dictParams];
    ASIHTTPRequest *httpRequest=[HttpBridge invokeAPIWithRequest:request];
    XCTAssertNil(httpRequest, @"Alerts HTTP Request object is nil");
    dictParams=nil;
    
}
-(void)testGetRequestForAlertsThatNeedToPassWithOutParams
{
    APIRequest *request=[APIRequest apiRequestWithKey:API_ALERTS_GET];
    ASIHTTPRequest *httpRequest=[HttpBridge invokeAPIWithRequest:request];
    XCTAssert(httpRequest, @"Alerts Fetch Success !");
    
}

-(void)testGetRequestForNewsWithParams
{
    NSDictionary *dictParams=[[NSDictionary alloc] initWithObjectsAndKeys:
                              @1, @"Param1",
                              @2, @"Param2",
                              @3, @"Param3",
                              nil];
    APIRequest *request=[APIRequest apiRequestWithKey:API_NEWS_GET_ALL];
    [request setRequestDataWithDictionary:dictParams];
    ASIHTTPRequest *httpRequest=[HttpBridge invokeAPIWithRequest:request];
    XCTAssertNil(httpRequest, @"HTTP Request object is nil");
    dictParams=nil;
}
-(void)testGetRequestForNewsWithOutParams
{
//    NSDictionary *dictParams=[[NSDictionary alloc] initWithObjectsAndKeys:
//                              @1, @"Param1",
//                              @2, @"Param2",
//                              @3, @"Param3",
//                              nil];
    APIRequest *request=[APIRequest apiRequestWithKey:API_NEWS_GET_ALL];
//    [request setRequestDataWithDictionary:dictParams];
    ASIHTTPRequest *httpRequest=[HttpBridge invokeAPIWithRequest:request];
    XCTAssert(httpRequest, @"News Fetch Success !");
//    dictParams=nil;
}
-(void)testGetRequestForEventsThatNeedToFailWithParams
{
    NSDictionary *dictParams=[[NSDictionary alloc] initWithObjectsAndKeys:
                              @1, @"Param1",
                              @2, @"Param2",
                              @3, @"Param3",
                              nil];
    APIRequest *request=[APIRequest apiRequestWithKey:API_TRAININGSANDEVENTS_GET_ALL];
    [request setRequestDataWithDictionary:dictParams];
    ASIHTTPRequest *httpRequest=[HttpBridge invokeAPIWithRequest:request];
    XCTAssertNil(httpRequest, @"Trainings And Events HTTP Request object is nil");
    dictParams=nil;
}
-(void)testGetRequestForEventsThatNeedToPassWithOutParams
{
    APIRequest *request=[APIRequest apiRequestWithKey:API_TRAININGSANDEVENTS_GET_ALL];
    ASIHTTPRequest *httpRequest=[HttpBridge invokeAPIWithRequest:request];
    XCTAssert(httpRequest, @"Trainings And Events Fetch Success !");
    //    dictParams=nil;
}
-(void)testGetRequestForImmigrationBasicsThatNeedToFailWithParams
{
    NSDictionary *dictParams=[[NSDictionary alloc] initWithObjectsAndKeys:
                              @1, @"Param1",
                              @2, @"Param2",
                              @3, @"Param3",
                              nil];
    APIRequest *request=[APIRequest apiRequestWithKey:API_IMMIGRATIONBASICS_GET_ALL];
    [request setRequestDataWithDictionary:dictParams];
    ASIHTTPRequest *httpRequest=[HttpBridge invokeAPIWithRequest:request];
    XCTAssertNil(httpRequest, @"Immigration Basics HTTP Request object is nil");
    dictParams=nil;
}
-(void)testGetRequestForImmigrationBasicsThatNeedToPassWithOutParams
{
    APIRequest *request=[APIRequest apiRequestWithKey:API_IMMIGRATIONBASICS_GET_ALL];
    ASIHTTPRequest *httpRequest=[HttpBridge invokeAPIWithRequest:request];
    XCTAssert(httpRequest, @"Immigration Basics Fetch Success !");
    //    dictParams=nil;
}
-(void)testGetRequestForLegalHelpThatNeedToPassWithZipCode
{
    NSDictionary *dictParams=[[NSDictionary alloc] initWithObjectsAndKeys:
                    @"12221",@"zipcode",
                    nil];

    APIRequest *request=[APIRequest apiRequestWithKey:API_LEGALORGANIZATIONS_GET_ALL];
    [request setRequestDataWithDictionary:dictParams];
    ASIHTTPRequest *httpRequest=[HttpBridge invokeAPIWithRequest:request];
    XCTAssert(httpRequest, @"Legal Help Finder With ZipCode HTTP Request object is nil");
    dictParams=nil;
}
-(void)testGetRequestForLegalHelpThatNeedToPassWithCurrentLocation
{
    //        //40.7142700%2C74.0059700
            NSString *gpsString = [NSString stringWithFormat:@"%@,%@",@"40.7142700",@"74.0059700"];
            //        NSString *gpsString = [NSString stringWithFormat:@"%@,%@",@"40.7142700",@"-74.0059700"];
       NSDictionary *dictParams=[[NSDictionary alloc] initWithObjectsAndKeys:
                        gpsString,@"gps",
                        nil];
    
    APIRequest *request=[APIRequest apiRequestWithKey:API_LEGALORGANIZATIONS_GET_ALL];
    [request setRequestDataWithDictionary:dictParams];
    ASIHTTPRequest *httpRequest=[HttpBridge invokeAPIWithRequest:request];
    XCTAssert(httpRequest, @"Legal Help Finder With CurrentLocation HTTP Request object is nil");
    dictParams=nil;
}
-(void)testGetRequestForImmigrationPollsWithInvalidDeviceId
{
    NSDictionary *dictParams= [[NSDictionary alloc] initWithObjectsAndKeys:
                               @1, @"Param1",
                               @2, @"Param2",
                               @3, @"Param3",
                               nil];
    
    
    APIRequest *request = [APIRequest apiRequestWithKey:API_POLL_GET_ALL];
    [request setRequestDataWithDictionary:dictParams];
    ASIHTTPRequest *httpRequest=[HttpBridge invokeAPIWithRequest:request];
    XCTAssertNil(httpRequest, @"PollQuestion HTTP Request object is nil");
    dictParams=nil;
}
-(void)testGetRequestForAboutUsThatNeedToFailWithParams
{
    NSDictionary *dictParams= [[NSDictionary alloc] initWithObjectsAndKeys:
                               @1, @"Param1",
                               @2, @"Param2",
                               @3, @"Param3",
                               nil];
    
    
    APIRequest *request = [APIRequest apiRequestWithKey:API_ABOUT_GET];
    [request setRequestDataWithDictionary:dictParams];
    ASIHTTPRequest *httpRequest=[HttpBridge invokeAPIWithRequest:request];
    XCTAssertNil(httpRequest, @"About Us HTTP Request object is nil");
    dictParams=nil;
}
-(void)testGetRequestForAboutUsThatNeedToPassWithOutParams
{
    APIRequest *request = [APIRequest apiRequestWithKey:API_ABOUT_GET];
    ASIHTTPRequest *httpRequest=[HttpBridge invokeAPIWithRequest:request];
    XCTAssert(httpRequest, @"About Us Fetch Successfull !");
}
-(void)testGetRequestForPrivacyThatNeedToFailWithParams
{
    NSDictionary *dictParams= [[NSDictionary alloc] initWithObjectsAndKeys:
                               @1, @"Param1",
                               @2, @"Param2",
                               @3, @"Param3",
                               nil];
    
    
    APIRequest *request = [APIRequest apiRequestWithKey:API_PRIVACY_GET];
    [request setRequestDataWithDictionary:dictParams];
    ASIHTTPRequest *httpRequest=[HttpBridge invokeAPIWithRequest:request];
    XCTAssertNil(httpRequest, @"About Us HTTP Request object is nil");
    dictParams=nil;
}
-(void)testGetRequestForPrivacyThatNeedToPassWithOutParams
{
    APIRequest *request = [APIRequest apiRequestWithKey:API_PRIVACY_GET];
    ASIHTTPRequest *httpRequest=[HttpBridge invokeAPIWithRequest:request];
    XCTAssert(httpRequest, @"Privacy Fetch Successfull !");
}
-(void)testGetRequestForDisclaimerThatNeedToFailWithParams
{
    NSDictionary *dictParams= [[NSDictionary alloc] initWithObjectsAndKeys:
                               @1, @"Param1",
                               @2, @"Param2",
                               @3, @"Param3",
                               nil];
    
    
    APIRequest *request = [APIRequest apiRequestWithKey:API_TERMSOFUSE_GET];
    [request setRequestDataWithDictionary:dictParams];
    ASIHTTPRequest *httpRequest=[HttpBridge invokeAPIWithRequest:request];
    XCTAssertNil(httpRequest, @"Disclaimer HTTP Request object is nil");
    dictParams=nil;
}
-(void)testGetRequestForDisclaimerThatNeedToPassWithOutParams
{
    APIRequest *request = [APIRequest apiRequestWithKey:API_TERMSOFUSE_GET];
    ASIHTTPRequest *httpRequest=[HttpBridge invokeAPIWithRequest:request];
    XCTAssert(httpRequest, @"Disclaimer Fetch Successfull !");
}

@end
