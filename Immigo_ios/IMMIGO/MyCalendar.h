//
//  MyCalendar.h
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/9/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyCalendar : NSObject

+ (void)requestAccess:(void (^)(BOOL granted, NSError *error))success;
+ (BOOL)addEventAt:(NSDate*)eventStartDate eventEndDate:(NSDate *)endDate withTitle:(NSString*)title inLocation:(NSString*)location;

@end
