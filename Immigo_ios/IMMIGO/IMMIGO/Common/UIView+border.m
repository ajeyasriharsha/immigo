//
//  UIView+border.m
//  ComplianceWire
//
//  Created by Shineeth Hamza on 16/06/14.
//  Copyright (c) 2014 EduNeering Holdings, Inc. All rights reserved.
//

#import "UIView+border.h"

@implementation UIView (border)
- (void)setBorderForSides:(UIBorderSide)sides withColor:(UIColor *)borderColor andWidth:(CGFloat)borderWidth
{
    if ([[UIScreen mainScreen] scale] == 1) {
        borderWidth = ((int)borderWidth)?(int)borderWidth:1.0f;
    }
    if (sides == UIBorderNone) {
        //Remove all border if available
        if ([self viewWithTag:UIBorderTop]) {
            UIView * borderView = [self viewWithTag:UIBorderTop];
            [borderView removeFromSuperview];
            borderView = nil;
        }
        if ([self viewWithTag:UIBorderLeft]) {
            UIView * borderView = [self viewWithTag:UIBorderLeft];
            [borderView removeFromSuperview];
            borderView = nil;
        }
        if ([self viewWithTag:UIBorderBottom]) {
            UIView * borderView = [self viewWithTag:UIBorderBottom];
            [borderView removeFromSuperview];
            borderView = nil;
        }
        if ([self viewWithTag:UIBorderRight]) {
            UIView * borderView = [self viewWithTag:UIBorderRight];
            [borderView removeFromSuperview];
            borderView = nil;
        }
        return;
    }
    
    // Top Border
    if (sides & UIBorderTop) {
        // Add or reset border for side
        CGFloat x = 0.0;
        CGFloat y = 0.0;
        CGFloat w = CGRectGetWidth(self.bounds);
        CGFloat h = borderWidth;
        UIView * borderView = [self viewWithTag:UIBorderTop];
        if (!borderView){
            borderView = [[UIView alloc] initWithFrame:CGRectMake(x, y, w, h)];
            borderView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleWidth;
            borderView.backgroundColor = borderColor;
            borderView.tag = UIBorderTop;
            [self addSubview:borderView];
        }
        else{
            borderView.frame = CGRectMake(x, y, w, h);
            borderView.backgroundColor = borderColor;
            [self bringSubviewToFront:borderView];
        }
    }
    else if ([self viewWithTag:UIBorderTop]){
        // remove border for side
        UIView * borderView = [self viewWithTag:UIBorderTop];
        [borderView removeFromSuperview];
        borderView = nil;
    }
    
    // Left Border
    if (sides & UIBorderLeft) {
        // Add or reset border for side
        CGFloat x = 0.0;
        CGFloat y = 0.0;
        CGFloat w = borderWidth;
        CGFloat h = CGRectGetHeight(self.bounds);
        UIView * borderView = [self viewWithTag:UIBorderLeft];
        if (!borderView){
            borderView = [[UIView alloc] initWithFrame:CGRectMake(x, y, w, h)];
            borderView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleHeight;
            borderView.backgroundColor = borderColor;
            borderView.tag = UIBorderLeft;
            [self addSubview:borderView];
        }
        else{
            borderView.frame = CGRectMake(x, y, w, h);
            borderView.backgroundColor = borderColor;
            [self bringSubviewToFront:borderView];
        }
    }
    else if ([self viewWithTag:UIBorderLeft]){
        // remove border for side
        UIView * borderView = [self viewWithTag:UIBorderLeft];
        [borderView removeFromSuperview];
        borderView = nil;
    }
    
    // Bottom Border
    if (sides & UIBorderBottom) {
        // Add or reset border for side
        CGFloat x = 0.0;
        CGFloat y = CGRectGetHeight(self.bounds)-borderWidth;
        CGFloat w = CGRectGetWidth(self.bounds);
        CGFloat h = borderWidth;
        UIView * borderView = [self viewWithTag:UIBorderBottom];
        if (!borderView){
            borderView = [[UIView alloc] initWithFrame:CGRectMake(x, y, w, h)];
            borderView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
            borderView.backgroundColor = borderColor;
            borderView.tag = UIBorderBottom;
            [self addSubview:borderView];
        }
        else{
            borderView.frame = CGRectMake(x, y, w, h);
            borderView.backgroundColor = borderColor;
            [self bringSubviewToFront:borderView];
        }
    }
    else if ([self viewWithTag:UIBorderBottom]){
        // remove border for side
        UIView * borderView = [self viewWithTag:UIBorderBottom];
        [borderView removeFromSuperview];
        borderView = nil;
    }
    
    // Right Border
    if (sides & UIBorderRight) {
        // Add or reset border for side
        CGFloat x = CGRectGetWidth(self.bounds)-borderWidth;
        CGFloat y = 0.0;
        CGFloat w = borderWidth;
        CGFloat h = CGRectGetHeight(self.bounds);
        UIView * borderView = [self viewWithTag:UIBorderRight];
        if (!borderView){
            borderView = [[UIView alloc] initWithFrame:CGRectMake(x, y, w, h)];
            borderView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleHeight;
            borderView.backgroundColor = borderColor;
            borderView.tag = UIBorderRight;
            [self addSubview:borderView];
        }
        else{
            borderView.frame = CGRectMake(x, y, w, h);
            borderView.backgroundColor = borderColor;
            [self bringSubviewToFront:borderView];
        }
    }
    else if ([self viewWithTag:UIBorderRight]){
        // remove border for side
        UIView * borderView = [self viewWithTag:UIBorderRight];
        [borderView removeFromSuperview];
        borderView = nil;
    }
}
@end
