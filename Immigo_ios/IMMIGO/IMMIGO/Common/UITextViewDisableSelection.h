//
//  UITextViewDisableSelection.h
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/21/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextViewDisableSelection : UITextView {
    
   
}

@property (nonatomic, assign, getter = isTextSelectable) bool textSelectable;

- (void)alignToTop;

/**
 Client should call this to stop KVO.
 */
- (void)disableAlginment;

- (void)enableObserver:(BOOL)enable onObject:(id)object selector:(SEL)selector;
@end
