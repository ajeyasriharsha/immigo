//
//  LinkedInActivity.h
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/21/15.
//  Copyright (c) 2015 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol LinkedINActivityDelegate <NSObject>

-(void)linkedInButtonAction;

@end
@interface LinkedInActivity : UIActivity

@property (nonatomic,strong) NSString *postCommentText;
@property (nonatomic,strong) NSString *postUrl;
@property(strong, nonatomic) id<LinkedINActivityDelegate> delegate;
@end
