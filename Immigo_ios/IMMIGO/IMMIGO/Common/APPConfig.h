//Om Sri Sai Ram
//  Configuration.h
//  Immigo
//
//  Created by PrasadBabu KN on 6/3/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define AUTH_TOKEN @"13BED2C12D0A7D48309498D6BCA63FED"

#define APNS_DEVICE_TOKEN_KEY @"APNS_DEVICE_TOKEN"
#define APP_LAUNCH @"AppLaunch"
#define DEVICE_REGISTER_SUCCESSFUL_APPLAUNCH @"isRegisteredAtAppLaunch"
#define DEVICE_REGISTER_SUCCESSFUL @"isRegistered"
#pragma mark - API Config
extern NSString *const APP_PROBONO_BASE_URL;
extern NSString *const APP_CMS_BASE_URL;
extern const int APP_HTTP_TIMEOUT;
extern const int APP_HTTP_QUEUE_SIZE;

extern BOOL isAPPOnline;



extern NSTimeInterval const API_REFRESH_DELAY;

extern NSString *const API_DATETIME_FORMAT;
extern NSString *const API_DATE_FORMAT;
extern NSString *const API_TIME_FORMAT;


extern float const APP_TIMEFORSPLASH_FOREGROUND;

#pragma mark -
@interface APPConfig : NSObject

+(NSString *) urlencode: (NSString *) url;

+(void)setTextFieldLeftPadding:(UITextField *)textField;


@end
