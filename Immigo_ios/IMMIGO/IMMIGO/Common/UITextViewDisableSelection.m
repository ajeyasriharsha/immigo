//
//  UITextViewDisableSelection.m
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/21/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "UITextViewDisableSelection.h"
#import <objc/runtime.h>

#define TEXT_SELECTABLE_PROPERTY_KEY @"textSelectablePropertyKey"
@implementation UITextViewDisableSelection
@dynamic textSelectable;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)setTextSelectable:(bool)textSelectable {
    objc_setAssociatedObject(self, TEXT_SELECTABLE_PROPERTY_KEY, [NSNumber numberWithBool:textSelectable], OBJC_ASSOCIATION_ASSIGN);
}

-(bool)isTextSelectable {
    return [objc_getAssociatedObject(self, TEXT_SELECTABLE_PROPERTY_KEY) boolValue];
}

-(BOOL)canBecomeFirstResponder {
    return [self isTextSelectable];
}
-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [UIMenuController sharedMenuController].menuVisible = NO;
    return NO;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)alignToTop
{
    
    [self addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:NULL];
    
    // Get a message whenever the content size changes
}

/*
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{

    // When content size change, make sure the content is positioned at the top of the scroll view.
    if ([keyPath isEqualToString:@"contentSize"])
    {
//        UITextViewDisableSelection *tv = object;
        CGFloat topCorrect = ([self bounds].size.height - [self contentSize].height * [self zoomScale])/2.0;
        topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect );
        self.contentOffset = (CGPoint){.x = 0, .y = -topCorrect};
    }
}
*/
- (void)disableAlginment {
    if (self) {
        
        @try{
            [self removeObserver:self forKeyPath:@"contentSize"];
        }@catch(id anException){}
        
    }
    
}

#pragma mark - KVO

static void *_myContextPointer = &_myContextPointer;
- (void)enableObserver:(BOOL)enable onObject:(id)object selector:(SEL)selector {
    NSString *selectorKeyPath = NSStringFromSelector(selector);
    
    
    
    if (enable) {
        [object addObserver:self forKeyPath:selectorKeyPath options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:&_myContextPointer];
                    NSLog(@"Adding Observer");
        NSString *oldString =((UITextView *)object).text;
        [((UITextView *)object) setText:@"AJ"];
        [((UITextView *)object) setText:oldString];
    } else {
        @try {
            [object removeObserver:self forKeyPath:selectorKeyPath context:&_myContextPointer];
            NSLog(@"Removing Observer");
        } @catch (NSException *__unused exception) {}
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if (context != _myContextPointer) {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        
        return;
    }
            [self observerActionForKeyPath:keyPath ofObject:object change:change];
    
    
    
}
- (void)observerActionForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change {
    NSString *contentSizeKeyPath = NSStringFromSelector(@selector(contentSize));
    if ([keyPath isEqualToString:contentSizeKeyPath]) {
        
        UITextView *tv = object;
        CGFloat topCorrect = ([tv bounds].size.height - [tv contentSize].height * [tv zoomScale])  / 2.0;
        topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect );
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                tv.contentOffset = (CGPoint){.x = 0, .y = -topCorrect};
            NSLog(@"Setting Proper COntent offset to make centered");
        });

           }
}

@end
