//Om Sri Sai Ram
//  Enums.m
//  Immigo
//
//  Created by PrasadBabu KN on 6/3/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "Enums.h"

@implementation Enums

- (id)init 
{ 
    //Do not allow the create objects, Acts like static class
    self=nil;
    //    [super doesNotRecognizeSelector:_cmd];
    return nil;
}


//Enum Variables
static NSArray *eHM_Names;
static NSArray *eHM_Ids;

//static NSDictionary *dicQuestionOptionTypes;


+(void)initialize
{
    eHM_Names=[[NSArray alloc] initWithObjects:
               @"GET", 
               @"POST",
               @"PUT",
               @"DELETE",
               @"HEAD",
               @"OPTIONS",
               nil] ;
    eHM_Ids=[[NSArray alloc] initWithObjects:
             [NSNumber numberWithInt:HM_GET],
             [NSNumber numberWithInt:HM_POST],
             [NSNumber numberWithInt:HM_PUT],
             [NSNumber numberWithInt:HM_DELETE],
             [NSNumber numberWithInt:HM_HEAD],
             [NSNumber numberWithInt:HM_OPTIONS],
             nil] ;
    
}

+(NSString*) StringForHTTPMethod:(HttpMethod) httpMethod
{
    int index=0;
    for(NSNumber* num in eHM_Ids)
    {
        if(num.intValue==httpMethod)
            return [eHM_Names objectAtIndex:index];
        index++;
    }
    return nil;
}
@end
