//
//  FacebookActivity.h
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 6/22/15.
//  Copyright (c) 2015 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FacebookActivity : UIActivity
@property (nonatomic,strong) NSString *postCommentText;
@property (nonatomic,strong) NSString *postUrl;
@end
