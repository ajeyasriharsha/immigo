//
//  FaceBookActivityProvider.m
//  ActivityProviderSample
//
//  Created by Ajeya Sriharsha on 5/18/15.
//  Copyright (c) 2015 Ajeya Sriharsha. All rights reserved.
//

#import "FaceBookActivityProvider.h"
#import "Common.h"
@implementation FaceBookActivityProvider

- (instancetype)initWithPlaceholderItem:(id)placeholderItem twitterItem:(NSString *)twitterItem andUrlForStore:(NSString *)urlForStore
{
    self = [super initWithPlaceholderItem:placeholderItem];
    
    if (self) {
        _twitterItem = twitterItem;
        _urlForStore =urlForStore;
        
    }
    
    return self;
}

- (id)item
{
    if ([self.activityType isEqualToString:UIActivityTypePostToTwitter]) {
        
        
        NSString *topic=@"";
        NSString *subTopic=@"";
        NSString *downloadPrefix =@" Download the Immigo App!";
        int totalLength =140;
        NSString *header = _twitterItem;
        NSString *url =_urlForStore;
        int remainigLength =totalLength-(url.length +downloadPrefix.length);
        NSArray *listItems = [_twitterItem componentsSeparatedByString:@"/"];
        if (listItems && [listItems count]>0) {
            topic =[listItems objectAtIndex:0];
            subTopic =[listItems objectAtIndex:1];
        }
        
        if (header.length >remainigLength) {
            //Pass Only SubTopic
            _twitterItem =subTopic;
        }
        
        return [_twitterItem stringByAppendingString: [NSString stringWithFormat:@" Download the Immigo App! "]];
    }
    else if([self.activityType isEqualToString:UIActivityTypePostToFacebook]){
       // NSURL *mypostUrl =[NSURL URLWithString:@"https://itunes.apple.com/in/app/immigo/id891595380"];
        //return [_twitterItem stringByAppendingString: [NSString stringWithFormat:@" Download Immigo App! %@",[mypostUrl absoluteString]]];
        return [_twitterItem stringByAppendingString: [NSString stringWithFormat:@" Download the Immigo App! "]];

    }
    
    else if ([self.activityType isEqualToString:UIActivityTypeMail]){
        
        return self.placeholderItem;
    }//com.linkedin.LinkedIn.ShareExtension
    else if ([self.activityType isEqualToString:UIActivityTypePrint]){
        
        return self.placeholderItem;
        
    }
    else if ([self.activityType isEqualToString:@"com.linkedin.LinkedIn.ShareExtension"]){
        
       // NSURL *mypostUrl =[NSURL URLWithString:@"https://itunes.apple.com/in/app/immigo/id891595380"];
       // NSString *linkedInUrltoShare =[NSString stringWithFormat:@" Download Immigo App!  %@",[mypostUrl absoluteString]];
        
        return [_twitterItem stringByAppendingString: [NSString stringWithFormat:@" Download the Immigo App! "]];
        
    }
    else {
        return nil;
    }
}
@end
