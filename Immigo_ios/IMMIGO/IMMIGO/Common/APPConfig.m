//Om Sri Sai Ram
//  Configuration.m
//  Immigo
//
//  Created by PrasadBabu KN on 6/3/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "APPConfig.h"

BOOL isAPPOnline = NO;
NSDate *lastSyncedOn=nil;

NSString *const APP_PROBONO_BASE_URL=@"http://api.probono.net/"; //Production

NSString *const APP_CMS_BASE_URL=@"http://immigolulac.azurewebsites.net/api/Immigoapi/";//@"http://immigolulactest.azurewebsites.net/api/Immigoapi/"; //Production   ///http://pbn.azurewebsites.net/api/Immigoapi/
//http://immigolulac.azurewebsites.net/api/Immigoapi/  ---local
//http://immigolulac.azurewebsites.net/api/Immigoapi/   ---AZure
// http://immigolulactest.azurewebsites.net/api/Immigoapi/   --Test
const int APP_HTTP_TIMEOUT=20;
const int APP_HTTP_QUEUE_SIZE=5;
const int APP_REQUEST_PAGECOUNT=10;

NSString *const API_DATETIME_FORMAT=@"yyyy-MM-dd'T'HH:mm:ss";//@"yyyy-MM-dd'T'HH:mm:ss";//@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'+'00:00"; //ISO date format
NSString *const API_DATE_FORMAT=@"yyyy-MM-dd";//@"yyyy-MM-dd";//@"MM/dd/yyyy";
NSString *const API_TIME_FORMAT=@"HH:mm";

NSTimeInterval const API_REFRESH_DELAY=(15.0f*60.0f); //15 Minutes

float const APP_TIMEFORSPLASH_FOREGROUND = 5.0f;

#pragma mark -
@implementation APPConfig

- (id)init 
{ 
    //Do not allow the create objects, Acts like static class
    self=nil;
    //    [super doesNotRecognizeSelector:_cmd];
    return nil;
}


#pragma mark - setTextFieldLeftPadding

//for left padding of text in uitextfield
+(void)setTextFieldLeftPadding:(UITextField *)textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 5.0, textField.frame.size.height)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
}

+(NSString *) urlencode: (NSString *) url
{
    NSArray *escapeChars = [NSArray arrayWithObjects:@";" , @"/" , @"?" , @":" ,
                            @"@" , @"&" , @"=" , @"+" ,
                            @"$" , @"," , @"[" , @"]",
                            @"#", @"!", @"'", @"(", 
                            @")", @"*", @" ", @"\"",nil];
    
    NSArray *replaceChars = [NSArray arrayWithObjects:@"%3B" , @"%2F" , @"%3F" ,
                             @"%3A" , @"%40" , @"%26" ,
                             @"%3D" , @"%2B" , @"%24" ,
                             @"%2C" , @"%5B" , @"%5D", 
                             @"%23", @"%21", @"%27",
                             @"%28", @"%29", @"%2A", @"%20",@"%22",nil];
    
    NSUInteger len = [escapeChars count];
    
    NSMutableString *temp = [url mutableCopy];
    
    int i;
    for(i = 0; i < len; i++)
    {
        
        [temp replaceOccurrencesOfString: [escapeChars objectAtIndex:i]
                              withString:[replaceChars objectAtIndex:i]
                                 options:NSLiteralSearch
                                   range:NSMakeRange(0, [temp length])];
    }
    return temp;
}
@end
