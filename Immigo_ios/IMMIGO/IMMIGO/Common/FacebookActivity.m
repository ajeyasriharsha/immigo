//
//  FacebookActivity.m
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 6/22/15.
//  Copyright (c) 2015 pradeep ISPV. All rights reserved.
//

#import "FacebookActivity.h"
#import "ShareToSocialNetworkingSites.h"
#import "Common.h"

@implementation FacebookActivity
@synthesize postCommentText,postUrl;
- (NSString *)activityTitle {
    
    return NSLocalizedString(@"Facebook", @"");
}

- (UIImage *)activityImage {
    
    if ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] integerValue] <= 6) {
        return [UIImage imageNamed:@"activity-image"];
    }
    else if ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] integerValue] >= 8) {
        return [UIImage imageNamed:@"activity-image"];
    }else{
        
        return [UIImage imageNamed:@"activity-image"];
    }
    
}

-(NSString *)activityType{
    
    return NSLocalizedString(@"FacebookActivityType", @"");
}
+ (UIActivityCategory)activityCategory {
    return UIActivityCategoryShare;
}
- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems {
    
    return YES;
}

#pragma mark ZYActivity
- (UIViewController *)performWithActivityItems:(NSArray *)activityItems {
    
    //    ZYHelloViewController *helloViewController =
    //    [[ZYHelloViewController alloc] initWithNibName:@"ZYHelloViewController"
    //                                            bundle:nil];
    //
    //    helloViewController.activity = self;
    //
    //    return helloViewController;
    return nil;
}

- (void)performActivity
{
    // This is where you can do anything you want, and is the whole reason for creating a custom
    // UIActivity
    [self activityDidFinish:YES];
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        ShareToSocialNetworkingSites *share =[[ShareToSocialNetworkingSites alloc] init];
        
        NSURL *mypostUrl =[NSURL URLWithString:@"https://itunes.apple.com/in/app/immigo/id891595380"];
        self.postUrl =[NSString stringWithFormat:@"%@",[mypostUrl absoluteString]];
        [share fbshareUsingSDK:APP_STORE_URL andText:[NSString stringWithFormat:@"%@ Download the Immigo App!",self.postCommentText] ];
    });
    
    return;
}

@end
