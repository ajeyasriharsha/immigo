//
//  Categories.m
//  IMMIGO
//
//  Created by pradeep ISPV on 3/27/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "Categories.h"
@implementation NSDate (DisplayFormat)
@end




@implementation NSDictionary (JSONExtension)

-(BOOL) hasValidJSONKey:(NSString*) key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
        return  true;
    return false;
}

-(id) getObjectForKey:(NSString*) key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
        return [self objectForKey:key];
    return nil;
}

-(int) getIntForKey:(NSString*) key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
        return [[self objectForKey:key] intValue];
    return 0;
}
-(float) getFloatForKey:(NSString*) key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
        return [[self objectForKey:key] floatValue];
    return 0.0;
}


-(NSNumber*) getNSNumberForKey:(NSString*) key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
    {
        if([[self objectForKey:key] isKindOfClass:[NSNumber class]])
            return (NSNumber*)[self objectForKey:key];
    }
    return [NSNumber numberWithInt:0];
}

-(BOOL) getBoolForKey:(NSString*) key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
        return [[self objectForKey:key] boolValue];
    return false;
}

-(NSString*) getStringForKey:(NSString*) key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
        return [self objectForKey:key];
    return nil;
}

-(NSString*) getURLStringForKey:(NSString*) key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
    {
        NSString *urlStr = [self objectForKey:key];
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        return urlStr;
    }
    return nil;
}

-(NSData*) getNSDataForKey:(NSString*) key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
        return [[self objectForKey:key] dataUsingEncoding:NSUTF8StringEncoding];
    return false;
}

-(NSDate *) getDateForKey:(NSString*) key
{
//    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
//    {
//        NSDateFormatter *dateFmt = [[NSDateFormatter alloc] init];
//        [dateFmt setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
//        [dateFmt setDateFormat:API_DATE_FORMAT];
//        NSDate *date;
//        date= [dateFmt dateFromString:[self valueForKey:key]];
//        if(!date)
//        {
//            [dateFmt setDateFormat:API_DATETIME_FORMAT];
//            date=[dateFmt dateFromString:[self valueForKey:key]];
//        }
//        return date;
//    }
    return nil;
}

-(NSTimeInterval) getNSTimeIntervalForKey:(NSString*) key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
    {
        NSDate *date=[self getDateForKey:key];
        return [date timeIntervalSince1970];
    }
    return 0;
}

@end

@implementation NSString (RemoveWhiteSpace)

-(NSString *)get12HrStringFrom
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm";
    NSDate *date = [dateFormatter dateFromString:self];
    
    dateFormatter.dateFormat = @"h:mm a";
    NSString *pmamDateString = [dateFormatter stringFromDate:date];
    return pmamDateString;
}
- (BOOL)checkPhoneNumber
{
    NSRange range1 = [self rangeOfString:@"("];
    NSRange range2 = [self rangeOfString:@")"];

    if ((range1.length == 1) && (range2.length == 1) && (range2.location > range1.location))
    {
        return YES;
    }
    else{
        return NO;
    }
}

- (NSString *)frameContactNumber
{
    //(214) 828-5192
    if ([self checkPhoneNumber] && [self length]<14) {
        return self;
        
    }
    else{
        //NSLog(@"Does Not Contain Need to Phrase");
        NSArray *final;
        ///self == (214) 828-5192
        NSString *finalContact = [self stringByReplacingOccurrencesOfString:@"-" withString:@" "];
        
        //(214) 828-5192
        
        if ([finalContact checkPhoneNumber])
        {
            NSString *finalContact1 = [finalContact stringByReplacingOccurrencesOfString:@"(" withString:@""];
            NSString *finalContact2 = [finalContact1 stringByReplacingOccurrencesOfString:@")" withString:@""];
            final =[finalContact2 componentsSeparatedByString:@" "];
        }else
        {
            final =[finalContact componentsSeparatedByString:@" "];
        }

        NSString *startingString =[final objectAtIndex:0];
        startingString =[NSString stringWithFormat:@"(%@) ",startingString];
        NSString *midString =[final objectAtIndex:1];
        NSString *lastString =[final objectAtIndex:2];
        lastString =[NSString stringWithFormat:@"-%@",lastString];
        NSString *temp =[startingString stringByAppendingString:midString];
        NSString *temp1 =[temp stringByAppendingString:lastString];
        //NSLog(@"Required Contact:%@",temp1);
        return temp1;
    }
   
}

-(NSString *)trimmedString
{
    NSRange range = [self rangeOfString:@"^\\s*" options:NSRegularExpressionSearch];
    NSString *trimmedString = [self stringByReplacingCharactersInRange:range withString:@""];
    return trimmedString;
}

-(NSString *)displayDateFormat
{
    //2014-04-03
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:self];
    
    [dateFormatter setDateFormat:@"MMM d, YYYY"];
    return [dateFormatter stringFromDate:date];
}
-(NSString *)listDateFormat
{
    /*
     Returns a user-visible date time string that corresponds to the specified
     RFC 3339 date time string. Note that this does not handle all possible
     RFC 3339 date time strings, just one of the most common styles.
     */
    
    NSDateFormatter *rfc3339DateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    [rfc3339DateFormatter setLocale:enUSPOSIXLocale];
    [rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
    [rfc3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    // Convert the RFC 3339 date time string to an NSDate.
    NSDate *date = [rfc3339DateFormatter dateFromString:self];
    
    NSString *userVisibleDateTimeString;
    if (date != nil) {
        // Convert the date object to a user-visible date string.
        NSDateFormatter *userVisibleDateFormatter = [[NSDateFormatter alloc] init];
        assert(userVisibleDateFormatter != nil);
        
        [userVisibleDateFormatter setDateFormat:@"MMM d, YYYY"];
        
        userVisibleDateTimeString = [userVisibleDateFormatter stringFromDate:date];
    }
    return userVisibleDateTimeString;
}

-(NSString *)newsDateFormat
{
    /*
     Returns a user-visible date time string that corresponds to the specified
     RFC 3339 date time string. Note that this does not handle all possible
     RFC 3339 date time strings, just one of the most common styles.
     */
    
    NSDateFormatter *rfc3339DateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    [rfc3339DateFormatter setLocale:enUSPOSIXLocale];
    [rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
    [rfc3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    // Convert the RFC 3339 date time string to an NSDate.
    NSDate *date = [rfc3339DateFormatter dateFromString:self];
    
    NSString *userVisibleDateTimeString;
    if (date != nil) {
        // Convert the date object to a user-visible date string.
        NSDateFormatter *userVisibleDateFormatter = [[NSDateFormatter alloc] init];
        assert(userVisibleDateFormatter != nil);

        [userVisibleDateFormatter setDateFormat:@"MMM d, YYYY hh:mm a"];

        userVisibleDateTimeString = [userVisibleDateFormatter stringFromDate:date];
    }
    return userVisibleDateTimeString;
}

- (NSString *)userVisibleDateTimeStringForRFC3339DateTimeString
{
    /*
     Returns a user-visible date time string that corresponds to the specified
     RFC 3339 date time string. Note that this does not handle all possible
     RFC 3339 date time strings, just one of the most common styles.
     */
    
    NSDateFormatter *rfc3339DateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    [rfc3339DateFormatter setLocale:enUSPOSIXLocale];
    [rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
    [rfc3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    // Convert the RFC 3339 date time string to an NSDate.
    NSDate *date = [rfc3339DateFormatter dateFromString:self];
    
    NSString *userVisibleDateTimeString;
    if (date != nil) {
        // Convert the date object to a user-visible date string.
        NSDateFormatter *userVisibleDateFormatter = [[NSDateFormatter alloc] init];
        assert(userVisibleDateFormatter != nil);
        
        [userVisibleDateFormatter setDateStyle:NSDateFormatterShortStyle];
        [userVisibleDateFormatter setTimeStyle:NSDateFormatterShortStyle];
        
        userVisibleDateTimeString = [userVisibleDateFormatter stringFromDate:date];
    }
    return userVisibleDateTimeString;
}

@end
@implementation UILabel (UILabelDynamicHeight)

-(CGSize)sizeOfMultiLineLabel{
    
    NSAssert(self, @"UILabel was nil");
    
    //Label text
    NSString *aLabelTextString = [self text];
    
    //Label font
    UIFont *aLabelFont = [self font];
    
    //Width of the Label
    CGFloat aLabelSizeWidth = self.frame.size.width;
    
    
    if (SYSTEM_VERSION_LESS_THAN(iOS7_0)) {
        //version < 7.0
        
        return [aLabelTextString sizeWithFont:aLabelFont
                            constrainedToSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                lineBreakMode:NSLineBreakByWordWrapping];
    }
    else if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(iOS7_0)) {
        //version >= 7.0
        
        //Return the calculated size of the Label
        return [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{
                                                        NSFontAttributeName : aLabelFont
                                                        }
                                              context:nil].size;
        
    }
    
    return [self bounds].size;
    
}
@end
@implementation UIButton (UIButtonDynamicHeight)


#pragma mark - Calculate the size,bounds,frame of the Multi line Label
/*====================================================================*/

/* Calculate the size,bounds,frame of the Multi line Label */

/*====================================================================*/
/**
 *  Returns the size of the Label
 *
 *  @param aLabel To be used to calculte the height
 *
 *  @return size of the Label
 */
-(CGSize)sizeOfMultiLineButton{
    
    NSAssert(self, @"UIButton was nil");
    
    //Label text
    NSString *aButtonTextString = [[self titleLabel] text];
    
    //Label font
    UIFont *aButtonFont = [[self titleLabel] font];
    
    //Width of the Label
    CGFloat aButtonSizeWidth = self.frame.size.width;
    
    
    if (SYSTEM_VERSION_LESS_THAN(iOS7_0)) {
        //version < 7.0
        
        return [aButtonTextString sizeWithFont:aButtonFont
                            constrainedToSize:CGSizeMake(aButtonSizeWidth, MAXFLOAT)
                                lineBreakMode:NSLineBreakByWordWrapping];
    }
    else if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(iOS7_0)) {
        //version >= 7.0
        
        //Return the calculated size of the Label
        return [aButtonTextString boundingRectWithSize:CGSizeMake(aButtonSizeWidth, MAXFLOAT)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{
                                                        NSFontAttributeName : aButtonFont
                                                        }
                                              context:nil].size;
        
    }
    
    return [self bounds].size;
    
}
@end

@implementation Categories


@end
