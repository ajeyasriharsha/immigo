#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>

@interface NSURL (MailComposeViewController)

// Is the given request a mailto URL.
- (BOOL)isMailtoRequest;

@end
	
@interface MFMailComposeViewController (URLRequest)

// Parse the request URL and display an email mail composition interface, filled with
// the elements resulting of this parsing.
// Return NO if the device doesn't provide an email sending service.
+ (BOOL)presentModalComposeViewControllerWithURL:(NSURL *)aURL
										delegate:(id<MFMailComposeViewControllerDelegate>)delegate;

@end
