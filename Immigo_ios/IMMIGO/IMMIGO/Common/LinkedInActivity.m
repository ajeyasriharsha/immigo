//
//  LinkedInActivity.m
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/21/15.
//  Copyright (c) 2015 pradeep ISPV. All rights reserved.
//

#import "LinkedInActivity.h"
#import "ShareToSocialNetworkingSites.h"

@implementation LinkedInActivity
@synthesize delegate;
@synthesize postCommentText,postUrl;
- (NSString *)activityTitle {
    
    return NSLocalizedString(@"LinkedIn", @"");
}

- (UIImage *)activityImage {
    
    if ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] integerValue] <= 6) {
        return [UIImage imageNamed:@"LinkedIn_Activity_ios6"];
    }
    else if ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] integerValue] >= 8) {
        return [UIImage imageNamed:@"LinkedIn_iPad"];
    }else{
        
        return [UIImage imageNamed:@"LinkedIn_Activity_new"];
    }
    
}

-(NSString *)activityType{
    
    return NSLocalizedString(@"LinkedInActivityType", @"");
}
+ (UIActivityCategory)activityCategory {
    return UIActivityCategoryShare;
}
- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems {
    
    return YES;
}

#pragma mark ZYActivity
- (UIViewController *)performWithActivityItems:(NSArray *)activityItems {
    
    //    ZYHelloViewController *helloViewController =
    //    [[ZYHelloViewController alloc] initWithNibName:@"ZYHelloViewController"
    //                                            bundle:nil];
    //
    //    helloViewController.activity = self;
    //
    //    return helloViewController;
    return nil;
}

- (void)performActivity
{
    // This is where you can do anything you want, and is the whole reason for creating a custom
    // UIActivity
    [self activityDidFinish:YES];
    double delayInSeconds = 5.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        ShareToSocialNetworkingSites *share =[[ShareToSocialNetworkingSites alloc] init];
        
        NSURL *mypostUrl =[NSURL URLWithString:@"https://itunes.apple.com/in/app/immigo/id891595380"];
        self.postUrl =[NSString stringWithFormat:@" Download the Immigo App!  %@",[mypostUrl absoluteString]];
        [share linkedInLogIn:self.postUrl andText:self.postCommentText fromViewContoller:nil];
    });
    
    return;
    if (delegate && [delegate respondsToSelector:@selector(linkedInButtonAction)]) {
        
        
                double delayInSeconds = 5.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [delegate linkedInButtonAction];
                });

        
    }
}

@end
