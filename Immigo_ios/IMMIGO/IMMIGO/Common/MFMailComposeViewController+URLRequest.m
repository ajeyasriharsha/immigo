#import "MFMailComposeViewController+URLRequest.h"


@implementation NSURL (MailComposeViewController)


// Is the given request a mailto URL.
- (BOOL)isMailtoRequest {
	
	return [[self scheme] isEqualToString:@"mailto"];
}


@end


@implementation MFMailComposeViewController (URLRequest)


// Parse the request URL and display an email mail composition interface, filled with
// the elements resulting of this parsing.
// Return NO if the device doesn't provide an email sending service or if the URL is not valid.
+ (BOOL)presentModalComposeViewControllerWithURL:(NSURL *)aUrl
										delegate:(UIViewController<MFMailComposeViewControllerDelegate> *)aDelegate {
	
	if ([MFMailComposeViewController canSendMail]) {
		
		MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
		mailViewController.mailComposeDelegate = aDelegate;
		
		NSArray *rawURLparts = [[aUrl resourceSpecifier] componentsSeparatedByString:@"?"];
		if (rawURLparts.count > 2) {
			return NO; // invalid URL
		}
		
		NSMutableArray *toRecipients = [NSMutableArray array];
		NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
		if (defaultRecipient.length) {
			[toRecipients addObject:defaultRecipient];
		}
		
		if (rawURLparts.count == 2) {
			NSString *queryString = [rawURLparts objectAtIndex:1];
			
			NSArray *params = [queryString componentsSeparatedByString:@"&"];
			for (NSString *param in params) {
				NSArray *keyValue = [param componentsSeparatedByString:@"="];
				if (keyValue.count != 2) {
					continue;
				}
				NSString *key = [[keyValue objectAtIndex:0] lowercaseString];
				NSString *value = [keyValue objectAtIndex:1];
				
				value =  (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault,
																							 (CFStringRef)value,
																							 CFSTR(""),
																							 kCFStringEncodingUTF8));
				
				if ([key isEqualToString:@"subject"]) {
					[mailViewController setSubject:value];
				}
				
				if ([key isEqualToString:@"body"]) {
					[mailViewController setMessageBody:value isHTML:NO];
				}
				
				if ([key isEqualToString:@"to"]) {
					[toRecipients addObjectsFromArray:[value componentsSeparatedByString:@","]];
				}
				
				if ([key isEqualToString:@"cc"]) {
					NSArray *recipients = [value componentsSeparatedByString:@","];
					[mailViewController setCcRecipients:recipients];
				}
				
				if ([key isEqualToString:@"bcc"]) {
					NSArray *recipients = [value componentsSeparatedByString:@","];
					[mailViewController setBccRecipients:recipients];
				}
			}
		}
		
		[mailViewController setToRecipients:toRecipients];
        if ([aDelegate respondsToSelector:@selector(presentViewController:animated:completion:)])
        {
            
            [aDelegate presentViewController:mailViewController animated:YES completion:nil];
        }
		
		return YES;
	}
	else {
		
		return NO;
	}
}


@end
