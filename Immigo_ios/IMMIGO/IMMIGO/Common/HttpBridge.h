//Om Sri Sai Ram
//  HttpBridge.h
//  Immigo
//
//  Created by PrasadBabu KN on 6/3/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIRequest.h"
#import "ASIHTTPRequest.h"

@protocol HTTPResponseErrorDelegate
@optional
-(void)NetworkErrorWithMessage:(NSString*) errMessage;
-(void)UserTokenExpired;
-(void)UnauthorizedAccess;
-(void)NetworkUnknownAPI;
@end

@interface HttpBridge : NSObject

+(ASIHTTPRequest*)invokeAPIWithRequest:(APIRequest*) request;
+(void)invokeAPIWithRequest:(APIRequest*) request ResponseDelegate:(id<ASIHTTPRequestDelegate>) asiDelegate;
+(NSString *)getCurrentLanguage;
@end
