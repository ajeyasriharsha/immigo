//
//  Common.m
//  Immigo
//
//  Created by pradeep ISPV on 3/3/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "Common.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"

#import <SystemConfiguration/SystemConfiguration.h>

#import <netinet/in.h>
#import <netinet6/in6.h>
#import <sys/utsname.h>


//FontNames
NSString *const FONT_REGULAR                 = @"OpenSans";
NSString *const FONT_LIGHT                   = @"OpenSans-Light";
NSString *const FONT_LIGHT_ITALIC            = @"OpenSans-LightItalic";
NSString *const FONT_ITALIC                  = @"OpenSans-Italic";
NSString *const FONT_BOLD                    = @"OpenSans-Bold_0";
NSString *const FONT_BOLD_ITALIC             = @"OpenSans-BoldItalic_0";
NSString *const FONT_EXTRA_BOLD              = @"OpenSans-ExtraBold";
NSString *const FONT_EXTRA_BOLD_ITALIC       = @"OpenSans-ExtraBoldItalic";
NSString *const FONT_SEMIBOLD                = @"OpenSans-Semibold";
NSString *const FONT_SEMIBOLD_ITALIC         = @"OpenSans-SemiboldItalic";


NSString *const FONT_IPAD                = @"Aaargh";

/********************IPAD - LEGAL********************/

/********************END********************/
/*
 NCLR: www.nclr.org; Immigration Advocates Network: www.immigrationadvocates.org; Verizon Foundation: http://www.verizonfoundation.org/; probono.net: www.probono.net
 */
float PARTNERS_DELAY_TIME = 3.0;
NSString *const PARTNERS_NCLR_URL=@"http://www.nclr.org";
NSString *const PARTNERS_IMMIGRATION_URL=@"http://www.immigrationadvocates.org";
NSString *const PARTNERS_VERIZON_URL=@"http://www.verizon.com/about/responsibility/verizon-foundation/";//@"http://www.verizonfoundation.org/";
NSString *const PARTNERS_PROBONO_URL=@"http://www.probono.net";


unsigned long int UI_TITLE_HEADER_COLOR = 0x00e2ff;
unsigned long int UI_ALERT_DESCRIPTION_COLOR = 0x575757;

unsigned long int UI_BLUE_TEXT_COLOR = 0X124A7F;
//search List
unsigned long int UI_LIST_TEXT_COLOR = 0x4c4c4c;
//
//Training Ipad
unsigned long int UI_TRAINING_HEADER_TEXT_COLOR_IPAD = 0X636363;
unsigned long int UI_TRAINING_DESC_TEXT_COLOR_IPAD = 0x4c4c4c;
unsigned long int UI_TRAINING_DESC_LABEL_TEXT_COLOR_IPAD = 0xe61e28;

unsigned long int UI_TRAINING_DESC_CONTENTLABEL_TEXT_COLOR_IPAD = 0x6a6a6a;



//Training
unsigned long int UI_TRAINING_HEADER_TEXT_COLOR = 0Xe62129;
unsigned long int UI_TRAINING_DESC_TEXT_COLOR = 0x4c4c4c;
//

//Sharing
NSString *const kLinkedInOAuthConsumerKey     = @"77ln980vsu5a0a";
NSString *const kLinkedInOAuthConsumerSecret  = @"hLcfnM3RMRy5zlGC";
//

//For Google Analytics GUID
NSString *const IMMIGO_GUID = @"IMMIGO_GUID";
NSString *const DEVICE_REGISTER = @"DEVICE_REGISTER";

//

@implementation Common


#pragma mark -  logEventToGoogleAnalytics
//log screen
+ (void)logEventToGoogleAnalytics:(NSString *)eventName
{
    if ([eventName length] == 0)
    {
        eventName = @"Immigo iOS App";
    }
    
    eventName = [eventName stringByAppendingString:[[NSUserDefaults standardUserDefaults] valueForKey:IMMIGO_GUID]];
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:eventName];
    
    // Send the screen view.
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createAppView] build]];
}

#pragma mark -  logEventToGoogleAnalytics
//log events
+ (void)logMainEventsToGoogleAnalytics:(NSString *)categoryName withAction:(NSString *)action andLabel:(NSString *)label and:(int)value
{
        if ([categoryName length] == 0 || [action length] == 0)
        {
            categoryName = @"Immigo iOS App Open/Close";
            action = @"Immigo iOS App Open/Close";
        }
//        categoryName = [categoryName stringByAppendingString:[self getUserDetails]];
        label = [[NSUserDefaults standardUserDefaults] valueForKey:IMMIGO_GUID];
        [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:categoryName action:action label:label value:nil] build]];
}

+ (NSString*)getUserDetails
{
    // UserID +  device type + OS version + Datetime
    NSString *guid = [[NSUserDefaults standardUserDefaults] valueForKey:IMMIGO_GUID];
    
    NSDate *currDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd \'T:\'HH:mm:ssZZZZZ"];//2013-04-10 T:13:40:47+05:30
    NSString *dateString = [dateFormatter stringFromDate:currDate];
    //    //NSLog(@"%@",dateString);
    //    NSString *userDetails = [NSString stringWithFormat:@"%@_%@_%@_%d", userID, deviceType, deviceOS, timeElapsedInSecs];
    NSString *userDetails;
    if (NULL != guid)
    {
        userDetails = [NSString stringWithFormat:@"_GUID:%@_dt:%@", guid,dateString];
    }else
    {
        userDetails = @"";
    }
    
    return userDetails;
}

//Don't call this method more than 1 time in an app until you want multiple unique ids -- Arun HS
+ (NSString *)generateGUID
{
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    CFStringRef uuidStr = CFUUIDCreateString(NULL, uuid);
    CFRelease(uuid);
    return (__bridge NSString *) uuidStr;
}
/*
+ (BOOL) currentNetworkStatus
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    BOOL connected;
    BOOL isConnected;
    const char *host = "www.apple.com";
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL, host);
    SCNetworkReachabilityFlags flags;
    connected = SCNetworkReachabilityGetFlags(reachability, &flags);
    isConnected = NO;
    isConnected = connected && (flags & kSCNetworkFlagsReachable) && !(flags & kSCNetworkFlagsConnectionRequired);
    CFRelease(reachability);
    return isConnected;
}
 */

+ (BOOL)isValidUrl:(NSString *)urlString{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    return [NSURLConnection canHandleRequest:request];
}
+ (BOOL) currentNetworkStatus
{
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr*)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    
    if(reachability == NULL)
        return false;
    
    if (!(SCNetworkReachabilityGetFlags(reachability, &flags)))
        return false;
    
    if ((flags & kSCNetworkReachabilityFlagsReachable) == 0)
        // if target host is not reachable
        return false;
    
    
    BOOL isReachable = false;
    
    
    if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0)
    {
        // if target host is reachable and no connection is required
        //  then we'll assume (for now) that your on Wi-Fi
        isReachable = true;
    }
    
    
    if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
         (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0))
    {
        // ... and the connection is on-demand (or on-traffic) if the
        //     calling application is using the CFSocketStream or higher APIs
        
        if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
        {
            // ... and no [user] intervention is needed
            isReachable = true;
        }
    }
    
    if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
    {
        // ... but WWAN connections are OK if the calling application
        //     is using the CFNetwork (CFSocketStream?) APIs.
        isReachable = true;
    }
    
    
    return isReachable;
    
    
}

+ (NSString*)deviceName {
    static NSString *deviceType =nil;
    
  deviceType =  [[UIDevice currentDevice] model];
    
    /*
    static NSDictionary* deviceNamesByCode = nil;
    static NSString* deviceName = nil;
    
    if (deviceName) {
        return deviceName;
    }
    
    deviceNamesByCode = @{
                          @"i386"      :@"Simulator",
                          @"iPod1,1"   :@"iPod Touch",      // (Original)
                          @"iPod2,1"   :@"iPod Touch",      // (Second Generation)
                          @"iPod3,1"   :@"iPod Touch",      // (Third Generation)
                          @"iPod4,1"   :@"iPod Touch",      // (Fourth Generation)
                          @"iPhone1,1" :@"iPhone",          // (Original)
                          @"iPhone1,2" :@"iPhone",          // (3G)
                          @"iPhone2,1" :@"iPhone",          // (3GS)
                          @"iPad1,1"   :@"iPad",            // (Original)
                          @"iPad2,1"   :@"iPad 2",          //
                          @"iPad3,1"   :@"iPad",            // (3rd Generation)
                          @"iPhone3,1" :@"iPhone 4",        //
                          @"iPhone4,1" :@"iPhone 4S",       //
                          @"iPhone5,1" :@"iPhone 5",        // (model A1428, AT&T/Canada)
                          @"iPhone5,2" :@"iPhone 5",        // (model A1429, everything else)
                          @"iPad3,4"   :@"iPad",            // (4th Generation)
                          @"iPad2,5"   :@"iPad Mini",       // (Original)
                          @"iPhone5,3" :@"iPhone 5c",       // (model A1456, A1532 | GSM)
                          @"iPhone5,4" :@"iPhone 5c",       // (model A1507, A1516, A1526 (China), A1529 | Global)
                          @"iPhone6,1" :@"iPhone 5s",       // (model A1433, A1533 | GSM)
                          @"iPhone6,2" :@"iPhone 5s",       // (model A1457, A1518, A1528 (China), A1530 | Global)
                          @"iPad4,1"   :@"iPad Air",        // 5th Generation iPad (iPad Air) - Wifi
                          @"iPad4,2"   :@"iPad Air",        // 5th Generation iPad (iPad Air) - Cellular
                          @"iPad4,4"   :@"iPad Mini",       // (2nd Generation iPad Mini - Wifi)
                          @"iPad4,5"   :@"iPad Mini"        // (2nd Generation iPad Mini - Cellular)
                          };
    
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString* code = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    deviceName = [deviceNamesByCode objectForKey:code];
    
    if (!deviceName) {
        // Not found in database. At least guess main device type from string contents:
        
        if ([code rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        } else if([code rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        } else if([code rangeOfString:@"iPhone"].location != NSNotFound){
            deviceName = @"iPhone";
        } else {
            deviceName = @"Simulator";
        }
    }
     
     */
    
    return deviceType;
}

@end
