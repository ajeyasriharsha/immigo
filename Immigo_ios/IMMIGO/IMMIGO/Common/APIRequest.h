//Om Sri Sai Ram
//  APIRequest.h
//  Immigo
//
//  Created by PrasadBabu KN on 6/3/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequestDelegate.h"
#import "Enums.h"

typedef enum
{
    API_TRAININGSANDEVENTS_GET_ALL,
    API_NEWS_GET_ALL,
    API_LEGALORGANIZATIONS_GET_ALL,
    
    API_ABOUT_GET,
    API_PRIVACY_GET,
    API_TERMSOFUSE_GET,
    API_ALERTS_GET,
    API_IMMIGRATIONBASICS_GET_ALL,
    API_POLL_GET_ALL,
    API_POLL_POST_ALL,
    API_DEVICE_POST,
    API_FEEDBACK_POST,
    API_GET_CIVIL_RIGHTS,
    API_GET_CIVIC_ENGAGEMENT,
    
// Always add new Key above this. make sure API_COUNT key is at the end.
    API_COUNT
}
APIKey;

@interface APIRequest : NSObject

@property(readonly, nonatomic) APIKey apiKey;
@property(readonly,strong,nonatomic) NSString* apiUri;
@property(readonly, nonatomic) bool fromProbonoAPI;
@property(strong,nonatomic) NSData* requestData;
@property(strong,nonatomic) NSDictionary* aditionalHeaders;
@property(readonly, nonatomic) HttpMethod httpMethod;
@property(strong,nonatomic) id<ASIHTTPRequestDelegate> asiDelegate;

+(APIRequest*) apiRequestWithKey:(APIKey) key;

-(id)initWithApiKey:(APIKey) aKey URI:(NSString*) aUri HttpMethod:(HttpMethod) aMethod FromProbonoAPI:(bool) fromProbono;

-(void)setRequestDataWithDictionary:(NSDictionary*) dictionary;
-(void)setRequestDataWithArray:(NSArray *)array;

@end
