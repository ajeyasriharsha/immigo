//
//  Categories.h
//  IMMIGO
//
//  Created by pradeep ISPV on 3/27/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface NSDate (DisplayFormat)
@end

@interface NSString (RemoveWhiteSpace)
-(NSString *)trimmedString;
-(NSString *)displayDateFormat;
-(NSString *)newsDateFormat;
-(NSString *)listDateFormat;
-(NSString *)get12HrStringFrom;
-(NSString *)frameContactNumber;
- (BOOL)checkPhoneNumber;

@end
@interface UILabel (UILabelDynamicHeight)

-(CGSize)sizeOfMultiLineLabel;

@end
@interface UIButton (UIButtonDynamicHeight)
-(CGSize)sizeOfMultiLineButton;
@end

@interface NSDictionary (JSONExtension)
-(BOOL) hasValidJSONKey:(NSString*) key;
-(id) getObjectForKey:(NSString*) key;
-(int) getIntForKey:(NSString*) key;
-(NSNumber*) getNSNumberForKey:(NSString*) key;
-(BOOL) getBoolForKey:(NSString*) key;
-(NSString*) getStringForKey:(NSString*) key;
-(NSString*) getURLStringForKey:(NSString*) key;
-(NSData*) getNSDataForKey:(NSString*) key;
-(NSDate *) getDateForKey:(NSString*) key;
-(NSTimeInterval) getNSTimeIntervalForKey:(NSString*) key;
-(float) getFloatForKey:(NSString*) key;
@end

@interface NSDate (UIDateTimeFormat)
@end
@interface Categories : NSObject

@end
