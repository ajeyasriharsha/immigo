//Om Sri Sai Ram
//  Enums.h
//  Immigo
//
//  Created by PrasadBabu KN on 6/3/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    HM_GET,
    HM_POST,
    HM_PUT,
    HM_DELETE,
    HM_HEAD,
    HM_OPTIONS
}HttpMethod;
typedef enum
{
    TP_IMMIGRATION,
    TP_CIVIC_ENGAGEMENT,
    TP_CIVIL_RIGHTS
}TP_TopicType;
typedef enum
{
    RT_SUCCESS,
    RT_FAILED
}APIResponseType;

typedef enum
{
    KST_PROBONO,
    KST_CMS
}KeySourceType;

@interface Enums : NSObject

// Http Methods
+(NSString*) StringForHTTPMethod:(HttpMethod) httpMethod;

//QuestionOptionType
//+(QuestionOptionType) questionOptionTypeFromString:(NSString*) strOptionType;
//+(NSString*) StringFromFormChoiceType:(QuestionOptionType) optionType;

@end
