//
//  ShareToSocialNetworkingSites.m
//  IMMIGO
//
//  Created by ArunHS on 4/7/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "ShareToSocialNetworkingSites.h"
#import <Social/Social.h>

//Newly added for linked IN
#import "AFHTTPRequestOperation.h"
#import "LIALinkedInHttpClient.h"
#import "LIALinkedInApplication.h"
//Immigo
static NSString *const kLinkedInOAuthConsumerKey     = @"77ln980vsu5a0a";
static NSString *const kLinkedInOAuthConsumerSecret  = @"hLcfnM3RMRy5zlGC";




@interface ShareToSocialNetworkingSites(){
    
    LIALinkedInHttpClient *_client;
}
//- (void)updateLinkedInUI:(NSString *)status;
//- (void)fetchProfile;
- (void)showAlert:(NSString *)message;
@end

@implementation ShareToSocialNetworkingSites


- (id)init
{
    self = [super init];
    if (self)
    {
        //custom initilization
        _client = [self client];
        

    }
    return self;
}
- (LIALinkedInHttpClient *)client {
    LIALinkedInApplication *application = [LIALinkedInApplication applicationWithRedirectURL:@"https://linkedin_oauth/success"//@"http://linkedin_oauth/success"
                                                                                    clientId:kLinkedInOAuthConsumerKey
                                                                                clientSecret:kLinkedInOAuthConsumerSecret
                                                                                       state:@"IZpQSXKnjWbTGI93"
                                                                               grantedAccess:@[@"r_basicprofile",@"w_share"]];
    
    return [LIALinkedInHttpClient clientForApplication:application];
}

#pragma mark - Using Social.framework
//Tweet using Social.framework
#pragma mark - Twitter Methods
- (void)tweetTapped:(NSString *)urlStr andText:(NSString *)text fromViewContoller:(UIViewController*)parentVC
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        //completionHandler of tweet -- Arun HS
        tweetSheet.completionHandler = ^(SLComposeViewControllerResult result)
        {
            if (result == SLComposeViewControllerResultDone)
            {
               [self showAlert:@"Tweet Success"];
            }
            else if (result == SLComposeViewControllerResultCancelled){
                
                [self showAlert:@"Tweet Failure"];
            }
        };
        
        [tweetSheet setInitialText:text];//tweet text
        
//        [tweetSheet addImage:[UIImage imageNamed:@"music.jpeg"]];//attaching images in tweet dialog
        
        [tweetSheet addURL:[NSURL URLWithString:urlStr]];//attaching url for the tweet dialog
    
        [parentVC presentViewController:tweetSheet animated:YES completion:nil];
        if ([tweetSheet respondsToSelector:@selector(popoverPresentationController)])
        {
            // iOS 8+
            UIPopoverPresentationController *presentationController = [tweetSheet popoverPresentationController];
            
            presentationController.sourceView = parentVC.view; // if button or change to self.view.
        }
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't send a tweet right now, make sure your device has an Internet connection and you have at least one Twitter account setup"
                                  delegate:self
                                  cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                  otherButtonTitles:nil];
        [alertView show];
    }
}





/*
//Facebook using Social.framework
-(void) fbshareUsingSDK:(NSString *)urlString andText:(NSString *)text{
    
    
    NSString *messagePassed =[NSString stringWithFormat:@"%@ \n %@",text,[NSURL URLWithString:urlString]];
    if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
        
        NSLog(@"Already authenticated so directly posting");
        [[[FBSDKGraphRequest alloc]
          initWithGraphPath:@"me/feed"
          parameters: @{ @"message" : messagePassed}
          HTTPMethod:@"POST"]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 NSLog(@"Post id:%@", result[@"id"]);
             }
         }];
        return;
    }
    
    
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithPublishPermissions:@[@"publish_actions"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                NSLog(@"Not authenticated so authenticating");
        if (error) {
            // Process error
        } else if (result.isCancelled) {
            // Handle cancellations
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if ([result.grantedPermissions containsObject:@"publish_actions"]) {
                // Do work
                
                if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
                    [[[FBSDKGraphRequest alloc]
                      initWithGraphPath:@"me/feed"
                      parameters: @{ @"message" : messagePassed}
                      HTTPMethod:@"POST"]
                     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                         if (!error) {
                             NSLog(@"Post id:%@", result[@"id"]);
                         }
                     }];
                }
            }
        }
    }];
}

*/ 
 
#pragma mark - Facebook Methods
- (void)fbShareBtnTapped:(NSString *)urlStr andText:(NSString *)text fromViewContoller:(UIViewController*)parentVC{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *fbController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        //completionHandler of tweet -- Arun HS
        fbController.completionHandler = ^(SLComposeViewControllerResult result)
        {
            if (result == SLComposeViewControllerResultDone)
            {
                [self showAlert:@"facebook Share Success"];
                
            }
        };
        
        [fbController setInitialText:text];//tweet text
        
//        [fbController addImage:[UIImage imageNamed:@"music.jpeg"]];//attaching images in tweet dialog
        
        [fbController addURL:[NSURL URLWithString:urlStr]];//attaching url for the tweet dialog
//        [fbController addImage:nil];
        [parentVC presentViewController:fbController animated:YES completion:nil];
        if ([fbController respondsToSelector:@selector(popoverPresentationController)])
        {
            // iOS 8+
            UIPopoverPresentationController *presentationController = [fbController popoverPresentationController];
            
            presentationController.sourceView = parentVC.view; // if button or change to self.view.
        }
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't send share to facebook right now, make sure your device has an Internet connection and you have at least one facebook account setup"
                                  delegate:self
                                  cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
 

-(NSString *) URLEncodeString:(NSString *) str
{
    
    NSMutableString *tempStr = [NSMutableString stringWithString:str];
    [tempStr replaceOccurrencesOfString:@" " withString:@"+" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [tempStr length])];
    
    
    return [[NSString stringWithFormat:@"%@",tempStr] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}
////////////////////// Linked In Integration /////////////////////////////////////////////////////////////
#pragma mark - LinkedIn Methods
#pragma mark - LinkedIn Login/Logout Methods

- (void)linkedInLogIn:(NSString *)urlStr andText:(NSString *)text fromViewContoller:(UIViewController*)parentVC
{
    linkedInURL = urlStr;
   
    if(self.client.validToken && self.client.accessToken.length>0)
    {
        
        [self postStatusUpdate:self.client.accessToken withUrlString:urlStr andCommentText:text];
        return;
    }
    else
    {
        [self.client getAuthorizationCode:^(NSString *code)
         {
             [self.client getAccessToken:code success:^(NSDictionary *accessTokenData) {
                 NSString *accessToken = [accessTokenData objectForKey:@"access_token"];
                 [self postStatusUpdate:accessToken withUrlString:urlStr andCommentText:text];
             }                   failure:^(NSError *error) {
                 NSLog(@"Quering accessToken failed %@", error);
             }];
         }                      cancel:^{
             NSLog(@"Authorization was cancelled by user");
         }                     failure:^(NSError *error) {
             NSLog(@"Authorization failed %@", error);
         }];
    }
}
-(void)postStatusUpdate:(NSString *)accessToken withUrlString:(NSString *)urlStr andCommentText:(NSString *)comment{
    //https://api.linkedin.com/v1/people/~/shares?format=json

    NSString *postTitle = comment;
//    NSString *postDescription = comment;
    NSString *postURL = urlStr;
//    NSString *postImageURL = @"https://www.facebook.com/";
//    NSString *postComment = comment;
    
    NSString *finalComment =[NSString stringWithFormat:@"%@\r\n%@",postTitle,postURL];
    
        NSString *stringRequest = [NSString stringWithFormat:@"https://api.linkedin.com/v1/people/~/shares?oauth2_access_token=%@&format=json",accessToken] ;
    
    NSDictionary *param = @{
//                            @"content":@{
//                                    @"title":postTitle,
//                                    @"description":postDescription,
//                                    @"submitted-url":postURL,
//                                    //@"submitted-image-url":postImageURL
//                                    },
                            @"comment": finalComment,
                            @"visibility": @{
                                    @"code": @"anyone"
                                    }
                            };
    NSLog(@"Linked In dict params:%@",param);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:stringRequest]];
    [request setHTTPBody:[NSJSONSerialization dataWithJSONObject:param options:0 error:nil]];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:@{@"Content-Type":@"application/json",
                                      @"x-li-format":@"json"
                                      
                                      }];
    
    AFHTTPRequestOperation *op = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"success response from LinkedIn: %@", responseObject);
        [self showAlert:@"LinkedIn share success"];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if ([operation.responseString class] == [NSDictionary class]) {
            //[Utility showAlert:@"LinkedIn" mess:[(NSDictionary *)operation.responseString objectForKey:@"message"]];
            
            
            NSLog(@"response from LinkedIn: %@", [(NSDictionary *)operation.responseString objectForKey:@"message"]);
        }
        if (error) {
                NSLog(@"error: %@", error);
             NSLog(@"error code %d",[operation.response statusCode]);
            [self showAlert:@"LinkedIn share failed"];
            
            int statusCode=   [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
            
            if (statusCode == 401) {
                NSLog(@"Authentication error need to reset accesstoken");
            }
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            
            [userDefaults setObject:@"" forKey:LINKEDIN_TOKEN_KEY];
            [userDefaults setDouble:0 forKey:LINKEDIN_EXPIRATION_KEY];
            [userDefaults setDouble:0 forKey:LINKEDIN_CREATION_KEY];
            [userDefaults synchronize];
        }
        
    }];
    [op start];
    
}



/*
- (void)linkedInLogIn:(NSString *)urlStr andText:(NSString *)text fromViewContoller:(UIViewController*)parentVC
{
    linkedInURL = urlStr;
    RDLinkedInAuthorizationController* controller = [RDLinkedInAuthorizationController authorizationControllerWithEngine:self.engine delegate:self];
    if( controller ) {
        //        [self presentModalViewController:controller animated:YES];
        [parentVC presentViewController:controller animated:YES completion:nil];
    }
    else
    {
        //NSLog(@"Already authenticated, just sharing the url");
        [engine updateStatus:[NSString stringWithFormat:@"%@\n%@",text,linkedInURL]];//Already authenticated, just sharing the url
        
       // [engine shareUrl:@"" imageUrl:@"" title:linkedInURL comment:text];
        //    [engine shareLink:[NSDictionary dictionary]];
    }
}




- (void)linkedInLogOut:(id)sender
{
    if( self.engine.isAuthorized ) {
        [self.engine requestTokenInvalidation];
    }
}


- (void)fetchProfile {
    self.fetchConnection = [self.engine profileForCurrentUser];
    //NSLog(@"%@",[NSString stringWithFormat:@"fetching profile on %@",[self.fetchConnection description]]);
//    [self updateLinkedInUI:[@"fetching profile on " stringByAppendingString:[self.fetchConnection description]]];
}


#pragma mark - RDLinkedInEngineDelegate
- (void)linkedInEngineAccessToken:(RDLinkedInEngine *)engine setAccessToken:(OAToken *)token {
    if( token ) {
        [token rd_storeInUserDefaultsWithServiceProviderName:@"LinkedIn" prefix:@"Demo"];
    }
    else {
        [OAToken rd_clearUserDefaultsUsingServiceProviderName:@"LinkedIn" prefix:@"Demo"];
//        [self updateLinkedInUI:@"logged out"];
        //NSLog(@"logged out");
    }
}

- (OAToken *)linkedInEngineAccessToken:(RDLinkedInEngine *)engine {
    return [OAToken rd_tokenWithUserDefaultsUsingServiceProviderName:@"LinkedIn" prefix:@"Demo"];
}

- (void)linkedInEngine:(RDLinkedInEngine *)engine requestSucceeded:(RDLinkedInConnectionID *)identifier withResults:(id)results {
    //NSLog(@"++ LinkedIn engine reports success for connection %@\n%@", identifier, results);
//    if( identifier == self.fetchConnection ) {
//        NSDictionary* profile = results;
//        [self updateLinkedInUI:[NSString stringWithFormat:@"got profile for %@ %@", [profile objectForKey:@"first-name"], [profile objectForKey:@"last-name"]]];
//    }
    [self showAlert:@"LinkedIn Share Success"];
}

- (void)linkedInEngine:(RDLinkedInEngine *)engine requestFailed:(RDLinkedInConnectionID *)identifier withError:(NSError *)error {
    //NSLog(@"++ LinkedIn engine reports failure for connection %@\n%@", identifier, [error localizedDescription]);
    [self showAlert:@"LinkedIn Share failure"];
}


#pragma mark - RDLinkedInAuthorizationControllerDelegate

- (void)linkedInAuthorizationControllerSucceeded:(RDLinkedInAuthorizationController *)controller {
    [self fetchProfile];
    //After successful authorization just share the url
    [engine updateStatus:linkedInURL];
   // [engine shareUrl:@"" imageUrl:@"" title:linkedInURL comment:@"Test"];
   // [engine shareLink:[NSDictionary dictionary]];
}

- (void)linkedInAuthorizationControllerFailed:(RDLinkedInAuthorizationController *)controller {
//    [self updateLinkedInUI:@"failed!"];
    //NSLog(@"failed!");
}

- (void)linkedInAuthorizationControllerCanceled:(RDLinkedInAuthorizationController *)controller {
//    [self updateLinkedInUI:@"cancelled"];
    //NSLog(@"cancelled");
}

//#pragma mark - linkedInShareALink Method
//- (IBAction)linkedInShareALink
//{
//    //    NSDictionary *dict = [[NSDictionary alloc] init];
//    //    [dict setValue:@"http://www.probono.net/" forKey:@"link"];
//    //    [dict setValue:@"Here goes my comment: Sharing this from Immigo iOS mobile App" forKey:@"link_msg"];
//    //    [engine shareLink:dict];
//    //    [engine shareUrl:@"http://www.probono.net/" imageUrl:@"http://i637.photobucket.com/albums/uu99/mytravelguideposts/800px-Ggb_by_night.jpg" title:@"Here goes the title: Probono" comment:@"Here goes my comment: Sharing this from Immigo iOS mobile App"];
//    [engine updateStatus:@"http://www.probono.net/"];
//}


*/
#pragma mark - showAlert Method
- (void)showAlert:(NSString *)message
{
    
//    if(![NSThread isMainThread])
//    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Share to Social Media Networks" message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil, nil];
            [alert show];
        });
//    }
    return;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Share to Social Media Networks" message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil, nil];
    [alert show];
}


@end
