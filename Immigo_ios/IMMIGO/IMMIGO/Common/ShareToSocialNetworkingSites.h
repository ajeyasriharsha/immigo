//
//  ShareToSocialNetworkingSites.h
//  IMMIGO
//
//  Created by ArunHS on 4/7/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShareToSocialNetworkingSites : NSObject
{
    NSString *linkedInURL;
    
}



- (void)tweetTapped:(NSString *)urlStr andText:(NSString *)text fromViewContoller:(UIViewController*)parentVC;
- (void)fbShareBtnTapped:(NSString *)urlStr andText:(NSString *)text fromViewContoller:(UIViewController*)parentVC;
- (void)linkedInLogIn:(NSString *)urlStr andText:(NSString *)text fromViewContoller:(UIViewController*)parentVC;
//- (void)linkedInLogOut:(id)sender;
- (void) fbshareUsingSDK:(NSString *)urlString andText:(NSString *)text;

@end
