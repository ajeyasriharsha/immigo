//
//  UIView+border.h
//  ComplianceWire
//
//  Created by Shineeth Hamza on 16/06/14.
//  Copyright (c) 2014 EduNeering Holdings, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum
{
    UIBorderNone = 0,
    UIBorderTop = 1<<1,
    UIBorderLeft = 1<<2,
    UIBorderBottom = 1<<3,
    UIBorderRight = 1<<4
}UIBorderSide;

@interface UIView (border)
- (void)setBorderForSides:(UIBorderSide)sides withColor:(UIColor *)borderColor andWidth:(CGFloat)borderWidth;
@end
