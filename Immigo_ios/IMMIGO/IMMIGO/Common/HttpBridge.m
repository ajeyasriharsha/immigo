//Om Sri Sai Ram
//  HttpBridge.m
//  Immigo
//
//  Created by PrasadBabu KN on 6/3/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

//http://immigolulactest.azurewebsites.net/api/ImmigoApi/ImmigrationTopicContent/en
//http://immigolulactest.azurewebsites.net/api/Immigoapi/ImmigrationTopicContent/sp

#import "HttpBridge.h"
#import "APPConfig.h"
#import "Enums.h"

//#import "MediaDownloader.h"
@implementation HttpBridge

- (id)init 
{ 
    //Do not allow the create objects, Acts like static class
    self=nil;
    //    [super doesNotRecognizeSelector:_cmd];
    return nil;
}

+(void)invokeAPIWithRequest:(APIRequest*) request ResponseDelegate:(id<ASIHTTPRequestDelegate>) asiDelegate
{
    request.asiDelegate=asiDelegate;
    [self invokeAPIWithRequest:request];
}

+ (NSString *)getCurrentLanguage{
    NSString *languageSelected=@"";
     languageSelected= [BundleLocalization sharedInstance].language;//[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    
    if ([languageSelected isEqualToString:@"en"]) {
        languageSelected = @"en";
    }else if ([languageSelected isEqualToString:@"es"]){
        languageSelected = @"sp";
    }
    return languageSelected;
}

+(ASIHTTPRequest*)invokeAPIWithRequest:(APIRequest*) request
{
    NSString *path=nil;
    if (request.fromProbonoAPI) //If request is for PROBONO API
    {
        path =[APP_PROBONO_BASE_URL stringByAppendingString:request.apiUri];
    }
    else //if request is for CMS
    {
        path =[APP_CMS_BASE_URL stringByAppendingString:request.apiUri];
        path =[path stringByAppendingString:[NSString stringWithFormat:@"/%@",[self getCurrentLanguage]]];
    }
    if(HM_GET==request.httpMethod && request.requestData)
        path=[path stringByAppendingString:[NSString stringWithUTF8String:[request.requestData bytes]]];

    ASIHTTPRequest *httpRequest = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:path]];
    [httpRequest setDelegate:request.asiDelegate];
    [httpRequest setTimeOutSeconds:APP_HTTP_TIMEOUT];
    [httpRequest setTag:request.apiKey];

    [httpRequest setRequestMethod:[Enums StringForHTTPMethod:request.httpMethod]];
    if(HM_GET!=request.httpMethod)
    {
        [httpRequest appendPostData:request.requestData];
        [httpRequest addRequestHeader:@"Content-Type" value:@"application/json"];
    }
    if(request.fromProbonoAPI)
    {
//        [httpRequest addBasicAuthenticationHeaderWithUsername:@"Immigo" andPassword:@"?qV85\"4\"0S"];
        
        //NSLog(@"%@", [httpRequest requestHeaders]);
        if(request.aditionalHeaders)
        {
            for(id key in request.aditionalHeaders)
            {
                id value = [request.aditionalHeaders objectForKey:key];
                [httpRequest addRequestHeader:key value:value];
            }
        }

    }
    [httpRequest startAsynchronous];
    return httpRequest;
}

@end
