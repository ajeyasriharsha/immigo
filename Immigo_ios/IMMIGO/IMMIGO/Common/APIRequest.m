//Om Sri Sai Ram
//  APIRequest.m
//  Immigo
//
//  Created by PrasadBabu KN on 6/3/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

/*
 
 1.       Get all Active - http://192.168.33.45/api/immigoapi/Topics
 
 2.       Get all Active Alerts - http://192.168.33.45/api/immigoapi/alerts
 
 3.       Get the Active About Us page http://192.168.33.45/api/immigoapi/AboutUsPage
 
 4.       Get the Active Privacy Policy page - http://192.168.33.45/api/immigoapi/PrivacyPolicyPage
 
 5.       Get the Active Terms of Use page - http://192.168.33.45/api/immigoapi/TermsofUsePage
 */
#import "APIRequest.h"
#import "APPConfig.h"

@implementation APIRequest

@synthesize apiKey, apiUri, fromProbonoAPI, requestData, aditionalHeaders, httpMethod, asiDelegate;

//const int API_COUNT=21;
static NSString* apiUris[API_COUNT];
static HttpMethod apiMethods[API_COUNT];
static bool apiFromProbono[API_COUNT];

+(void)initialize
{
    //************************* PBN (Probono) APIService **********************//

    apiUris[API_TRAININGSANDEVENTS_GET_ALL]=@"events/current";
    apiMethods[API_TRAININGSANDEVENTS_GET_ALL]=HM_GET;
    apiFromProbono[API_TRAININGSANDEVENTS_GET_ALL] = YES;
    
    apiUris[API_NEWS_GET_ALL]=@"news";
    apiMethods[API_NEWS_GET_ALL]=HM_GET;
    apiFromProbono[API_NEWS_GET_ALL] = YES;
    
    apiUris[API_LEGALORGANIZATIONS_GET_ALL]=@"legalorganizations";
    apiMethods[API_LEGALORGANIZATIONS_GET_ALL]=HM_GET;
    apiFromProbono[API_LEGALORGANIZATIONS_GET_ALL] = YES;
    
//     http://api.probono.net/legalorganizations?zipcode=10001
//     http://api.probono.net/legalorganizations?gps=40.7536854,-73.9991637.

    //************************* CMS (Content Management System) APIService **********************//
    apiUris[API_ABOUT_GET]=@"AboutUsPage";
    apiMethods[API_ABOUT_GET]=HM_GET;
    apiFromProbono[API_ABOUT_GET] = NO;

    apiUris[API_PRIVACY_GET]=@"PrivacyPolicyPage";
    apiMethods[API_PRIVACY_GET]=HM_GET;
    apiFromProbono[API_PRIVACY_GET] = NO;

    apiUris[API_TERMSOFUSE_GET]=@"TermsofUsePage";
    apiMethods[API_TERMSOFUSE_GET]=HM_GET;
    apiFromProbono[API_TERMSOFUSE_GET] = NO;

    apiUris[API_ALERTS_GET]=@"alerts";
    apiMethods[API_ALERTS_GET]=HM_GET;
    apiFromProbono[API_ALERTS_GET] = NO;

    apiUris[API_IMMIGRATIONBASICS_GET_ALL]=@"ImmigrationTopicContent";//TopicContents//ImmigrationTopic
    apiMethods[API_IMMIGRATIONBASICS_GET_ALL]=HM_GET;
    apiFromProbono[API_IMMIGRATIONBASICS_GET_ALL] = NO;
    
    apiUris[API_GET_CIVIL_RIGHTS] =@"CivilRightsTopicContent";
    apiMethods[API_GET_CIVIL_RIGHTS] =HM_GET;
    apiFromProbono[API_GET_CIVIL_RIGHTS] = NO;
    
    apiUris[API_GET_CIVIC_ENGAGEMENT] =@"CivicEngagementsTopicContent";
    apiMethods[API_GET_CIVIC_ENGAGEMENT] =HM_GET;
    apiFromProbono[API_GET_CIVIC_ENGAGEMENT] = NO;

    apiUris[API_POLL_GET_ALL]=@"PollQuestions";
    apiMethods[API_POLL_GET_ALL]=HM_POST;
    apiFromProbono[API_POLL_GET_ALL] = NO;

    
    apiUris[API_POLL_POST_ALL]=@"SubmitPollAnswers";
    apiMethods[API_POLL_POST_ALL]=HM_POST;
    apiFromProbono[API_POLL_POST_ALL] = NO;

    apiUris[API_DEVICE_POST]=@"AddUser";
    apiMethods[API_DEVICE_POST]=HM_POST;
    apiFromProbono[API_DEVICE_POST] = NO;

    apiUris[API_FEEDBACK_POST]=@"AddUserFeedback";
    apiMethods[API_FEEDBACK_POST]=HM_POST;
    apiFromProbono[API_FEEDBACK_POST] = NO;

}

+(APIRequest*) apiRequestWithKey:(APIKey) key
{    
    return [[APIRequest alloc] initWithApiKey:key URI:apiUris[key] HttpMethod:apiMethods[key] FromProbonoAPI:apiFromProbono[key]];
}


-(id)initWithApiKey:(APIKey) aKey URI:(NSString*) aUri HttpMethod:(HttpMethod) aMethod FromProbonoAPI:(bool) fromProbono
{
    if ((self = [super init])) 
    {
        apiKey=aKey;
        apiUri=aUri;
        httpMethod=aMethod;
        fromProbonoAPI=fromProbono;
    }
    return self;
}

-(void)setRequestDataWithDictionary:(NSDictionary*) dictionary
{
    if(HM_GET==httpMethod)
    {
        NSMutableString *strQuery=[[NSMutableString alloc] initWithString:@""];
        if([dictionary count]>0)
        {
            [strQuery appendString:@"?"];
            bool appendAperstand=false;
            for (id key in dictionary) 
            {
                if(appendAperstand)
                    [strQuery appendString:@"&"];
                NSString *paramValue=[NSString stringWithFormat:@"%@",[dictionary objectForKey:key]];
                paramValue=[APPConfig urlencode:paramValue];
                [strQuery appendFormat:@"%@=%@",key,paramValue];
                appendAperstand=true;
            }
        }
        [strQuery appendString:@"\0"];
        requestData=[strQuery dataUsingEncoding:NSUTF8StringEncoding];
        strQuery=nil;
    }
    else
    {
        if(dictionary==nil || [dictionary isEqual:[NSNull null]])
            return;

        NSError *jsonError;
        NSData *jsonData=[NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&jsonError];
        if(!jsonData)
        {
            //NSLog(@"Dictionary to JSON Error:\n%@", jsonError);
            return;
        }
        requestData=jsonData;
        
//        //NSLog(@"Request JSON: %@", [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    }
}
-(void)setRequestDataWithArray:(NSArray *)array
{
    {
        if(array.count == 0)
            return;
        
        NSError *jsonError;
        NSData *jsonData=[NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&jsonError];
        if(!jsonData)
        {
            //NSLog(@"Dictionary to JSON Error:\n%@", jsonError);
            return;
        }
        requestData=jsonData;
        
        //        //NSLog(@"Request JSON: %@", [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    }
}

@end
