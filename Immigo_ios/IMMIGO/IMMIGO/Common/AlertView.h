//Om Sri Sai Ram
//  AlertView.h
//  Immigo
//
//  Created by PrasadBabu KN on 6/6/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlertView : UIAlertView //<UIAlertViewDelegate>

+ (AlertView *)getSingletonInstance;
+ (void) releaseInstance;

+ (void)alertWithError : (NSError *)error;
+ (void)alertWithMessage : (NSString *)message;
+ (void)alertWithMessage : (NSString *)message andDelegate:(id)delegate;
+(void)alertWithMessage:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;



+(AlertView *)getMultiBtnInstance;
+(void) releaseMultiBtnAlert;
+ (void)alertWithMessage:(NSString *)message delegate:(id)delegate otherButtonTitles:(NSArray *)otherButtonTitlesArray;

@end


