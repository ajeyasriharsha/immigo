//
//  Common.h
//  Immigo
//
//  Created by pradeep ISPV on 3/3/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"


#define APP_STORE_URL @"http://appstore.com/immigo"
#define APP_STORE_ITUNES_URL @"https://itunes.apple.com/in/app/immigo/id891595380"
#define APP_STORE_COMMON_LINK @"http://onelink.to/mducr3"
//*****************FONTS*****************//

extern NSString *const FONT_REGULAR;
extern NSString *const FONT_REGULAR;
extern NSString *const FONT_LIGHT;
extern NSString *const FONT_LIGHT_ITALIC;
extern NSString *const FONT_ITALIC;
extern NSString *const FONT_BOLD;
extern NSString *const FONT_BOLD_ITALIC;
extern NSString *const FONT_EXTRA_BOLD;
extern NSString *const FONT_EXTRA_BOLD_ITALIC;
extern NSString *const FONT_SEMIBOLD;
extern NSString *const FONT_SEMIBOLD_ITALIC;
extern NSString *const FONT_IPAD;
//*************END*******************//

extern float PARTNERS_DELAY_TIME;
extern NSString *const PARTNERS_NCLR_URL;
extern NSString *const PARTNERS_IMMIGRATION_URL;
extern NSString *const PARTNERS_VERIZON_URL;
extern NSString *const PARTNERS_PROBONO_URL;

extern unsigned long int UI_TITLE_HEADER_COLOR;
extern unsigned long int UI_ALERT_DESCRIPTION_COLOR;
extern unsigned long int UI_BLUE_TEXT_COLOR;
//search List
extern unsigned long int UI_LIST_TEXT_COLOR;
//

//Training
extern unsigned long int UI_TRAINING_HEADER_TEXT_COLOR;
extern unsigned long int UI_TRAINING_DESC_TEXT_COLOR;
//


//Training Ipad
extern unsigned long int UI_TRAINING_HEADER_TEXT_COLOR_IPAD;
extern unsigned long int UI_TRAINING_DESC_TEXT_COLOR_IPAD;
extern unsigned long int UI_TRAINING_DESC_LABEL_TEXT_COLOR_IPAD;
extern unsigned long int UI_TRAINING_DESC_CONTENTLABEL_TEXT_COLOR_IPAD;
//Sharing
extern NSString *const kLinkedInOAuthConsumerKey;
extern NSString *const kLinkedInOAuthConsumerSecret;
//

//For Google Analytics GUID
extern NSString *const IMMIGO_GUID;
extern NSString *const DEVICE_REGISTER;
//

@interface Common : NSObject

+ (void)logEventToGoogleAnalytics:(NSString *)eventName;
+ (void)logMainEventsToGoogleAnalytics:(NSString *)categoryName withAction:(NSString *)action andLabel:(NSString *)label and:(int)value;
+ (NSString*)getUserDetails;
+ (NSString *)generateGUID;
+ (BOOL)isValidUrl:(NSString *)urlString;
+ (BOOL) currentNetworkStatus;
+ (NSString*) deviceName;
@end
