//
//  FaceBookActivityProvider.h
//  ActivityProviderSample
//
//  Created by Ajeya Sriharsha on 5/18/15.
//  Copyright (c) 2015 Ajeya Sriharsha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FaceBookActivityProvider : UIActivityItemProvider
{
     NSString *_twitterItem;
     NSString *_urlForStore;
     NSString *_contentForPrint;
    
}
- (instancetype)initWithPlaceholderItem:(id)placeholderItem twitterItem:(NSString *)twitterItem andUrlForStore:(NSString *)urlForStore;
@end
