//Om Sri Sai Ram
//  AlertView.m
//  ImmigoMultiTouch
//
//  Created by PrasadBabu KN on 6/6/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "AlertView.h"

static AlertView *singletonObj = nil;
static AlertView *multiBtnAlert = nil;

@implementation AlertView

#pragma mark - Singleton Method
+(AlertView *)getSingletonInstance
{
    @synchronized(self){
        if (singletonObj == nil) 
        {
            singletonObj = [[self alloc] init];//tis ll call the init method defined below
            [singletonObj addButtonWithTitle:NSLocalizedString(@"OK", @"OK")];
            singletonObj.cancelButtonIndex=0;
        }
        return singletonObj;
    }
}

+(AlertView *)getMultiBtnInstance
{
    @synchronized(self)
    {
        if (multiBtnAlert == nil)
        {
            multiBtnAlert = [[self alloc] init];//tis ll call the init method defined below
        }
        return multiBtnAlert;
    }
}


+(void) releaseInstance
{
    [singletonObj dismissWithClickedButtonIndex:0 animated:NO];
    singletonObj = nil;
    [self releaseMultiBtnAlert];
}

+ (void) releaseMultiBtnAlert
{
    [multiBtnAlert dismissWithClickedButtonIndex:0 animated:NO];
    multiBtnAlert = nil;
}

#pragma mark - init
-(id)init
{
    self = [super init];
    return self;
}

#pragma mark - AlertView Methods
+ (void)alertWithError : (NSError *)error
{
    if(error)
    {
        NSString *message = [NSString stringWithFormat:@"Error! %@ %@",
                             [error localizedDescription],
                             [error localizedFailureReason]];
        [self alertWithMessage:message];
    }
}

+ (void)alertWithMessage : (NSString *)message
{
    if(message && [message length]>0)
    {
        if(!singletonObj)
        {
            [self getSingletonInstance];
        }
        singletonObj.title=NSLocalizedString(@"APP_NAME", "");
        singletonObj.message=[[NSBundle mainBundle] localizedStringForKey:message value:message table:nil];
        singletonObj.delegate=nil;
        
        [singletonObj buttonTitleAtIndex:0];
        [singletonObj show];
    }
    
}

+ (void)alertWithMessage : (NSString *)message andDelegate:(id)delegate
{
    if(message && [message length]>0)
    {
        if(!singletonObj){
            [self getSingletonInstance];
        }
        singletonObj.title=NSLocalizedString(@"APP_NAME", "");
        singletonObj.message=[[NSBundle mainBundle] localizedStringForKey:message value:message table:nil];
        singletonObj.delegate=delegate;
        [singletonObj buttonTitleAtIndex:0];
        [singletonObj show];
    }
}

+(void)alertWithMessage:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION
{
    
    if(!singletonObj){
        [self getSingletonInstance];
    }
    singletonObj.title=NSLocalizedString(@"APP_NAME", "");
    singletonObj.message=[[NSBundle mainBundle] localizedStringForKey:message value:message table:nil];
    singletonObj.delegate=delegate;
    [singletonObj addButtonWithTitle:cancelButtonTitle];
    singletonObj.cancelButtonIndex = 0;
    
    va_list args;
    va_start(args, otherButtonTitles);
    
    id arg = nil;
    while ((arg = va_arg(args,id))) {
        [singletonObj addButtonWithTitle:(NSString*)arg];
    }
    
    va_end(args);
    [singletonObj show];
}


+ (void)alertWithMessage:(NSString *)message delegate:(id<UIAlertViewDelegate>)delegate otherButtonTitles:(NSArray *)otherButtonTitlesArray
{
    
    if(message && [message length]>0)
    {
        [self releaseMultiBtnAlert];
        if(!multiBtnAlert)
        {
            [self getMultiBtnInstance];
        }
        
        multiBtnAlert.title=NSLocalizedString(@"APP_NAME", "");
        multiBtnAlert.message=[[NSBundle mainBundle] localizedStringForKey:message value:message table:nil];
        multiBtnAlert.delegate=delegate;
        [multiBtnAlert buttonTitleAtIndex:0];
        
        if ([otherButtonTitlesArray count] > 0)
        {
            for (int i=0; i<[otherButtonTitlesArray count]; i++)
            {
                [multiBtnAlert addButtonWithTitle:[otherButtonTitlesArray objectAtIndex:i]];
            }
        }
        [multiBtnAlert show];
    }
    else
    {
        [delegate alertView:nil clickedButtonAtIndex:0];
    }
}
@end
