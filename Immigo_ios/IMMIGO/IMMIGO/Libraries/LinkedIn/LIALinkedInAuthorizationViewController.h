
#import <UIKit/UIKit.h>
#import "LIALinkedInApplication.h"

typedef void(^LIAAuthorizationCodeSuccessCallback)(NSString *code);
typedef void(^LIAAuthorizationCodeCancelCallback)(void);
typedef void(^LIAAuthorizationCodeFailureCallback)(NSError *errorReason);

@interface LIALinkedInAuthorizationViewController : UIViewController

- (id)initWithApplication:(LIALinkedInApplication *)application success:(LIAAuthorizationCodeSuccessCallback)success cancel:(LIAAuthorizationCodeCancelCallback)cancel failure:(LIAAuthorizationCodeFailureCallback)failure;

@end
