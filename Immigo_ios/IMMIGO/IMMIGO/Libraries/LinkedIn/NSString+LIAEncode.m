

#import "NSString+LIAEncode.h"

@implementation NSString (LIAEncode)

-(NSString *) LIAEncode {
	return (NSString *)CFBridgingRelease(
		CFURLCreateStringByAddingPercentEscapes(
			NULL,
			(__bridge CFStringRef) self,
			NULL,
			CFSTR("!*'();:@&=+$,/?%#[]"),
			kCFStringEncodingUTF8
			)
		);
}

@end
