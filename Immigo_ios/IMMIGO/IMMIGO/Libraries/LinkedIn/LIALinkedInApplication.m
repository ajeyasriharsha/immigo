

#import "LIALinkedInApplication.h"


@implementation LIALinkedInApplication

- (id)initWithRedirectURL:(NSString *)redirectURL clientId:(NSString *)clientId clientSecret:(NSString *)clientSecret state:(NSString *)state grantedAccess:(NSArray *)grantedAccess {
    self = [super init];
    if (self) {
        self.redirectURL = redirectURL;
        self.clientId = clientId;
        self.clientSecret = clientSecret;
        self.state = state;
        self.grantedAccess = grantedAccess;
    }

    return self;
}

+ (id)applicationWithRedirectURL:(NSString *)redirectURL clientId:(NSString *)clientId clientSecret:(NSString *)clientSecret state:(NSString *)state grantedAccess:(NSArray *)grantedAccess {
    return [[self alloc] initWithRedirectURL:redirectURL clientId:clientId clientSecret:clientSecret state:state grantedAccess:grantedAccess];
}

- (NSString *)grantedAccessString {
    return [self.grantedAccess componentsJoinedByString: @"%20"];
}

@end