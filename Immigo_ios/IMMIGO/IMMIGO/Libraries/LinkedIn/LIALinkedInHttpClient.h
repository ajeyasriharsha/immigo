

#import "AFHTTPRequestOperationManager.h"
#define LINKEDIN_TOKEN_KEY          @"linkedin_token"
#define LINKEDIN_EXPIRATION_KEY     @"linkedin_expiration"
#define LINKEDIN_CREATION_KEY       @"linkedin_token_created_at"
@class LIALinkedInApplication;

@interface LIALinkedInHttpClient : AFHTTPRequestOperationManager

+ (LIALinkedInHttpClient *)clientForApplication:(LIALinkedInApplication *)application;

+ (LIALinkedInHttpClient *)clientForApplication:(LIALinkedInApplication *)application presentingViewController:viewController;

- (BOOL)validToken;

- (NSString *)accessToken;

- (void)getAccessToken:(NSString *)authorizationCode success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure;

- (void)getAuthorizationCode:(void (^)(NSString *))success cancel:(void (^)(void))cancel failure:(void (^)(NSError *))failure;
@end
