//
//  RegisterDeviceModel.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/30/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegisterDeviceModel : NSObject
+ (RegisterDeviceModel*)sharedDataSource;
+ (void) releaseInstance;

- (void) registerDeviceWithId:(NSString *)deviceId AndType:(NSString *)deviceType AndPushNotificationStatus:(BOOL)isAllowed AtAppLaunch:(BOOL)isFirstTime;
@end
