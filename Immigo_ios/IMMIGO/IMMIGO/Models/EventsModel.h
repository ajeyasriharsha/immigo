//
//  EventsModel.h
//  Immigo
//
//  Created by pradeep ISPV on 3/25/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataDownloader.h"
#import "Event.h"

@interface EventsModel : NSObject <DataDownloaderDelegate>

{
    NSMutableArray *allEvents;
    
    Event *_selectedEvent;
}
@property (nonatomic,strong) Event *selectedEvent;

-(void)insertEvent:(Event *)event;
-(NSArray *)allTrainings;

+ (EventsModel*)sharedDataSource;
+ (void) releaseInstance;
-(void)selectEventAtindex:(NSInteger)index;

-(void)fetchTrainings;
@end
