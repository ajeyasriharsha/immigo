//
//  LegalAssistanceModel.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/14/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LegalAssistanceModel : NSObject
{
    NSMutableArray *allValues;
    NSDictionary *dictionary;
}

//-(void)insertAreasOfLaw:(AreasOfLaw *)aol;
-(NSArray *)allObjects;

+ (LegalAssistanceModel*)sharedDataSource;
+ (void) releaseInstance;

-(NSString *)getIdForValue:(NSString *)value;

@end
