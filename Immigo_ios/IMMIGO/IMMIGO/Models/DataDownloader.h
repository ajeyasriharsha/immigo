//
//  DataDownloader.h
//  Immigo
//
//  Created by pradeep ISPV on 3/25/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HttpBridge.h"
#import "Event.h"

@protocol DataDownloaderDelegate <NSObject, HTTPResponseErrorDelegate>
@optional
-(void)TrainingsLoaded;
-(void)TrainingsLoadFailure;

-(void)NewsLoaded;
-(void)NewsLoadFailure;

-(void)legalFindersDownloadedSuccessfully;
-(void)legalFindersLoadedFailure;


-(void)Alertsloaded;
-(void)AlertsLoadFailure;

-(void)aboutUsDownloadedSuccessfully;
-(void)aboutUsLoadedFailure;

-(void)privacyDownloadedSuccessfully;
-(void)privacyLoadedFailure;

-(void)termsDownloadedSuccessfully;
-(void)termsLoadedFailure;

-(void)immigrationBasicsDownloadedSuccessfully;
-(void)immigrationBasicsLoadedFailure;

-(void)pollQuestionsDownloadedSuccessfully;
-(void)pollQuestionsLoadedFailure;

-(void)pollQuestionsSubmittedSuccessfully;
-(void)pollQuestionsSubmittedFailure;

-(void)feedBackSubmittedSuccessfully;
-(void)feedBackSubmittedFailure;

-(void)deviceSubmitFailureAtAppLaunch;
-(void)deviceSubmitFailure;
-(void)deviceSubmitSuccess;
@end

@interface DataDownloader : NSObject <ASIHTTPRequestDelegate,NSXMLParserDelegate>
{
//    bool _isTrainingsRequestPipeline;
//    bool _isTrainingsRefreshing;

    Event *_currentEvent;
    ASIHTTPRequest *getTrainingsRequest;
    ASIHTTPRequest *getNewsRequest;
    ASIHTTPRequest *getLegalFindersRequest;

    //CMS SERVICES
    ASIHTTPRequest *getAboutUsRequest;
    ASIHTTPRequest *getPrivacyRequest;
    ASIHTTPRequest *getTermsRequest;
    ASIHTTPRequest *getAlertsRequest;
    ASIHTTPRequest *getImmigrationBaiscsRequest;
    ASIHTTPRequest *getCivicEngagementRequest;
    ASIHTTPRequest *getCivilRightstRequest;
    ASIHTTPRequest *getPollQuestionsRequest;
    ASIHTTPRequest *postPollQuestionsRequest;
    ASIHTTPRequest *postRegisterDeviceRequest;
    ASIHTTPRequest *postFeedBackRequest;
    
    BOOL notifAtAppLaunch;
    BOOL isAllowedPushNotf;
}
@property (nonatomic, retain) Event *currentEvent;

@property(strong, nonatomic) id<DataDownloaderDelegate> delegate;
@property(strong, nonatomic) id<DataDownloaderDelegate> alertsDelegate;

//Class method.
+ (DataDownloader *)sharedDownloader;

//Method the release the meory for singleton Object
+(void) releaseInstance;

- (bool) fetchTrainingsAndEventsData;
- (bool) fetchNewsDetails;
- (bool) fetchLegalFindersWithZipCode:(NSString *)zipCode OrWithCurrentLocationLattitude:(NSString *)lattitude AndLongitude:(NSString *)longitude;
- (bool) fetchLegalFindersWithZipCode:(NSString *)zipCode OrWithCurrentLocationLattitude:(NSString *)lattitude AndLongitude:(NSString *)longitude withLanguages:(NSString*)languages withAreasOfLaws:(NSString *)aols withLegalAssistance:(NSString *)legalassistances;
//- (bool) fetchLegalFindersWithZipCode:(NSString *)zipCode  withLanguages:(NSString*)languages withAreasOfLaws:(NSString *)aols withLegalAssistance:(NSString *)legalassistances;

//CMS services
- (bool) fetchAboutUsPage;
- (bool) fetchPrivacyPolicy;
- (bool) fetchTermsOfUse;
- (bool) fetchAlerts;
- (bool) fetchImmigrationBasics;
- (bool) fetchCivicEngagement;
- (bool) fetchCivilRights;
- (bool) fetchPollQuestionsForDevice:(NSString *)deviceId;
//- (bool) registerDeviceWithId:(NSString *)deviceId AndType:(NSString *)deviceType AndPushNotificationStatus:(BOOL)isAllowed;
- (bool) registerDeviceWithId:(NSString *)deviceId AndType:(NSString *)deviceType AndPushNotificationStatus:(BOOL)isAllowed AtAppLaunch:(BOOL)isFirstTime;
- (bool) submitPollQuestionAnswerWithQuestionId:(int )quesionId AnswerId:(int)answerId AndDeviceId:(NSString *)deviceId;
- (bool) submitFeedBackWithDeviceId:(NSString *)deviceId AndUserName:(NSString *)userName AndEmailId:(NSString *)emailId AndPhoneNumber:(NSString *)phoneNumber AndFeedback:(NSString *)feedbackMessage AndStatus:(BOOL)isAllowed;
/************ Cancel All Requests *************/
- (void)cancelAllRequests;
@end
