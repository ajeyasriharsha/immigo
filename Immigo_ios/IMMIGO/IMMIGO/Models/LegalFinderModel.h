//
//  LegalFinderModel.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/9/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LegalFinder.h"
#import "DataDownloader.h"

@interface LegalFinderModel : NSObject <DataDownloaderDelegate>
{
    NSMutableArray *allObjects;
    
    LegalFinder *_selectedFinder;
}
@property (nonatomic,strong) LegalFinder *selectedFinder;

-(void)insertOrg:(LegalFinder *)finder;
-(NSArray *)allItems;

+ (LegalFinderModel*)sharedDataSource;
+ (void) releaseInstance;
-(void)selectFinderAtindex:(NSInteger)index;

-(void)fetchLegalFindersWithZipCode:(NSString *)zipCode OrWithCurrentLocationLattitude:(NSString *)lattitude AndLongitude:(NSString *)longitude;
-(void)fetchLegalFindersWithZipCode:(NSString *)zipCode OrWithCurrentLocationLattitude:(NSString *)lattitude AndLongitude:(NSString *)longitude withLanguages:(NSString*)languages withAreasOfLaws:(NSString *)aols withLegalAssistance:(NSString *)legalassistances;

@end
