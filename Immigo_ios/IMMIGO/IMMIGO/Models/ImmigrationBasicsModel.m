//
//  ImmigrationBasicsModel.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/24/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "ImmigrationBasicsModel.h"
#import "ImmigrationBasicsDAO.h"

@implementation ImmigrationBasicsModel
@synthesize selectedTopic =_selectedTopic;
#pragma mark "Implemention of Single Ton"

static ImmigrationBasicsModel *_sharedInstance;
static dispatch_once_t oncePredicate;

+ (ImmigrationBasicsModel*)sharedDataSource
{
	if(!_sharedInstance)
    {
		dispatch_once(&oncePredicate, ^{
			_sharedInstance = [[super allocWithZone:nil] init];
        });
    }
    return _sharedInstance;
}

+(void) releaseInstance
{
    _sharedInstance=nil;
    oncePredicate=0;
}

+ (id)allocWithZone:(NSZone *)zone
{
	return [self sharedDataSource];
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

#if (!__has_feature(objc_arc))

- (id)retain
{
	return self;
}

- (unsigned)retainCount
{
	return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
	//do nothing
}

- (id)autorelease
{
	return self;
}
#endif

#pragma mark -
#pragma mark Custom Methods

- (id)init
{
	@synchronized(self)
    {
		if ((self = [super init]))
		{
 		}
		return self;
	}
}

-(void)fetchCivilRights{
    
    [[DataDownloader sharedDownloader] fetchCivilRights];
}

-(void)fetchCivicEngagement{
    
    [[DataDownloader sharedDownloader] fetchCivicEngagement];
}
-(void)fetchTopics
{
    [[DataDownloader sharedDownloader] fetchImmigrationBasics];
}
-(void)insertTopic:(Topic *)topic
{
    
}
-(NSArray *)allItems
{
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"orderNo"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [[ImmigrationBasicsDAO allObjects] sortedArrayUsingDescriptors:sortDescriptors];
    
    return sortedArray;
}

@end
