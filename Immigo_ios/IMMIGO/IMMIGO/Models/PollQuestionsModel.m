//
//  PollQuestionsModel.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/28/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "PollQuestionsModel.h"

@implementation PollQuestionsModel
@synthesize selectedQuestion = _selectedQuestion;
#pragma mark "Implemention of Single Ton"

static PollQuestionsModel *_sharedInstance;
static dispatch_once_t oncePredicate;

+ (PollQuestionsModel*)sharedDataSource
{
	if(!_sharedInstance)
    {
		dispatch_once(&oncePredicate, ^{
			_sharedInstance = [[super allocWithZone:nil] init];
        });
    }
    return _sharedInstance;
}

+(void) releaseInstance
{
    _sharedInstance=nil;
    oncePredicate=0;
}

+ (id)allocWithZone:(NSZone *)zone
{
	return [self sharedDataSource];
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

#if (!__has_feature(objc_arc))

- (id)retain
{
	return self;
}

- (unsigned)retainCount
{
	return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
	//do nothing
}

- (id)autorelease
{
	return self;
}
#endif

#pragma mark -
#pragma mark Custom Methods

- (id)init
{
	@synchronized(self)
    {
		if ((self = [super init]))
		{
 		}
		return self;
	}
}
-(void)fetchPollQuestionsForDevice:(NSString *)deviceId
{
    [[DataDownloader sharedDownloader] fetchPollQuestionsForDevice:deviceId];
}
-(NSArray *)allObjects
{
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"orderNo"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [[PollQuestionsDAO allObjects] sortedArrayUsingDescriptors:sortDescriptors];
    
    return sortedArray;
}
-(void)selectQuestionWithID:(int)key
{
    _selectedQuestion = [PollQuestionsDAO objectWithID:key];
}
- (void) submitPollQuestionAnswerWithQuestionId:(int )quesionId AnswerId:(int)answerId AndDeviceId:(NSString *)deviceId
{
    [[DataDownloader sharedDownloader] submitPollQuestionAnswerWithQuestionId:quesionId AnswerId:answerId AndDeviceId:deviceId];
}

@end
