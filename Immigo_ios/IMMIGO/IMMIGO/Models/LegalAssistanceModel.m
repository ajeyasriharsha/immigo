//
//  LegalAssistanceModel.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/14/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "LegalAssistanceModel.h"

@implementation LegalAssistanceModel
#pragma mark "Implemention of Single Ton"

static LegalAssistanceModel *_sharedInstance;
static dispatch_once_t oncePredicate;

+ (LegalAssistanceModel*)sharedDataSource
{
	if(!_sharedInstance)
    {
		dispatch_once(&oncePredicate, ^{
			_sharedInstance = [[super allocWithZone:nil] init];
        });
    }
    return _sharedInstance;
}

+(void) releaseInstance
{
    _sharedInstance=nil;
    oncePredicate=0;
}

+ (id)allocWithZone:(NSZone *)zone
{
	return [self sharedDataSource];
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

#if (!__has_feature(objc_arc))

- (id)retain
{
	return self;
}

- (unsigned)retainCount
{
	return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
	//do nothing
}

- (id)autorelease
{
	return self;
}
#endif
/*
 
 1	Help completing forms
 2	Filings with USCIS
 3	Representation at Asylum Interviews (Credible Fear Interviews, Reasonable Fear Interviews)
 4	Representation before the Immigration Court
 5	Representation before the Board of Immigration Appeals (BIA)
 6	Federal court appeals
 */
#pragma mark -
#pragma mark Custom Methods

- (id)init
{
	@synchronized(self)
    {
		if ((self = [super init]))
		{
            [self insertValues];
 		}
		return self;
	}
}
-(void)insertValues
{
    dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                  @"Help completing forms",@"1",
                  @"Filings with USCIS",@"2",
                  @"Representation at Asylum Interviews (Credible Fear Interviews  Reasonable Fear Interviews)",@"3",
                  @"Representation before the Immigration Court",@"4",
                  @"Representation before the Board of Immigration Appeals (BIA)",@"5",
                  @"Federal court appeals",@"6",
                  nil];
    
    
    
    //    if (areasOfLaws)
    //    {
    //        [areasOfLaws removeAllObjects];
    //        areasOfLaws = nil;
    //    }
    allValues = [NSMutableArray arrayWithArray:[dictionary allValues]];
}
-(NSArray *)allObjects
{
    return [allValues sortedArrayUsingSelector:@selector(compare:)];
}
-(NSString *)getIdForValue:(NSString *)value
{
    NSString *string;
    NSArray *array =[dictionary allKeysForObject:value];
    if (array && array.count>0) {
           string = [array objectAtIndex:0];
    }
    return string;
}

@end
