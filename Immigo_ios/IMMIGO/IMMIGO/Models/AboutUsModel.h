//
//  AboutUsModel.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/2/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataDownloader.h"

@interface AboutUsModel : NSObject <DataDownloaderDelegate>
{
    NSString *_aboutString;
}
@property(nonatomic,strong) NSString *aboutString;
+ (AboutUsModel*)sharedDataSource;
+ (void) releaseInstance;

-(void)getAboutUs;
-(void)aboutUsString:(NSString *)htmlString;
@end
