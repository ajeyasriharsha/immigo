//
//  ImmigrationBasicsModel.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/24/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Topic.h"
#import "DataDownloader.h"

@interface ImmigrationBasicsModel : NSObject <DataDownloaderDelegate>
{
    NSMutableArray *allObjects;
    
    Topic *_selectedTopic;
}
@property (nonatomic,strong) Topic *selectedTopic;


+ (ImmigrationBasicsModel*)sharedDataSource;
+ (void) releaseInstance;
//-(void)selectFinderAtindex:(NSInteger)index;
-(void)fetchCivicEngagement;
-(void)fetchCivilRights;
-(void)fetchTopics;
-(void)insertTopic:(Topic *)topic;
-(NSArray *)allItems;


@end
