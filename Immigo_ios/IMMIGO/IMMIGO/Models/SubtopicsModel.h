//
//  SubtopicsModel.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/28/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SubTopic.h"
#import "SubTopicsDAO.h"

@interface SubtopicsModel : NSObject
{
    NSMutableArray *allObjects;
    
    SubTopic *_selectedSubTopic;
}
@property (nonatomic,strong) SubTopic *selectedSubTopic;


+ (SubtopicsModel*)sharedDataSource;
+ (void) releaseInstance;
//-(void)selectFinderAtindex:(NSInteger)index;

-(NSArray *)fetchSubTopicsForTopic:(Topic *)topic;


@end
