//
//  LanguageModel.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/14/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "LanguageModel.h"

@implementation LanguageModel
#pragma mark "Implemention of Single Ton"

static LanguageModel *_sharedInstance;
static dispatch_once_t oncePredicate;

+ (LanguageModel*)sharedDataSource
{
	if(!_sharedInstance)
    {
		dispatch_once(&oncePredicate, ^{
			_sharedInstance = [[super allocWithZone:nil] init];
        });
    }
    return _sharedInstance;
}

+(void) releaseInstance
{
    _sharedInstance=nil;
    oncePredicate=0;
}

+ (id)allocWithZone:(NSZone *)zone
{
	return [self sharedDataSource];
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

#if (!__has_feature(objc_arc))

- (id)retain
{
	return self;
}

- (unsigned)retainCount
{
	return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
	//do nothing
}

- (id)autorelease
{
	return self;
}
#endif
/*
 
 58	Afrikaans
 59	Akan-Twi
 11	Albanian
 60	Amharic
 6	Arabic
 105	Arabic (Egyptian)
 106	Arabic (Levantine)
 12	Armenian
 61	Azerbaijani
 7	Bengali
 62
 63
 48
 64
 13
 65
 66
 3
 67
 14
 68
 69
 15
 1
 70
 16
 50
 71
 17
 10
 72
 18
 73
 19
 20
 74
 21
 4
 22
 75
 76
 23
 77
 107
 24
 25
 108
 109
 78
 26
 49
 27
 28
 79
 29
 80
 81
 82
 9
 110
 30
 111
 83
 84
 85
 86
 31
 87
 88
 32
 8
 33
 89
 5
 90
 91
 34
 35
 92
 93
 36
 94
 37
 2
 38
 39
 95
 40
 41
 96
 97
 98
 99
 42
 112
 43
 100
 113
 101
 44
 102
 45
 46
 103
 47
 104
 
 */
#pragma mark -
#pragma mark Custom Methods

- (id)init
{
	@synchronized(self)
    {
		if ((self = [super init]))
		{
            [self insertValues];
 		}
		return self;
	}
}
-(void)insertValues
{
    dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                  @"Afrikaans",@"58",
                  @"Akan-Twi",@"59",
                  @"Albanian",@"11",
                  @"Amharic",@"60",
                  @"Arabic",@"6",
                  @"Arabic (Egyptian)",@"105",
                  @"Arabic (Levantine)",@"106",
                  @"Armenian",@"12",
                  @"Azerbaijani",@"61",
                  @"Bengali",@"7",
                  @"Bosnian",@"62",
                  @"Bulgarian",@"63",
                  @"Burmese",@"48",
                  @"Cambodian",@"64",
                  @"Cantonese Chinese",@"13",
                  @"Cebuano",@"65",
                  @"Chavacano",@"66",
                  @"Chinese",@"3",
                  @"Croatian",@"67",
                  @"Czech",@"14",
                  @"Danish",@"68",
                  @"Dari",@"69",
                  @"Dutch",@"15",
                  @"English",@"1",
                  @"Estonian",@"70",
                  @"Farsi",@"16",
                  @"Filipino",@"50",
                  @"Finnish",@"71",
                  @"Foughenese",@"17",
                  @"French",@"10",
                  @"Georgian",@"72",
                  @"German",@"18",
                  @"Greek",@"73",
                  @"Gujarati",@"19",
                  @"Haitian Creole",@"20",
                  @"Hausa",@"74",
                  @"Hebrew",@"21",
                  @"Hindi",@"4",
                  @"Hmong",@"22",
                  @"Hungarian",@"75",
                  @"Igbo",@"76",
                  @"Ilocano",@"23",
                  @"Indonesian",@"77",
                  @"Iraqi",@"107",
                  @"Italian",@"24",
                  @"Japanese",@"25",
                  @"Javanese",@"108",
                  @"Kashmir",@"109",
                  @"Kazakh",@"78",
                  @"Khmer",@"26",
                  @"Kirundi",@"49",
                  @"Korean",@"27",
                  @"Kurdish",@"28",
                  @"Kyrgyz",@"79",
                  @"Laotian",@"29",
                  @"Latvian",@"80",
                  @"Lithuanian",@"81",
                  @"Macedonian",@"82",
                  @"Malay-Indonesian",@"9",
                  @"Malayalam",@"110",
                  @"Mandarin Chinese",@"30",
                  @"Marshallese",@"111",
                  @"Mongolian",@"83",
                  @"Navajo",@"84",
                  @"Nepali",@"85",
                  @"Norwegian",@"86",
                  @"Oromo",@"31",
                  @"Pashto",@"87",
                  @"Persian",@"88",
                  @"Polish",@"32",
                  @"Portuguese",@"8",
                  @"Punjabi",@"33",
                  @"Romanian",@"89",
                  @"Russian",@"5",
                  @"Samoan",@"90",
                  @"Serbian",@"91",
                  @"Serbo-Croatian",@"34",
                  @"Signers for the deaf",@"35",
                  @"Sindhi",@"92",
                  @"Sinhalese",@"93",
                  @"Slovak",@"36",
                  @"Slovenian",@"94",
                  @"Somali",@"37",
                  @"Spanish",@"2",
                  @"Sudanese",@"38",
                  @"Swahili",@"39",
                  @"Swedish",@"95",
                  @"Tagalog",@"40",
                  @"Taiwanese",@"41",
                  @"Tajik",@"96",
                  @"Tamil",@"97",
                  @"Tausug",@"98",
                  @"Telugu",@"99",
                  @"Thai",@"42",
                  @"Tigrinya",@"112",
                  @"Turkish",@"43",
                  @"Turkmen",@"100",
                  @"Uighur",@"113",
                  @"Ukrainian",@"101",
                  @"Urdu",@"44",
                  @"Uzbek",@"102",
                  @"Vietnamese",@"45",
                  @"Wolof",@"46",
                  @"Wu",@"103",
                  @"Yiddish",@"47",
                  @"Yoruba",@"104",
                  nil];
    
    
    
    //    if (areasOfLaws)
    //    {
    //        [areasOfLaws removeAllObjects];
    //        areasOfLaws = nil;
    //    }
    allValues = [NSMutableArray arrayWithArray:[[dictionary allValues] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]];

}
-(NSArray *)allObjects
{
    return allValues;
}
-(NSString *)getIdForValue:(NSString *)value
{
    //NSLog(@"valueskeys %@",[dictionary allKeysForObject:value]);
    NSString *string = [[dictionary allKeysForObject:value] objectAtIndex:0];
    return string;
}


@end
