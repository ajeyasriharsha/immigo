//
//  PushNotificationsModel.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/8/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PushNotificationsModel : NSObject
+ (PushNotificationsModel*)sharedDataSource;
+ (void) releaseInstance;

-(void)updatePushNotificationStatus:(BOOL)status;

@end
