//
//  CheckListModel.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/28/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "CheckListModel.h"
#import "CheckListItemsDAO.h"

@implementation CheckListModel
#pragma mark "Implemention of Single Ton"

static CheckListModel *_sharedInstance;
static dispatch_once_t oncePredicate;

+ (CheckListModel*)sharedDataSource
{
	if(!_sharedInstance)
    {
		dispatch_once(&oncePredicate, ^{
			_sharedInstance = [[super allocWithZone:nil] init];
        });
    }
    return _sharedInstance;
}

+(void) releaseInstance
{
    _sharedInstance=nil;
    oncePredicate=0;
}

+ (id)allocWithZone:(NSZone *)zone
{
	return [self sharedDataSource];
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

#if (!__has_feature(objc_arc))

- (id)retain
{
	return self;
}

- (unsigned)retainCount
{
	return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
	//do nothing
}

- (id)autorelease
{
	return self;
}
#endif

#pragma mark -
#pragma mark Custom Methods

- (id)init
{
	@synchronized(self)
    {
		if ((self = [super init]))
		{
 		}
		return self;
	}
}
-(NSArray *)fetchCheckListItemsForSubtopic:(SubTopic *)subTopic
{
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"key"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [[subTopic.checkListItems allObjects] sortedArrayUsingDescriptors:sortDescriptors];

    return sortedArray;
}

-(void)selectTheCheckListItemWithKey:(int)key AndSubTopicID:(int)subtopicId IfSelected:(BOOL)selected
{
    [CheckListItemsDAO selectTheCheckListItemWithKey:key AndSubTopicID:subtopicId IfSelected:selected];
}

@end
