//
//  EventsModel.m
//  Immigo
//
//  Created by pradeep ISPV on 3/25/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "EventsModel.h"
#import "Event.h"
#import "EventDAO.h"

@implementation EventsModel
@synthesize selectedEvent =_selectedEvent;
#pragma mark "Implemention of Single Ton"

static EventsModel *_sharedInstance;
static dispatch_once_t oncePredicate;

+ (EventsModel*)sharedDataSource
{
	if(!_sharedInstance)
    {
		dispatch_once(&oncePredicate, ^{
			_sharedInstance = [[super allocWithZone:nil] init];
        });
    }
    return _sharedInstance;
}

+(void) releaseInstance
{
    _sharedInstance=nil;
    oncePredicate=0;
}

+ (id)allocWithZone:(NSZone *)zone
{
	return [self sharedDataSource];
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

#if (!__has_feature(objc_arc))

- (id)retain
{
	return self;
}

- (unsigned)retainCount
{
	return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
	//do nothing
}

- (id)autorelease
{
	return self;
}
#endif

#pragma mark -
#pragma mark Custom Methods

- (id)init
{
	@synchronized(self)
    {
		if ((self = [super init]))
		{
 		}
		return self;
	}
}
-(void)fetchTrainings
{
//    if (allEvents)
//    {
//        [allEvents removeAllObjects];
//        allEvents = nil;
//    }
//    allEvents = [NSMutableArray array];

    [[DataDownloader sharedDownloader] fetchTrainingsAndEventsData];
}
-(void)insertEvent:(Event *)event
{
    [allEvents addObject:event];
}
-(NSArray *)allTrainings
{
    NSSortDescriptor *col1SD = [[NSSortDescriptor alloc] initWithKey:@"startDate" ascending:YES];
    NSSortDescriptor *col2SD = [[NSSortDescriptor alloc] initWithKey:@"dailyStartTime" ascending:YES];
    
    NSArray *sortDescriptors = [NSArray arrayWithObjects:col1SD, col2SD, nil];
    
//    [someMutableArray sortUsingDescriptors:sortDescriptors];
//    NSSortDescriptor *sortDescriptor;
//    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"startDate && dailyStartTime"
//                                                 ascending:YES];
//    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [[EventDAO allObjects] sortedArrayUsingDescriptors:sortDescriptors];
    
    return sortedArray;
}
-(void)selectEventAtindex:(NSInteger)index
{
    _selectedEvent = [allEvents objectAtIndex:index];
}
@end
