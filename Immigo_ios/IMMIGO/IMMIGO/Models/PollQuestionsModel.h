//
//  PollQuestionsModel.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/28/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PollQuestionsDAO.h"
#import "PollQuestions.h"
#import "DataDownloader.h"

@interface PollQuestionsModel : NSObject
{
    NSMutableArray *allObjects;
    
    PollQuestions *_selectedQuestion;
}
@property (nonatomic,strong) PollQuestions *selectedQuestion;

+ (PollQuestionsModel*)sharedDataSource;
+ (void) releaseInstance;
//-(void)selectFinderAtindex:(NSInteger)index;

-(void)fetchPollQuestionsForDevice:(NSString *)deviceId;
-(NSArray *)allObjects;
-(void)selectQuestionWithID:(int)key;

- (void) submitPollQuestionAnswerWithQuestionId:(int )quesionId AnswerId:(int)answerId AndDeviceId:(NSString *)deviceId;
@end
