//
//  PrivacyModel.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/2/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "PrivacyModel.h"
#import "PrivacyDAO.h"
@implementation PrivacyModel
@synthesize privacyString=_privacyString;
#pragma mark "Implemention of Single Ton"

static PrivacyModel *_sharedInstance;
static dispatch_once_t oncePredicate;

+ (PrivacyModel*)sharedDataSource
{
	if(!_sharedInstance)
    {
		dispatch_once(&oncePredicate, ^{
			_sharedInstance = [[super allocWithZone:nil] init];
        });
    }
    return _sharedInstance;
}

+(void) releaseInstance
{
    _sharedInstance=nil;
    oncePredicate=0;
}

+ (id)allocWithZone:(NSZone *)zone
{
	return [self sharedDataSource];
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

#if (!__has_feature(objc_arc))

- (id)retain
{
	return self;
}

- (unsigned)retainCount
{
	return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
	//do nothing
}

- (id)autorelease
{
	return self;
}
#endif

#pragma mark -
#pragma mark Custom Methods

- (id)init
{
	@synchronized(self)
    {
		if ((self = [super init]))
		{
            
 		}
		return self;
	}
}
-(void)getPrivacyPolicyContent
{
    [[DataDownloader sharedDownloader] fetchPrivacyPolicy];
}
-(void)privacyString:(NSString *)htmlString
{
    _privacyString=htmlString;
}
-(NSString *)privacyContent
{
    Privacy *privacy = [[PrivacyDAO allObjects] objectAtIndex:0];
    NSString *htmlString = [[PrivacyDAO objectWithID:privacy.key] content];
    return htmlString;
}
@end
