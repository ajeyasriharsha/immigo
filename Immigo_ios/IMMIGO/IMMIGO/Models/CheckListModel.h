//
//  CheckListModel.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/28/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SubTopic.h"
#import "CheckListItems.h"

@interface CheckListModel : NSObject
{
    NSMutableArray *allObjects;
}

+ (CheckListModel*)sharedDataSource;
+ (void) releaseInstance;
//-(void)selectFinderAtindex:(NSInteger)index;

-(NSArray *)fetchCheckListItemsForSubtopic:(SubTopic *)subTopic;
-(void)selectTheCheckListItemWithKey:(int)key AndSubTopicID:(int)subtopicId IfSelected:(BOOL)selected;
@end
