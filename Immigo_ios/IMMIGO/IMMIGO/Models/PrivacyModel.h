//
//  PrivacyModel.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/2/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataDownloader.h"

@interface PrivacyModel : NSObject <DataDownloaderDelegate>
{
    NSString *_privacyString;
}
@property(nonatomic,strong) NSString *privacyString;

+ (PrivacyModel*)sharedDataSource;
+ (void) releaseInstance;

-(void)getPrivacyPolicyContent;
-(void)privacyString:(NSString *)htmlString;


@end
