//
//  AlertsModel.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/2/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataDownloader.h"
#import "Alerts.h"

@interface AlertsModel : NSObject <DataDownloaderDelegate>
{
    NSMutableArray *allAlerts;
    
}

//-(void)insertAlerts:(Alert *)alerts;
-(NSArray *)allAlerts;

+ (AlertsModel*)sharedDataSource;
+ (void) releaseInstance;

-(void)fetchAlerts;


@end
