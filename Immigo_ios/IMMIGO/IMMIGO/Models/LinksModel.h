//
//  LinksModel.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/28/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Links.h"
#import "SubTopic.h"
@interface LinksModel : NSObject
{
    NSMutableArray *allObjects;
}

+ (LinksModel*)sharedDataSource;
+ (void) releaseInstance;
//-(void)selectFinderAtindex:(NSInteger)index;

-(NSArray *)fetchLinksForSubtopic:(SubTopic *)subTopic;


@end
