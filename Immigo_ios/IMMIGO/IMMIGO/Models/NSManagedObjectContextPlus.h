//
//  NSManagedObjectContextPlus.h
//  Immigo
//
//  Created by Shineeth Hamza on 8/30/13.
//
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContextPlus : NSManagedObjectContext
@property (nonatomic) BOOL saveLock;
@end
