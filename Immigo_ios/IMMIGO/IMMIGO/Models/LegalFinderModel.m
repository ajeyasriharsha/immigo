//
//  LegalFinderModel.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/9/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "LegalFinderModel.h"

@implementation LegalFinderModel
@synthesize selectedFinder =_selectedFinder;
#pragma mark "Implemention of Single Ton"

static LegalFinderModel *_sharedInstance;
static dispatch_once_t oncePredicate;

+ (LegalFinderModel*)sharedDataSource
{
	if(!_sharedInstance)
    {
		dispatch_once(&oncePredicate, ^{
			_sharedInstance = [[super allocWithZone:nil] init];
        });
    }
    return _sharedInstance;
}

+(void) releaseInstance
{
    _sharedInstance=nil;
    oncePredicate=0;
}

+ (id)allocWithZone:(NSZone *)zone
{
	return [self sharedDataSource];
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

#if (!__has_feature(objc_arc))

- (id)retain
{
	return self;
}

- (unsigned)retainCount
{
	return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
	//do nothing
}

- (id)autorelease
{
	return self;
}
#endif

#pragma mark -
#pragma mark Custom Methods

- (id)init
{
	@synchronized(self)
    {
		if ((self = [super init]))
		{
 		}
		return self;
	}
}
-(void)fetchLegalFindersWithZipCode:(NSString *)zipCode OrWithCurrentLocationLattitude:(NSString *)lattitude AndLongitude:(NSString *)longitude
{
    if (allObjects)
    {
        [allObjects removeAllObjects];
        allObjects = nil;
    }
    allObjects = [NSMutableArray array];
    
    [[DataDownloader sharedDownloader] fetchLegalFindersWithZipCode:zipCode OrWithCurrentLocationLattitude:lattitude AndLongitude:longitude];
}



-(void)fetchLegalFindersWithZipCode:(NSString *)zipCode OrWithCurrentLocationLattitude:(NSString *)lattitude AndLongitude:(NSString *)longitude withLanguages:(NSString*)languages withAreasOfLaws:(NSString *)aols withLegalAssistance:(NSString *)legalassistances
{
    if (allObjects)
    {
        [allObjects removeAllObjects];
        allObjects = nil;
    }
    allObjects = [NSMutableArray array];
        
    [[DataDownloader sharedDownloader] fetchLegalFindersWithZipCode:zipCode OrWithCurrentLocationLattitude:lattitude AndLongitude:longitude withLanguages:languages withAreasOfLaws:aols withLegalAssistance:legalassistances];
    
}


-(void)insertOrg:(LegalFinder *)finder
{
//    //NSLog(@"finder name %@",finder.finderName);
    [allObjects addObject:finder];
}
-(NSArray *)allItems
{
    return allObjects;
}
-(void)selectFinderAtindex:(NSInteger)index
{
    _selectedFinder = [allObjects objectAtIndex:index];
}

@end
