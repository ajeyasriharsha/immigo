//
//  FeedbackModel.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/3/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "FeedbackModel.h"
#import "DataDownloader.h"

@implementation FeedbackModel


#pragma mark "Implemention of Single Ton"

static FeedbackModel *_sharedInstance;
static dispatch_once_t oncePredicate;

+ (FeedbackModel*)sharedDataSource
{
	if(!_sharedInstance)
    {
		dispatch_once(&oncePredicate, ^{
			_sharedInstance = [[super allocWithZone:nil] init];
        });
    }
    return _sharedInstance;
}

+(void) releaseInstance
{
    _sharedInstance=nil;
    oncePredicate=0;
}

+ (id)allocWithZone:(NSZone *)zone
{
	return [self sharedDataSource];
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

#if (!__has_feature(objc_arc))

- (id)retain
{
	return self;
}

- (unsigned)retainCount
{
	return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
	//do nothing
}

- (id)autorelease
{
	return self;
}
#endif

#pragma mark -
#pragma mark Custom Methods

- (id)init
{
	@synchronized(self)
    {
		if ((self = [super init]))
		{
 		}
		return self;
	}
}

- (void) submitFeedBackWithDeviceId:(NSString *)deviceId AndUserName:(NSString *)userName AndEmailId:(NSString *)emailId AndPhoneNumber:(NSString *)phoneNumber AndFeedback:(NSString *)feedbackMessage AndStatus:(BOOL)isAllowed
{
    [[DataDownloader sharedDownloader] submitFeedBackWithDeviceId:deviceId AndUserName:userName AndEmailId:emailId AndPhoneNumber:phoneNumber AndFeedback:feedbackMessage AndStatus:isAllowed];
}

@end
