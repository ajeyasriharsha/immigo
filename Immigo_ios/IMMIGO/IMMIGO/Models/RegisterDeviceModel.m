//
//  RegisterDeviceModel.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/30/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "RegisterDeviceModel.h"
#import "DataDownloader.h"

@implementation RegisterDeviceModel
#pragma mark "Implemention of Single Ton"

static RegisterDeviceModel *_sharedInstance;
static dispatch_once_t oncePredicate;

+ (RegisterDeviceModel*)sharedDataSource
{
	if(!_sharedInstance)
    {
		dispatch_once(&oncePredicate, ^{
			_sharedInstance = [[super allocWithZone:nil] init];
        });
    }
    return _sharedInstance;
}

+(void) releaseInstance
{
    _sharedInstance=nil;
    oncePredicate=0;
}

+ (id)allocWithZone:(NSZone *)zone
{
	return [self sharedDataSource];
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

#if (!__has_feature(objc_arc))

- (id)retain
{
	return self;
}

- (unsigned)retainCount
{
	return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
	//do nothing
}

- (id)autorelease
{
	return self;
}
#endif

#pragma mark -
#pragma mark Custom Methods

- (id)init
{
	@synchronized(self)
    {
		if ((self = [super init]))
		{
 		}
		return self;
	}
}

- (void) registerDeviceWithId:(NSString *)deviceId AndType:(NSString *)deviceType AndPushNotificationStatus:(BOOL)isAllowed AtAppLaunch:(BOOL)isFirstTime
{
    [[DataDownloader sharedDownloader] registerDeviceWithId:deviceId AndType:deviceType AndPushNotificationStatus:isAllowed AtAppLaunch:isFirstTime];
}
@end
