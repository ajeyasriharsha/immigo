//
//  NSManagedObjectContextPlus.m
//  Immigo
//
//  Created by Shineeth Hamza on 8/30/13.
//
//

#import "NSManagedObjectContextPlus.h"

@implementation NSManagedObjectContextPlus
@synthesize saveLock;
-(id)init
{
    self = [super init];
    if (self) {
        saveLock = NO;
    }
    return self;
}
@end
