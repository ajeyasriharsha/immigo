//
//  AOLModel.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/14/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AOLModel : NSObject
{
    NSMutableArray *areasOfLaws;
    NSDictionary *dictionary;
}

//-(void)insertAreasOfLaw:(AreasOfLaw *)aol;
-(NSArray *)allAreas;

+ (AOLModel*)sharedDataSource;
+ (void) releaseInstance;

-(NSString *)getLegalIdForValue:(NSString *)value;
@end
