//
//  NewsModel.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/1/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "NewsModel.h"
#import "NewsDAO.h"
#import "CoreDataBase.h"

@implementation NewsModel
@synthesize selectedNews =_selectedNews;
#pragma mark "Implemention of Single Ton"

static NewsModel *_sharedInstance;
static dispatch_once_t oncePredicate;

+ (NewsModel*)sharedDataSource
{
	if(!_sharedInstance)
    {
		dispatch_once(&oncePredicate, ^{
			_sharedInstance = [[super allocWithZone:nil] init];
        });
    }
    return _sharedInstance;
}

+(void) releaseInstance
{
    _sharedInstance=nil;
    oncePredicate=0;
}

+ (id)allocWithZone:(NSZone *)zone
{
	return [self sharedDataSource];
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

#if (!__has_feature(objc_arc))

- (id)retain
{
	return self;
}

- (unsigned)retainCount
{
	return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
	//do nothing
}

- (id)autorelease
{
	return self;
}
#endif

#pragma mark -
#pragma mark Custom Methods

- (id)init
{
	@synchronized(self)
    {
		if ((self = [super init]))
		{
 		}
		return self;
	}
}
-(void)fetchNews
{
//    if (allNews)
//    {
//        [allNews removeAllObjects];
//        allNews = nil;
//    }
//    allNews = [NSMutableArray array];

    [[DataDownloader sharedDownloader] fetchNewsDetails];
}
-(void)insertNews:(News *)news
{
    [allNews addObject:news];
}
-(NSArray *)allNewsItems
{
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date"
                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [[NewsDAO allObjects] sortedArrayUsingDescriptors:sortDescriptors];

    return sortedArray;
}
-(void)selectNewsAtindex:(NSInteger)index
{
    _selectedNews = [allNews objectAtIndex:index];
}

@end
