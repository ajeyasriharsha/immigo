//Om Sri Sai Ram
//  CoreDataBase.h
//  Immigo
//
//  Created by Shineeth Hamza on 7/3/13.
//
//

#import <Foundation/Foundation.h>
#import "NSManagedObjectContextPlus.h"

//App Level Tables
extern NSString *const TBL_TOPICS;
extern NSString *const TBL_SUBTOPICS;
extern NSString *const TBL_LINKS;
extern NSString *const TBL_CHECKLISTITEMS;

extern NSString *const TBL_NEWS;
extern NSString *const TBL_AUTHOR;

extern NSString *const TBL_EVENTS;

extern NSString *const TBL_DISCLAIMER;
extern NSString *const TBL_ABOUT;
extern NSString *const TBL_PRIVACY;
extern NSString *const TBL_ALERTS;
extern NSString *const TBL_POLLQUESTIONS;
extern NSString *const TBL_PUSHNOTIFICATIONS;

//extern NSString *const TBL_ADDRESS_STATE;
//extern NSString *const TBL_NOTIFICATIONS;


@interface CoreDataBase : NSObject
+ (void)saveContext;
+ (void)enableSave;
+ (void)disableSave;
+ (NSManagedObjectContextPlus *)managedObjectContext;
@end
