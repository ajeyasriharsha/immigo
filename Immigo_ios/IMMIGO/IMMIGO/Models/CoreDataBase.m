//Om Sri Sai Ram
//  CoreDataBase.m
//  Immigo
//
//  Created by Shineeth Hamza on 7/3/13.
//
//

#import "CoreDataBase.h"

//App Level Tables
NSString *const TBL_TOPICS = @"Topic";
NSString *const TBL_SUBTOPICS = @"SubTopic";
NSString *const TBL_LINKS = @"Links";
NSString *const TBL_CHECKLISTITEMS = @"CheckListItems";

NSString *const TBL_NEWS = @"News";
NSString *const TBL_AUTHOR = @"Author";

NSString *const TBL_EVENTS = @"Event";

NSString *const TBL_DISCLAIMER = @"Disclaimer";
NSString *const TBL_ABOUT = @"About";
NSString *const TBL_PRIVACY = @"Privacy";
NSString *const TBL_ALERTS = @"Alerts";
NSString *const TBL_POLLQUESTIONS = @"PollQuestions";
NSString *const TBL_PUSHNOTIFICATIONS = @"PushNotifications";
//NSString *const TBL_ADDRESS_STATE = @"AddressState";
//NSString *const TBL_NOTIFICATIONS = @"Notifications";

@implementation CoreDataBase

NSManagedObjectContextPlus *managedObjectContext;
NSManagedObjectModel *managedObjectModel;
NSPersistentStoreCoordinator *persistentStoreCoordinator;

#pragma mark - Core Data stack
// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
+ (NSManagedObjectModel *)managedObjectModel
{
    if (managedObjectModel != nil)
    {
        return managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"IMMIGOModel" withExtension:@"momd"];
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
+ (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (persistentStoreCoordinator != nil)
    {
        return persistentStoreCoordinator;
    }
    
    NSURL *appDocumentDirectory= [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                                         inDomains:NSUserDomainMask] lastObject];
    
    NSURL *storeURL = [appDocumentDirectory URLByAppendingPathComponent:@"IMMIGOModel.sqlite"];
    
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    //todo: need to be fix the below error, problem in XCode 4.2
    NSDictionary *options=[NSDictionary dictionaryWithObjectsAndKeys:
                           [NSNumber numberWithBool:YES],NSMigratePersistentStoresAutomaticallyOption,
                           [NSNumber numberWithBool:YES],NSInferMappingModelAutomaticallyOption,
                           nil];
    //    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption:@YES,
    //                              NSInferMappingModelAutomaticallyOption:@YES};
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                  configuration:nil
                                                            URL:storeURL
                                                        options:options
                                                          error:&error]) {
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
#ifdef DEBUG
        //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
#endif
    }
    
    return persistentStoreCoordinator;
}

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
+ (NSManagedObjectContext *)managedObjectContext
{
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContextPlus alloc] init];
        [managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return managedObjectContext;
}


+(void)initialize
{
    //Todo: Change the below methods only of initalizeing the clss variables
    //[self persistentStoreCoordinator]; is called in -managedObjectContext method
    //[self managedObjectModel]; is called in -persistentStoreCoordinator method
    [self managedObjectContext];
}

+ (void)saveContext
{
    NSError *error = nil;
    if (![managedObjectContext saveLock] && managedObjectContext != nil)
    {
        if ( [managedObjectContext hasChanges] && ![managedObjectContext save:&error])
        {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
#ifdef DEBUG
            //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
#endif
        }
    }
}

+ (void)enableSave
{
    [managedObjectContext setSaveLock:NO];
}

+ (void)disableSave
{
    [managedObjectContext setSaveLock:YES];
}

@end
