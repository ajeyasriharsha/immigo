//
//  NewsModel.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/1/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataDownloader.h"
#import "News.h"


@interface NewsModel : NSObject <DataDownloaderDelegate>

{
    NSMutableArray *allNews;
    
    News *_selectedNews;
}
@property (nonatomic,strong) News *selectedNews;

-(void)insertNews:(News *)news;
-(NSArray *)allNewsItems;

+ (NewsModel*)sharedDataSource;
+ (void) releaseInstance;
-(void)selectNewsAtindex:(NSInteger)index;

-(void)fetchNews;


@end
