//
//  AOLModel.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/14/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "AOLModel.h"

@implementation AOLModel
#pragma mark "Implemention of Single Ton"

static AOLModel *_sharedInstance;
static dispatch_once_t oncePredicate;

+ (AOLModel*)sharedDataSource
{
	if(!_sharedInstance)
    {
		dispatch_once(&oncePredicate, ^{
			_sharedInstance = [[super allocWithZone:nil] init];
        });
    }
    return _sharedInstance;
}

+(void) releaseInstance
{
    _sharedInstance=nil;
    oncePredicate=0;
}

+ (id)allocWithZone:(NSZone *)zone
{
	return [self sharedDataSource];
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

#if (!__has_feature(objc_arc))

- (id)retain
{
	return self;
}

- (unsigned)retainCount
{
	return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
	//do nothing
}

- (id)autorelease
{
	return self;
}
#endif
/*
 
 1	Adjustment of Status
 2	Asylum applications
 3	Consular Processing
 4	Employment authorization
 5	Employment-based immigrant and non-immigrant petitions
 6
 7
 8	NACARA
 9
 10
 11
 12
 13
 14
 15
 16
 
 */
#pragma mark -
#pragma mark Custom Methods

- (id)init
{
	@synchronized(self)
    {
		if ((self = [super init]))
		{
            [self fetchAreasOfLaws];
 		}
		return self;
	}
}
-(void)fetchAreasOfLaws
{
    dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"Adjustment of Status",@"1",
                                @"Asylum applications",@"2",
                                @"Consular Processing",@"3",
                                @"Employment authorization",@"4",
                                @"Employment-based immigrant and non-immigrant petitions",@"5",
                                @"Family-based petitions",@"6",
                                @"Habeas Corpus",@"7",
                                @"NACARA",@"8",
                                @"Naturalization/Citizenship",@"9",
                                @"Removal hearings",@"10",
                                @"Special Immigrant Juvenile Status",@"11",
                                @"T visas",@"12",
                                @"Temporary Protected Status (TPS)",@"13",
                                @"U visas",@"14",
                                @"Violence Against Women Act (VAWA) petitions",@"15",
                                @"Deferred Action for Childhood Arrivals (DACA)",@"16",
                                @"Deferred Action for Parents of Americans and Lawful Permanent Residents (DAPA)",@"17",
                                nil];
    
    
        areasOfLaws = [NSMutableArray arrayWithArray:[dictionary allValues]];
}
-(NSArray *)allAreas
{
    return [areasOfLaws  sortedArrayUsingSelector:@selector(compare:)];
}
-(NSString *)getLegalIdForValue:(NSString *)value
{
    //NSLog(@"valueskeys %@",[dictionary allKeysForObject:value]);
    NSString *string = [[dictionary allKeysForObject:value] objectAtIndex:0];
    return string;
}
@end
