//
//  LanguageModel.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/14/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LanguageModel : NSObject
{
    NSMutableArray *allValues;
    NSDictionary *dictionary;
}

//-(void)insertAreasOfLaw:(AreasOfLaw *)aol;
-(NSArray *)allObjects;

+ (LanguageModel*)sharedDataSource;
+ (void) releaseInstance;

-(NSString *)getIdForValue:(NSString *)value;

@end
