//
//  FeedbackModel.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/3/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FeedbackModel : NSObject
+ (FeedbackModel*)sharedDataSource;
+ (void) releaseInstance;

- (void) submitFeedBackWithDeviceId:(NSString *)deviceId AndUserName:(NSString *)userName AndEmailId:(NSString *)emailId AndPhoneNumber:(NSString *)phoneNumber AndFeedback:(NSString *)feedbackMessage AndStatus:(BOOL)isAllowed;

@end
