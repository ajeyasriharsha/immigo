//
//  DataDownloader.m
//  Immigo
//
//  Created by pradeep ISPV on 3/25/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "DataDownloader.h"
#import "APPConfig.h"
#import "XMLReader.h"
#import "EventDAO.h"
#import "NewsDAO.h"
#import "AlertView.h"
#import "AboutDAO.h"
#import "AlertsDAO.h"
#import "LegalFinderDAO.h"
#import "CoreDataBase.h"

#import "DisclaimerDAO.h"
#import "AboutImmigoDAO.h"
#import "PrivacyDAO.h"
#import "ImmigrationBasicsDAO.h"
#import "PollQuestionsDAO.h"

#import "SubTopicsDAO.h"
#import "CheckListItemsDAO.h"
#import "LinksDAO.h"
#import "PushNotificationsDAO.h"

#import "Common.h"

@implementation DataDownloader

@synthesize delegate, alertsDelegate;
@synthesize currentEvent;
#pragma mark "Implemention of Single Ton"

static DataDownloader *_sharedInstance;
static dispatch_once_t oncePredicate;

+ (DataDownloader  *)sharedDownloader
{
	if(!_sharedInstance)
    {
		dispatch_once(&oncePredicate, ^
                      {
                          _sharedInstance = [[super allocWithZone:nil] init];
                      });
    }
    return _sharedInstance;
}

+(void) releaseInstance
{
    [_sharedInstance cancelPipeLineRequests];
    [NSObject cancelPreviousPerformRequestsWithTarget:_sharedInstance selector:@selector(fetchTrainingsAndEventsData) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:_sharedInstance selector:@selector(fetchNewsDetails) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:_sharedInstance selector:@selector(fetchLegalFindersWithZipCode:OrWithCurrentLocationLattitude:AndLongitude:) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:_sharedInstance selector:@selector(fetchAboutUsPage) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:_sharedInstance selector:@selector(fetchPrivacyPolicy) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:_sharedInstance selector:@selector(fetchTermsOfUse) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:_sharedInstance selector:@selector(fetchAlerts) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:_sharedInstance selector:@selector(fetchImmigrationBasics) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:_sharedInstance selector:@selector(fetchCivicEngagement) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:_sharedInstance selector:@selector(fetchCivilRights) object:nil];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:_sharedInstance selector:@selector(fetchPollQuestionsForDevice:) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:_sharedInstance selector:@selector(registerDeviceWithId:AndType:AndPushNotificationStatus:AtAppLaunch:) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:_sharedInstance selector:@selector(submitPollQuestionAnswerWithQuestionId:AnswerId:AndDeviceId:) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:_sharedInstance selector:@selector(submitFeedBackWithDeviceId:AndUserName:AndEmailId:AndPhoneNumber:AndFeedback:AndStatus:) object:nil];

    _sharedInstance = nil;
    oncePredicate = 0;
}

+ (id)allocWithZone:(NSZone *)zone
{
	return [self sharedDownloader];
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

#if (!__has_feature(objc_arc))

- (id)retain
{
	return self;
}

- (unsigned)retainCount
{
	return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
	//do nothing
}

- (id)autorelease
{
	return self;
}
#endif

#pragma mark -
#pragma mark Custom Methods

- (id)init
{
	@synchronized(self)
    {
		if ((self = [super init]))
		{
 		}
		return self;
	}
}

#pragma mark - Cancel All Requests
- (void)cancelAllRequests
{
    /*
     ASIHTTPRequest *getTrainingsRequest;
     ASIHTTPRequest *getNewsRequest;
     ASIHTTPRequest *getLegalFindersRequest;
     
     //CMS SERVICES
     ASIHTTPRequest *getAboutUsRequest;
     ASIHTTPRequest *getPrivacyRequest;
     ASIHTTPRequest *getTermsRequest;
     ASIHTTPRequest *getAlertsRequest;
     ASIHTTPRequest *getImmigrationBaiscsRequest;
     */
    [getTrainingsRequest cancel];
    [getNewsRequest cancel];
    [getLegalFindersRequest cancel];
    [getAboutUsRequest cancel];
    [getPrivacyRequest cancel];
    [getTermsRequest cancel];
    [getAlertsRequest cancel];
    [getImmigrationBaiscsRequest cancel];
    [getCivicEngagementRequest cancel];
    [getCivilRightstRequest cancel];
    [getPollQuestionsRequest cancel];
}

#pragma mark - Fetch Immigration basics
- (bool) fetchImmigrationBasics
{
    APIRequest *request = [APIRequest apiRequestWithKey:API_IMMIGRATIONBASICS_GET_ALL];
    
    [getImmigrationBaiscsRequest cancel];
    
    request.asiDelegate = self;
    getImmigrationBaiscsRequest=[HttpBridge invokeAPIWithRequest:request];
    
    if(nil==getImmigrationBaiscsRequest)
        return false;
    return true;
}

#pragma mark -Fetch CivicEngagement

- (bool) fetchCivicEngagement
{
    APIRequest *request = [APIRequest apiRequestWithKey:API_GET_CIVIC_ENGAGEMENT];
    
    [getCivicEngagementRequest cancel];
    
    request.asiDelegate = self;
    getCivicEngagementRequest=[HttpBridge invokeAPIWithRequest:request];
    
    if(nil==getCivicEngagementRequest)
        return false;
    return true;
}

#pragma mark -Fetch Civil Rights

- (bool) fetchCivilRights
{
    APIRequest *request = [APIRequest apiRequestWithKey:API_GET_CIVIL_RIGHTS];
    
    [getCivilRightstRequest cancel];
    
    request.asiDelegate = self;
    getCivilRightstRequest=[HttpBridge invokeAPIWithRequest:request];
    
    if(nil==getCivilRightstRequest)
        return false;
    return true;
}


#pragma mark - Fetch ALERTS
- (bool) fetchAlerts
{
    APIRequest *request = [APIRequest apiRequestWithKey:API_ALERTS_GET];
    request.asiDelegate = self;
    getAlertsRequest=[HttpBridge invokeAPIWithRequest:request];
    
    if(nil==getAlertsRequest)
        return false;
    return true;
}

#pragma mark - Fetch PrivacyPolicy
- (bool) fetchPrivacyPolicy
{
    APIRequest *request = [APIRequest apiRequestWithKey:API_PRIVACY_GET];
    request.asiDelegate = self;
    getPrivacyRequest=[HttpBridge invokeAPIWithRequest:request];
    
    if(nil==getPrivacyRequest)
        return false;
    return true;
}

#pragma mark - Fetch Terms Of Use
- (bool) fetchTermsOfUse
{
    APIRequest *request = [APIRequest apiRequestWithKey:API_TERMSOFUSE_GET];
    request.asiDelegate = self;
    getTermsRequest=[HttpBridge invokeAPIWithRequest:request];
    
    if(nil==getTermsRequest)
        return false;
    return true;
}

#pragma mark - Fetch About
- (bool) fetchAboutUsPage
{
    APIRequest *request = [APIRequest apiRequestWithKey:API_ABOUT_GET];
    request.asiDelegate = self;
    getAboutUsRequest=[HttpBridge invokeAPIWithRequest:request];
    
    if(nil==getAboutUsRequest)
        return false;
    return true;
}
#pragma mark - Register Device POST FOR push notifications
- (bool) registerDeviceWithId:(NSString *)deviceId AndType:(NSString *)deviceType AndPushNotificationStatus:(BOOL)isAllowed AtAppLaunch:(BOOL)isFirstTime
//- (bool) registerDeviceWithId:(NSString *)deviceId AndType:(NSString *)deviceType AndPushNotificationStatus:(BOOL)isAllowed
{
    notifAtAppLaunch = isFirstTime;
    isAllowedPushNotf = isAllowed;
    NSString *languageSelected=@"";
    languageSelected= [BundleLocalization sharedInstance].language;   // NSString *passingDeviceId =deviceId.length?deviceId:@"";
    APIRequest *request = [APIRequest apiRequestWithKey:API_DEVICE_POST];
    NSDictionary *dictParams= [[NSDictionary alloc] initWithObjectsAndKeys:
                               deviceId,@"DeviceId",
                               deviceType,@"DeviceType",
                               isAllowed ? @"Yes":@"No",@"AllowPushNotifications",
                               languageSelected,@"language",
//                               @"Yes",@"AllowPushNotifications",
                               nil];
    
    [request setRequestDataWithDictionary:dictParams];
    
    request.asiDelegate = self;
    postRegisterDeviceRequest=[HttpBridge invokeAPIWithRequest:request];
    if(nil==postRegisterDeviceRequest)
        return false;
    return true;

}
#pragma mark - PostFeedback
/*
 
 public virtual string DeviceId { get; set; }
 public virtual string UserName { get; set; }
 public virtual string EmailId { get; set; }
 public virtual string PhoneNumber { get; set; }
 public virtual string Feedback { get; set; }

 */
- (bool) submitFeedBackWithDeviceId:(NSString *)deviceId AndUserName:(NSString *)userName AndEmailId:(NSString *)emailId AndPhoneNumber:(NSString *)phoneNumber AndFeedback:(NSString *)feedbackMessage AndStatus:(BOOL)isAllowed
{
    APIRequest *request = [APIRequest apiRequestWithKey:API_FEEDBACK_POST];
    
    [postFeedBackRequest cancel];
    
    NSDictionary *dictParams= [[NSDictionary alloc] initWithObjectsAndKeys:
                               deviceId,@"DeviceId",
                               userName,@"UserName",
                               emailId,@"EmailId",
                               phoneNumber,@"PhoneNumber",
                               feedbackMessage,@"Feedback",
                               [NSNumber numberWithBool:isAllowed],@"ReceiveCommFromImmigoPartners",
                               nil];
    
    [request setRequestDataWithDictionary:dictParams];

    request.asiDelegate = self;
    postFeedBackRequest=[HttpBridge invokeAPIWithRequest:request];
    
    if(nil==postFeedBackRequest)
        return false;
    return true;
}

#pragma mark - Fetch Poll Questions
- (bool) fetchPollQuestionsForDevice:(NSString *)deviceId
{
    APIRequest *request = [APIRequest apiRequestWithKey:API_POLL_GET_ALL];
    
    [getPollQuestionsRequest cancel];
    
    NSDictionary *dictParams= [[NSDictionary alloc] initWithObjectsAndKeys:
                               deviceId,@"DeviceId",
                               nil];
    
    [request setRequestDataWithDictionary:dictParams];

    request.asiDelegate = self;
    getPollQuestionsRequest=[HttpBridge invokeAPIWithRequest:request];
    
    if(nil==getPollQuestionsRequest)
        return false;
    return true;
}

#pragma mark - Register Device POST

- (bool) submitPollQuestionAnswerWithQuestionId:(int )quesionId AnswerId:(int)answerId AndDeviceId:(NSString *)deviceId
{
    APIRequest *request = [APIRequest apiRequestWithKey:API_POLL_POST_ALL];
    NSDictionary *dictParams= [[NSDictionary alloc] initWithObjectsAndKeys:
                               deviceId,@"DeviceId",
                               [NSNumber numberWithInt:answerId],@"AnswerId",
                               [NSNumber numberWithInt:quesionId],@"QuestionId",
                               nil];
   // NSArray *Array = [NSArray arrayWithObject:dictParams];
    [request setRequestDataWithDictionary:dictParams];
   // [request setRequestDataWithArray:dictParams];
    
    request.asiDelegate = self;
    postPollQuestionsRequest=[HttpBridge invokeAPIWithRequest:request];
    if(nil==postPollQuestionsRequest)
        return false;
    return true;

}
#pragma mark - Fetch Finders
- (bool) fetchLegalFindersWithZipCode:(NSString *)zipCode OrWithCurrentLocationLattitude:(NSString *)lattitude AndLongitude:(NSString *)longitude
{
    APIRequest *request = [APIRequest apiRequestWithKey:API_LEGALORGANIZATIONS_GET_ALL];
    NSDictionary *dictParams= nil;
    if ([zipCode length] >0)
    {
        dictParams=[[NSDictionary alloc] initWithObjectsAndKeys:
                    zipCode,@"zipcode",
                    AUTH_TOKEN,@"auth_token",
                    nil];
    }else
    {
        //40.7142700%2C74.0059700
        NSString *gpsString = [NSString stringWithFormat:@"%@,%@",lattitude,longitude];
        //        NSString *gpsString = [NSString stringWithFormat:@"%@,%@",@"40.7142700",@"-74.0059700"];
        dictParams=[[NSDictionary alloc] initWithObjectsAndKeys:
                    gpsString,@"gps",
                    AUTH_TOKEN,@"auth_token",
                    nil];
    }
    
    [request setRequestDataWithDictionary:dictParams];
    
    request.asiDelegate = self;
    getLegalFindersRequest=[HttpBridge invokeAPIWithRequest:request];
    if(nil==getLegalFindersRequest)
        return false;
    return true;
}


/*
- (bool) fetchLegalFindersWithZipCode:(NSString *)zipCode OrWithCurrentLocationLattitude:(NSString *)lattitude AndLongitude:(NSString *)longitude withLanguages:(NSString*)languages withAreasOfLaws:(NSString *)aols withLegalAssistance:(NSString *)legalassistances
{
    APIRequest *request = [APIRequest apiRequestWithKey:API_LEGALORGANIZATIONS_GET_ALL];
    NSDictionary *dictParams= nil;

    
    if ([zipCode length] >0)
    {
        dictParams=[[NSDictionary alloc] initWithObjectsAndKeys:
                    zipCode,@"zipcode",
                    AUTH_TOKEN,@"auth_token",
                    nil];
    }else
    {
        //40.7142700%2C74.0059700
        NSString *gpsString = [NSString stringWithFormat:@"%@,%@",lattitude,longitude];
        //        NSString *gpsString = [NSString stringWithFormat:@"%@,%@",@"40.7142700",@"-74.0059700"];
        dictParams=[[NSDictionary alloc] initWithObjectsAndKeys:
                    gpsString,@"gps",
                    AUTH_TOKEN,@"auth_token",
                    nil];
    }

    if (((languages && [languages length]>0) || (aols && [aols length]>0) || (legalassistances && [legalassistances length]>0)) || [zipCode length]>0)
    {
//        NSMutableDictionary *dict = [NSMutableDictionary alloc];
//        [dict setObject:<#(id)#> forKey:<#(id<NSCopying>)#>]
        
//        dictParams=[[[NSDictionary alloc] init];WithObjectsAndKeys:
//                    zipCode,@"zipcode",
//                    AUTH_TOKEN,@"auth_token",
//                    nil];
        //legalAreas=nil&legalServices=nil&languages=5
        
        dictParams = [NSDictionary dictionary];
        if ([zipCode length]>0)
        {
            [dictParams setValue:zipCode forKey:@"zipcode"];
        }
        if ([aols length]>0)
        {
            [dictParams setValue:aols forKey:@"legalAreas"];
        }
        if ([legalassistances length]>0)
        {
            [dictParams setValue:legalassistances forKey:@"legalServices"];
        }
        if ([languages length]>0)
        {
            [dictParams setValue:languages forKey:@"languages"];
        }
        [dictParams setValue:AUTH_TOKEN forKey:@"auth_token"];
        
        NSLog(@"dictionary as per advacned search %@",dictParams);
    }
    
    
    
    [request setRequestDataWithDictionary:dictParams];
    
    request.asiDelegate = self;
    getLegalFindersRequest=[HttpBridge invokeAPIWithRequest:request];
    if(nil==getLegalFindersRequest)
        return false;

    return true;
}
*/

- (bool) fetchLegalFindersWithZipCode:(NSString *)zipCode OrWithCurrentLocationLattitude:(NSString *)lattitude AndLongitude:(NSString *)longitude withLanguages:(NSString*)languages withAreasOfLaws:(NSString *)aols withLegalAssistance:(NSString *)legalassistances
{
    APIRequest *request = [APIRequest apiRequestWithKey:API_LEGALORGANIZATIONS_GET_ALL];
    NSMutableDictionary *dictParams= nil;
    

    if (((languages && [languages length]>0) || (aols && [aols length]>0) || (legalassistances && [legalassistances length]>0)) || [zipCode length]>0 || (lattitude && [lattitude length]>0) || (longitude && [longitude length]>0))
    {
        dictParams = [NSMutableDictionary dictionary];
        if ([zipCode length]>0)
        {
            [dictParams setValue:zipCode forKey:@"zipcode"];
        }else {
            NSString *gpsString = [NSString stringWithFormat:@"%@,%@",lattitude,longitude];
            [dictParams setValue:gpsString forKey:@"gps"];
            
        }
//        if ([lattitude length]>0 || [longitude length]>0) {
//            NSString *gpsString = [NSString stringWithFormat:@"%@,%@",lattitude,longitude];
//            [dictParams setValue:gpsString forKey:@"gps"];
//        }
        if ([aols length]>0)
        {
            [dictParams setValue:aols forKey:@"legalAreas"];
        }
        if ([legalassistances length]>0)
        {
            [dictParams setValue:legalassistances forKey:@"legalServices"];
        }
        if ([languages length]>0)
        {
            [dictParams setValue:languages forKey:@"languages"];
        }
        [dictParams setValue:AUTH_TOKEN forKey:@"auth_token"];
        
        NSLog(@"dictionary as per advacned search %@",dictParams);
    }
    
    
    
    [request setRequestDataWithDictionary:dictParams];
    
    request.asiDelegate = self;
    getLegalFindersRequest=[HttpBridge invokeAPIWithRequest:request];
    if(nil==getLegalFindersRequest)
        return false;
    
    return true;
}


#pragma mark - Fetch News
- (bool) fetchNewsDetails
{
    APIRequest *request = [APIRequest apiRequestWithKey:API_NEWS_GET_ALL];
    NSDictionary *dictParams= [[NSDictionary alloc] initWithObjectsAndKeys:
                    AUTH_TOKEN,@"auth_token",
                    nil];
    
    [request setRequestDataWithDictionary:dictParams];

    request.asiDelegate = self;
    getNewsRequest=[HttpBridge invokeAPIWithRequest:request];
    
    if(nil==getNewsRequest)
        return false;
    return true;
}
#pragma mark - Fetch Trainings
- (bool) fetchTrainingsAndEventsData     
{
    APIRequest *request = [APIRequest apiRequestWithKey:API_TRAININGSANDEVENTS_GET_ALL];
    NSDictionary *dictParams= [[NSDictionary alloc] initWithObjectsAndKeys:
                               AUTH_TOKEN,@"auth_token",
                               [NSNumber numberWithInt:1],@"shared",
                               nil];
    
    [request setRequestDataWithDictionary:dictParams];
    request.asiDelegate = self;
    getTrainingsRequest=[HttpBridge invokeAPIWithRequest:request];
    
    if(nil==getTrainingsRequest)
        return false;
    return true;
}
//- (bool) fetchTrainingsAndEventsData:(bool) refreshFlag
//{
//    if(_isTrainingsRequestPipeline && !refreshFlag)
//    {
//        return false;
//    }
//    _isTrainingsRequestPipeline=true;
//    _isTrainingsRefreshing=refreshFlag;
//    APIRequest *request = [APIRequest apiRequestWithKey:API_TRAININGSANDEVENTS_GET_ALL];
//    request.asiDelegate = self;
//    getTrainingsRequest=[HttpBridge invokeAPIWithRequest:request];
//    
//    if(nil==getTrainingsRequest)
//        return false;
//    
//    return  true;
//}
-(void) cancelPipeLineRequests
{
//    getTrainingsRequest.delegate=nil;
//    [getTrainingsRequest cancel];
//    
//    getNewsRequest.delegate = nil;
//    [getNewsRequest cancel];
}
-(void) requestFinished:(ASIHTTPRequest *)request
{
    if(200!=[request responseStatusCode])
    {
        [self requestFailed:request];
        return;
    }
    
     switch (request.tag)
    {
        case API_LEGALORGANIZATIONS_GET_ALL:
        {
            NSString *xmlString = [[NSString alloc] initWithData:[request rawResponseData] encoding:NSUTF8StringEncoding];
            
            // Parse the XML into a dictionary
            NSError *parseError = nil;
            NSDictionary *dicServiceResponse = [XMLReader dictionaryForXMLString:xmlString error:&parseError];
            
            if (parseError)
            {
                if(delegate && [delegate respondsToSelector:@selector(legalFindersLoadedFailure)])
                    [delegate NewsLoadFailure];
                //NSLog(@"ERROR On course API respose: %@", [parseError localizedDescription]);
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERT_TITLE", nil) message:NSLocalizedString(@"PARSING_ERROR", nil) delegate:self cancelButtonTitle:NSLocalizedString(NSLocalizedString(@"OK", @"OK"), nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                NSArray *listFinders = [[dicServiceResponse objectForKey:@"feed"] valueForKey:@"entry"];
                
                if (![listFinders isKindOfClass:[NSArray class]])
                {
                    // if 'list' isn't an array, we create a new array containing our object
                    listFinders = [NSArray arrayWithObject:listFinders];
                }

                
                for (int i=0; i < [listFinders count]; i++)
                {
                    NSDictionary *dictionary = [listFinders objectAtIndex:i];
                    [LegalFinderDAO insertIfDoesNotExists:dictionary];
                }
                
                if(delegate && [delegate respondsToSelector:@selector(legalFindersDownloadedSuccessfully)])
                    [delegate legalFindersDownloadedSuccessfully];
            }
            
            break;
        }
            
        case API_NEWS_GET_ALL:
        {
            [CoreDataBase disableSave];
            

            NSString *xmlString = [[NSString alloc] initWithData:[request rawResponseData] encoding:NSUTF8StringEncoding];
            
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"NEWS \n %@",xmlString] message:@"SUCCESS." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];

            // Parse the XML into a dictionary
            NSError *parseError = nil;
            NSDictionary *dicServiceResponse = [XMLReader dictionaryForXMLString:xmlString error:&parseError];
            
            if (parseError)
            {
                if(delegate && [delegate respondsToSelector:@selector(NewsLoadFailure)])
                    [delegate NewsLoadFailure];
                //NSLog(@"ERROR On course API respose: %@", [parseError localizedDescription]);
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERT_TITLE", nil) message:NSLocalizedString(@"PARSING_ERROR", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                NSArray *listNews = [[dicServiceResponse objectForKey:@"feed"] valueForKey:@"entry"];
                
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"NEWS list  : %@",dicServiceResponse] message:@"SUCCESS." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [alert show];
//
                if(listNews)
                {
                    [NewsDAO markAsOldRecords];
                }

                for (int i=0; i < [listNews count]; i++)
                {
                    NSDictionary *dictionary = [listNews objectAtIndex:i];
                    [NewsDAO insertIfDoesNotExists:dictionary];
                }
                
                [CoreDataBase enableSave];
                [CoreDataBase saveContext];
                
                if(listNews)
                {
                    [NewsDAO deleteOldRecords];
                }

                if(delegate && [delegate respondsToSelector:@selector(NewsLoaded)])
                    [delegate NewsLoaded];
            }
            break;
        }
        case API_TRAININGSANDEVENTS_GET_ALL:
        {
            [CoreDataBase disableSave];

//            _isTrainingsRequestPipeline=false;
            NSString *xmlString = [[NSString alloc] initWithData:[request rawResponseData] encoding:NSUTF8StringEncoding];
            
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"TRAININGS AND EVENTS \n %@",xmlString] message:@"SUCCESS." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];

            // Parse the XML into a dictionary
            NSError *parseError = nil;
            NSDictionary *dicServiceResponse = [XMLReader dictionaryForXMLString:xmlString error:&parseError];
            
            if (parseError)
            {
                if(delegate && [delegate respondsToSelector:@selector(TrainingsLoadFailure)])
                    [delegate TrainingsLoadFailure];
                //NSLog(@"ERROR On course API respose: %@", [parseError localizedDescription]);
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERT_TITLE", nil) message:NSLocalizedString(@"PARSING_ERROR", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                NSArray *listTrainings = [[dicServiceResponse objectForKey:@"feed"] valueForKey:@"entry"];
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"TRAININGS and EVENTS list  : %@",dicServiceResponse] message:@"SUCCESS." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [alert show];

//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"TRAININGS AND EVENTS list count : %d",listTrainings.count] message:@"SUCCESS." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [alert show];

                if(listTrainings)
                {
                    [EventDAO markAsOldRecords];
                }
                

                for (int i=0; i < [listTrainings count]; i++)
                {
                    NSDictionary *dictionary = [listTrainings objectAtIndex:i];
                    [EventDAO insertIfDoesNotExists:dictionary];
                }
                
                [CoreDataBase enableSave];
                [CoreDataBase saveContext];
                
                if(listTrainings)
                {
                    [EventDAO deleteOldRecords];
                }

                if(delegate && [delegate respondsToSelector:@selector(TrainingsLoaded)])
                    [delegate TrainingsLoaded];
            }
            
            break;
        }

        //CMS API Calls

        case API_IMMIGRATIONBASICS_GET_ALL:
        case API_GET_CIVIL_RIGHTS:
        case API_GET_CIVIC_ENGAGEMENT:
        {
            [CoreDataBase disableSave];

            NSError *error = nil;
            NSArray *topics = [NSJSONSerialization JSONObjectWithData:[request responseData]
                                                              options:kNilOptions
                                                                error:&error];
            NSError * err;
            NSData * jsonData = [NSJSONSerialization dataWithJSONObject:topics options:0 error:&err];
            NSString * jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

            if(topics)
            {
                [ImmigrationBasicsDAO markAsOldRecords];
                [SubTopicsDAO markAsOldRecords];
                [LinksDAO markAsOldRecords];
            }

            for (int i=0; i < [topics count]; i++)
            {
                NSDictionary *dictionary = [topics objectAtIndex:i];
                //[topics valueForKey:@"MainTopic"];
                NSError * err;
                NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:&err];
                NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

                [ImmigrationBasicsDAO insertIfDoesNotExists:dictionary];
            }
            
            [CoreDataBase enableSave];
            [CoreDataBase saveContext];
            
            if(topics)
            {
                [ImmigrationBasicsDAO deleteOldRecords];
                [SubTopicsDAO deleteOldRecords];
                [LinksDAO deleteOldRecords];
            }

            if (delegate && [delegate respondsToSelector:@selector(immigrationBasicsDownloadedSuccessfully)]) {
                [delegate immigrationBasicsDownloadedSuccessfully];
            }

            break;
        }
        case API_POLL_GET_ALL:
        {
//            NSString *filePath = [[NSBundle mainBundle] pathForResource:@"PollQuestion" ofType:@"json"];
//            NSString *fileContent = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
//            
//            NSData * data=[fileContent dataUsingEncoding:NSUTF8StringEncoding];
            
            [CoreDataBase disableSave];
            
            NSError *error = nil;
            NSArray *questions = [NSJSONSerialization JSONObjectWithData:[request responseData]
                                                              options:kNilOptions
                                                                error:&error];
            
            
            if(questions)
            {
                [PollQuestionsDAO markAsOldRecords];
            }

            for (int i=0; i < [questions count]; i++)
            {
                NSDictionary *dictionary = [questions objectAtIndex:i];
                [PollQuestionsDAO insertIfDoesNotExists:dictionary];
            }
            
            [CoreDataBase enableSave];
            [CoreDataBase saveContext];
            
            if(questions)
            {
                [PollQuestionsDAO deleteOldRecords];
            }

            if (delegate && [delegate respondsToSelector:@selector(pollQuestionsDownloadedSuccessfully)]) {
                [delegate pollQuestionsDownloadedSuccessfully];
            }
            
            break;
        }

        case API_ABOUT_GET:
        {
            
            //NSLog(@"response data for ABOUT US %@",request.responseString);
            NSError *error = nil;
            NSArray *dictResponse = [NSJSONSerialization JSONObjectWithData:[request responseData]
                                                                    options:kNilOptions
                                                                      error:&error];
            if (dictResponse && dictResponse.count>0) {
                [AboutImmigoDAO insertIfDoesNotExists:dictResponse];
                
                if (delegate && [delegate respondsToSelector:@selector(aboutUsDownloadedSuccessfully)])
                {
                    [delegate aboutUsDownloadedSuccessfully];
                }

            }
            //NSLog(@"dict response about %@",dictResponse);
            break;
        }
        case API_TERMSOFUSE_GET:
        {
            //NSLog(@"response data for Terms Of USE %@",request.responseString);
            NSError *error = nil;
            NSArray *dictResponse = [NSJSONSerialization JSONObjectWithData:[request responseData]
                                                                    options:kNilOptions
                                                                      error:&error];
            if (dictResponse && [dictResponse count]>0) {
                [DisclaimerDAO insertIfDoesNotExists:dictResponse];
                
                if (delegate && [delegate respondsToSelector:@selector(termsDownloadedSuccessfully)]) {
                    [delegate termsDownloadedSuccessfully];
                }

            }
            
            break;
        }
            
        case API_PRIVACY_GET:
        {
            //NSLog(@"response data for Privacy Policy %@",request.responseString);
            NSError *error = nil;
            NSArray *dictResponse = [NSJSONSerialization JSONObjectWithData:[request responseData]
                                                                    options:kNilOptions
                                                                      error:&error];
            if(dictResponse && [dictResponse count]>0){
                [PrivacyDAO insertIfDoesNotExists:dictResponse];
                if (delegate && [delegate respondsToSelector:@selector(privacyDownloadedSuccessfully)]) {
                    [delegate privacyDownloadedSuccessfully];
                }

            }
                        
            break;
        }
        case API_ALERTS_GET:
        {
//            //NSLog(@"response data for Alert %@",request.responseString);
            [CoreDataBase disableSave];
            
            NSError *error = nil;
            NSArray *alerts = [NSJSONSerialization JSONObjectWithData:[request responseData]
                                                              options:kNilOptions
                                                                error:&error];
            
            if(alerts)
            {
                [AlertsDAO markAsOldRecords];
            }
            
            //TODO:PLease make alert count to be "10" always as per SRS.
//            int alertCount =10;
            
            for (int i=0; i < alerts.count; i++)
            {
                NSDictionary *dictionary = [alerts objectAtIndex:i];//[topics valueForKey:@"MainTopic"];
                [AlertsDAO insertIfDoesNotExists:dictionary];
            }
            
            [CoreDataBase enableSave];
            [CoreDataBase saveContext];
            
            if(alerts)
            {
                [AlertsDAO deleteOldRecords];
            }
            
            if (delegate && [delegate respondsToSelector:@selector(Alertsloaded)])
            {
                [delegate Alertsloaded];
            }
            if (alertsDelegate && [alertsDelegate respondsToSelector:@selector(Alertsloaded)])
            {
                [alertsDelegate Alertsloaded];
            }


            break;
        }
        case API_DEVICE_POST:
        {
            [PushNotificationsDAO insertIfDoesNotExistsWithStatus:isAllowedPushNotf];
            [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:DEVICE_REGISTER_SUCCESSFUL];
            
            
            [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:DEVICE_REGISTER_SUCCESSFUL_APPLAUNCH];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        }
        case API_FEEDBACK_POST:
        {
            if (delegate && [delegate respondsToSelector:@selector(feedBackSubmittedSuccessfully)]) {
                [delegate feedBackSubmittedSuccessfully];
            }
            break;
        }

            case API_POLL_POST_ALL:
        {
            NSError *error = nil;
            NSDictionary *questions = [NSJSONSerialization JSONObjectWithData:[request responseData]
                                                                 options:kNilOptions
                                                                   error:&error];
            
            
//            for (int i=0; i < [questions count]; i++)
//            {
//                NSDictionary *dictionary = [questions objectAtIndex:i];
                [PollQuestionsDAO insertIfDoesNotExists:questions];
//            }
            
            if (delegate && [delegate respondsToSelector:@selector(pollQuestionsSubmittedSuccessfully)]) {
                [delegate pollQuestionsSubmittedSuccessfully];
            }
            break;
        }
        default:
        {
            if(delegate && [delegate respondsToSelector:@selector(NetworkErrorWithMessage:)])
            {
                [delegate NetworkErrorWithMessage:@"Unhandled API Response"];
            }
            break;
        }
      
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    switch (request.tag)
    {
        case API_TRAININGSANDEVENTS_GET_ALL:
        {
            if (delegate && [delegate respondsToSelector:@selector(TrainingsLoadFailure)]) {
                [delegate TrainingsLoadFailure];
            }
            break;
        }
        case API_NEWS_GET_ALL:
        {

            if (delegate && [delegate respondsToSelector:@selector(NewsLoadFailure)]) {
                [delegate NewsLoadFailure];
            }
            break;
        }
        case API_TERMSOFUSE_GET:
        {
            //NSLog(@"from About request failed");
            if (delegate && [delegate respondsToSelector:@selector(termsLoadedFailure)]) {
                [delegate termsLoadedFailure];
            }
            break;
        }
        case API_ALERTS_GET:
        {
            if(delegate && [delegate respondsToSelector:@selector(AlertsLoadFailure)])
                [delegate AlertsLoadFailure];

            break;
        }
        case API_PRIVACY_GET:
        {
            //NSLog(@"from privacy request failed");
            if (delegate && [delegate respondsToSelector:@selector(privacyLoadedFailure)]) {
                [delegate privacyLoadedFailure];
            }
            break;
        }
        case API_ABOUT_GET:
        {
            if (delegate && [delegate respondsToSelector:@selector(aboutUsLoadedFailure)]) {
                [delegate aboutUsLoadedFailure];
            }
            NSLog(@"from About request failed");
            break;
        }
            case API_IMMIGRATIONBASICS_GET_ALL:
        {
            if (delegate && [delegate respondsToSelector:@selector(immigrationBasicsLoadedFailure)]) {
                [delegate immigrationBasicsLoadedFailure];
            }

            break;
        }
        case API_POLL_GET_ALL:
        {
            if (delegate && [delegate respondsToSelector:@selector(pollQuestionsLoadedFailure)]) {
                [delegate pollQuestionsLoadedFailure];
            }
            
            break;
        }
        case API_POLL_POST_ALL:
        {
            if (delegate && [delegate respondsToSelector:@selector(pollQuestionsSubmittedFailure)]) {
                //NSLog(@"Poll Question Submission Failed:%@",[[request error] localizedDescription]);
                [delegate pollQuestionsSubmittedFailure];
            }
            break;
        }
        case API_DEVICE_POST:
        {
            if (notifAtAppLaunch)
            {
                [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:DEVICE_REGISTER_SUCCESSFUL_APPLAUNCH];
                return;
            }

            //NSLog(@"calling on API_DEVICE_POST failure");
            [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:DEVICE_REGISTER_SUCCESSFUL];
            [[NSUserDefaults standardUserDefaults] synchronize];
            if (!notifAtAppLaunch)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"SETTINGS_TITLE", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
                [alert show];
                alert = nil;
            }

            break;
        }
        case API_FEEDBACK_POST:
        {
            if (delegate && [delegate respondsToSelector:@selector(feedBackSubmittedFailure)]) {
                [delegate feedBackSubmittedFailure];
            }
            break;
        }


    }
    
//    _isTrainingsRequestPipeline=false;
    if(request.tag == API_TRAININGSANDEVENTS_GET_ALL)
    {
        //        if(delegate && [delegate respondsToSelector:@selector(userAssignmentsLoadFailure)])
        //            [delegate userAssignmentsLoadFailure];
    }
    
    if(delegate && [delegate respondsToSelector:@selector(NetworkErrorWithMessage:)])
    {
        [delegate NetworkErrorWithMessage:[[request error] localizedDescription]];
    }
}

#pragma mark Network related message...
-(void) NetworkErrorWithMessage:(NSString *)errMessage
{
    return;
}

@end
