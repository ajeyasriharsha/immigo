//
//  TermsModel.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/2/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataDownloader.h"


@interface TermsModel : NSObject <DataDownloaderDelegate>
{
    NSString *_termsString;
}
@property(nonatomic,strong) NSString *termsString;

+ (TermsModel*)sharedDataSource;
+ (void) releaseInstance;

-(void)getTermsOfUseContent;
-(void)termsString:(NSString *)htmlString;
@end
