//
//  ImmigrationNewsTile_iPad.h
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "News.h"
@interface ImmigrationNewsTile_iPad : UIView {
    
    News *_selectedNews;
    UILabel *topicTitle;
    UILabel *subtopicTitle;
    UIImageView *seperatorimageView;

    
    
}
-(void)setNews:(News *)news;
-(News *)News;
@end
