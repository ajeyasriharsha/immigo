//
//  AboutImmigoVC_iPad.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/26/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "AboutImmigoVC_iPad.h"
#import "Common.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "Categories.h"
#import "AboutUsModel.h"
#import "About.h"
#import "AboutImmigoDAO.h"

#import "TermsModel.h"
#import "DataDownloader.h"
#import "Disclaimer.h"
#import "DisclaimerDAO.h"

#import "PrivacyModel.h"
#import "DataDownloader.h"
#import "PrivacyDAO.h"
#import "APPConfig.h"

#import "PushNotificationsDAO.h"
#import "PushNotificationsModel.h"
#import "RegisterDeviceModel.h"
#import "AppDelegate.h"
#import "FeedbackModel.h"
#import "MFMailComposeViewController+URLRequest.h"


@interface AboutImmigoVC_iPad ( )<PullableViewDelegate,UIWebViewDelegate,UIScrollViewDelegate,MTPopupWindowDelegate>
{
    IBOutlet UILabel *selectedTitle;
    IBOutlet UIView *settingsView;
    IBOutlet UIView *feedBackView;
    IBOutlet UIWebView *myWebView;
    IBOutlet UITableView *contentTableView;
    NSString *htmlString;
    NSString *previousSelectedItem;
    NSArray *menuOptions;
    NSIndexPath *selectedIndexpath;
    IBOutlet TPKeyboardAvoidingScrollView *scrollView;
    __weak IBOutlet UITextField *nameField;
    __weak IBOutlet UITextField *emailField;
    __weak IBOutlet UITextField *phoneField;
    __weak IBOutlet UITextView *messageView;
    __weak IBOutlet UIButton *cancelButton;
    __weak IBOutlet UIButton *sendButton;
    __weak IBOutlet UIButton *communicationButton;
    __weak IBOutlet UIActivityIndicatorView *loadingView;
    __weak IBOutlet UIButton *allowPushNotify;
     IBOutlet UIImageView *dividerImage;
   IBOutlet UIActivityIndicatorView *loadingIndicator;
    IBOutlet UIButton *registerBtn;
    __unsafe_unretained IBOutlet UILabel *pushNotifyLabel;

    IBOutlet UIImageView *scrollIndicatorImgView;
}

@end

@implementation AboutImmigoVC_iPad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)pullableView:(PullableView *)pView didChangeState:(BOOL)opened
{
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
   
    UIImage *divider = [UIImage imageNamed:@"menuTitleDivider"];
    dividerImage.image = [divider stretchableImageWithLeftCapWidth:divider.size.width/2 topCapHeight:divider.size.height/2];
    loadingView.hidesWhenStopped = YES;
    loadingView.hidden = YES;
    
    UIImage *offIcon = [UIImage imageNamed:@"checkbox"];
    UIImage *onIcon = [UIImage imageNamed:@"checkbox_selected"];
    [communicationButton setImage:offIcon forState:UIControlStateNormal];
    [communicationButton setImage:onIcon forState:UIControlStateHighlighted];
    [communicationButton setImage:onIcon forState:UIControlStateSelected];
    
    pushNotifyLabel.font =[UIFont fontWithName:FONT_SEMIBOLD size:16.0f];
    [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    sendButton.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:12.8];
    cancelButton.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:12.8];
     [cancelButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];

   
    selectedTitle.font = [UIFont fontWithName:FONT_IPAD size:22.0f];
    selectedTitle.textAlignment = NSTextAlignmentRight;
    menuOptions =[[NSArray alloc] initWithObjects:@"ABOUT IMMIGO",@"DISCLAIMER",@"PRIVACY",@"FEEDBACK",@"SETTINGS", nil];
    [self setTextFieldBorders];
    
    UIImage *sendbtnBg = [UIImage imageNamed:@"button_send"];
    [sendButton setBackgroundImage:[sendbtnBg stretchableImageWithLeftCapWidth:sendbtnBg.size.width/2 topCapHeight:sendbtnBg.size.height/2] forState:UIControlStateNormal];
    sendButton.contentMode = UIViewContentModeRedraw;
    
    UIImage *btnBg = [UIImage imageNamed:@"button_cancel"];
    [cancelButton setBackgroundImage:[btnBg stretchableImageWithLeftCapWidth:btnBg.size.width/2 topCapHeight:btnBg.size.height/2] forState:UIControlStateNormal];
    cancelButton.contentMode = UIViewContentModeRedraw;
    
    
    messageView.font = [UIFont fontWithName:FONT_SEMIBOLD_ITALIC size:13.585f];
    messageView.textColor = [UIColor grayColor];
    // Do any additional setup after loading the view.
    
    //Enable Push Notifications Button
    
    [myWebView.scrollView setDelegate:self];
    
    

}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate]refreshFakeViewController];
    scrollIndicatorImgView.image =[UIImage imageNamed:@"scrollIndicator_down"];
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    [self createPartnersView];
   

}
- (IBAction)registerDevice:(id)sender
{
    [[RegisterDeviceModel sharedDataSource] registerDeviceWithId:[[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY] AndType:@"iOS" AndPushNotificationStatus:YES AtAppLaunch:NO];

    return;
    
    if ([self notificationServicesEnabled]) {
        [[RegisterDeviceModel sharedDataSource] registerDeviceWithId:[[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY] AndType:@"iOS" AndPushNotificationStatus:YES AtAppLaunch:NO];
        [PushNotificationsDAO insertIfDoesNotExistsWithStatus:YES];
    }else{
        
        [[RegisterDeviceModel sharedDataSource] registerDeviceWithId:[[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY] AndType:@"iOS" AndPushNotificationStatus:NO AtAppLaunch:NO];
        [PushNotificationsDAO insertIfDoesNotExistsWithStatus:NO];
    }
    
    
    /*
    UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    if(types & UIRemoteNotificationTypeAlert)
    {
        [[RegisterDeviceModel sharedDataSource] registerDeviceWithId:[[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY] AndType:@"iOS" AndPushNotificationStatus:YES AtAppLaunch:NO];
        [PushNotificationsDAO insertIfDoesNotExistsWithStatus:YES];
    }
    else
    {
        [[RegisterDeviceModel sharedDataSource] registerDeviceWithId:[[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY] AndType:@"iOS" AndPushNotificationStatus:NO AtAppLaunch:NO];
        [PushNotificationsDAO insertIfDoesNotExistsWithStatus:NO];
    }
     */
    
}

-(BOOL)notificationServicesEnabled {
    BOOL isEnabled = NO;
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(currentUserNotificationSettings)]){
        UIUserNotificationSettings *notificationSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
        
        if (!notificationSettings || (notificationSettings.types == UIUserNotificationTypeNone)) {
            isEnabled = NO;
        } else {
            isEnabled = YES;
        }
    } else {
        UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if (types & UIRemoteNotificationTypeAlert) {
            isEnabled = YES;
        } else{
            isEnabled = NO;
        }
    }
    
    return isEnabled;
}

-(void)createPartnersView
{
    CGSize viewFrame = CGSizeZero;
    //NSLog(@"Width:%f Height:%f",self.view.frame.size.width,self.view.frame.size.height);
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        
        if (UIInterfaceOrientationIsPortrait(orientation))
        {
            viewFrame = CGSizeMake(768, 1004);
        }
        else
        {
            viewFrame = CGSizeMake(1024,748);
        }
        UIImage *parternsImg = [UIImage imageNamed:@"partners_iPad"];
        UIImageView *parternsImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, parternsImg.size.width, parternsImg.size.height)];
        bottompullRightView = [[PartnersPullableView_Ipad alloc] initWithFrame:CGRectMake(0, viewFrame.width, viewFrame.width-40, parternsImageView.frame.size.height)];
        parternsImageView.image = parternsImg;
        bottompullRightView.backgroundColor = [UIColor clearColor];
        
        if (UIInterfaceOrientationIsPortrait(orientation))
        {
            
            bottompullRightView.openedCenter = CGPointMake(viewFrame.width/2+20,viewFrame.width+65);
            
            bottompullRightView.closedCenter = CGPointMake(bottompullRightView.openedCenter.x+(bottompullRightView.frame.size.width-parternsImg.size.width), bottompullRightView.openedCenter.y);
            
        }
        else
        {
            
            
            bottompullRightView.openedCenter = CGPointMake(viewFrame.width/2+20,
                                                           ((viewFrame.height -160)-(bottompullRightView.frame.size.height/2)+50));
            
            bottompullRightView.closedCenter = CGPointMake(viewFrame.width+bottompullRightView.openedCenter.x-(parternsImg.size.width)*2-10, bottompullRightView.openedCenter.y);
            
            
        }
        
        bottompullRightView.center = bottompullRightView.closedCenter;
        
        bottompullRightView.animationDuration =0.9f;
        bottompullRightView.animate = YES;
        
        bottompullRightView.handleView.frame = CGRectMake(0, 0, parternsImageView.frame.size.width, bottompullRightView.frame.size.height);
        [bottompullRightView.handleView addSubview:parternsImageView];
        [bottompullRightView bringSubviewToFront:bottompullRightView.handleView];
        bottompullRightView.delegate = self;
        
        
        [self.view addSubview:bottompullRightView];
    }
    else{
        if (UIInterfaceOrientationIsPortrait(orientation))
        {
            viewFrame = CGSizeMake(768, 1024);
        }
        else
        {
            viewFrame = CGSizeMake(1024,768);
        }
        UIImage *parternsImg = [UIImage imageNamed:@"partners_iPad"];
        UIImageView *parternsImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, parternsImg.size.width, parternsImg.size.height)];
        bottompullRightView = [[PartnersPullableView_Ipad alloc] initWithFrame:CGRectMake(0, viewFrame.width, viewFrame.width-40, parternsImageView.frame.size.height)];
        parternsImageView.image = parternsImg;
        bottompullRightView.backgroundColor = [UIColor clearColor];
        
        if (UIInterfaceOrientationIsPortrait(orientation))
        {
            
            bottompullRightView.openedCenter = CGPointMake(viewFrame.width/2+20,viewFrame.width+85);
            
            bottompullRightView.closedCenter = CGPointMake(bottompullRightView.openedCenter.x+(bottompullRightView.frame.size.width-parternsImg.size.width), bottompullRightView.openedCenter.y);
            
        }
        else
        {
            
            
            bottompullRightView.openedCenter = CGPointMake(viewFrame.width/2+20,
                                                           ((viewFrame.height -160)-(bottompullRightView.frame.size.height/2)+50));
            
            bottompullRightView.closedCenter = CGPointMake(viewFrame.width+bottompullRightView.openedCenter.x-(parternsImg.size.width)*2-10, bottompullRightView.openedCenter.y);
            
            
        }
        
        bottompullRightView.center = bottompullRightView.closedCenter;
        
        bottompullRightView.animationDuration =0.9f;
        bottompullRightView.animate = YES;
        
        bottompullRightView.handleView.frame = CGRectMake(0, 0, parternsImageView.frame.size.width, bottompullRightView.frame.size.height);
        [bottompullRightView.handleView addSubview:parternsImageView];
        [bottompullRightView bringSubviewToFront:bottompullRightView.handleView];
        bottompullRightView.delegate = self;
        
        
        [self.view addSubview:bottompullRightView];
        
    }

    
}

-(void)setTextFieldBorders{
    UIImageView *myView = [[UIImageView alloc]initWithFrame:nameField.frame];
    UIImage *divider = [UIImage imageNamed:@"text field"];
    myView.image = [divider stretchableImageWithLeftCapWidth:divider.size.width/2 topCapHeight:divider.size.height/2];
    [nameField setBorderStyle:UITextBorderStyleNone];
    [emailField setBorderStyle:UITextBorderStyleNone];
    [phoneField setBorderStyle:UITextBorderStyleNone];
    nameField.background=myView.image;
    emailField.background=myView.image;
    phoneField.background=myView.image;
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame: messageView.frame];
    UIImage *textFieldBackGround = [UIImage imageNamed:@"message field"];
    imgView.image = [textFieldBackGround stretchableImageWithLeftCapWidth:textFieldBackGround.size.width/2 topCapHeight:textFieldBackGround.size.height/2];
    
    /*
    UIImage *patternImage = [UIImage imageNamed:@"message field"];
    UIColor* color = [UIColor colorWithPatternImage:patternImage];
    messageView.backgroundColor = color;
    */
    
    
    [messageView addSubview: imgView];
    messageView.opaque = NO;
    [messageView setBackgroundColor:[UIColor clearColor]];
    [messageView sendSubviewToBack: imgView];
    
    //   nameField.layer.borderColor = [[UIColor lightGrayColor] CGColor];
//   nameField.layer.borderWidth = 1.0;
//    emailField.layer.borderColor = [[UIColor lightGrayColor] CGColor];
//    emailField.layer.borderWidth = 1.0;
//    phoneField.layer.borderColor = [[UIColor lightGrayColor] CGColor];
//    phoneField.layer.borderWidth = 1.0;
    messageView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    messageView.layer.borderWidth = 0.75;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Tableview delegate and Data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return menuOptions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    cell.textLabel.text = [menuOptions objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:FONT_IPAD size:22.0f];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath == selectedIndexpath)
    {
        cell.textLabel.textColor =UIColorFromRGB(UI_BLUE_TEXT_COLOR);
    }
    
    if (indexPath.row == 0 && menuOptions.count > 0)
    {
        [self tableView:tableView didSelectRowAtIndexPath:indexPath];
        cell.textLabel.textColor =UIColorFromRGB(UI_BLUE_TEXT_COLOR);
    }

    //    cell.textLabel.highlightedTextColor =
    return cell;
}
//- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"TopicCell_BG"]];
//}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (selectedIndexpath)
    {
        UITableViewCell* theCell = [tableView cellForRowAtIndexPath:selectedIndexpath];
        theCell.textLabel.textColor = [UIColor darkGrayColor];
        
    }
    previousSelectedItem =[menuOptions objectAtIndex:selectedIndexpath.row];
    selectedIndexpath = indexPath;
    

    {
        UITableViewCell* theCell = [tableView cellForRowAtIndexPath:indexPath];
        theCell.textLabel.textColor = UIColorFromRGB(UI_BLUE_TEXT_COLOR);
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    UITableViewCell* theCell = [tableView cellForRowAtIndexPath:indexPath];
    theCell.textLabel.textColor = UIColorFromRGB(UI_BLUE_TEXT_COLOR);
    
    [self handleIndexpathSelection:selectedIndexpath];
    
}
-(void)handleIndexpathSelection:(NSIndexPath *)myIndexPath{
    
    
    NSString *selectedMenu = [menuOptions objectAtIndex:myIndexPath.row];
    CGFloat width;
    CGFloat oldWidth;
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        oldWidth = [previousSelectedItem sizeWithFont:[UIFont fontWithName:FONT_IPAD size:22.0f]].width;
        width = [selectedMenu sizeWithFont:[UIFont fontWithName:FONT_IPAD size:22.0f]].width;
    }
    else
    {
        oldWidth = [previousSelectedItem sizeWithFont:[UIFont fontWithName:FONT_IPAD size:22.0f]].width;
        width = ceil([selectedMenu sizeWithAttributes:@{NSFontAttributeName: [UIFont fontWithName:FONT_IPAD size:22.0f]}].width);
        //NSLog(@"Old:%f New:%f",oldWidth,width);
    }
    
    [dividerImage setFrame:CGRectMake(dividerImage.frame.origin.x+oldWidth-width, dividerImage.frame.origin.y, width, dividerImage.frame.size.height)];
    [selectedTitle setText:selectedMenu];
    [self handleViewHiding:myIndexPath];
}
-(BOOL)canShowScrollIndicator{
    CGFloat webviewHeight =myWebView.frame.size.height;
    float scrollContentSizeHeight = myWebView.scrollView.contentSize.height;
    return scrollContentSizeHeight > webviewHeight;
}
-(void)handleViewHiding:(NSIndexPath *)selectedIndex
{
    [feedBackView endEditing:YES];
    
    
    switch (selectedIndex.row) {
        case 0:
            //NSLog(@"0");
            myWebView.hidden =NO;
            settingsView.hidden=YES;
            feedBackView.hidden=YES;
            loadingView.hidden = false;
            [loadingView startAnimating];
            [DataDownloader sharedDownloader].delegate = self;
            [[AboutUsModel sharedDataSource] getAboutUs];
            
            break;
        case 1:
            //NSLog(@"1");
            myWebView.hidden =NO;
            settingsView.hidden=YES;
            feedBackView.hidden=YES;
             loadingView.hidden = false;
            [loadingView startAnimating];
            [DataDownloader sharedDownloader].delegate = self;
            [[TermsModel sharedDataSource] getTermsOfUseContent];
            break;
        case 2:
            //NSLog(@"2");
            myWebView.hidden =NO;
            settingsView.hidden=YES;
            feedBackView.hidden=YES;
            [loadingView startAnimating];
            [DataDownloader sharedDownloader].delegate = self;
            [[PrivacyModel sharedDataSource] getPrivacyPolicyContent];
            break;
        case 3:
            myWebView.hidden =YES;
            settingsView.hidden=YES;
            feedBackView.hidden=NO;
            [self resetFields];
            scrollIndicatorImgView.hidden = YES;
            communicationButton.selected = YES;
            if ([loadingView isAnimating]) {
                loadingView.hidden =YES;
                [loadingView stopAnimating];
            }
            //NSLog(@"3");
            break;
        case 4:
            myWebView.hidden =YES;
            [self handlepushnotifyButton];
            settingsView.hidden=NO;
            feedBackView.hidden=YES;
            if ([loadingView isAnimating]) {
                loadingView.hidden =YES;
                [loadingView stopAnimating];
            }
            scrollIndicatorImgView.hidden = YES;
            //NSLog(@"4");
            break;
        default:
            break;
    }
}

-(void)handlepushnotifyButton
{
    
    [allowPushNotify setExclusiveTouch:YES];
    
    PushNotifications *notification = [PushNotificationsDAO objectWithID:1];

    if ([[[NSUserDefaults standardUserDefaults] valueForKey:APP_LAUNCH] isEqualToString:@"YES"])
    {
        registerBtn.hidden = YES;
    }
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:DEVICE_REGISTER_SUCCESSFUL_APPLAUNCH] isEqualToString:@"NO"])
    {
        registerBtn.hidden = NO;
    }
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:DEVICE_REGISTER_SUCCESSFUL] isEqualToString:@"NO"])
    {
        registerBtn.hidden = YES;
        //NSLog(@"Calling NOT in app launch time--- deviceSubmitFailure");
        
//        PushNotifications *notification = [PushNotificationsDAO objectWithID:1];
        if ([[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY])
        {
            [[RegisterDeviceModel sharedDataSource] registerDeviceWithId:[[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY] AndType:@"iOS" AndPushNotificationStatus:notification.isEnabled AtAppLaunch:NO];
        }
    }
    
    
    
    
//    registerBtn.hidden = YES;
    UIImage *onimage = [UIImage imageNamed:@"PushNotifyon"];
    UIImage *offimage = [UIImage imageNamed:@"PushNotifyoff"];

    
    
    if ([self notificationServicesEnabled]) {
        allowPushNotify.userInteractionEnabled = YES;
        if (notification.isEnabled)
        {
            [allowPushNotify setSelected:YES];
            [allowPushNotify setBackgroundImage:onimage forState:UIControlStateNormal];
            
        }
        else
        {
            [allowPushNotify setSelected:NO];
            [allowPushNotify setBackgroundImage:offimage forState:UIControlStateNormal];
        }
    }else{
        
        allowPushNotify.userInteractionEnabled = NO;
        if (notification.isEnabled)
        {
            [allowPushNotify setSelected:NO];
            [allowPushNotify setBackgroundImage:offimage forState:UIControlStateNormal];
        }
        else
        {
            [allowPushNotify setSelected:NO];
            [allowPushNotify setBackgroundImage:offimage forState:UIControlStateNormal];
        }

    }
/*
    UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    if(types & UIRemoteNotificationTypeAlert)
    {
        allowPushNotify.userInteractionEnabled = YES;
        if (notification.isEnabled)
        {
            [allowPushNotify setSelected:YES];
            [allowPushNotify setBackgroundImage:onimage forState:UIControlStateNormal];
            
        }
        else
        {
            [allowPushNotify setSelected:NO];
            [allowPushNotify setBackgroundImage:offimage forState:UIControlStateNormal];
        }
        
    }
    else
    {
        //        [PushNotificationsDAO insertIfDoesNotExistsWithStatus:NO];
        allowPushNotify.userInteractionEnabled = NO;
        if (notification.isEnabled)
        {
            [allowPushNotify setSelected:NO];
            [allowPushNotify setBackgroundImage:offimage forState:UIControlStateNormal];
        }
        else
        {
            [allowPushNotify setSelected:NO];
            [allowPushNotify setBackgroundImage:offimage forState:UIControlStateNormal];
        }

    }
 
 */
    
}
#pragma mark TextField Delegates
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        if ([messageView.text length]==0 || [[messageView.text trimmedString] length]==0)
        {
            messageView.text = @"Your Message";
            textView.textColor = [UIColor grayColor];
        }
        
        return NO;
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //activeField = textField;
    [textField resignFirstResponder];
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == phoneField)
    {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 10 || ![self isNumeric:string]) ? NO : YES;
    }
    return YES;
}

-(BOOL)isNumeric:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}
-(void)resetFields{
    
    [nameField setText:@""];
    [phoneField setText:@""];
    [emailField setText:@""];
    [messageView setText:@""];
    messageView.text = @"Your Message";
}
-(IBAction)cancelButtonTapped:(id)sender{
    
    [self resetFields];
    [nameField becomeFirstResponder];
}


-(IBAction)tappedOnPushNotifyButton:(UIButton *)sender
{
    UIImage *onimage = [UIImage imageNamed:@"PushNotifyon"];
    UIImage *offimage = [UIImage imageNamed:@"PushNotifyoff"];

//    if (![Common currentNetworkStatus])
//    {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Settings" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alertView show];
//        return;
//    }
    sender.selected = !sender.selected;

    if ([allowPushNotify isSelected])
    {
        [[PushNotificationsModel sharedDataSource] updatePushNotificationStatus:YES];
        [[RegisterDeviceModel sharedDataSource] registerDeviceWithId:[[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY] AndType:@"iOS" AndPushNotificationStatus:YES AtAppLaunch:NO];
        [allowPushNotify setBackgroundImage:onimage forState:UIControlStateNormal];
    }else
    {
        [[PushNotificationsModel sharedDataSource] updatePushNotificationStatus:NO];
        [[RegisterDeviceModel sharedDataSource] registerDeviceWithId:[[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY] AndType:@"iOS" AndPushNotificationStatus:NO AtAppLaunch:NO];
        [allowPushNotify setBackgroundImage:offimage forState:UIControlStateNormal];
    }
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
- (IBAction)sendFeedback:(id)sender
{
    {
        if ([nameField.text length]==0 || [[nameField.text trimmedString] length]==0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Feedback" message:@"Name is a required field." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
        }else if ([emailField.text length]==0 || [[emailField.text trimmedString] length]==0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Feedback" message:@"Email address is a required field." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }else if ([messageView.text length]==0 || [[messageView.text trimmedString] length]==0 || [messageView.text isEqualToString:@"Your Message"])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Feedback" message:@"Message is a required field." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }else if(([emailField.text length]>0 || [[emailField.text trimmedString] length]>0) && ([messageView.text length]>0 || [[messageView.text trimmedString] length]>0  || ![messageView.text isEqualToString:@"Your Message"]) && ([nameField.text length]>0 || [[nameField.text trimmedString] length]>0))
        {
            if (![self validateEmailWithString:emailField.text])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Please enter valid email address." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                return;
            }
            if (![Common currentNetworkStatus])
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Feedback" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
                return;
            }

        [[FeedbackModel sharedDataSource] submitFeedBackWithDeviceId:[[NSUserDefaults standardUserDefaults] valueForKey:IMMIGO_GUID] AndUserName:nameField.text AndEmailId:emailField.text AndPhoneNumber:phoneField.text AndFeedback:messageView.text AndStatus:communicationButton.selected];
            
            //I would like to receive communications from Immigo partners
            NSString *messageBody = [NSString stringWithFormat:@"<!DOCTYPE html> <html> <head> <style> table,th,td { border:1px solid black; border-collapse:collapse; } th,td { padding:5px; } </style> </head> <body> <table style=\"width:300px\"> <tr> <th>Name</th> <td>%@</td> </tr> <tr> <th>Email</th> <td>%@</td> </tr> <tr> <th>Phone Number</th> <td>%@</td> </tr> <tr> <th>Your Feedback</th> <td>%@</td> </tr><tr> <th>I would like to receive communications from Immigo partners</th> <td>%@</td> </tr> </table> </body> </html>",nameField.text,emailField.text,phoneField.text,messageView.text,communicationButton.selected ? @"YES":@"NO"];
            
            
            if ([MFMailComposeViewController canSendMail])
            {
                // set the sendTo address
                NSMutableArray *recipients = [[NSMutableArray alloc] initWithCapacity:1];
                [recipients addObject:@"immigo@immigrationadvocates.org"];
                
                MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
                controller.mailComposeDelegate = self;
                [controller setMessageBody:messageBody isHTML:YES];
                [controller setSubject:@"Immigo feedback"];
                
                [controller setToRecipients:recipients];
                
                if ([self respondsToSelector:@selector(presentViewController:animated:completion:)])
                {
//                    UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//                    UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//                    titlView.image = titleLogo;
//                    controller.navigationItem.titleView = titlView;
//                    [[controller navigationBar] sendSubviewToBack:titlView];
//                    [[controller navigationBar] setTintColor:[UIColor blueColor]];
//                    [self presentViewController:controller animated:YES completion:nil];
//                    [self resetFields];
                    
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No email account is setup to send mail." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                
            }
        }
    }
    
    
}

- (IBAction)optionbuttonTapped:(UIButton *)sender
{
    sender.selected =! sender.selected;
}

#pragma mark - Feedback Datadownloader delegate methods
-(void)feedBackSubmittedSuccessfully
{
    [scrollView scrollsToTop];
    [self resetFields];
    //feedBackWishView.hidden  = NO;
    
    
}
-(void)feedBackSubmittedFailure
{
    //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"FeedBack" message:@"Sorry! Feedback is not submitted." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    //    [alert show];
    //
    
}
#pragma mark -WebView Delegate Methods
-(void)webViewDidStartLoad:(UIWebView *)webView{
    
    loadingIndicator.hidden = NO;
    [loadingIndicator startAnimating];
//    myWebView.scalesPageToFit = YES;
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
   
     // myWebView.scrollView.delegate = self; // set delegate method of UISrollView
    
    
  //[myWebView stringByEvaluatingJavaScriptFromString:@"document.body.style.zoom = 2.0;"];
    
    loadingIndicator.hidden = YES;
    [loadingIndicator stopAnimating];
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    //NSLog(@"Error : %@",error);
    
    loadingIndicator.hidden = YES;
    [loadingIndicator stopAnimating];
    
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale; // scale between minimum and maximum. called after any 'bounce' animations

{
   // myWebView.scrollView.minimumZoomScale = 1; // set similar to previous.
   // myWebView.scrollView.maximumZoomScale = 5; // set similar to previous.
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked){
        
        
            if ([request.URL.scheme isEqualToString:@"tel"])
            {
               
                return YES;
            }
            
            else if ([request.URL.scheme isEqualToString:@"mailto"])
            {
                if ([request.URL isMailtoRequest])
                {
                    
                    [MFMailComposeViewController presentModalComposeViewControllerWithURL:request.URL  delegate:self];
                    return NO;
                    
                }
                
            }
            else{
                
                if (![Common currentNetworkStatus])
                {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"About Immigo" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alertView show];
                    return NO;
                }
                
                NSString *linkUrl =[request.URL absoluteString];
                //    NSURL *myURL;
                if ([linkUrl hasPrefix:@"http://"] || [linkUrl hasPrefix:@"https://"])
                {
                    
                }
                else
                {
                    linkUrl = [NSString stringWithFormat:@"http://%@",linkUrl];
                }
                
                
               
                    if ([Common isValidUrl:linkUrl])
                    {
                        MTPopupWindow *popup = [[MTPopupWindow alloc] init];
                        popup.delegate = self;
                        popup.fileName = linkUrl;
                        [popup show];
                        return NO;
                    }else
                    {
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"About Immigo" message:@"Invalid Url" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alertView show];
                        
                    }
                    
               

            }
       
        
    }
    
    return YES;
    
    
}
#pragma mark RotationDelegate

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    
    if (!htmlString || [htmlString isKindOfClass:[NSNull class]])
    {
        [myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    }else{

    NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body><p><font face=\"opensans\" color=\"#4c4c4c\" size=\"3\">%@</font></p></body></html>",htmlString];
    [myWebView loadHTMLString:html baseURL:nil];
    }
    [loadingView stopAnimating];
   
}
-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    
    
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    [bottompullRightView removeFromSuperview];
    [self createPartnersView];
    
    
}
#pragma mark DataDownloader Delegates
-(void)termsDownloadedSuccessfully
{
    Disclaimer *disclimer = [[DisclaimerDAO allObjects] objectAtIndex:0];
    htmlString = [[DisclaimerDAO objectWithID:disclimer.key] content];
    NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body><p><font face=\"opensans\" color=\"#4c4c4c\" size=\"3\">%@</font></p></body></html>",htmlString];
   // -webkit-text-size-adjust: none;
    [myWebView loadHTMLString:html baseURL:nil];
    [loadingView stopAnimating];
}
-(void)termsLoadedFailure
{
    Disclaimer *disclimer = [[DisclaimerDAO allObjects] objectAtIndex:0];
    htmlString = [[DisclaimerDAO objectWithID:disclimer.key] content];
    
    if (!htmlString || [htmlString isKindOfClass:[NSNull class]])
    {
        [myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    }
    else{
        
        NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body><p><font face=\"opensans\" color=\"#4c4c4c\" size=\"3\">%@</font></p></body></html>",htmlString];
        
        [myWebView loadHTMLString:html baseURL:nil];
    }

    [loadingView stopAnimating];
    
}
-(void)aboutUsDownloadedSuccessfully
{
    About *about = [[AboutImmigoDAO allObjects] objectAtIndex:0];
    //    //NSLog(@"all bjects %@",[[DisclaimerDAO objectWithID:disclimer.key] content]);
    ////    Disclaimer *disclaimer =     [DisclaimerDAO allObjects]
    //    //NSLog(@"string %@",[[TermsModel sharedDataSource] termsString]);
    htmlString =@"";
    htmlString = [[AboutImmigoDAO objectWithID:about.key] content];
    //NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body><p><font face=\"opensans\" color=\"black\" size=\"3.5\">%@</font></p></body></html>",htmlString];
    NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body><p><font face=\"opensans\" color=\"#4c4c4c\" size=\"3\">%@</font></p></body></html>",htmlString];
    
    [myWebView loadHTMLString:html baseURL:nil];
     [loadingView stopAnimating];
}
-(void)aboutUsLoadedFailure
{
    //NSLog(@"terms is not loading");
   
    About *about = [[AboutImmigoDAO allObjects] objectAtIndex:0];
    htmlString = [[AboutImmigoDAO objectWithID:about.key] content];

    if (!htmlString || [htmlString isKindOfClass:[NSNull class]])
    {
        [myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    }else{
        
        NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body><p><font face=\"opensans\" color=\"#4c4c4c\" size=\"3\">%@</font></p></body></html>",htmlString];
        [myWebView loadHTMLString:html baseURL:nil];
    }
    [loadingView stopAnimating];
    
}

-(void)privacyDownloadedSuccessfully
{
        
        Privacy *privacy = [[PrivacyDAO allObjects] objectAtIndex:0];
        htmlString = [[PrivacyDAO objectWithID:privacy.key] content];
        // NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body><p><font face=\"opensans\" color=\"black\" size=\"3.5\">%@</font></p></body></html>",htmlString];
    NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body><p><font face=\"opensans\" color=\"#4c4c4c\" size=\"3\">%@</font></p></body></html>",htmlString];
    
        [myWebView loadHTMLString:html baseURL:nil];
    [loadingView stopAnimating];
}
-(void)privacyLoadedFailure
{
    
    Privacy *privacy = [[PrivacyDAO allObjects] objectAtIndex:0];
    htmlString = [[PrivacyDAO objectWithID:privacy.key] content];
    if (!htmlString || [htmlString isKindOfClass:[NSNull class]])
    {
        [myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    }else{

   
       NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body><p><font face=\"opensans\" color=\"#4c4c4c\" size=\"3\">%@</font></p></body></html>",htmlString];
    [myWebView loadHTMLString:html baseURL:nil];
    
    }
    
  [loadingView stopAnimating];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - MFMail Composer Delegate methods
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	switch (result)
	{
		case MFMailComposeResultCancelled:
		{
			break;
		}
		case MFMailComposeResultSaved:
		{
			break;
		}
		case MFMailComposeResultSent:
		{
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Email sent successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            //			[alert show];
			break;
		}
		case MFMailComposeResultFailed:
		{
			break;
		}
		default:
		{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Email Failed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
            
		}
			break;
	}
	if ([self respondsToSelector:@selector(dismissModalViewControllerAnimated:)]) {
        [self performSelector:@selector(dismissModalViewControllerAnimated:) withObject:[NSNumber numberWithBool:YES]];
    } else {
        [controller dismissViewControllerAnimated:YES completion:nil];
    }
	
    
    if (![feedBackView isHidden]) {
        [nameField becomeFirstResponder];
    }
	
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    // register for keyboard notifications
    
    if ([textView.textColor isEqual:[UIColor grayColor]] || [messageView.text isEqualToString:@"Your Message"])
    {
        textView.text= @"";
        textView.textColor = [UIColor darkGrayColor];
        
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
   
    [bottompullRightView removeFromSuperview];
}
- (void)scrollViewDidScroll:(UIScrollView *)optscrollView
{
    
    BOOL toEnd =NO;
    if (optscrollView.contentOffset.y >= (optscrollView.contentSize.height - optscrollView.frame.size.height)) {
        //reach bottom
        NSLog(@"we are at the endddd");
        toEnd =YES;
    }
    
    if (optscrollView.contentOffset.y < 0){
        //reach top
        NSLog(@"we are at the top");
        toEnd =NO;
    }
    
    if (optscrollView.contentOffset.y >= 0 && optscrollView.contentOffset.y < (optscrollView.contentSize.height - optscrollView.frame.size.height)){
        //not top and not bottom
    }
    
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    scrollIndicatorImgView.image =toEnd?[UIImage imageNamed:@"scrollIndicator_up"]:[UIImage imageNamed:@"scrollIndicator_down"];
    
}

- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
