//
//  LegalSearchDetailVC_iPad.m
//  IMMIGO
//
//  Created by pradeep ISPV on 6/5/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "LegalSearchListVC_iPad.h"
#import "DataDownloader.h"
#import "Common.h"
#import "SearchListCell.h"
#import "LegalFinderModel.h"
#import "LegalFinder.h"
#import "Categories.h"
#import "LegalFinderDetailVC_iPad.h"
#import "AppDelegate.h"

@interface LegalSearchListVC_iPad ()<DataDownloaderDelegate,MKMapViewDelegate>{
    
    __weak IBOutlet UIButton *backButton;
    IBOutlet UILabel *resultsNearLabel;
    
    IBOutlet UILabel *searchAreaName;
    IBOutlet UITableView *searchResultsTable;
    IBOutlet MKMapView *legalMapView;
    NSMutableArray *legalFindersArray;

    IBOutlet UILabel *screenHeaderName;
    IBOutlet UIView *leftParentView;
    IBOutlet UIImageView *bottomDivider;
    IBOutlet UIButton *fullscreen;
    IBOutlet UIImageView *scrollIndicatorImgView;
    
    CGRect mapViewP_OriginalFrame;
    CGRect leftParentP_OriginalFrame;
    CGRect mapViewL_OriginalFrame;
    CGRect leftParentL_OriginalFrame;


    BOOL isMapInFullView;
    
    CGFloat mapX_BeforeFull;
    CGFloat leftX_BeforeFull;
}
- (IBAction)tappedOnFullscreen:(id)sender;

@end

@implementation LegalSearchListVC_iPad
@synthesize zipCode;
@synthesize isFromAdvacnedSearch;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate]refreshFakeViewController];
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;

}

-(BOOL)canShowScrollIndicator{
    return searchResultsTable.contentSize.height > searchResultsTable.bounds.size.height;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];

    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        //NSLog(@"frame change in portrait left %@  -------------  map %@",NSStringFromCGRect(leftParentView.frame),NSStringFromCGRect(legalMapView.frame));
        
        CGRect leftFrame = leftParentView.frame;
        leftFrame.origin.x = 50.0f;
        leftFrame.size.width = 317.0f;
        leftFrame.size.height = 685.0f;
        leftParentView.frame = leftFrame;
        
        
        CGRect mapViewFrame = legalMapView.frame;
        mapViewFrame.origin.x = 399.0f;
        mapViewFrame.size.width = 350.0f;
        mapViewFrame.size.height = 685.0f;
        legalMapView.frame = mapViewFrame;
        
        mapViewP_OriginalFrame = legalMapView.frame;
        leftParentP_OriginalFrame = leftParentView.frame;
        
    }
    else if (UIInterfaceOrientationIsLandscape(orientation))
    {
        CGRect leftFrame = leftParentView.frame;
        leftFrame.origin.x = 50.0f;
        leftFrame.size.width = 324.0f;
        leftFrame.size.height = 491.0f;
        leftParentView.frame = leftFrame;
        
        
        CGRect mapViewFrame = legalMapView.frame;
        mapViewFrame.origin.x = 406.0f;
        mapViewFrame.size.width = 606.0f;
        mapViewFrame.size.height = 491.0f;
        legalMapView.frame = mapViewFrame;
        
        mapViewL_OriginalFrame = legalMapView.frame;
        leftParentL_OriginalFrame = leftParentView.frame;
        //NSLog(@"frame change in landscpe left %@  -------------  map %@",NSStringFromCGRect(leftParentView.frame),NSStringFromCGRect(legalMapView.frame));
    }
    mapX_BeforeFull = legalMapView.frame.origin.x;
    leftX_BeforeFull = leftParentView.frame.origin.x;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    [DataDownloader sharedDownloader].delegate = self;
    //    [[LegalFinderModel sharedDataSource] fetchFinders];
    
    searchResultsTable.rowHeight = 69.0f;
    
    screenHeaderName.textColor = [UIColor grayColor];
    screenHeaderName.font = [UIFont fontWithName:FONT_IPAD size:22.0f];
    
    resultsNearLabel.font = [UIFont fontWithName:FONT_IPAD size:17.5f];
    searchAreaName.font = [UIFont fontWithName:FONT_SEMIBOLD_ITALIC size:15.0f];
    
    if (isFromAdvacnedSearch){
        
        resultsNearLabel.text =NSLocalizedString(@"Advanced Search Results", @"Advanced Search Results");
        [searchAreaName setHidden:YES];
    }else{
        resultsNearLabel.text =NSLocalizedString(@"RESULTS NEAR", @"RESULTS NEAR");
        [searchAreaName setHidden:NO];
        
    }
    
    searchAreaName.text = [zipCode length]>0 ? zipCode : NSLocalizedString(@"Current Location", @"Current Location");//@"500 Streetname Granville MI";

    UIImage *dividerImage = [UIImage imageNamed:@"menuTitleDivider"];
    bottomDivider.image = [dividerImage stretchableImageWithLeftCapWidth:dividerImage.size.width/2 topCapHeight:dividerImage.size.height/2];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)legalFindersDownloadedSuccessfully
{
    //No results are available based on your search criteria.
    NSArray *array = [[LegalFinderModel sharedDataSource] allItems];
    if ([array count] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Legal Help Finder" message:@"No results are available based on your search criteria." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alert show];
        return;
    }
   // mapButton.userInteractionEnabled = YES;
    legalFindersArray = [NSMutableArray arrayWithArray:array];
    [searchResultsTable reloadData];
    
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    
    [self dropPins];

}
-(void)legalFindersLoadedFailure
{
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Legal Help Finder" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
    }
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return legalFindersArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LegalFinder *finder = (LegalFinder *)[legalFindersArray objectAtIndex:indexPath.row];
    //NSLog(@"adder %@ %@",finder.finderAddress.latitude,finder.finderAddress.longitude);
    
    NSString *address = @"";
    if ([finder.finderAddress.street length]>0)
    {
        address = finder.finderAddress.street;
        address = [address stringByAppendingString:@","];
    }
    if ([finder.finderAddress.city length]>0)
    {
        address = [address stringByAppendingString:finder.finderAddress.city];
        address = [address stringByAppendingString:@","];
    }
    if ([finder.finderAddress.state length]>0)
    {
        address = [address stringByAppendingString:finder.finderAddress.state];
        address = [address stringByAppendingString:@" "];
    }
    NSString *string = [NSString stringWithFormat:@"%@",finder.finderAddress.zipcode];
    if ([string length]>0)
    {
        address = [address stringByAppendingString:string];
        //        address = [address stringByAppendingString:@"."];
    }
    
    SearchListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell)
    {
        cell = [[SearchListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    cell.name.text = [finder.finderName trimmedString];//@"ABS Immigration";
    cell.address.text = [address trimmedString];
    cell.distance.text = @"5.99 Mi";
    cell.address.numberOfLines = 2;
    cell.name.font = [UIFont fontWithName:FONT_REGULAR size:15.0f];//[UIFont fontWithName:@"OpenSans-Semibold" size:16.91f];
    cell.address.font = [UIFont fontWithName:FONT_REGULAR size:12.5f];
    cell.distance.font = [UIFont fontWithName:FONT_REGULAR size:13.7f];
    cell.name.textColor = [UIColor colorWithRed:(92.0f/255.0f) green:(92.0f/255.0f) blue:(92.0f/255.0f) alpha:1.0];
    
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[LegalFinderModel sharedDataSource] selectFinderAtindex:indexPath.row];
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    LegalFinderDetailVC_iPad* toVC = [storyboard instantiateViewControllerWithIdentifier:@"LegalFinderDetailVC_iPad"];
    [self.navigationController pushViewController:toVC animated:YES];

}

- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
										 duration:(NSTimeInterval)duration
{
    
    if (UIInterfaceOrientationIsPortrait(interfaceOrientation))
    {
        //NSLog(@"frame change in portrait left %@  -------------  map %@",NSStringFromCGRect(leftParentView.frame),NSStringFromCGRect(legalMapView.frame));
        
        if (isMapInFullView)
        {
            CGRect mapViewFrame = legalMapView.frame;
            mapViewFrame.origin.x = 50.0f;
            mapViewFrame.size.width = 704.0f;
            mapViewFrame.size.height = 685.0f;
            legalMapView.frame = mapViewFrame;
        }
        else
        {
            CGRect leftFrame = leftParentView.frame;
            leftFrame.origin.x = leftX_BeforeFull;
            leftFrame.size.width = 317.0f;
            leftFrame.size.height = 685.0f;
            leftParentView.frame = leftFrame;
            
            
            CGRect mapViewFrame = legalMapView.frame;
            mapViewFrame.size.width = 350.0f;
            mapViewFrame.size.height = 685.0f;
            legalMapView.frame = mapViewFrame;
        }
        mapViewP_OriginalFrame = CGRectMake(399, 133, 350, 685);
        leftParentP_OriginalFrame = CGRectMake(50, 133, 317, 685);

    }
    else
    {
        if (isMapInFullView)
        {
            CGRect mapViewFrame = legalMapView.frame;
            mapViewFrame.origin.x = 50.0f;
            mapViewFrame.size.width = 960.0f;
            mapViewFrame.size.height = 491.0f;
            legalMapView.frame = mapViewFrame;
        }
        else
        {
            CGRect leftFrame = leftParentView.frame;
            leftFrame.origin.x = leftX_BeforeFull;
            leftFrame.size.width = 324.0f;
            leftFrame.size.height = 491.0f;
            leftParentView.frame = leftFrame;
            
            
            CGRect mapViewFrame = legalMapView.frame;
            mapViewFrame.size.width = 606.0f;
            mapViewFrame.size.height = 491.0f;
            legalMapView.frame = mapViewFrame;
            mapViewL_OriginalFrame = legalMapView.frame;
            leftParentL_OriginalFrame = leftParentView.frame;
        }
        mapViewL_OriginalFrame = CGRectMake(406, 133, 606, 491);
        leftParentL_OriginalFrame = CGRectMake(50, 133, 324, 491);
        
        //NSLog(@"frame change in landscpe left %@  -------------  map %@",NSStringFromCGRect(leftParentView.frame),NSStringFromCGRect(legalMapView.frame));
    }

    
   // [scrollIndicatorImgView setFrame:CGRectMake(scrollIndicatorImgView.frame.origin.x, mapViewL_OriginalFrame.size.height+20, scrollIndicatorImgView.frame.size.width, scrollIndicatorImgView.frame.size.height)];
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)
    {
        [legalMapView showAnnotations:arr animated:NO];
    }
}
-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    
    
}
- (IBAction)tappedOnFullscreen:(UIButton *)sender
{
    /*
     [UIView beginAnimations:nil context:nil];
     [UIView setAnimationDuration:0.6];
     
     [UIView setAnimationDelegate: self];
     //    [UIView setAnimationDidStopSelector:
     //     @selector(animationFinished:finished:context:)];
     searchList.hidden = NO;
     legalMapView.hidden = YES;
     
     [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
     forView:searchList cache:YES];
     [UIView commitAnimations];

     */
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
//    if (UIInterfaceOrientationIsPortrait(orientation))

//    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    sender.selected = !sender.selected;
    
    if (sender.selected)
    {
        CGRect newmapViewFrame;CGRect newleftParentFrame;
        
        NSTimeInterval animationDuration = 0.5f;/* determine length of animation */;
        
        CGFloat x = 50.0f;//mapViewOriginalFrame.origin.x-leftParentOriginalFrame.size.width;

        if (UIInterfaceOrientationIsPortrait(orientation))
        {
             newmapViewFrame = CGRectMake(x, mapViewP_OriginalFrame.origin.y, 704.0f, 685.0f)/* determine what the frame size should be */;
            
             newleftParentFrame = CGRectMake(-leftParentP_OriginalFrame.size.width, leftParentP_OriginalFrame.origin.y, leftParentP_OriginalFrame.size.width, leftParentP_OriginalFrame.size.height)/* determine what the frame size should be */;
        }else
        {
             newmapViewFrame = CGRectMake(x, mapViewL_OriginalFrame.origin.y, 960.0f, 491.0f)/* determine what the frame size should be */;
            
             newleftParentFrame = CGRectMake(-leftParentL_OriginalFrame.size.width, leftParentL_OriginalFrame.origin.y, leftParentL_OriginalFrame.size.width, leftParentL_OriginalFrame.size.height)/* determine what the frame size should be */;
        }

        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:animationDuration];
        
        legalMapView.frame = newmapViewFrame;
        leftParentView.frame = newleftParentFrame;
        
        [UIView commitAnimations];
        
        isMapInFullView = YES;
        scrollIndicatorImgView.hidden = YES;

    }else
    {
        CGRect newmapViewFrame;CGRect newleftParentFrame;
        
        NSTimeInterval animationDuration = 0.5f;/* determine length of animation */;
        
        if (UIInterfaceOrientationIsPortrait(orientation))
        {
            newmapViewFrame = CGRectMake(mapViewP_OriginalFrame.origin.x, mapViewP_OriginalFrame.origin.y, mapViewP_OriginalFrame.size.width, mapViewP_OriginalFrame.size.height)/* determine what the frame size should be */;
            
            newleftParentFrame = CGRectMake(leftParentP_OriginalFrame.origin.x, leftParentP_OriginalFrame.origin.y, leftParentP_OriginalFrame.size.width, leftParentP_OriginalFrame.size.height)/* determine what the frame size should be */;
        }else
        {
            newmapViewFrame = CGRectMake(mapViewL_OriginalFrame.origin.x, mapViewL_OriginalFrame.origin.y, mapViewL_OriginalFrame.size.width, mapViewL_OriginalFrame.size.height)/* determine what the frame size should be */;
            
            newleftParentFrame = CGRectMake(leftParentL_OriginalFrame.origin.x, leftParentL_OriginalFrame.origin.y, leftParentL_OriginalFrame.size.width, leftParentL_OriginalFrame.size.height)/* determine what the frame size should be */;
        }

        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:animationDuration];
        
        legalMapView.frame = newmapViewFrame;
        leftParentView.frame = newleftParentFrame;

        [UIView commitAnimations];

        isMapInFullView = NO;
        BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
        scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    }
    
}

#pragma mark - mapview Delegates
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    // here we illustrate how to detect which annotation type was clicked on for its callout
    id <MKAnnotation> annotation = [view annotation];
    
//    //NSLog(@"annotation %d",[[mapView annotations] indexOfObject:annotation]);
    NSInteger index = 0;
    for (LegalFinder *finder in legalFindersArray)
    {
        if ([finder.finderName isEqualToString:[annotation title]])
        {
            index = [legalFindersArray indexOfObject:finder];
            break;
        }
    }
    
    [[LegalFinderModel sharedDataSource] selectFinderAtindex:index];
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    LegalFinderDetailVC_iPad* toVC = [storyboard instantiateViewControllerWithIdentifier:@"LegalFinderDetailVC_iPad"];
    [self.navigationController pushViewController:toVC animated:YES];
}

//- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
//
//
//    static NSString *AnnotationViewID = @"annotationViewID";
//
//    MKAnnotationView *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
//
//    if (annotationView == nil)
//    {
//        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
//    }
//
//    UIImage *imgPinBorder = [UIImage imageNamed:@"callOut_Bg.png"];
//    UIImageView *imageViewPinBorder = [[UIImageView alloc] initWithImage:imgPinBorder];
//    imageViewPinBorder.center = annotationView.center;
//    [annotationView addSubview:imageViewPinBorder];
//
//    UIImage *img = [UIImage imageNamed:@"pin.png"];
//    UIImageView *imageView = [[UIImageView alloc] initWithImage:img];
//    imageView.center = annotationView.center;
//    [annotationView addSubview:imageView];
//
//    annotationView.annotation = annotation;
//
//
//    UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//    [rightButton addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
//    [rightButton setTitle:annotation.title forState:UIControlStateNormal];
//
//    annotationView.rightCalloutAccessoryView = rightButton;
//    annotationView.canShowCallout = YES;
//    annotationView.draggable = NO;
//
//
//
//    return annotationView;
//}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *SFAnnotationIdentifier = @"mapView";
    MKAnnotationView *flagAnnotationView = [legalMapView dequeueReusableAnnotationViewWithIdentifier:SFAnnotationIdentifier];
    
    if (flagAnnotationView == nil)
    {
        MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                                        reuseIdentifier:SFAnnotationIdentifier];
        annotationView.canShowCallout = YES;
        annotationView.backgroundColor = [UIColor clearColor];
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        [rightButton setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
        [rightButton addTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
        annotationView.rightCalloutAccessoryView = rightButton;
        UIImage *flagImage = [UIImage imageNamed:@"pin_iPad.png"];
        annotationView.image = flagImage;
        //        if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)
        //        {
        annotationView.rightCalloutAccessoryView.tintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"arrow"]];
        //        }
        annotationView.opaque = NO;
        
        
        
        //change her
        return annotationView;
    }
    else
    {
        flagAnnotationView.annotation = annotation;
    }
    return flagAnnotationView;
}
- (void)showAnnotations:(NSArray *)annotations animated:(BOOL)animated
{
    
}
-(void)dropPins
{
    int count = legalFindersArray.count;
    arr = [NSMutableArray array];
    for (int i=0; i<count; i++)
    {
        LegalFinder *finder = [legalFindersArray objectAtIndex:i];
        //Create your annotation
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        // Set your annotation to point at your coordinate
        point.coordinate = finder.finderAddress.coordinate;
        point.title = finder.finderName;
        //Drop pin on map
        [legalMapView addAnnotation:point];
        [arr addObject:point];
    }
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)
    {
        [legalMapView showAnnotations:arr animated:YES];
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    BOOL toEnd =NO;
    if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
        //reach bottom
        NSLog(@"we are at the endddd");
        toEnd =YES;
    }
    
    if (scrollView.contentOffset.y < 0){
        //reach top
        NSLog(@"we are at the top");
        toEnd =NO;
    }
    
    if (scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y < (scrollView.contentSize.height - scrollView.frame.size.height)){
        //not top and not bottom
    }
    
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    scrollIndicatorImgView.image =toEnd?[UIImage imageNamed:@"scrollIndicator_up"]:[UIImage imageNamed:@"scrollIndicator_down"];
    
}

@end
