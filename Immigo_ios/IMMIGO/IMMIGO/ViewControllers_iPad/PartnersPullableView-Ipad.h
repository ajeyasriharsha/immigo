//
//  PartnersPullableView-Ipad.h
//  PullableView
//
//  Created by Ajeya Sriharsha on 6/5/14.
//
//

#import <UIKit/UIKit.h>
#import "PullableView.h"
#import "MTPopupWindow.h"

@protocol PartnersPullableViewDelegate_iPad <NSObject>

-(void)stopAnimatingPartnersView;

@end
@interface PartnersPullableView_Ipad : PullableView<MTPopupWindowDelegate>{
    
    UIWebView *partnersWebView;
    UIButton *closeButton;
    UIActivityIndicatorView *activityController;

}
@property (nonatomic,strong) id <PartnersPullableViewDelegate_iPad> partnersDelegate;

@end
