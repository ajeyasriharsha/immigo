//
//  TrainingTileView_iPad.h
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
@interface TrainingTileView_iPad : UIView {
    
    UILabel *topicTitle;
    UILabel *subtopicTitle;
    UIImageView *seperatorimageView;
    UIImageView *arrowImage;
    UIButton *disclosureButton;
    
    Event *_event;
}
- (NSDate*)getDateFromString:(NSString *)dateString withTime:(NSString *)time ;
-(void)setEvent:(Event *)event;
-(Event *)event;
@end
