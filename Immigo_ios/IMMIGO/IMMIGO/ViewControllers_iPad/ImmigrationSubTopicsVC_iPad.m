//
//  ImmigrationSubTopicsVC_iPad.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/26/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "ImmigrationSubTopicsVC_iPad.h"
#import "SubtopicsModel.h"
#import "ImmigrationCheckListVC.h"
#import "ImmigrationLinkVC.h"
#import "Common.h"
#import "CheckListModel.h"
#import "Links.h"
#import "LinksModel.h"
#import "MTPopupWindow.h"
#import "FakeViewController_iPad.h"
#import "AppDelegate.h"
#import "MFMailComposeViewController+URLRequest.h"
@interface ImmigrationSubTopicsVC_iPad () <MTPopupWindowDelegate,UIWebViewDelegate,UIScrollViewDelegate>
{
    //For Left side section
    NSArray *subTopicsArray;
    IBOutlet UILabel *heading;
    IBOutlet UITableView *subTopicsTbl;
    NSIndexPath *selectedIndexpath;
    
    //For Right side section
    IBOutlet UILabel *subTopicName;
    IBOutlet UITextView *subTopicHeading;
    
    NSArray *checkLists;
    IBOutlet UIView *checkListView;
    
    NSArray *links;
    IBOutlet UITableView *linksTableView;
    
    IBOutlet UIWebView *textContentView;
    IBOutlet UIScrollView *checkListScrollView;
    IBOutlet UIView *contentView;
    
    SubTopic *mainSubtopic;
    
    CGRect originalHeadingFrame;
    CGRect originalTextContentFrame;
    CGRect originalCheckListFrame;
    CGRect originalLinksFrame;

    BOOL firstTime;
    IBOutlet UIImageView *rightHeaderDivider;
    IBOutlet UIImageView *leftHeaderDivider;
    
    IBOutlet UIImageView *bottomDivider;
    IBOutlet UILabel *screenHeadingLabel;

    SubTopic *selectedSubTopic;
    UILabel *linkHeading;
    IBOutlet UIView *textContentBg;
    IBOutlet UIActivityIndicatorView *loadingIndicator;
    NSString *htmlString;
    IBOutlet UIImageView *scrollIndicatorImgView;
    
    __unsafe_unretained IBOutlet UIButton *linkedinButton;
    __unsafe_unretained IBOutlet UIButton *facebookButton;
    __unsafe_unretained IBOutlet UIButton *twitterButton;
    __unsafe_unretained IBOutlet UIButton *printButton;
    __unsafe_unretained IBOutlet UIButton *emailButton;
}

- (IBAction)backButtonAction:(id)sender;
@end

@implementation ImmigrationSubTopicsVC_iPad
@synthesize topic;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [(AppDelegate *)[[UIApplication sharedApplication] delegate]refreshFakeViewController];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObj = [[ShareToSocialNetworkingSites alloc] init];
    checkListView.clipsToBounds = YES;
    
    UIImage *dividerImage = [UIImage imageNamed:@"menuTitleDivider"];
    bottomDivider.image = [dividerImage stretchableImageWithLeftCapWidth:dividerImage.size.width/2 topCapHeight:dividerImage.size.height/2];

    originalHeadingFrame = subTopicHeading.frame;
    firstTime = YES;
    
    screenHeadingLabel.font = [UIFont fontWithName:FONT_IPAD size:22.0f];
    screenHeadingLabel.textColor = [UIColor grayColor];

    heading.font = [UIFont fontWithName:FONT_IPAD size:22.0f];
    subTopicHeading.font = [UIFont fontWithName:FONT_ITALIC size:16.91f];
    subTopicHeading.textColor = UIColorFromRGB(0x4c4c4c);
    subTopicsTbl.rowHeight = 50.0f;
    linksTableView.rowHeight = 70.0f;
    
//    heading.textColor = [UIColor lightGrayColor];
    heading.text = [topic topicTitle];
    
    subTopicsArray = [[SubtopicsModel sharedDataSource] fetchSubTopicsForTopic:topic];
    
    UIImage *image = [UIImage imageNamed:@"headerDivider"];
    leftHeaderDivider.image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    rightHeaderDivider.image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    subTopicHeading.text = [NSString string];
    
    subTopicsTbl.tableFooterView = [UIView new];
    linksTableView.tableFooterView = [UIView new];
    
    checkListScrollView.delegate =self;
    textContentView.scrollView.delegate =self;
    
    
    UIImage *linkdinbtnBg = [UIImage imageNamed:@"linkedin"];
    [linkedinButton setBackgroundImage:[linkdinbtnBg stretchableImageWithLeftCapWidth:linkdinbtnBg.size.width/2 topCapHeight:linkdinbtnBg.size.height/2] forState:UIControlStateNormal];
    linkedinButton.contentMode = UIViewContentModeRedraw;
    
    UIImage *fbbtnBg = [UIImage imageNamed:@"facebook"];
    [facebookButton setBackgroundImage:[fbbtnBg stretchableImageWithLeftCapWidth:fbbtnBg.size.width/2 topCapHeight:fbbtnBg.size.height/2] forState:UIControlStateNormal];
    facebookButton.contentMode = UIViewContentModeRedraw;
    
    UIImage *twitterbtnBg = [UIImage imageNamed:@"twitter"];
    [twitterButton setBackgroundImage:[twitterbtnBg stretchableImageWithLeftCapWidth:twitterbtnBg.size.width/2 topCapHeight:twitterbtnBg.size.height/2] forState:UIControlStateNormal];
    twitterButton.contentMode = UIViewContentModeRedraw;
    
    //Print
    UIImage *printbtnBg = [UIImage imageNamed:@"Print"];
    [printButton setBackgroundImage:[printbtnBg stretchableImageWithLeftCapWidth:printbtnBg.size.width/2 topCapHeight:printbtnBg.size.height/2] forState:UIControlStateNormal];
    printButton.contentMode = UIViewContentModeRedraw;

    //email
    UIImage *emailbtnBg = [UIImage imageNamed:@"mail_iPad_new"];
    [emailButton setBackgroundImage:[emailbtnBg stretchableImageWithLeftCapWidth:emailbtnBg.size.width/2 topCapHeight:emailbtnBg.size.height/2] forState:UIControlStateNormal];
    emailButton.contentMode = UIViewContentModeRedraw;
    
    
    

    // Do any additional setup after loading the view.
}


-(IBAction)fbTapped:(id)sender
{
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Trainings & Events" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    [self shareToSocialNetworks:NSLocalizedString(@"FaceBook", @"")];
}


-(NSString *)getwebviewHtmlisFromPrint:(BOOL)isFromPrint{
    
    
    NSMutableString *bodyHtml=[NSMutableString string];
    
    if (selectedSubTopic.heading.length) {
        NSString *linkHtml =[NSString stringWithFormat:@"<p> %@ </p>",selectedSubTopic.heading];
        [bodyHtml appendString:linkHtml];
    }
    NSString *linkHtml =[NSString stringWithFormat:@"<p> %@ </p>",selectedSubTopic.textContent];
    [bodyHtml appendString:linkHtml];
    
    if (!isFromPrint) {
        NSURL *appstoreUrl = [NSURL URLWithString:APP_STORE_URL];
        NSString *mailSignature =[NSString stringWithFormat:@"<p> Sent from Immigo for <a href=\"%@\">%@</a></p>",appstoreUrl,[Common deviceName]];
        [bodyHtml appendString:mailSignature];
    }
    
    NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body> %@</body></html>",bodyHtml];
    return html;
}

-(NSString*)getCheckListHtmlisFromPrint:(BOOL)fromPrint{
    
    NSMutableString *bodyHtml=[NSMutableString string];
    int  numberOfOptions = checkLists.count;
    for (int i=1; i<=numberOfOptions; i++)
    {
        
        CheckListItems *checkListItem = [checkLists objectAtIndex:i-1];
        
        NSString *linkText =checkListItem.title;
        NSString *titleHtml =[NSString stringWithFormat:@"<p>%@</p>",selectedSubTopic.heading.length ?selectedSubTopic.heading:@""];
        NSString *linkHtml =[NSString stringWithFormat:@"<p><input type=\"checkbox\" %@ > %@<br></p>",checkListItem.isChecked ? @"checked=\"checked\")" :@"",linkText];
        if (i==1) {
            linkHtml =[titleHtml stringByAppendingString:linkHtml];
        }
        [bodyHtml appendString:linkHtml];
        
    }
    if (!fromPrint) {
        NSURL *appstoreUrl = [NSURL URLWithString:APP_STORE_URL];
        NSString *mailSignature =[NSString stringWithFormat:@"<p> Sent from Immigo for <a href=\"%@\">%@</a></p>",appstoreUrl,[Common deviceName]];
        [bodyHtml appendString:mailSignature];
    }
    
    NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body> %@</body></html>",bodyHtml];
    return html;
}
-(NSString *)getformsHtmlContentisFromPrint:(BOOL)fromPrint{
    
    
    NSArray *linksArray =links;
    NSMutableString *bodyHtml=[NSMutableString string];
    if (selectedSubTopic.heading.length) {
        NSString *linkHtml =[NSString stringWithFormat:@"<p> %@ </p>",selectedSubTopic.heading];
        [bodyHtml appendString:linkHtml];
    }
    
    if (!fromPrint) {
        for (int i=0; i<linksArray.count; i++) {
            
            Links *link = [links objectAtIndex:i];
            NSString *linkText =link.linktext;
            NSString *linkHtml =[NSString stringWithFormat:@"<p><a href=\"%@\">%@</a></p>",link.linkUrl,linkText];
            [bodyHtml appendString:linkHtml];
            
        }
    }else{
        for (int i=0; i<linksArray.count; i++) {
            
            Links *link = [links objectAtIndex:i];
            NSString *linkText =link.linktext;
           NSString *linkHtml =[NSString stringWithFormat:@"<p>%@ - <a href=\" %@ /\"> %@</a> </p>",linkText,link.linkUrl,link.linkUrl];
            [bodyHtml appendString:linkHtml];
            
        }
        
    }
    
    if (!fromPrint) {
        NSURL *appstoreUrl = [NSURL URLWithString:APP_STORE_URL];
        NSString *mailSignature =[NSString stringWithFormat:@"<p> Sent from Immigo for <a href=\"%@\">%@</a></p>",appstoreUrl,[Common deviceName]];
        [bodyHtml appendString:mailSignature];
    }
    
    NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body> %@</body></html>",bodyHtml];
    return html;
}

-(IBAction)emailButtonTapped:(id)sender{
    
    
    NSString *messageBody =[self getHtmlContentWithSubTopic:selectedSubTopic isFromPrint:NO];
    
  

        if ([MFMailComposeViewController canSendMail])
    {
        
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setMessageBody:messageBody isHTML:YES];
        NSString *appname =@"IMMIGO";
        NSString *subject =[NSString stringWithFormat:@"%@ - %@ - %@",appname,selectedSubTopic.topic.topicTitle,selectedSubTopic.subtopicTitle];
        
        [controller setSubject:subject];
        
        if ([self respondsToSelector:@selector(presentViewController:animated:completion:)])
        {
            UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
            UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
            titlView.image = titleLogo;
            controller.navigationItem.titleView = titlView;
            [[controller navigationBar] sendSubviewToBack:titlView];
            [[controller navigationBar] setTintColor:[UIColor blueColor]];
            [self presentViewController:controller animated:YES completion:nil];
            
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No email account is setup to send mail." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles: nil];
        [alert show];
        
    }
}


-(NSString *)getHtmlContentWithSubTopic:(SubTopic *)selTopic isFromPrint:(BOOL)fromPrint{
    
    NSString *returnigHtml =nil;
    if ([selTopic.contentType isEqual:@"Text"])
    {
        //get webview content
       returnigHtml= [self getwebviewHtmlisFromPrint:fromPrint];
    }
    else if ([selTopic.contentType isEqual:@"Checklist"])
    {
       //get checklist Html
        returnigHtml =[self getCheckListHtmlisFromPrint:fromPrint];
        
    }
    else if ([selTopic.contentType isEqual:@"Links"])
    {
       //get links table data
        returnigHtml =[self getformsHtmlContentisFromPrint:fromPrint];
    
    }

    return returnigHtml;
}
-(IBAction)printButtonTapped:(UIButton*)sender{
    
    NSString *str =[self getHtmlContentWithSubTopic:selectedSubTopic isFromPrint:YES];
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    pic.delegate = self;
    
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    printInfo.jobName = [NSString stringWithFormat:@"%@-%@",selectedSubTopic.topic.topicTitle,selectedSubTopic.subtopicTitle];
    pic.printInfo = printInfo;
    
    UIMarkupTextPrintFormatter *htmlFormatter = [[UIMarkupTextPrintFormatter alloc]
                                                 initWithMarkupText:str];
    htmlFormatter.startPage = 0;
    htmlFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
    pic.printFormatter = htmlFormatter;
    pic.showsPageRange = YES;
    
    void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
    ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
        if (!completed && error) {
            NSLog(@"Printing could not complete because of error: %@", error);
        }
    };
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        //[pic presentFromBarButtonItem:(UIBarButtonItem *)sender animated:YES completionHandler:completionHandler];
        [pic presentFromRect:sender.bounds inView:sender animated:YES completionHandler:completionHandler];
    } else {
        [pic presentAnimated:YES completionHandler:completionHandler];
    }
    

}
-(IBAction)LinkedInTapped:(id)sender
{
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Trainings & Events" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    [self shareToSocialNetworks:@"LinkedIn"];
}

-(IBAction)TwitterTapped:(id)sender
{
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Trainings & Events" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    [self shareToSocialNetworks:@"Twitter"];
}
-(void) shareToSocialNetworks:(NSString *)sharetype{
    
    NSString *sharingContentForSocialMedia =[NSString stringWithFormat:@"%@/%@",selectedSubTopic.topic.topicTitle,
                                             selectedSubTopic.subtopicTitle];
    
    NSString *appstoreUrl =[NSString stringWithFormat:@" Download the Immigo App! %@",APP_STORE_COMMON_LINK];
    
  //  NSURL *mypostUrl =[NSURL URLWithString:@"https://itunes.apple.com/in/app/immigo/id891595380"];
      NSURL *mypostUrl =[NSURL URLWithString:APP_STORE_COMMON_LINK];
    NSString *appStoreLinkForLinkedIn =[NSString stringWithFormat:@" Download the Immigo App!  %@",[mypostUrl absoluteString]];
    
    if ([sharetype isEqualToString:@"LinkedIn"]) {
        //NSLog(@"linkedin was pressed");
        
        [shareObj linkedInLogIn:appStoreLinkForLinkedIn andText:sharingContentForSocialMedia fromViewContoller:self];
        
    }
    else if([sharetype isEqualToString:@"Twitter"]){
        
        NSString *header = sharingContentForSocialMedia;
        NSString *url =appstoreUrl;
        NSString *topicString=@"";
        NSString *subTopic=@"";
        NSString *downloadPrefix =@" Download the Immigo App!";
        int totalLength =140;
        int remainigLength =totalLength-(url.length +downloadPrefix.length);
        NSArray *listItems = [sharingContentForSocialMedia componentsSeparatedByString:@"/"];
        if (listItems && [listItems count]>0) {
            topicString =[listItems objectAtIndex:0];
            subTopic =[listItems objectAtIndex:1];
        }
        
        if (header.length >remainigLength) {
            //Pass Only SubTopic
            sharingContentForSocialMedia =subTopic;
        }
        [shareObj tweetTapped:[mypostUrl absoluteString] andText:[NSString stringWithFormat:@"%@ \nDownload the Immigo App! ",sharingContentForSocialMedia] fromViewContoller:self];
        
    }
    else if  ([sharetype isEqualToString:NSLocalizedString(@"FaceBook", @"")]){
        //NSLog(@"fb was pressed");
       // [shareObj fbShareBtnTapped:APP_STORE_URL andText:sharingContentForSocialMedia fromViewContoller:self];
        [shareObj fbShareBtnTapped:[mypostUrl absoluteString] andText:[NSString stringWithFormat:@"%@ \nDownload the Immigo App! ",sharingContentForSocialMedia] fromViewContoller:self];
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
//    NSString *String = @"A person who has U.S. citizenship or a \"green card\" (legal permanent residency) can help some family members get a green card too. A citizen can apply for a spouse or children. A citizen who is at least 21 years old can apply for parents, sisters, and brothers. A legal permanent resident can apply for a spouse or unmarried child.The rules for getting a green card through a family member depend on immigration status, age, relationship, marital status, trips outside the U.S., how a person came into the U.S., and more. Some cases are fast, and others have to wait for years to get an immigrant visa. Some people can wait in the U.S. for the process, but others have to leave for an interview at a consulate. There are special rules for U.S. military families.";

    if (tableView.tag == 2)
    {
        if (indexPath.row == 0 && selectedSubTopic.heading != nil)
        {
            return [self frameForLabel:linkHeading WithText:selectedSubTopic.heading].size.height+40;
//            return [self frameForLabel:linkHeading WithText:String].size.height+20;
        }
        else
        {
            return 70.0f;
        }
    }
    return 50.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //(_subtopic.heading != nil) ? links.count+1 : links.count;
    
    if (tableView.tag == 1)
    {
        return subTopicsArray.count;
    }
    else
    {
        return (selectedSubTopic.heading != nil) ? links.count+1 : links.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 1)
    {
        SubTopic *subtopic = [subTopicsArray objectAtIndex:indexPath.row];
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.highlightedTextColor = UIColorFromRGB(UI_BLUE_TEXT_COLOR);
        cell.textLabel.text = subtopic.subtopicTitle;//[subTopicsArray objectAtIndex:indexPath.row];//[NSString stringWithFormat:@"SubTopic SubTopic SubTopic SubTopic SubTopic SubTopic SubTopic SubTopic SubTopic SubTopic %ld",indexPath.row+1];
        cell.textLabel.font = [UIFont fontWithName:FONT_IPAD size:22.0f];
        cell.textLabel.textColor = [UIColor darkGrayColor];
        //    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];
        if (indexPath == selectedIndexpath)
        {
            cell.textLabel.textColor =UIColorFromRGB(UI_BLUE_TEXT_COLOR);
        }
        if (indexPath.row == 0 && subTopicsArray.count > 0)
        {
            [self tableView:tableView didSelectRowAtIndexPath:indexPath];
            cell.textLabel.textColor =UIColorFromRGB(UI_BLUE_TEXT_COLOR);
        }
        return cell;
    }
    else
    {
//        Links *link = [links objectAtIndex:indexPath.row];
        
        UITableViewCell *cell = [linksTableView dequeueReusableCellWithIdentifier:@"LinkCell"];
        
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"LinkCell"];
        }
        
        if (indexPath.row == 0 && selectedSubTopic.heading !=nil)
        {
            cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
            cell.textLabel.font = [UIFont fontWithName:FONT_ITALIC size:16.91];
            cell.textLabel.numberOfLines =0;
            cell.textLabel.textColor = UIColorFromRGB(0x4c4c4c);
            cell.textLabel.backgroundColor  = [UIColor clearColor];
            cell.textLabel.text =  selectedSubTopic.heading;
            cell.textLabel.textAlignment = NSTextAlignmentLeft;
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        else
        {
            Links *link = (selectedSubTopic.heading == nil) ? [links objectAtIndex:indexPath.row]:[links objectAtIndex:indexPath.row-1];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            //    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.textColor = UIColorFromRGB(UI_BLUE_TEXT_COLOR);
            cell.textLabel.text = link.linktext;//[subTopicsArray objectAtIndex:indexPath.row];//[NSString stringWithFormat:@"SubTopic SubTopic SubTopic SubTopic SubTopic SubTopic SubTopic SubTopic SubTopic SubTopic %ld",indexPath.row+1];
            //        cell.textLabel.textColor = [UIColor colorWithRed:(25.0f/255.0f) green:(141.0f/255.0f) blue:(156.0f/255.0f) alpha:1];
            cell.textLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:18.0f];
            cell.detailTextLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:15.0f];
            
            cell.detailTextLabel.text = link.linkDescription;
            cell.detailTextLabel.textColor = [UIColor grayColor];
        }
        return cell;

    }
    return nil;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 1) // THis is left side tableview
    {
        if ([selectedIndexpath isEqual:indexPath])
        {
            return; //Not to select the same again.
        }
       
        if (selectedIndexpath)
        {
            UITableViewCell* theCell = [tableView cellForRowAtIndexPath:selectedIndexpath];
            theCell.selectionStyle = UITableViewCellSelectionStyleNone;
            theCell.textLabel.textColor = [UIColor darkGrayColor];
        }//else
          selectedIndexpath = indexPath;
        {
            UITableViewCell* theCell = [tableView cellForRowAtIndexPath:indexPath];
            theCell.selectionStyle = UITableViewCellSelectionStyleNone;
            theCell.textLabel.textColor = UIColorFromRGB(UI_BLUE_TEXT_COLOR);
        }
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        
        
        
        selectedSubTopic = [subTopicsArray objectAtIndex:indexPath.row];
        subTopicName.font = [UIFont fontWithName:FONT_IPAD size:22.0f];
        subTopicName.text = selectedSubTopic.subtopicTitle;
        [self hideAndEnableDetailControlsWrt:selectedSubTopic];
        //    toVC.topicTitle = topic.topicTitle;
    }
    else
    {
        if (indexPath.row == 0 && selectedSubTopic.heading != nil)
        {
            return;
        }

        if (![Common currentNetworkStatus])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Immigration Basics" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
            [alertView show];
            return;
        }
//        Links *selectedLink = [links objectAtIndex:indexPath.row];
        Links *selectedLink = (selectedSubTopic.heading == nil) ? [links objectAtIndex:indexPath.row]:[links objectAtIndex:indexPath.row-1];

        MTPopupWindow *popup = [[MTPopupWindow alloc] init];
        popup.delegate = self;
        popup.fileName = selectedLink.linkUrl;
        //    [popup.webView loadHTMLString:@"<!DOCTYPE html><html><body>    <h1>My First Heading</h1><p>My first paragraph.</p>    </body>    </html>" baseURL:nil];
        [popup show];

    }
}


#pragma mark -Rotation Delegates

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
								duration:(NSTimeInterval)duration {
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    checkListScrollView.contentSize =CGSizeMake(checkListView.frame.size.width, checkListScrollView.contentSize.height);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
										 duration:(NSTimeInterval)duration
{
    
    if (!htmlString || [htmlString isKindOfClass:[NSNull class]])
    {
        [textContentView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    }else{
        
        NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body><p><font face=\"opensans\" color=\"#4c4c4c\" size=\"3\">%@</font></p></body></html>",htmlString];
        [textContentView loadHTMLString:html baseURL:nil];
    }

    [checkListScrollView setContentSize:CGSizeMake(checkListScrollView.frame.size.width, checkListScrollView.contentSize.height)];
    
    [subTopicHeading scrollRangeToVisible:NSMakeRange(0, 2)];

}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    if (fromInterfaceOrientation == UIInterfaceOrientationIsLandscape(fromInterfaceOrientation)||fromInterfaceOrientation == UIInterfaceOrientationPortrait) {
        
        loadingIndicator.center =textContentView.center;
        
    }
}

-(void)hideAndEnableDetailControlsWrt:(SubTopic *)selecteSubTopic
{
    
    if ([selecteSubTopic.contentType isEqual:@"Text"])
    {
        checkListView.hidden = YES;
        linksTableView.hidden = YES;
        textContentBg.hidden = NO;
        if (selectedSubTopic.heading != nil)
        {
            subTopicHeading.hidden = NO;
            CGRect textContentFrame = textContentView.frame;
            textContentFrame.origin.y =subTopicHeading.frame.origin.y+subTopicHeading.frame.size.height;
            textContentFrame.size.height =textContentBg.frame.size.height - textContentView.frame.origin.y;
            textContentView.frame = textContentFrame;
            
            subTopicHeading.text = selectedSubTopic.heading;
        }
        else
        {
            subTopicHeading.hidden = YES;
            CGRect textContentFrame = textContentView.frame;
            textContentFrame.origin.y =0;
            textContentFrame.size.height =textContentBg.frame.size.height;
            textContentView.frame = textContentFrame;
        }
        
        NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body><p><font face=\"opensans\" color=\"#4c4c4c\" size=\"3\">%@</font></p></body></html>",selecteSubTopic.textContent];
        htmlString =html;
        [textContentView loadHTMLString:html baseURL:nil];
    }
    else if ([selecteSubTopic.contentType isEqual:@"Checklist"])
    {
        checkLists = [[CheckListModel sharedDataSource] fetchCheckListItemsForSubtopic:selecteSubTopic];
        
        textContentBg.hidden = YES;
        checkListView.hidden = NO;
        linksTableView.hidden = YES;
        if ([checkListScrollView subviews].count > 0)
        {
            [[checkListScrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        }
        
        [self createCheckListItemsWithSubtopic:selecteSubTopic];

    }
    else if ([selecteSubTopic.contentType isEqual:@"Links"])
    {
//        NSString *String = @"A person who has U.S. citizenship or a \"green card\" (legal permanent residency) can help some family members get a green card too. A citizen can apply for a spouse or children. A citizen who is at least 21 years old can apply for parents, sisters, and brothers. A legal permanent resident can apply for a spouse or unmarried child.The rules for getting a green card through a family member depend on immigration status, age, relationship, marital status, trips outside the U.S., how a person came into the U.S., and more. Some cases are fast, and others have to wait for years to get an immigrant visa. Some people can wait in the U.S. for the process, but others have to leave for an interview at a consulate. There are special rules for U.S. military families.";
        
        textContentBg.hidden = YES;
        checkListView.hidden = YES;
        linksTableView.hidden = NO;
     
        links = [[LinksModel sharedDataSource] fetchLinksForSubtopic:selecteSubTopic];
        
        if (selecteSubTopic.heading)
        {
            linkHeading = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, self.view.frame.size.width-40, 20)];
            linkHeading.lineBreakMode = NSLineBreakByWordWrapping;
            linkHeading.font = [UIFont fontWithName:FONT_ITALIC size:16.91];
            linkHeading.frame = [self frameForLabel:linkHeading WithText:selecteSubTopic.heading];
//            linkHeading.frame = [self frameForLabel:linkHeading WithText:String];

            //    questionText.frame = [self frameForLabel:questionText WithText:@"dfhvdhjsfgsdh gdsgsdhj bghbhjdbhbdhsbgkjsdbg kjsdbgjkdsbgkj bdsjkgbdsjkgbsdkjbgsdkjbgkjsdbgkj dsgkjds bgkjdsbgj dbsgkjdsbgjksdb kgjbdskjgbdksjbg jksdbgkj bdsk jgbdskjbg kjdsbgkjdsbgkjsdbg jkbsdkjgbsdlkngpsdriehoisdojbgioewhtgiweknfgskjdanglngljdshaiepniohaong;ldsjaprje"];
            linkHeading.numberOfLines =0;
            linkHeading.textColor = UIColorFromRGB(0x4c4c4c);
            linkHeading.backgroundColor  = [UIColor clearColor];
            linkHeading.text = selecteSubTopic.heading;
            linkHeading.autoresizingMask =  UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
            linkHeading.textAlignment = NSTextAlignmentLeft;
        }
        
        [linksTableView reloadData];
    }
    
    BOOL  canShowScrollIndicator=[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !canShowScrollIndicator;

}

-(BOOL)canShowScrollIndicator{
    
    BOOL showScroll =NO;

    //NSLog(@"Selected subtopic type:%@",selectedSubTopic.contentType);
    if ([selectedSubTopic.contentType isEqual:@"Text"])
    {
        
        //Visible view is Webview
        
        showScroll =textContentView.scrollView.frame.size.height < textContentView.scrollView.contentSize.height;
        return showScroll;
        
    }
    else if ([selectedSubTopic.contentType isEqual:@"Checklist"])
    {
       //Visibleview is Checklist view
        showScroll = checkListScrollView.frame.size.height < checkListScrollView.contentSize.height;
        return showScroll;
        
    }
    else if ([selectedSubTopic.contentType isEqual:@"Links"])
    {
       //visible view is links table
        
        showScroll = linksTableView.contentSize.height > linksTableView.bounds.size.height;
        return showScroll;
    
    }
    return showScroll;
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    BOOL toEnd =NO;
    if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
        //reach bottom
        NSLog(@"we are at the endddd");
        toEnd =YES;
    }
    
    if (scrollView.contentOffset.y < 0){
        //reach top
        NSLog(@"we are at the top");
        toEnd =NO;
    }
    
    if (scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y < (scrollView.contentSize.height - scrollView.frame.size.height)){
        //not top and not bottom
    }
    
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    scrollIndicatorImgView.image =toEnd?[UIImage imageNamed:@"scrollIndicator_up"]:[UIImage imageNamed:@"scrollIndicator_down"];
   
}

#pragma mark - CreationOf ChecklistItems
- (void)createCheckListItemsWithSubtopic:(SubTopic *)_subtopic
{
        float optionsGap = 5.0f;
    
    CGRect totalFrame = checkListView.frame;
    CGPoint cursor = CGPointMake(5.0f, 5.0f);
    checkListView.autoresizingMask =UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleLeftMargin;
    
    CGSize size = CGSizeMake(totalFrame.size.width-10.0f, totalFrame.size.height-cursor.y);
    
    UIImage *offIcon = [UIImage imageNamed:@"checkbox"];
    UIImage *onIcon = [UIImage imageNamed:@"checkbox_selected"];

    UILabel * questionText=nil;
    
    if (!questionText)
        questionText = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, size.width-10, 20)];
    questionText.lineBreakMode = NSLineBreakByWordWrapping;
    questionText.font = [UIFont fontWithName:FONT_ITALIC size:16.91];
    questionText.frame = [self frameForLabel:questionText WithText:_subtopic.heading];
    questionText.numberOfLines =0;
    questionText.textColor = UIColorFromRGB(0x4c4c4c);
    questionText.backgroundColor  = [UIColor clearColor];
    questionText.text = _subtopic.heading;
    questionText.autoresizingMask =  UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
    questionText.textAlignment = NSTextAlignmentLeft;
    [checkListScrollView addSubview:questionText];
    cursor.y += questionText.frame.size.height+30;

    int  numberOfOptions = checkLists.count;
    CGFloat scrollViewHeight= checkListScrollView.contentSize.height;
    for (int i=1; i<=numberOfOptions; i++)
    {
        CheckListItems *checkListItem = [checkLists objectAtIndex:i-1];
        
        //        CGFloat buttonHeight = option1.frame.size.height<44.0 ? 44.0f:option1.frame.size.height;
        UILabel *option1;
        if (!option1)
            option1 = [[UILabel alloc] initWithFrame:CGRectMake(20+offIcon.size.width, cursor.y+3, size.width*0.9, size.height*0.25)];
        option1.lineBreakMode = NSLineBreakByWordWrapping;
        option1.font = [UIFont fontWithName:FONT_LIGHT size:20.0];
        option1.frame = [self frameForLabel:option1 WithText:checkListItem.title];
        option1.textColor = [UIColor darkGrayColor];//UIColorFromRGB(0x5d5d5d);
        option1.numberOfLines =0;
        option1.autoresizingMask =  UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
        option1.backgroundColor  = [UIColor clearColor];
        option1.text = checkListItem.title;
        option1.tag = 10*i;
        option1.textAlignment = NSTextAlignmentLeft;
        
        
        UIButton *optionButton1= [UIButton buttonWithType:UIButtonTypeCustom];
        optionButton1.frame = CGRectMake(0, cursor.y, totalFrame.size.width-20, option1.frame.size.height+6);
        optionButton1.imageEdgeInsets = UIEdgeInsetsMake(0,+05, 0, 0);
        [optionButton1 addTarget:self action:@selector(checkListSelected:) forControlEvents:UIControlEventTouchUpInside];
//        [optionButton1 setBackgroundImage:[optionBG stretchableImageWithLeftCapWidth:optionBG.size.width/2 topCapHeight:optionBG.size.height/2] forState:UIControlStateNormal];
        [optionButton1 setImage:offIcon forState:UIControlStateNormal];
        [optionButton1 setImage:onIcon forState:UIControlStateHighlighted];
        [optionButton1 setImage:onIcon forState:UIControlStateSelected];
        optionButton1.titleLabel.backgroundColor = [UIColor clearColor];
        optionButton1.tag = checkListItem.key;
        optionButton1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        optionButton1.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
        optionButton1.imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [optionButton1 setSelected:checkListItem.isChecked] ;
        
        
        [checkListScrollView addSubview:optionButton1];
        [checkListScrollView addSubview:option1];
        
        cursor.y +=optionButton1.frame.size.height+optionsGap;
        
        if (numberOfOptions == i)
        {
            scrollViewHeight = optionButton1.frame.origin.y +optionButton1.frame.size.height +90;
        }
    }
    //    [bgView addSubview:scrollView];
    //    [bgView bringSubviewToFront:scrollView];
    checkListScrollView.contentSize = CGSizeMake(totalFrame.size.width, scrollViewHeight);

    [checkListScrollView flashScrollIndicators];
}
-(IBAction)checkListSelected:(UIButton *)sender
{
    sender.selected = !sender.selected;
    
    [[CheckListModel sharedDataSource] selectTheCheckListItemWithKey:(int)sender.tag AndSubTopicID:selectedSubTopic.subTopicId IfSelected:sender.selected];
}


-(void)webViewDidStartLoad:(UIWebView *)webView
{
    
    loadingIndicator.hidden = NO;
    [loadingIndicator startAnimating];
//    textContentView.scalesPageToFit = YES;
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    //webView.scalesPageToFit = YES;
//    webView.scrollView.delegate = self; // set delegate method of UISrollView
//    webView.scrollView.maximumZoomScale = 10; // set as you want.
//    webView.scrollView.minimumZoomScale = 0; // set as you want.
//
//    [textContentView stringByEvaluatingJavaScriptFromString:@"document.body.style.zoom = 2.0;"];
    loadingIndicator.hidden = YES;
    [loadingIndicator stopAnimating];
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    //NSLog(@"Error : %@",error);
    
    loadingIndicator.hidden = YES;
    [loadingIndicator stopAnimating];
    
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale; // scale between minimum and maximum. called after any 'bounce' animations

{
//    textContentView.scrollView.minimumZoomScale = 0; // set similar to previous.
//    textContentView.scrollView.maximumZoomScale = 10; // set similar to previous.
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked){
        
        
        if ([request.URL.scheme isEqualToString:@"tel"])
        {
            
            return YES;
        }
        
        else if ([request.URL.scheme isEqualToString:@"mailto"])
        {
            if ([request.URL isMailtoRequest])
            {
                
                [MFMailComposeViewController presentModalComposeViewControllerWithURL:request.URL  delegate:self];
                return NO;
                
            }
            
        }
        
        else {
        if (![Common currentNetworkStatus])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Immigration Basics" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
            [alertView show];
            return NO;
        }
        
        NSString *linkUrl =[request.URL absoluteString];
        //    NSURL *myURL;
        if ([linkUrl hasPrefix:@"http://"] || [linkUrl hasPrefix:@"https://"])
        {
            
        }
        else
        {
            linkUrl = [NSString stringWithFormat:@"http://%@",linkUrl];
        }
            
            if ([Common isValidUrl:linkUrl])
            {
                MTPopupWindow *popup = [[MTPopupWindow alloc] init];
                popup.delegate = self;
                popup.fileName = linkUrl;
                [popup show];
                return NO;
            }else
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"About Immigo" message:@"Invalid Url" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
                [alertView show];
                
            }
            
        
        }
        
    }
    return YES;
    
    
}

#pragma mark - MFMail Composer Delegate methods
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	switch (result)
	{
		case MFMailComposeResultCancelled:
		{
			break;
		}
		case MFMailComposeResultSaved:
		{
			break;
		}
		case MFMailComposeResultSent:
		{
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Email sent successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            //			[alert show];
			break;
		}
		case MFMailComposeResultFailed:
		{
			break;
		}
		default:
		{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"EMAIL_TITLE", @"") message:@"Email Failed" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles: nil];
			[alert show];
            
		}
			break;
	}
	if ([self respondsToSelector:@selector(dismissModalViewControllerAnimated:)]) {
        [self performSelector:@selector(dismissModalViewControllerAnimated:) withObject:[NSNumber numberWithBool:YES]];
    } else {
        [controller dismissViewControllerAnimated:YES completion:nil];
    }
	
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

-(CGRect)frameForLabel:(UILabel *)label WithText:(NSString *)text
{
    CGSize maximumLabelSize = CGSizeMake(label.frame.size.width, FLT_MAX);
    CGSize expectedLabelSize = [text sizeWithFont:label.font constrainedToSize:maximumLabelSize lineBreakMode:label.lineBreakMode];
    //adjust the label the the new height.
    CGRect newFrame = label.frame;
    newFrame.size.height = expectedLabelSize.height<44.0f ? 44.0f :expectedLabelSize.height;
    //    label.frame = newFrame;
    
    return newFrame;
}

- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
