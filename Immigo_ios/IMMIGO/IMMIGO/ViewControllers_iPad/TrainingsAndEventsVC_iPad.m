//
//  TrainingsAndEventsVC_iPadViewController.m
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "TrainingsAndEventsVC_iPad.h"
#import "GridView.h"
#import "EventsModel.h"
#import "TrainingTileView_iPad.h"
#import "Event.h"
#import "Common.h"
#import "TrainigsAndEventsDetail_iPad.h"
#import "AppDelegate.h"

@interface TrainingsAndEventsVC_iPad ()<DataDownloaderDelegate>{
    
    GridView *itemsGrid;
    NSArray *trainings;
    
    IBOutlet UIImageView *scrollIndicatorImgView;
    IBOutletCollection(UIImageView) NSArray *backButton;
   
    __unsafe_unretained IBOutlet UILabel *titleLabel;
    __weak IBOutlet UIImageView *dividerImageView;
}

@end

@implementation TrainingsAndEventsVC_iPad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[DataDownloader sharedDownloader] setDelegate:self];
//    [[DataDownloader sharedDownloader] cancelAllRequests];
    
    [[EventsModel sharedDataSource] fetchTrainings];

    [(AppDelegate *)[[UIApplication sharedApplication] delegate]refreshFakeViewController];
    
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;

}
- (void)viewDidLoad
{
    [super viewDidLoad];
     titleLabel.font = [UIFont fontWithName:FONT_IPAD size:22.0f];
    titleLabel.textColor= UIColorFromRGB(UI_TRAINING_HEADER_TEXT_COLOR_IPAD);
    titleLabel.textColor = [UIColor lightGrayColor];
    
    UIImage *divider = [UIImage imageNamed:@"menuTitleDivider"];
    dividerImageView.image = [divider stretchableImageWithLeftCapWidth:divider.size.width/2 topCapHeight:divider.size.height/2];

    [Common logEventToGoogleAnalytics:@"Immigo Trainings & Events Screen"];
    //    }

    itemsGrid = [[GridView alloc] initWithFrame:CGRectMake(-20, 150, self.view.frame.size.width+10, self.view.frame.size.height-300)];
    itemsGrid.backgroundColor = [UIColor clearColor];
    itemsGrid.gridViewDelegate = self;
    itemsGrid.tileSizePortrait = CGSizeMake(250,100);
    itemsGrid.tileSizeLandScape = CGSizeMake(350,100);
    itemsGrid.tiles_gap = CGSizeMake(78, 10);
    [itemsGrid reloadData];
    itemsGrid.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    
    [self.view addSubview:itemsGrid];

}


-(BOOL)canShowScrollIndicator{
    CGFloat gridViewFrameHeight =itemsGrid.frame.size.height;
    float scrollContentSizeHeight = itemsGrid.contentScrollView.contentSize.height;
    return scrollContentSizeHeight > gridViewFrameHeight;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)backButtonAction:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma Mark #gridView delgate methods

- (int)numberOfTilesIngridView:(GridView *)gridView
{
    return (int)trainings.count;
}
- (UIView *)gridView:(GridView *)gridView viewAtIndex:(int)index
{
    Event *event = [trainings objectAtIndex:index];
    
    TrainingTileView_iPad *favTileView = [[TrainingTileView_iPad alloc] initWithFrame:CGRectMake(0, 0, itemsGrid.tileSizeLandScape.width, itemsGrid.tileSizeLandScape.height)];
    [favTileView setEvent:event];
    return favTileView;
}

-(void)gridView:(GridView*) gridView didSelectTileAtIndex:(int) index
{
    Event *event =[trainings objectAtIndex:index];
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    
    TrainigsAndEventsDetail_iPad* toVC = [storyboard instantiateViewControllerWithIdentifier:@"TrainigsAndEventsDetail_iPad"];
    toVC.selectedEvent = event;
        
    [self.navigationController pushViewController:toVC animated:YES];

    
}
-(void)gridView:(GridView*) gridView didSelectedTile:(UIView*) view
{
    

}

-(void)gridViewshouldChangeScrollIndicators:(BOOL)toEnd{
    
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    scrollIndicatorImgView.image =toEnd?[UIImage imageNamed:@"scrollIndicator_up"]:[UIImage imageNamed:@"scrollIndicator_down"];
}
- (void)gridRefresh
{
    [itemsGrid reloadData];
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
}


-(void)TrainingsLoaded
{
    trainings = [[EventsModel sharedDataSource] allTrainings];
    [self gridRefresh];
}
-(void)TrainingsLoadFailure
{
    trainings = [[EventsModel sharedDataSource] allTrainings];
    [self gridRefresh];
}

-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
   
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;


}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
