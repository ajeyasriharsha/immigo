//
//  AboutImmigoVC_iPad.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/26/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import "DataDownloader.h"
#import "PartnersPullableView-Ipad.h"

@interface AboutImmigoVC_iPad : UIViewController<MFMailComposeViewControllerDelegate,UITextFieldDelegate,UITextViewDelegate,DataDownloaderDelegate>{
    PullableView *bottompullRightView;

}
@end
