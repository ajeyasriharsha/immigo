//
//  FakeViewController_iPad.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/30/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "PartnersPullableView.h"
#import <CoreLocation/CoreLocation.h>

@interface FakeViewController_iPad : UIViewController<NIDropDownDelegate,UIWebViewDelegate,CLLocationManagerDelegate,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UIPopoverControllerDelegate,UITextFieldDelegate>
{
    NIDropDown *dropDown;
    UIControl *modalView;
    

    BOOL pageControlUsed;
    PartnersPullableView *pullUpView;
    CLLocationManager *locationManager;
    IBOutlet UIView *legalSearchView;
    IBOutlet UIView *legalAdvancedSearchView;
    IBOutlet UITextField *zipCodeField;
    IBOutlet UIButton *backButton;
    IBOutlet UILabel *advancedSearchCurrentLocationLbl;
    IBOutlet UITextField *advancedSearchZipCodeTf;
    IBOutlet UIButton *aolBtn;
    IBOutlet UIButton *typesofAssistanceBtn;
    IBOutlet UIButton *languageBtn;
    UIPopoverController *popoverController;
    NSMutableArray *dataArray;
    NSMutableArray *selectedIndexes;
    NSMutableArray *selectedValues;
    NSString *_searchString;
    BOOL isSearchEnabled;
}


@property (nonatomic, retain) UIPopoverController *popoverController;

@property (nonatomic,readonly) UIView *legalSearchView;
@property (nonatomic,readonly) UIView *legalAdvancedSearchView;
@property (nonatomic,readonly) UITextField *zipCodeField;
@property (strong,nonatomic) NIDropDown *dropDown;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIButton *bigMenuBtn;
@property (strong, nonatomic) IBOutlet UIPageControl *alertsPageControl;
@property (strong, nonatomic) IBOutlet UIScrollView *alertsScrollView;
@property (strong, nonatomic) IBOutlet UIView *alertsView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *homeButton;
-(void)reloadAlerts;
- (IBAction)tappedOnMenu:(id)sender;
@end
