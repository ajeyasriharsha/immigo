//
//  LegalSearchDetailVC_iPad.h
//  IMMIGO
//
//  Created by pradeep ISPV on 6/5/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
@interface LegalSearchListVC_iPad : UIViewController
{
    NSMutableArray *arr;
}
@property (nonatomic) CLLocationCoordinate2D coordinates;
@property (nonatomic,strong) NSString *zipCode;
@property (nonatomic,assign) BOOL isFromAdvacnedSearch;

@end
