//
//  ImmigrationNews_iPadVC.h
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataDownLoader.h"
#import "GridView.h"
@interface ImmigrationNews_iPadVC : UIViewController <MARGridViewDelegate,DataDownloaderDelegate>

@end
