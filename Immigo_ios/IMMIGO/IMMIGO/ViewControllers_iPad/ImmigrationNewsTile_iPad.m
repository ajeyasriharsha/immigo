//
//  ImmigrationNewsTile_iPad.m
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "ImmigrationNewsTile_iPad.h"
#import "Categories.h"
#import "Common.h"
@implementation ImmigrationNewsTile_iPad

-(void)customIntialization
{
    {
        self.clipsToBounds = YES;
        self.backgroundColor  =[UIColor clearColor];
        //        self.layer.borderColor  =[UIColor grayColor].CGColor;
        //        self.layer.borderWidth = 1.0f;
    }
    
    {
        topicTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, self.frame.size.width-10, self.frame.size.height*0.6)];
        topicTitle.autoresizingMask =UIViewAutoresizingFlexibleWidth;
        topicTitle.backgroundColor = [UIColor clearColor];
        topicTitle.numberOfLines = 2;
        topicTitle.font = [UIFont fontWithName:FONT_IPAD size:18.0f];
        topicTitle.textColor = [UIColor darkGrayColor];
    
        subtopicTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, topicTitle.frame.size.height+topicTitle.frame.origin.y, self.frame.size.width/2, self.frame.size.height*0.3)];
        subtopicTitle.autoresizingMask =UIViewAutoresizingFlexibleWidth;
        subtopicTitle.backgroundColor = [UIColor clearColor];
        subtopicTitle.alpha = 1;
        
        subtopicTitle.font = [UIFont fontWithName:FONT_SEMIBOLD size:10.0f];
        subtopicTitle.textColor = [UIColor redColor];
        
        seperatorimageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-1, self.frame.size.width, 1)];
        UIImage *textFiledBackGround = [UIImage imageNamed:@"menuTitleDivider"];
        seperatorimageView.image = [textFiledBackGround stretchableImageWithLeftCapWidth:textFiledBackGround.size.width/2 topCapHeight:textFiledBackGround.size.height/2];

        //        imgaeView.image = [image Stre];
        seperatorimageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        seperatorimageView.backgroundColor =[UIColor clearColor];
        
        
        [self addSubview:topicTitle];
        [self addSubview:subtopicTitle];
        [self addSubview:seperatorimageView];
        
        
    }
}

-(IBAction)shareMenuPressed:(id)sender{
    
//   UIView * popView=[[UIView alloc]initWithFrame:CGRectMake(10, 20, self.frame.size.width-20, self.frame.size.height-40)];
//    [popView setBackgroundColor:[UIColor yellowColor]];
//    [self addSubview: popView];
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        [self customIntialization];
    }
    return self;
}



-(void)setNews:(News *)news{
    
    _selectedNews =news;
    
    NSRange range = [_selectedNews.origanization rangeOfString:@"^\\s*" options:NSRegularExpressionSearch];
    NSString *trimmedTitle = [_selectedNews.origanization stringByReplacingCharactersInRange:range withString:@""];
    
    NSString *title = [trimmedTitle length] > 35 ? [[trimmedTitle substringToIndex:35] stringByAppendingString:@"..."] :trimmedTitle;
    
    topicTitle.text = [news.title trimmedString];
    subtopicTitle.text = [NSString stringWithFormat:@"%@ | %@",title,[_selectedNews.date listDateFormat]];
    
    
    
    
    
}
-(News *)News{
    
    return _selectedNews;
}




@end
