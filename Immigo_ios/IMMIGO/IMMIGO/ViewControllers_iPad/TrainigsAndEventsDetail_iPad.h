//
//  TrainigsAndEventsDetail_iPadViewController.h
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>
#import "Event.h"
#import <EventKit/EventKit.h>
#import <MessageUI/MessageUI.h>
#import "ShareToSocialNetworkingSites.h"
#import "Event.h"
@interface TrainigsAndEventsDetail_iPad : UIViewController<MFMailComposeViewControllerDelegate>{
    ShareToSocialNetworkingSites *shareObj;
    Event *selectedEvent;
}
@property (nonatomic,strong) Event *selectedEvent;
@property (nonatomic,retain) NSString *selectedEventLocation;
@property (nonatomic, retain) EKEventStore *eventStore;
@property (nonatomic, retain) NSDate *recursiveEventEndDate;

@end
