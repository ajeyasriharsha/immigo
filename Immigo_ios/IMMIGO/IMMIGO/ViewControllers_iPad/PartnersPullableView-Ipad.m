//
//  PartnersPullableView-Ipad.m
//  PullableView
//
//  Created by Ajeya Sriharsha on 6/5/14.
//
//

#import "PartnersPullableView-Ipad.h"
#import <QuartzCore/QuartzCore.h>
#import "Common.h"
#import "UIButton+VerticalLayout.h"
@interface PartnersPullableView_Ipad ()

@end

@implementation PartnersPullableView_Ipad

@synthesize partnersDelegate;
// custom intialization of parterners Tableview
-(void)customIntialization
{
    
    self.backgroundColor = [UIColor clearColor];
    {
        self.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleWidth;
    }
    
    
    UIButton *button;
    UIImage *partnershandle = [UIImage imageNamed:@"partners_iPad"];
    CGPoint originPoint = CGPointMake(partnershandle.size.width, (self.frame.size.width-20)/4);
    CGSize frame =CGSizeMake((self.frame.size.width-20)/4, self.frame.size.height);
    
    UILabel *partnersLabel =[[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width-275, 5, 200,30)];
    [partnersLabel setBackgroundColor:[UIColor clearColor]];
    partnersLabel.font = [UIFont fontWithName:FONT_IPAD size:12.0f];
    partnersLabel.textColor= [UIColor lightGrayColor];
    partnersLabel.text =@"IN PARTNERSHIP WITH";
    partnersLabel.hidden = YES;
    UIImage *parternsImg;
    UIImageView *buttonImageView;
    originPoint.x=20;
    NSArray *imagesArray =[[NSArray alloc] initWithObjects:@"Nuclear_iPad",@"ImmigrationAdvocates_iPad",@"verizon",@"Poweredby_iPad" ,nil];
    for (int i=0; i<4; i++) {
        buttonImageView= [[UIImageView alloc] init];
        button =[UIButton buttonWithType:UIButtonTypeCustom];
      parternsImg = [UIImage imageNamed:[imagesArray objectAtIndex:i]];
        button.frame =(CGRectMake(originPoint.x,0,frame.width, frame.height));
        //button.frame =(CGRectMake(originPoint.x,0,parternsImg.size.width, frame.height));
        buttonImageView.frame =button.frame;
        
        button.tag =i+1;
        [button setBackgroundColor:[UIColor clearColor]];
        [button setShowsTouchWhenHighlighted:NO];
        button.adjustsImageWhenHighlighted = NO;
        button.autoresizingMask =UIViewAutoresizingFlexibleLeftMargin;
        
        
       // buttonImageView.image = [parternsImg stretchableImageWithLeftCapWidth:parternsImg.size.width/2 topCapHeight:parternsImg.size.height/2];
        buttonImageView.image =parternsImg;
        buttonImageView.clipsToBounds =NO;
        [buttonImageView setBackgroundColor:[UIColor whiteColor]];
        buttonImageView.contentMode =UIViewContentModeScaleAspectFit;
        
        [button addTarget:self action:@selector(clickedOnPartners:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [self addSubview:buttonImageView];
        [self addSubview:button];
        [buttonImageView bringSubviewToFront:button];
          //  [self addSubview:button];
        originPoint.x+=button.frame.size.width;
       // originPoint.x+=button.frame.size.width;
        
    }
    
    [self addSubview:partnersLabel];
    [self bringSubviewToFront:partnersLabel];
    UIImage *image = [UIImage imageNamed:@"headerDivider"];
    UIImageView *topDivider = [[UIImageView alloc] initWithFrame:CGRectMake(20, 0, self.frame.size.width-20, image.size.height)];
    topDivider.image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    topDivider.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    topDivider.backgroundColor = [UIColor redColor];
    [self addSubview:topDivider];

    
}

- (UIColor *) getRandomColor {
    return [UIColor colorWithRed:(random()%100)/(float)100 green:(random()%100)/(float)100 blue:(random()%100)/(float)100 alpha:1];
}
-(IBAction)clickedOnPartners:(UIButton *)sender{
    
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Partners" message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    NSString *urlAddress = [NSString stringWithFormat:@"about:blank"];
    switch (sender.tag) {
        case 1:
            //
            urlAddress = PARTNERS_NCLR_URL;
            break;
        case 2:
            //
            urlAddress = PARTNERS_IMMIGRATION_URL;
            break;

        case 3:
            //
            urlAddress = PARTNERS_VERIZON_URL;
            break;

        case 4:
            //
            urlAddress = PARTNERS_PROBONO_URL;
            break;

            
        default:
            break;
    }
    
    MTPopupWindow *popup = [[MTPopupWindow alloc] init];
    popup.delegate = self;
    popup.fileName = urlAddress;
    //    [popup.webView loadHTMLString:@"<!DOCTYPE html><html><body>    <h1>My First Heading</h1><p>My first paragraph.</p>    </body>    </html>" baseURL:nil];
    [popup show];
    
}

-(IBAction)closeButtonTapped:(id)sender
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6];
	
    [UIView setAnimationDelegate: self];
    //    [UIView setAnimationDidStopSelector:
    //     @selector(animationFinished:finished:context:)];
    partnersWebView.hidden = closeButton.hidden =  YES;
	[partnersWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp
                           forView:partnersWebView cache:YES];
    [UIView commitAnimations];
    
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        [self customIntialization];
    }
    return self;
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [activityController startAnimating];
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked){
        
        
        
    }
    return YES;
    
    
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    //NSLog(@"error %@",[error debugDescription]);
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [activityController stopAnimating];
}
@end
