//
//  ImmigrationNews_iPadVC.m
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "ImmigrationNews_iPadVC.h"
#import "NewsModel.h"
#import "Categories.h"
#import "Common.h"
#import "GridView.h"
#import "ImmigrationNewsTile_iPad.h"
#import "ImmigrationNewsDetail_iPadVC.h"
#import "ShareToSocialNetworkingSites.h"
#import "AppDelegate.h"

@interface ImmigrationNews_iPadVC (){
    
    GridView *itemsGrid;
    NSArray *newsList;
    __unsafe_unretained IBOutlet UILabel *selectedTitle;
    
    __unsafe_unretained IBOutlet UIButton *backButton;
    __unsafe_unretained IBOutlet UIImageView *imageLogo;
    IBOutlet UIImageView *dividerImageView;
    NSArray *shareImgArray;
    ShareToSocialNetworkingSites *shareObj;
    int oldShareViewTag;
    IBOutlet UIImageView *scrollIndicatorImgView;
}

@end

@implementation ImmigrationNews_iPadVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    shareObj = [[ShareToSocialNetworkingSites alloc] init];
    oldShareViewTag = -1;
     selectedTitle.font = [UIFont fontWithName:FONT_IPAD size:22.0f];
    
    UIImage *divider = [UIImage imageNamed:@"menuTitleDivider"];
    dividerImageView.image = [divider stretchableImageWithLeftCapWidth:divider.size.width/2 topCapHeight:divider.size.height/2];

    
    itemsGrid = [[GridView alloc] initWithFrame:CGRectMake(-20, 150, self.view.frame.size.width+10, self.view.frame.size.height-300)];
    itemsGrid.backgroundColor = [UIColor clearColor];
    itemsGrid.gridViewDelegate = self;
    itemsGrid.tileSizePortrait = CGSizeMake(250,100);
    itemsGrid.tileSizeLandScape = CGSizeMake(350,100);
    itemsGrid.tiles_gap = CGSizeMake(78, 10);
    [itemsGrid reloadData];
    itemsGrid.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    
    [self.view addSubview:itemsGrid];

    shareImgArray = [NSArray arrayWithObjects:@"linkedin",@"facebook",@"twitter",nil];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[DataDownloader sharedDownloader] setDelegate:self];
//    [[DataDownloader sharedDownloader] cancelAllRequests];
    [[NewsModel sharedDataSource] fetchNews];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate]refreshFakeViewController];
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
}

-(BOOL)canShowScrollIndicator{
    CGFloat gridViewFrameHeight =itemsGrid.frame.size.height;
    float scrollContentSizeHeight = itemsGrid.contentScrollView.contentSize.height;
    return scrollContentSizeHeight > gridViewFrameHeight;
}

-(IBAction)shareMenuPressed:(UIButton *)sender
{
    //NSLog(@"news with Index  %@   %@",[newsList objectAtIndex:[sender tag]-1],sender.superview);
    int tag = 1000 *sender.tag;
    
//    for (UIView *view in [sender.superview subviews])
//    {
//        if ([view isKindOfClass:[UIView class]])
//        {
//            if (view.tag == tag)
//            {
//                //NSLog(@"news Item %@",[newsList objectAtIndex:sender.tag-1]);
//                
//                view.hidden = !view.hidden;
//                return;
//            }
//            
//        }
//    }

    for (UIView *view in [sender.superview subviews])
    {
        if ([view isKindOfClass:[UIView class]])
        {
            //NSLog(@"view tag %d , %d",oldShareViewTag,view.tag);
//            if (view.tag == oldShareViewTag && ![[view viewWithTag:oldShareViewTag] isHidden])
//            {
//                //                if (![[view viewWithTag:oldShareViewTag] isHidden] && oldShareViewTag)
//                //                {
//                [view viewWithTag:oldShareViewTag].hidden = YES;
//                //                }
//            }
             if (view.tag == tag)
            {
                //NSLog(@"news Item %@",[newsList objectAtIndex:sender.tag-1]);
                
                view.hidden = !view.hidden;
                oldShareViewTag = view.tag;
                return;
            }
        }
    }

}


-(void)shareWithOption:(NSString *)shareType andNews:(News *)newsTopic{
    
    
    
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Immigration News" message:NSLocalizedString(@"You are not connected to the Internet.", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        return ;
    }
    
    if ([shareType isEqualToString:@"LinkedIn"]) {
        
        [shareObj linkedInLogIn:newsTopic.newsLink andText:newsTopic.title fromViewContoller:self];
        
        
    }
    else if([shareType isEqualToString:@"Twitter"]){
        
        [shareObj tweetTapped:newsTopic.newsLink andText:newsTopic.title fromViewContoller:self];
        
    }
    else if  ([shareType isEqualToString:@"FaceBook"]){
        
        [shareObj fbShareBtnTapped:newsTopic.newsLink andText:newsTopic.title fromViewContoller:self];
        
    }
    
    
}

#pragma Mark #gridView delgate methods

- (int)numberOfTilesIngridView:(GridView *)gridView
{
    return newsList.count;
}
- (UIView *)gridView:(GridView *)gridView viewAtIndex:(int)index
{
    News *news = [newsList objectAtIndex:index];
    
    ImmigrationNewsTile_iPad *favTileView = [[ImmigrationNewsTile_iPad alloc] initWithFrame:CGRectMake(0, 0, itemsGrid.tileSizeLandScape.width, itemsGrid.tileSizeLandScape.height)];

    UIButton *shareButton =[[UIButton alloc] initWithFrame:CGRectMake(favTileView.frame.size.width-40, favTileView.frame.size.height-30, 20, 20)];
    [shareButton addTarget:self action:@selector(shareMenuPressed:) forControlEvents:UIControlEventTouchUpInside];
    [shareButton setBackgroundImage:[UIImage imageNamed:@"share_on"] forState:UIControlStateNormal];
    [shareButton setBackgroundImage:[UIImage imageNamed:@"share_of"] forState:UIControlStateSelected|UIControlStateHighlighted];
    shareButton.autoresizingMask =UIViewAutoresizingFlexibleLeftMargin;
    shareButton.tag = index+1;
    [favTileView addSubview:shareButton];
    
    UIView *shareView = [[UIView alloc] initWithFrame:CGRectMake(shareButton.frame.origin.x-120, shareButton.frame.origin.y-5, 115, shareButton.frame.size.height)];
    shareView.backgroundColor = [UIColor clearColor];
    shareView.tag = 1000 *(index+1);
    shareView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    float gap = 10.0f;
    CGPoint cursor = CGPointMake(gap, 2.5);
    CGSize size = CGSizeMake(25, 25);
    for(int i = 1; i <= shareImgArray.count; i++)
    {
        UIButton *numberButton = [[UIButton alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, size.height)];
        numberButton.backgroundColor = [UIColor clearColor];
        [numberButton setImage:[UIImage imageNamed:[shareImgArray objectAtIndex:i-1]] forState:UIControlStateNormal];
        numberButton.tag = 333 *i;
        [numberButton addTarget:self action:@selector(shareButtonsTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        [shareView addSubview:numberButton];
        cursor.x +=numberButton.frame.size.width+gap;
    }
    [favTileView addSubview:shareView];
    shareView.hidden = YES;
    
    
    [favTileView setNews:news];
    return favTileView;

}
-(void)gridViewshouldChangeScrollIndicators:(BOOL)toEnd{
    
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    scrollIndicatorImgView.image =toEnd?[UIImage imageNamed:@"scrollIndicator_up"]:[UIImage imageNamed:@"scrollIndicator_down"];
}

-(void)gridView:(GridView*) gridView didSelectTileAtIndex:(int) index
{
    
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Immigration News" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        return ;
    }
    
   News *news = [newsList objectAtIndex:index];
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    
   ImmigrationNewsDetail_iPadVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"ImmigrationNewsDetail_iPadVC"];
    toVC.selectedNews = news;
//    
   [self.navigationController pushViewController:toVC animated:YES];
    
    
}
-(void)gridView:(GridView*) gridView didSelectedTile:(UIView*) view
{
    //NSLog(@"Selected View in Tile:%@",view);
}
- (void)gridRefresh
{
    [itemsGrid reloadData];
    
}

-(IBAction)shareButtonsTapped:(UIButton *)sender
{
    int tag = ([sender superview].tag-1)/1000;
    News *news = [newsList objectAtIndex:tag];
    switch (sender.tag/333)
    {
        case 1:
        {
            //NSLog(@"facebook tapped on index %d and news %@",tag,[newsList objectAtIndex:tag]);
            
            [self shareWithOption:NSLocalizedString(@"LinkedIn", @"") andNews:news];
            //[self shareWithOption:@"FaceBook" andNews:news];

        }
            break;
        case 2:
        {
            //NSLog(@"twitter tapped on index %d and news %@",tag,[newsList objectAtIndex:tag]);
            [self shareWithOption:NSLocalizedString(@"FaceBook", @"") andNews:news];
        }
            break;
        case 3:
        {
            //NSLog(@"linkedin tapped on index %d and news %@",tag,[newsList objectAtIndex:tag]);
            [self shareWithOption:NSLocalizedString(@"Twitter", @"") andNews:news];
        }
            break;

        default:
            break;
    }
}
-(void)NewsLoaded
{
    newsList = [[NewsModel sharedDataSource] allNewsItems];
    
    [itemsGrid reloadData];
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
}
-(void)NewsLoadFailure
{
    newsList = [[NewsModel sharedDataSource] allNewsItems];
    [itemsGrid reloadData];
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backButtonAction:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
