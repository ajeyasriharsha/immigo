//
//  TrainigsAndEventsDetail_iPadViewController.m
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "TrainigsAndEventsDetail_iPad.h"
#import "Event.h"
#import "Common.h"
#import "Categories.h"
#import "NSDate+Helper.h"
#import "AppDelegate.h"
#import "MTPopupWindow.h"
#import "MFMailComposeViewController+URLRequest.h"
@interface TrainigsAndEventsDetail_iPad ()<UIWebViewDelegate,UIScrollViewDelegate,MTPopupWindowDelegate>{
    
    
    
    
    
    IBOutlet UIButton *addtoCal;
    IBOutlet UIButton *heading;
    IBOutlet UIWebView *contentWebView;
    IBOutlet UIScrollView *detailScrollView;
    IBOutlet UILabel *descLabel;
    __weak IBOutlet UILabel *descriptionTitle;
    IBOutlet UIButton *contentBtn;
    
    __unsafe_unretained IBOutlet UIButton *backButton;
    __unsafe_unretained IBOutlet UIImageView *logoImageView;
    __unsafe_unretained IBOutlet UIView *contentView;
    __unsafe_unretained IBOutlet UIButton *linkedinButton;
    __unsafe_unretained IBOutlet UIButton *facebookButton;
    __unsafe_unretained IBOutlet UIButton *twitterButton;
    __weak IBOutlet UIImageView *seperatorImageView;
    __weak IBOutlet UIActivityIndicatorView *loadingIndicator;
    IBOutlet UIImageView *scrollIndicatorImgView;
}

@end

@implementation TrainigsAndEventsDetail_iPad
@synthesize selectedEvent,selectedEventLocation,eventStore,recursiveEventEndDate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate]refreshFakeViewController];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    loadingIndicator.hidden =YES;
    loadingIndicator.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin);
    
    shareObj = [[ShareToSocialNetworkingSites alloc] init];
    descLabel.font = [UIFont fontWithName:FONT_IPAD size:22.0f];
    descLabel.textColor= UIColorFromRGB(UI_TRAINING_HEADER_TEXT_COLOR_IPAD);
    
    descriptionTitle.font = [UIFont fontWithName:FONT_LIGHT size:17.50f];
    descriptionTitle.textColor= UIColorFromRGB(UI_TRAINING_DESC_LABEL_TEXT_COLOR_IPAD);
    
    
    UIImage *divider = [UIImage imageNamed:@"menuTitleDivider"];
    seperatorImageView.image = [divider stretchableImageWithLeftCapWidth:divider.size.width/2 topCapHeight:divider.size.height/2];
    
    UIImage *btnBg = [UIImage imageNamed:@"add to calendar"];
    [addtoCal setBackgroundImage:[btnBg stretchableImageWithLeftCapWidth:btnBg.size.width/2 topCapHeight:btnBg.size.height/2] forState:UIControlStateNormal];
    addtoCal.contentMode = UIViewContentModeRedraw;
    
    UIImage *linkdinbtnBg = [UIImage imageNamed:@"linkedin"];
    [linkedinButton setBackgroundImage:[linkdinbtnBg stretchableImageWithLeftCapWidth:btnBg.size.width/2 topCapHeight:btnBg.size.height/2] forState:UIControlStateNormal];
    linkedinButton.contentMode = UIViewContentModeRedraw;
    
    UIImage *fbbtnBg = [UIImage imageNamed:@"facebook"];
    [facebookButton setBackgroundImage:[fbbtnBg stretchableImageWithLeftCapWidth:btnBg.size.width/2 topCapHeight:btnBg.size.height/2] forState:UIControlStateNormal];
    facebookButton.contentMode = UIViewContentModeRedraw;
    
    UIImage *twitterbtnBg = [UIImage imageNamed:@"twitter"];
    [twitterButton setBackgroundImage:[twitterbtnBg stretchableImageWithLeftCapWidth:btnBg.size.width/2 topCapHeight:btnBg.size.height/2] forState:UIControlStateNormal];
    twitterButton.contentMode = UIViewContentModeRedraw;
    
    [self loadContent];
    // Do any additional setup after loading the view.
}

-(BOOL)canShowScrollIndicator{
    CGFloat webViewFrameHeight =contentWebView.frame.size.height;
    float scrollContentSizeHeight = contentWebView.scrollView.contentSize.height;
    return scrollContentSizeHeight > webViewFrameHeight;
}

-(void)loadContent
{
    CGFloat headingsGap = 30.0f;
    CGPoint cursor = CGPointMake(20,3);
    CGSize size = CGSizeMake(detailScrollView.frame.size.width-cursor.x-cursor.x, detailScrollView.frame.size.height);
    
    [contentWebView.scrollView setDelegate:self];
    NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body><p><font face=\"opensans\" color=\"#4c4c4c\" size=\"3\">%@</font></p></body></html>",selectedEvent.content];
    [contentWebView loadHTMLString:html baseURL:nil];
    NSString *address = @"";
    if ([selectedEvent.locationStreetAddress length]>0)
    {
        address = selectedEvent.locationStreetAddress;
        address = [address stringByAppendingString:@","];
    }
    if ([selectedEvent.locationStreetAddress2 length]>0)
    {
        address = [address stringByAppendingString:selectedEvent.locationStreetAddress2];
        address = [address stringByAppendingString:@","];
    }
    if ([selectedEvent.locationCity length]>0)
    {
        address = [address stringByAppendingString:selectedEvent.locationCity];
        address = [address stringByAppendingString:@","];
    }
    if ([selectedEvent.locationState length]>0) {
        address = [address stringByAppendingString:selectedEvent.locationState];
        address = [address stringByAppendingString:@","];
    }
    if ([selectedEvent.locationCountry length]>0) {
        address = [address stringByAppendingString:selectedEvent.locationCountry];
    }
    
    
    
    {
        UILabel *newsTitle = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width-10, 40.0f)];
        newsTitle.autoresizingMask =UIViewAutoresizingFlexibleRightMargin;
        newsTitle.text = [selectedEvent.title trimmedString];
        newsTitle.font = [UIFont fontWithName:FONT_LIGHT size:17.50f];
        newsTitle.textColor= UIColorFromRGB(UI_TRAINING_DESC_LABEL_TEXT_COLOR_IPAD);
        newsTitle.backgroundColor = [UIColor clearColor];
        newsTitle.numberOfLines = 0;
        
        CGSize newsTitleSize = [newsTitle sizeOfMultiLineLabel];
        CGRect newsTitleFrame = newsTitle.frame;
        newsTitleFrame.size.height = newsTitleSize.height;
        newsTitle.frame = newsTitleFrame;
        [detailScrollView addSubview:newsTitle];
        
        cursor.y +=newsTitle.frame.size.height+headingsGap;
    }
    
    { // address label
        UIImage *image = [UIImage imageNamed:@"Location"];
        {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(cursor.x-2, cursor.y, image.size.width, image.size.height)];
            imageView.image = image;
            [detailScrollView addSubview:imageView];
            cursor.x +=imageView.frame.size.width;
        }
        
        UILabel *addressLbl = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width-image.size.width-10, 40.0f)];
        addressLbl.autoresizingMask =UIViewAutoresizingFlexibleRightMargin;
        addressLbl.text = [address trimmedString];
        addressLbl.font = [UIFont fontWithName:FONT_SEMIBOLD size:15.0f];
        addressLbl.textColor= UIColorFromRGB(UI_TRAINING_DESC_CONTENTLABEL_TEXT_COLOR_IPAD);
        addressLbl.numberOfLines = 0;
        
        CGSize newsTitleSize = [addressLbl sizeOfMultiLineLabel];
        CGRect newsTitleFrame = addressLbl.frame;
        newsTitleFrame.size.height = newsTitleSize.height;
        addressLbl.frame = newsTitleFrame;
        self.selectedEventLocation=addressLbl.text;
        [detailScrollView addSubview:addressLbl];
        cursor.x = 20.0f;
        cursor.y +=addressLbl.frame.size.height+headingsGap;
    }
    
    { // time label
        
        UIImage *image = [UIImage imageNamed:@"time"];
        {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(cursor.x-2, cursor.y, image.size.width, image.size.height)];
            imageView.image = image;
            [detailScrollView addSubview:imageView];
            cursor.x +=imageView.frame.size.width;
        }
        
        
        
        UILabel *timeLbl = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width-image.size.width-10, 40.0f)];
        timeLbl.autoresizingMask =UIViewAutoresizingFlexibleRightMargin;
        timeLbl.font = [UIFont fontWithName:FONT_SEMIBOLD size:15.0f];
        timeLbl.textColor= UIColorFromRGB(UI_TRAINING_DESC_CONTENTLABEL_TEXT_COLOR_IPAD);
        timeLbl.numberOfLines = 0;
        
        if (selectedEvent.dailyStartTime && selectedEvent.dailyEndTime)
        {
            NSString *datesString;
            NSString *startDtfString;
            NSString *endDtfString;
            NSDate *startformatDate;
            NSDate *endFormatDate;
            //   NSString *timeZoneString =[[NSTimeZone systemTimeZone] localizedName:NSTimeZoneNameStyleGeneric locale:[NSLocale currentLocale]];
            NSString *timeZoneString=[[NSTimeZone systemTimeZone]name];
            
            NSString *startTime;
            NSString *endTime;
            if ([[selectedEvent.startDate displayDateFormat] isEqual:[selectedEvent.endDate displayDateFormat]])
            {
                startformatDate =[self getDateFromString:[selectedEvent.startDate displayDateFormat] withTime:selectedEvent.dailyStartTime];
                endFormatDate = [self getDateFromString:[selectedEvent.startDate displayDateFormat] withTime:selectedEvent.dailyEndTime];
                startDtfString = [NSDate stringFromDate:startformatDate];
                endDtfString =[NSDate stringFromDate:endFormatDate];
                NSString *dbDateString = [NSDate stringFromDate:startformatDate withFormat:@"YYYY-MM-dd"];
                datesString = [dbDateString displayDateFormat];
            }
            else
            {
                //  datesString = [NSString stringWithFormat:@"%@ - %@",[selectedEvent.startDate displayDateFormat],[selectedEvent.endDate displayDateFormat]];
                startformatDate =[self getDateFromString:[selectedEvent.startDate displayDateFormat] withTime:selectedEvent.dailyStartTime];
                endFormatDate = [self getDateFromString:[selectedEvent.endDate displayDateFormat] withTime:selectedEvent.dailyEndTime];
                startDtfString = [NSDate stringFromDate:startformatDate];
                endDtfString =[NSDate stringFromDate:endFormatDate];
                NSString *startDateString = [NSDate stringFromDate:startformatDate withFormat:@"YYYY-MM-dd"];
                NSString *endDateString = [NSDate stringFromDate:endFormatDate withFormat:@"YYYY-MM-dd"];
                
                //TODO:NEEd to vchange the dates and check the condition to disoply only one
                
                
                NSArray *allDatesBetween =[self getDatesBetweenTwoDates:startformatDate andEndDate:endFormatDate];
                if ([allDatesBetween count]>1){
                    datesString = [NSString stringWithFormat:@"%@ - %@",[startDateString displayDateFormat],[endDateString displayDateFormat]];
                    
                }else{
                    datesString = [NSString stringWithFormat:@"%@",[startDateString displayDateFormat]];
                    
                }
                
                
            }
            
            
            startTime =[NSString stringWithFormat:@"%lu:%lu",(unsigned long)[startformatDate hour],(unsigned long)[startformatDate minute]];
            endTime =[NSString stringWithFormat:@"%lu:%lu",(unsigned long)[endFormatDate hour],(unsigned long)[endFormatDate minute]];
            timeLbl.text = [NSString stringWithFormat:@"%@ (%@ - %@) (%@) ",datesString,[startTime get12HrStringFrom],[endTime get12HrStringFrom],timeZoneString];
        }
        else
        {
            if ([[selectedEvent.startDate displayDateFormat] isEqual:[selectedEvent.endDate displayDateFormat]])
            {
                timeLbl.text = [NSString stringWithFormat:@"%@",[selectedEvent.startDate displayDateFormat]];
            }
            else
            {
                timeLbl.text = [NSString stringWithFormat:@"%@ - %@",[selectedEvent.startDate displayDateFormat],[selectedEvent.endDate displayDateFormat]];
            }
        }
        timeLbl.frame = [self frameForLabel:timeLbl WithText:timeLbl.text];
        
        
        [detailScrollView addSubview:timeLbl];
        cursor.x = 20.0f;
        cursor.y +=timeLbl.frame.size.height+headingsGap;
    }
    if ([selectedEvent.contactName length]>0 || [selectedEvent.contactPhone length]>0 || [selectedEvent.contactEmail length]>0)
    {
        {
            
        }
        if ([selectedEvent.contactName length]>0)
        { // name label
            
            UIImage *image = [UIImage imageNamed:@"contact"];
            {
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(cursor.x-2, cursor.y, image.size.width, image.size.height)];
                imageView.image = image;
                [detailScrollView addSubview:imageView];
                
                cursor.x +=imageView.frame.size.width;
            }
            
            
            UILabel *nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width-image.size.width-10, 40.0f)];
            nameLbl.autoresizingMask =UIViewAutoresizingFlexibleRightMargin;
            nameLbl.font = [UIFont fontWithName:FONT_SEMIBOLD size:15.0f];
            nameLbl.textColor= UIColorFromRGB(UI_TRAINING_DESC_CONTENTLABEL_TEXT_COLOR_IPAD);
            nameLbl.numberOfLines = 0;
            nameLbl.text = [selectedEvent.contactName trimmedString];
            
            
            CGSize newsTitleSize = [nameLbl sizeOfMultiLineLabel];
            CGRect newsTitleFrame = nameLbl.frame;
            newsTitleFrame.size.height = newsTitleSize.height;
            nameLbl.frame = newsTitleFrame;
            
            [detailScrollView addSubview:nameLbl];
            cursor.x=20;
            cursor.y +=nameLbl.frame.size.height;
        }
        if ([selectedEvent.contactEmail length]>0)
        {
            UIImage *image = [UIImage imageNamed:@"mail_iPad"];
            {
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(cursor.x+4, cursor.y+12, image.size.width, image.size.height)];
                imageView.image = image;
                [detailScrollView addSubview:imageView];
                cursor.x +=imageView.frame.size.width+4;
            }
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.autoresizingMask =UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
            [button addTarget:self action:@selector(tappedOnMail:) forControlEvents:UIControlEventTouchUpInside];
            [button setTitle:[selectedEvent.contactEmail trimmedString] forState:UIControlStateNormal];
            button.tag = 100;
            button.titleLabel.numberOfLines = 3;
            [button.titleLabel setFont:[UIFont fontWithName:FONT_REGULAR size:13.08f]];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            button.frame = CGRectMake(cursor.x+8, cursor.y+10, size.width-image.size.width-4, 25.0f);
            button.backgroundColor = [UIColor clearColor];
            
            [detailScrollView addSubview:button];
            cursor.x = 20.0f;
            cursor.y +=button.frame.size.height+10;
        }
        
        if ([selectedEvent.contactPhone length]>0)
        {
            UIImage *image = [UIImage imageNamed:@"phone_iPad"];
            {
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(cursor.x+4, cursor.y+12, image.size.width, image.size.height)];
                imageView.image = image;
                [detailScrollView addSubview:imageView];
                cursor.x +=imageView.frame.size.width+4;
            }
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.autoresizingMask =UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
            [button addTarget:self action:@selector(tappedOnPhone:) forControlEvents:UIControlEventTouchUpInside];
            NSString *phoneNumber = [selectedEvent.contactPhone trimmedString];
            [button setTitle:[phoneNumber frameContactNumber] forState:UIControlStateNormal];
            button.tag = 100;
            button.titleLabel.numberOfLines = 3;
            [button.titleLabel setFont:[UIFont fontWithName:FONT_REGULAR size:13.08f]];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            button.frame = CGRectMake(cursor.x+8, cursor.y+10, size.width-image.size.width-4, 25.0f);
            button.backgroundColor = [UIColor clearColor];
            [detailScrollView addSubview:button];
            cursor.x = 20.0f;
            cursor.y +=button.frame.size.height;
        }
        
    }
    
    detailScrollView.contentSize = CGSizeMake(detailScrollView.frame.size.width, cursor.y+30);
    detailScrollView.clipsToBounds =YES;
   
    
    
    
}

# pragma mark Scrollview Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    BOOL toEnd =NO;
    if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
        //reach bottom
        NSLog(@"we are at the endddd");
        toEnd =YES;
    }
    
    if (scrollView.contentOffset.y < 0){
        //reach top
        NSLog(@"we are at the top");
        toEnd =NO;
    }
    
    if (scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y < (scrollView.contentSize.height - scrollView.frame.size.height)){
        //not top and not bottom
    }
    
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    scrollIndicatorImgView.image =toEnd?[UIImage imageNamed:@"scrollIndicator_up"]:[UIImage imageNamed:@"scrollIndicator_down"];
    
}
-(void)webViewDidStartLoad:(UIWebView *)webView{
//    contentWebView.scalesPageToFit = YES;
    loadingIndicator.hidden = NO;
    [loadingIndicator startAnimating];
    
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    loadingIndicator.hidden = YES;
    [loadingIndicator stopAnimating];
    
    BOOL  canShowScrollIndicator=[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !canShowScrollIndicator;
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    //NSLog(@"Error : %@",error);
    
    loadingIndicator.hidden = YES;
    [loadingIndicator stopAnimating];
    
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked){
        
        
        
        if ([request.URL.scheme isEqualToString:@"tel"])
        {
            
            return YES;
        }
        
        else if ([request.URL.scheme isEqualToString:@"mailto"])
        {
            if ([request.URL isMailtoRequest])
            {
                
                [MFMailComposeViewController presentModalComposeViewControllerWithURL:request.URL  delegate:self];
                return NO;
                
            }
            
        }
        else{
            
            if (![Common currentNetworkStatus])
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Trainings & Events" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
                [alertView show];
                return NO;
            }
            NSString *linkUrl =[request.URL absoluteString];
            //    NSURL *myURL;
            if ([linkUrl hasPrefix:@"http://"] || [linkUrl hasPrefix:@"https://"])
            {
                
            }
            else
            {
                linkUrl = [NSString stringWithFormat:@"http://%@",linkUrl];
            }
            if ([Common isValidUrl:linkUrl])
            {
                MTPopupWindow *popup = [[MTPopupWindow alloc] init];
                popup.delegate = self;
                popup.fileName = linkUrl;
                [popup show];
                return NO;
            }else
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Trainings & Events" message:@"Invalid Url" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
                
            }

        }

       
        
    }
    return YES;
    
    
}

-(CGRect)frameForLabel:(UILabel *)label WithText:(NSString *)text
{
    CGSize maximumLabelSize = CGSizeMake(label.frame.size.width, FLT_MAX);
    CGSize expectedLabelSize = [text sizeWithFont:label.font constrainedToSize:maximumLabelSize lineBreakMode:label.lineBreakMode];
    //adjust the label the the new height.
    CGRect newFrame = label.frame;
    newFrame.size.height = expectedLabelSize.height<20.0f ? 20.0f :expectedLabelSize.height;
    //    label.frame = newFrame;
    
    return newFrame;
}
-(NSDate*)getDateFromString:(NSString *)dateString withTime:(NSString *)time {
    
    NSDateFormatter *reDateFormatter = [[NSDateFormatter alloc] init];
    [reDateFormatter setDateFormat:@"MMM d, yyyy"];
    NSDate *date = [reDateFormatter dateFromString:dateString];
    NSInteger hourVal=0;
    NSInteger minuteVal=0;
    NSCalendar *calendar=[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components=[calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    NSArray* stringComponents=[time componentsSeparatedByString:@":"];
    NSString *eventTme =[stringComponents objectAtIndex:1];
    
    //    if ([eventTme length]>0)
    {
        
        if ([eventTme rangeOfString:@"pm" options:NSCaseInsensitiveSearch].location == NSNotFound)
        {
            //NSLog(@"Event time is Morning");
            
            hourVal=[[stringComponents objectAtIndex:0] integerValue];
            minuteVal=[[stringComponents objectAtIndex:1] integerValue];
        }
        else
        {
            hourVal=[[stringComponents objectAtIndex:0] integerValue]+12;
            minuteVal=[[stringComponents objectAtIndex:1] integerValue];
            //NSLog(@"Event time is Night");
        }
    }
    
    [components setHour:hourVal];
    [components setMinute:minuteVal];
    NSDate *finalDate = [calendar dateFromComponents:components];
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:finalDate];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:finalDate];
    NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:finalDate];
    
    return destinationDate;
}
-(NSDate*)getDateFromString:(NSString *)dateString withTime:(NSString *)time isStartTime:(BOOL)isStart{
    
    NSDateFormatter *reDateFormatter = [[NSDateFormatter alloc] init];
    [reDateFormatter setDateFormat:@"MMM d, yyyy"];
    NSDate *date = [reDateFormatter dateFromString:dateString];
    NSInteger hourVal;
    NSInteger minuteVal;
    NSCalendar *calendar=[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components=[calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    NSArray* stringComponents=[time componentsSeparatedByString:@":"];
    NSString *eventTme =[stringComponents objectAtIndex:1];
    
    
    if (eventTme!=nil) {
        
        if ([eventTme rangeOfString:@"pm" options:NSCaseInsensitiveSearch].location == NSNotFound)
        {
            //NSLog(@"Event time is Morning");
            
            hourVal=[[stringComponents objectAtIndex:0] integerValue];
            minuteVal=[[stringComponents objectAtIndex:1] integerValue];
        }
        else
        {
            hourVal=[[stringComponents objectAtIndex:0] integerValue]+12;
            minuteVal=[[stringComponents objectAtIndex:1] integerValue];
            //NSLog(@"Event time is Night");
        }
    }
    else{
        
        if(isStart){
            
            hourVal=9;
            minuteVal=0;
        }
        else{
            
            hourVal=17;
            minuteVal=0;
        }
        
    }
    
    [components setHour:hourVal];
    [components setMinute:minuteVal];
    NSDate *finalDate = [calendar dateFromComponents:components];
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:finalDate];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:finalDate];
    NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:finalDate];
    
    /*
     NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
     [dateFormatters setDateFormat:@"dd-MMM-yyyy hh:mm"];
     [dateFormatters setDateStyle:NSDateFormatterShortStyle];
     [dateFormatters setTimeStyle:NSDateFormatterShortStyle];
     [dateFormatters setDoesRelativeDateFormatting:YES];
     [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
     NSString * dateStr = [dateFormatters stringFromDate: destinationDate];
     //NSLog(@"DateString : %@", dateStr);
     */
    //NSLog(@"Required Final Date is :%@",destinationDate);
    return destinationDate;
    
}

-(NSArray *)getDatesBetweenTwoDates:(NSDate *)startDate andEndDate:(NSDate *)endDate{
    
    NSMutableArray *dates = [NSMutableArray array];
    NSDate *curDate = startDate;
    while([curDate timeIntervalSince1970] <= [endDate timeIntervalSince1970]) //you can also use the earlier-method
    {
        [dates addObject:curDate];
        curDate = [NSDate dateWithTimeInterval:86400 sinceDate:curDate];//Adding one day to the Current Date
        
    }
    ////NSLog(@"Dates Between %@ and %@ are %@",startDate,endDate,dates);
    return dates;
}
-(IBAction)backButtonAction:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)addToCalendarTapped:(id)sender
{
    //    //NSLog(@"Add to Calendar:%@",selectedEvent.location);
    //NSLog(@"Event Start Date:%@",selectedEvent.startDate);
    //NSLog(@"Event End Date:%@",selectedEvent.endDate);
    //NSLog(@"Event Start time:%@",selectedEvent.dailyStartTime);
    //NSLog(@"Event End time:%@",selectedEvent.dailyEndTime);
//    NSArray* stringComponents = [selectedEvent.dailyStartTime componentsSeparatedByString:@":"];
    //NSLog(@"Hour Component %@",[stringComponents objectAtIndex:0]);
    //NSLog(@"Minutes Component %@",[stringComponents objectAtIndex:1]);
    NSDate *startDate;
    NSDate *endDate;
    if ([selectedEvent.startDate displayDateFormat] .length>0) {
        startDate=[self getDateFromString:[selectedEvent.startDate displayDateFormat] withTime:selectedEvent.dailyStartTime isStartTime:YES];
    }
    if ([selectedEvent.endDate displayDateFormat].length>0) {
        endDate=[self getDateFromString:[selectedEvent.endDate displayDateFormat] withTime:selectedEvent.dailyEndTime isStartTime:NO];
    }
    NSArray *allDatesBetween =[self getDatesBetweenTwoDates:startDate andEndDate:endDate];
    if ([allDatesBetween count]>1) {
        NSDate *eventStartDate=[self getDateFromString:[selectedEvent.startDate displayDateFormat] withTime:selectedEvent.dailyStartTime isStartTime:YES];
        NSDate *eventEndDate=[self getDateFromString:[selectedEvent.startDate displayDateFormat] withTime:selectedEvent.dailyEndTime isStartTime:NO];
        self.recursiveEventEndDate =[self getDateFromString:[selectedEvent.endDate displayDateFormat] withTime:selectedEvent.dailyEndTime isStartTime:NO];
        [self addToCalendarEvent:eventStartDate andEndDtae:eventEndDate inLocation:self.selectedEventLocation withTitle:self.selectedEvent.title];
    }
    else {
        
        [self addToCalendarEvent:startDate andEndDtae:endDate inLocation:self.selectedEventLocation withTitle:self.selectedEvent.title];
    }
    
    
}
-(void) shareToSocialNetworks:(NSString *)sharetype{
    
    NSString *date;
    
    if ([selectedEvent.dailyStartTime displayDateFormat] && [selectedEvent.dailyEndTime displayDateFormat])
    {
        NSString *datesString;
        if ([[selectedEvent.startDate displayDateFormat] isEqual:[selectedEvent.endDate displayDateFormat]])
        {
            datesString = [NSString stringWithFormat:@"%@",[selectedEvent.startDate displayDateFormat]];
        }
        else
        {
            datesString = [NSString stringWithFormat:@"%@ - %@",[selectedEvent.startDate displayDateFormat],[selectedEvent.endDate displayDateFormat]];
        }
        
        date = [NSString stringWithFormat:@"%@",datesString];
    }
    else
    {
        if ([[selectedEvent.startDate displayDateFormat] isEqual:[selectedEvent.endDate displayDateFormat]])
        {
            date = [NSString stringWithFormat:@"%@",[selectedEvent.startDate displayDateFormat]];
        }
        else
        {
            date = [NSString stringWithFormat:@"%@ - %@",[selectedEvent.startDate displayDateFormat],[selectedEvent.endDate displayDateFormat]];
        }
    }
    NSString *sharingText = [NSString stringWithFormat:@"%@ \n %@",[selectedEvent.title trimmedString],date];
    if ([sharetype isEqualToString:@"LinkedIn"]) {
        //NSLog(@"linkedin was pressed");
        
        [shareObj linkedInLogIn:selectedEvent.link andText:sharingText fromViewContoller:self];
        
    }
    else if([sharetype isEqualToString:@"Twitter"]){
        //NSLog(@"twitter was pressed");
        [shareObj tweetTapped:selectedEvent.link andText:sharingText fromViewContoller:self];
        
    }
    else if  ([sharetype isEqualToString:NSLocalizedString(@"FaceBook", @"")]){
        //NSLog(@"fb was pressed");
        [shareObj fbShareBtnTapped:selectedEvent.link andText:sharingText fromViewContoller:self];
    }
    
    
}
-(IBAction)fbTapped:(id)sender
{
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Trainings & Events" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    [self shareToSocialNetworks:NSLocalizedString(@"FaceBook", @"")];
}

-(IBAction)LinkedInTapped:(id)sender
{
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Trainings & Events" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    [self shareToSocialNetworks:@"LinkedIn"];
}

-(IBAction)TwitterTapped:(id)sender
{
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Trainings & Events" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    [self shareToSocialNetworks:@"Twitter"];
}

-(IBAction)tappedOnPhone:(UIButton *)sender
{
    //NSLog(@"Phone Tapped");
    if ([[selectedEvent.contactPhone trimmedString] length]>0)
    {
        if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        {
            NSString *fianlphoneNumber = [[selectedEvent.contactPhone trimmedString] frameContactNumber];
            
            NSString *phoneNumDecimalsOnly = [[fianlphoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
            
            NSString *phoneNumber = [@"telprompt://" stringByAppendingString:phoneNumDecimalsOnly];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
            
        } else {
            
            UIAlertView *warning =[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [warning show];
        }
    }
}
-(IBAction)tappedOnMail:(UIButton *)sender
{
    //NSLog(@"MAIL Tapped");
    
//    if (![Common currentNetworkStatus])
//    {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Trainigs & Events" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alertView show];
//        return;
//    }
    if ([MFMailComposeViewController canSendMail])
	{
		// set the sendTo address
		NSMutableArray *recipients = [[NSMutableArray alloc] initWithCapacity:1];
		[recipients addObject:selectedEvent.contactEmail];
		
		MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
		controller.mailComposeDelegate = self;
        //		[controller setSubject:@"Hi Its Wrkng"];
		
		[controller setToRecipients:recipients];
		
		if ([self respondsToSelector:@selector(presentViewController:animated:completion:)])
        {
            
            
            [self presentViewController:controller animated:YES completion:nil];
        }
	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Trainings & Events" message:@"No email account is setup to send mail." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		
	}
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	switch (result)
	{
		case MFMailComposeResultCancelled:
		{
			break;
		}
		case MFMailComposeResultSaved:
		{
			break;
		}
		case MFMailComposeResultSent:
		{
			break;
		}
		case MFMailComposeResultFailed:
		{
			break;
		}
		default:
		{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Email Failed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
            
		}
			break;
	}
	if ([self respondsToSelector:@selector(dismissModalViewControllerAnimated:)]) {
        [self performSelector:@selector(dismissModalViewControllerAnimated:) withObject:[NSNumber numberWithBool:YES]];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
	
	
}
-(void)addToCalendarEvent: (NSDate *)startDate andEndDtae:(NSDate *)endDate inLocation:(NSString *)eventLocation withTitle:(NSString *)eventTitle{
    
    self.eventStore = [[EKEventStore alloc] init];
    
    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        /* This code will run when uses has made his/her choice */
        
        if (error)
        {
            // display error message here
        }
        else if (!granted)
        {
            // display access denied error message here
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc]initWithTitle:nil message:@"Please Allow Calendar Permissions in Settings" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil)  otherButtonTitles: nil] show];
                return ;
            });
        }
        else
        {
            // access granted
            
            
            EKSource *localSource = nil;
            EKCalendar *cal;
            NSString *calendarIdentifier = [[NSUserDefaults standardUserDefaults] valueForKey:@"my_calendar_identifier"];
            // when identifier exists, my calendar probably already exists
            // note that user can delete my calendar. In that case I have to create it again.
            if (calendarIdentifier) {
                
                for (EKCalendar *aCal in [eventStore calendarsForEntityType:EKEntityTypeEvent]) {
                    
                    if([aCal.calendarIdentifier isEqualToString:calendarIdentifier])
                    {
                        cal = aCal;
                        break;
                    }
                    //cal = [eventStore calendarWithIdentifier:calendarIdentifier];
                }
                
            }
            if (!cal) {
                
                cal = [EKCalendar calendarForEntityType:EKEntityTypeEvent eventStore:eventStore];
                
                // set calendar name. This is what users will see in their Calendar app
                [cal setTitle:@"Immigo"];
                
                // find appropriate source type. I'm interested only in local calendars but
                // there are also calendars in iCloud, MS Exchange, ...
                // look for EKSourceType in manual for more options
                for (EKSource *source in eventStore.sources)
                {
                    if (source.sourceType == EKSourceTypeCalDAV && [source.title isEqualToString:@"iCloud"])
                    {
                        localSource = source;
                        cal.source =source;
                        break;
                    }
                }
                if (localSource == nil)
                {
                    for (EKSource *source in eventStore.sources) {
                        if (source.sourceType == EKSourceTypeLocal)
                        {
                            localSource = source;
                            cal.source =source;
                            break;
                        }
                    }
                }
                
                // save this in NSUserDefaults data for retrieval later
                NSString *calendarIdentifier = [cal calendarIdentifier];
                NSError *error = nil;
                BOOL saved = [eventStore saveCalendar:cal commit:YES error:&error];
                if (saved) {
                    // http://stackoverflow.com/questions/1731530/whats-the-easiest-way-to-persist-data-in-an-iphone-app
                    // saved successfuly, store it's identifier in NSUserDefaults
                    [[NSUserDefaults standardUserDefaults] setObject:calendarIdentifier forKey:@"my_calendar_identifier"];
                } else {
                    // unable to save calendar
                    //return NO;
                }
            }
            
            // this shouldn't happen
            if (!cal) {
                //return NO;
            }
            
            
            //calendar properties
            //NSLog(@"Calendar Properties %@", cal);
            
            //Add Event to Calendar
            //NSLog(@"Adding event!");
            EKEvent *event  = [EKEvent eventWithEventStore:eventStore];
            event.title     = eventTitle;
            event.calendar = cal;
            NSArray *allDatesBetween =[self getDatesBetweenTwoDates:startDate andEndDate:self.recursiveEventEndDate];
            if ([allDatesBetween count]>1) {
                EKRecurrenceEnd *recurrenceEnd =[EKRecurrenceEnd recurrenceEndWithEndDate:self.recursiveEventEndDate];
                EKRecurrenceRule *er =[[EKRecurrenceRule alloc] initRecurrenceWithFrequency:EKRecurrenceFrequencyDaily interval:1 end:recurrenceEnd];
                [event addRecurrenceRule:er];
                
            }
            event.calendar = cal;
            event.allDay = NO;
            event.startDate = startDate;
            event.endDate = endDate;
            event.location =eventLocation;
            
            NSError *error = nil;
            BOOL result = [eventStore saveEvent:event span:EKSpanThisEvent commit:YES error:&error];
            if (result) {
                //NSLog(@"Saved event to event store.");
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Event Added Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                    return ;
                    
                });
            } else {
                //NSLog(@"Error saving event: %@.", error);
                /*
                 dispatch_async(dispatch_get_main_queue(), ^{
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure!" message:@"Could Not Add Event" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                 [alert show];      return ;  });
                 */
            }
        }
        
        
    }];
    
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

- (void)willAnimateFirstHalfOfRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    
    //    [[[[UIApplication sharedApplication] windows] objectAtIndex:0] setWindowLevel:UIWindowLevelAlert];
    
}
- (void)didAnimateFirstHalfOfRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    BOOL  canShowScrollIndicator=[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !canShowScrollIndicator;
    
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    if (fromInterfaceOrientation == UIInterfaceOrientationIsLandscape(fromInterfaceOrientation)||fromInterfaceOrientation == UIInterfaceOrientationPortrait) {
        
        loadingIndicator.center =contentWebView.center;
        
    }
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
