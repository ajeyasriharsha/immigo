//
//  ImmigrationSubTopicsVC_iPad.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/26/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Topic.h"
#import <MessageUI/MessageUI.h>
#import "ShareToSocialNetworkingSites.h"

@interface ImmigrationSubTopicsVC_iPad : UIViewController<MFMailComposeViewControllerDelegate,UIPrintInteractionControllerDelegate>{
    
    ShareToSocialNetworkingSites *shareObj;
}
@property (nonatomic,strong) Topic *topic;


@end
