//
//  CommunityPollDetailVC_iPad.h
//  IMMIGO
//
//  Created by pradeep ISPV on 6/10/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMGProgressView.h"
#import "PollQuestions.h"

@interface CommunityPollDetailVC_iPad : UIViewController
{
    IBOutlet UIView *pollQuestionView;
    IBOutlet UIButton *heading;
    
}
//@property (nonatomic,strong) PollQuestions *selectedPoll
@property (nonatomic)int pollQuestionIndex;

@end
