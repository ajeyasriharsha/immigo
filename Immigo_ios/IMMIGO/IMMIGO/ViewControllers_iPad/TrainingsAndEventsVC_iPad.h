//
//  TrainingsAndEventsVC_iPadViewController.h
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GridView.h"

@interface TrainingsAndEventsVC_iPad : UIViewController <MARGridViewDelegate>

@end
