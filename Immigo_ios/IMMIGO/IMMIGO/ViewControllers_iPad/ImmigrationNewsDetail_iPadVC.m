//
//  ImmigrationNewsDetail_iPadVC.m
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 6/3/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "ImmigrationNewsDetail_iPadVC.h"
#import "Common.h"
#import "AppDelegate.h"
@interface ImmigrationNewsDetail_iPadVC ()<UIWebViewDelegate,UIScrollViewDelegate>{
    
    
    __weak IBOutlet UIWebView *contentWebView;
    IBOutlet UIActivityIndicatorView *activityView;
    __weak IBOutlet UIButton *backButton;
    __weak IBOutlet UILabel *headingLabel;
    __weak IBOutlet UIImageView *seperatorImageView;
    IBOutlet UIImageView *scrollIndicatorImgView;
}

@end

@implementation ImmigrationNewsDetail_iPadVC
@synthesize selectedNews;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    headingLabel.font = [UIFont fontWithName:FONT_IPAD size:22.0f];
    
    UIImage *divider = [UIImage imageNamed:@"menuTitleDivider"];
    seperatorImageView.image = [divider stretchableImageWithLeftCapWidth:divider.size.width/2 topCapHeight:divider.size.height/2];
    

    
    [self addWebViewBorder];
    [contentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:selectedNews.newsLink]]];
    // Do any additional setup after loading the view.
}
-(void)addWebViewBorder{
    
    
    [[contentWebView layer] setCornerRadius:1];
    [contentWebView setClipsToBounds:YES];
    
    // Create colored border using CALayer property
    [[contentWebView layer] setBorderColor:
     [[UIColor colorWithRed:(196.0f/255.0f) green:(196.0f/255.0f) blue:(196.0f/255.0f) alpha:1] CGColor]];
    [[contentWebView layer] setBorderWidth:1.0];
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate]refreshFakeViewController];
    //activityView.center =contentWebView.center;
   // activityView.autoresizingMask =nil;
    [activityView setCenter:contentWebView.center];
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)canShowScrollIndicator{
    CGFloat webViewFrameHeight =contentWebView.frame.size.height;
    float scrollContentSizeHeight = contentWebView.scrollView.contentSize.height;
    return scrollContentSizeHeight > webViewFrameHeight;
}



# pragma mark Scrollview Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    BOOL toEnd =NO;
    if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
        //reach bottom
        NSLog(@"we are at the endddd");
        toEnd =YES;
    }
    
    if (scrollView.contentOffset.y < 0){
        //reach top
        NSLog(@"we are at the top");
        toEnd =NO;
    }
    
    if (scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y < (scrollView.contentSize.height - scrollView.frame.size.height)){
        //not top and not bottom
    }
    
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    scrollIndicatorImgView.image =toEnd?[UIImage imageNamed:@"scrollIndicator_up"]:[UIImage imageNamed:@"scrollIndicator_down"];
    
}
#pragma WebView Delegates
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    activityView.hidden= NO;
    [activityView startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    webView.scrollView.delegate = self; // set delegate method of UISrollView
    webView.scrollView.maximumZoomScale = 10; // set as you want.
    webView.scrollView.minimumZoomScale = 0; // set as you want.
    activityView.hidden= YES;
    [activityView stopAnimating];
    
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;


}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    activityView.hidden= YES;
    [activityView stopAnimating];
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{

    if (navigationType == UIWebViewNavigationTypeLinkClicked){
        
        
        
        if ([request.URL.scheme isEqualToString:@"tel"])
        {
            
            return YES;
        }
        
        else if ([request.URL.scheme isEqualToString:@"mailto"])
        {
            return YES;
            
        }
        else{
            
        if (![Common currentNetworkStatus])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Immigration News" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
            [alertView show];
            return NO;
        }
        }
    }
    return YES;
    
    
}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale; // scale between minimum and maximum. called after any 'bounce' animations
{
    contentWebView.scrollView.minimumZoomScale = 0; // set similar to previous.
    contentWebView.scrollView.maximumZoomScale = 10; // set similar to previous.
}

- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
