//
//  FakeViewController_iPad.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/30/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "FakeViewController_iPad.h"
#import "HomeViewController_iPad.h"

#import "HomeViewController_iPad.h"
#import "TrainingsAndEventsVC_iPad.h"
#import "ImmigrationNews_iPadVC.h"
#import "CommunityPoll_iPadVC.h"
#import "AboutImmigoVC_iPad.h"
#import "Common.h"
#import "AlertsDAO.h"
#import "Alerts.h"
#import "AlertsModel.h"
#import "MTPopupWindow.h"
#import "LegalSearchListVC_iPad.h"
#import "LegalFinderModel.h"
#import "SplashViewController_iPad.h"
#import "AOLModel.h"
#import "LegalAssistanceModel.h"
#import "LanguageModel.h"
#import "UIView+border.h"
@interface FakeViewController_iPad () <DataDownloaderDelegate,MTPopupWindowDelegate,UIScrollViewDelegate,PullableViewDelegate,PartnersPullableViewDelegate,UINavigationControllerDelegate>
{
    NSArray *alertsArray;
    NSTimer* myTimer;
    NSMutableArray *views;
    UIWebView *alertsWebview;
     UIActivityIndicatorView *activityView;
    int currentPage;
   
    NSArray * arr;
    IBOutlet UILabel *currentLocationLbl;
    IBOutlet UIButton *advancedSearchBtn;
    IBOutlet UIButton *legalHelpFinderHeading;
    IBOutlet UIButton *legalsearchFinderAdvanceHeading;
    IBOutlet UIButton *basicSearchButton;
    IBOutlet UIButton *goToAdvancedSearch;
    IBOutlet UIButton *advancesearchSearchBtn;
    
    UITableView *pickerTable;
    IBOutlet UITextField *aolTf;

    IBOutlet UITextField *languageTf;
    IBOutlet UITextField *tolaTf;
    
    
    NSMutableArray *searchFilteredArray;

    
    IBOutlet UISwitch *advacnedSearchCurrentLocationSwitch;
    __weak IBOutlet UISwitch *currentLocationSwitch;
    BOOL locationOn;
    NSString *currentLattitude;
    NSString *currentLongitude;
    IBOutlet UIView *advSearchZipcodeTF;
    UILabel *infoTextLabel;

}
- (IBAction)legalHelpFinderHeadingTapped:(id)sender;
- (IBAction)legalHelpFInderAdvancedSearchHeadingTapped:(id)sender;
- (IBAction)advancedSearchBackButtonTapped:(id)sender;
- (IBAction)currentLocationTapped:(id)sender;

- (IBAction)homeButtonTapped:(id)sender;
- (IBAction)zipcodeTapped:(id)sender;
- (IBAction)advancedSearchTapped:(id)sender;
- (IBAction)aolTapped:(UIButton *)sender;
- (IBAction)legalAssistanceTapped:(id)sender;
- (IBAction)languagesTapped:(UIButton *)sender;


@end

@implementation FakeViewController_iPad
@synthesize popoverController =popoverController;
@synthesize legalSearchView,zipCodeField,legalAdvancedSearchView;
@synthesize bigMenuBtn,alertsPageControl,alertsScrollView,alertsView,homeButton,dropDown;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)dropDownHidden
{
    
}
-(void)stopAnimatingPartnersView
{
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[DataDownloader sharedDownloader] setDelegate:self];
    [[AlertsModel sharedDataSource] fetchAlerts];

    arr = [NSArray arrayWithObjects:@"IMMIGRATION BASICS", @"LEGAL HELP FINDER", @"IMMIGRATION NEWS", @"TRAININGS & EVENTS", @"COMMUNITY POLL", @"ABOUT IMMIGO",nil];

    zipCodeField.placeholder =advancedSearchZipCodeTf.placeholder= @"Enter Zip Code";
    zipCodeField.font =advancedSearchZipCodeTf.font = [UIFont fontWithName:FONT_IPAD size:13.0f];
    
    advancedSearchBtn.titleLabel.font =[UIFont fontWithName:FONT_IPAD size:13.0f];
    basicSearchButton.titleLabel.font =[UIFont fontWithName:FONT_IPAD size:13.0f];
    goToAdvancedSearch.titleLabel.font =[UIFont fontWithName:FONT_IPAD size:13.0f];
    advancesearchSearchBtn.titleLabel.font =[UIFont fontWithName:FONT_IPAD size:13.0f];
    
    currentLocationLbl.font =advancedSearchCurrentLocationLbl.font= [UIFont fontWithName:FONT_IPAD size:13.0f];
    
    legalHelpFinderHeading.titleLabel.font = [UIFont fontWithName:FONT_IPAD size:18.0f];
    legalsearchFinderAdvanceHeading.titleLabel.font = [UIFont fontWithName:FONT_IPAD size:18.0f];
    
    
   // aolBtn.titleLabel.font =typesofAssistanceBtn.titleLabel.font =languageBtn.titleLabel.font =[UIFont fontWithName:FONT_IPAD size:13.0f];
    aolTf.font =tolaTf.font =languageTf.font =[UIFont fontWithName:FONT_IPAD size:13.0f];
    
    if (!selectedIndexes) {
        selectedIndexes =[NSMutableArray array];
    }
    if (!selectedValues) {
        selectedValues =[NSMutableArray array];
    }
     isSearchEnabled =NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    legalSearchView.hidden = YES;
    legalAdvancedSearchView.hidden = YES;
    
    [self scrollToFirstAlert];
    self.navigationController.navigationBarHidden = YES;
    myTimer = [NSTimer scheduledTimerWithTimeInterval:10.0 target: self
                                             selector: @selector(scrollToNextAlert) userInfo: nil repeats: YES];
}

#pragma mark - AlertsPagecontrll methods
-(void)createAlertsPageControl
{
    
   // //NSLog(@"alert scrollview size %f",alertsScrollView.frame.size.height);
    for (UIView * view in alertsScrollView.subviews) {
        if (view.tag == 10) {
            [view removeFromSuperview];
        }
    }

    views = [[NSMutableArray alloc] initWithCapacity:[alertsArray count]];
    for (int i = 0; i < [alertsArray count]; i++)
    {
        Alerts *alert = [alertsArray objectAtIndex:i];
        
        UIView *scrollInnerView = [[UIView alloc] initWithFrame:alertsScrollView.bounds];
        scrollInnerView.tag =10;
        scrollInnerView.autoresizingMask =UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
        [scrollInnerView setBackgroundColor:[UIColor clearColor]];
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(3, 5, alertsScrollView.frame.size.width, 30)];
        //  title.text = @"This is a very simple ios app using a UICollectionView to view all the photos in your iPad (or iPhones) library. As far as I know it's the only full working project on the internet. Every tutorial I could find did not have a ";//alert.alertTitle;
        title.text =alert.alertTitle;
        title.textColor =UIColorFromRGB(UI_BLUE_TEXT_COLOR);
        title.autoresizingMask =UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
        title.backgroundColor = [UIColor clearColor];
        title.font = [UIFont fontWithName:FONT_SEMIBOLD size:17.0f];
        
        [scrollInnerView addSubview:title];
        
        UITextView *descTextView =[[UITextView alloc] initWithFrame:CGRectMake(-2,title.frame.size.height-7, alertsScrollView.frame.size.width+4, alertsScrollView.frame.size.height-(title.frame.size.height+title.frame.origin.y)+4)];
        descTextView.text = alert.alertDiscription;
        [descTextView setEditable:NO];
        descTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleBottomMargin |UIViewAutoresizingFlexibleHeight;
        descTextView.textColor = UIColorFromRGB(UI_ALERT_DESCRIPTION_COLOR);
        descTextView.font = [UIFont fontWithName:FONT_SEMIBOLD size:15.0f];
        descTextView.backgroundColor = [UIColor clearColor];
        
        // descTitle.numberOfLines =3;
        // [descTitle sizeToFit];
        [scrollInnerView addSubview:descTextView];
        
        if ([alert.link length]>0)
        {
            UIButton *linkButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [linkButton addTarget:self action:@selector(linkTapped:) forControlEvents:UIControlEventTouchUpInside];
            linkButton.frame = CGRectMake(title.frame.origin.x, 0, alertsScrollView.frame.size.width, alertsScrollView.frame.size.height);
            linkButton.backgroundColor = [UIColor clearColor];
            linkButton.autoresizingMask =UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
            linkButton.tag = 1*(i+1);
            [scrollInnerView addSubview:linkButton];
        }
        
        
        [alertsScrollView addSubview:scrollInnerView];
        [views addObject:scrollInnerView];
        [alertsScrollView flashScrollIndicators];
        
    }
    
    [self alignSubviews];
    
}

-(IBAction)linkTapped:(UIButton *)sender
{
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alerts" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    [self stopAnimatingAlerts];
    NSInteger index = sender.tag-1;
    Alerts *alert = [alertsArray objectAtIndex:index];
    
    
//    alertsWebview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-50, self.view.frame.size.height-80)];
//    alertsWebview.delegate = self;
//    
//    activityView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, alertsWebview.frame.size.width, alertsWebview.frame.size.height)];
//    activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
//    //    activityView.tintColor = [UIColor blueColor];
//    alertsWebview.scalesPageToFit = YES;
//    [alertsWebview addSubview:activityView];
//    activityView.hidden = YES;
    NSString *linkUrl =alert.link;
    //    NSURL *myURL;
    if ([linkUrl hasPrefix:@"http://"] || [linkUrl hasPrefix:@"https://"])
    {
        
    }
    else
    {
        linkUrl = [NSString stringWithFormat:@"http://%@",linkUrl];
    }
    
    MTPopupWindow *popup = [[MTPopupWindow alloc] init];
    popup.delegate = self;
    popup.fileName = linkUrl;
    [popup show];
}

- (void)alignSubviews
{
	// Position all the content views at their respective page positions
	alertsScrollView.contentSize = CGSizeMake([alertsArray count]*alertsScrollView.bounds.size.width,alertsScrollView.bounds.size.height);
	NSUInteger i = 0;
    
	for (UIView *v in views)
    {
		v.frame = CGRectMake(i * alertsScrollView.bounds.size.width, 0,
							 alertsScrollView.bounds.size.width, alertsScrollView.bounds.size.height);
        
        for (UIView*view in [v subviews])
        {
            if ([view isKindOfClass:[UITextView class]])
            {
                UITextView *textView =(UITextView *)view;
                //            [textView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                [textView scrollRangeToVisible:NSMakeRange(0, 0)];
            }
        }
		i++;
	}
}

-(void)scrollToFirstAlert
{
    CGPoint newOffset = CGPointMake(0, alertsScrollView.contentOffset.y);
    [alertsScrollView setContentOffset:newOffset animated:YES];
    alertsPageControl.currentPage = 0;
}
-(void)scrollToNextAlert
{
    
    NSUInteger page = alertsPageControl.currentPage + 1;
    if (page == [alertsArray count])
    {
        [self scrollToFirstAlert];
    }else
    {
        [UIView animateWithDuration:1.0 animations:^(void)
         {
             
             alertsPageControl.currentPage = alertsPageControl.currentPage+1;
             CGPoint newOffset = CGPointMake(alertsScrollView.bounds.size.width*alertsPageControl.currentPage, alertsScrollView.contentOffset.y);
             [alertsScrollView setContentOffset:newOffset animated:NO];
             
         }];
        
    }
    
}
#pragma mark -Rotation Delegates

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
								duration:(NSTimeInterval)duration {
	currentPage = alertsScrollView.contentOffset.x / alertsScrollView.bounds.size.width;
    
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
										 duration:(NSTimeInterval)duration {
    

    
    for (UIView *mine in [alertsScrollView subviews])
    {
        CGRect viewRect = mine.frame;
        CGRect mainRect = self.view.frame;
        if(CGRectIntersectsRect(mainRect, viewRect))
        {
            //NSLog(@"Current View Shown is not hidden");
            //view is visible
        }else{
            [mine setHidden:YES];
            
        }
        
        
    }
    [self alignSubviews];
    alertsScrollView.contentOffset = CGPointMake(alertsScrollView.bounds.size.width * currentPage, 0);
    
    
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    
    //scrollView is the main scroll view
    //mainview is scrollview.superview
    //view is the view inside the scroll view
    
    
    for (UIView *mine in [alertsScrollView subviews]) {
        [mine setHidden:NO];
        CGRect viewRect = mine.frame;
        CGRect mainRect = self.view.frame;
        if(CGRectIntersectsRect(mainRect, viewRect))
        {
            //view is visible
        }else{
            // [mine setHidden:NO];
            
        }
        
        
    }
}



#pragma mark - Pullableview methods
-(void)createPullableView
{

    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;

    UIImage *parternsImg = [UIImage imageNamed:@"partners_iPad"];
    UIImageView *parternsImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, parternsImg.size.width, parternsImg.size.height)];
    parternsImageView.image = parternsImg;
    
    pullUpView = [[PartnersPullableView alloc] initWithFrame:CGRectMake(500, 500, iOSDeviceScreenSize.width-100, parternsImg.size.height)];
    pullUpView.backgroundColor = [UIColor redColor];
    pullUpView.partnersDelegate = self;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        pullUpView.openedCenter = CGPointMake(400,iOSDeviceScreenSize.height-350);
        pullUpView.closedCenter = CGPointMake(1000, iOSDeviceScreenSize.height-350);

    }
    else
    {
        pullUpView.openedCenter = CGPointMake(400,iOSDeviceScreenSize.height-350);
        pullUpView.closedCenter = CGPointMake(1100, iOSDeviceScreenSize.height-350);
    }
    pullUpView.center = pullUpView.closedCenter;
    
    
    [pullUpView.handleView addSubview:parternsImageView];
    [pullUpView bringSubviewToFront:pullUpView.handleView];
    pullUpView.handleView.frame = CGRectMake(0, 0, parternsImageView.frame.size.width, parternsImageView.frame.size.height);
    pullUpView.delegate = self;
    [self.view addSubview:pullUpView];

    
    return;

    
    
}
- (void)pullableView:(PullableView *)pView didChangeState:(BOOL)opened
{
    if (opened)
    {
        //NSLog(@"Now I'm open!");
    }
    else
    {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(closePartner) object:self];
        //NSLog(@"Now I'm closed, pull me up again!");
    }
}
-(void)closePartner
{
    [pullUpView setOpened:NO animated:YES];
    
}
#pragma mark - UIScrollView Delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

/*
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //pageControlUsed = NO;
    [self stopAnimatingAlerts];
   // //NSLog(@"%f", alertsScrollView.contentOffset.x);
    
    CGFloat pageWidth = alertsScrollView.frame.size.width;
    //pageNumber = floor((self.scrollView.contentOffset.x - pageWidth / ([imageArray count]+2)) / pageWidth) + 1  ;
    currentPage = alertsScrollView.contentOffset.x / pageWidth;
    
    if(currentPage ==  0)
    {
        [alertsScrollView scrollRectToVisible:CGRectMake((alertsScrollView.frame.size.width * [alertsArray count]), 0, alertsScrollView.frame.size.width, alertsScrollView.frame.size.height) animated:NO];
        currentPage = [alertsArray count];
        //self.pageControl.currentPage = pageNumber;
    }
    else if(currentPage == ([alertsArray count]+1))
    {
        [alertsScrollView scrollRectToVisible:CGRectMake(alertsScrollView.frame.size.width, 0, alertsScrollView.frame.size.width, alertsScrollView.frame.size.height) animated:NO];
        currentPage = 1;
        //self.pageControl.currentPage = pageNumber;
    }
    
    alertsPageControl.currentPage = currentPage - 1;
    
}

*/

-(void)reloadAlerts
{
    
    [[DataDownloader sharedDownloader] setAlertsDelegate:self];
    [[AlertsModel sharedDataSource] fetchAlerts];
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = alertsScrollView.frame.size.width;
    int page = floor((alertsScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    alertsPageControl.currentPage = page;
    
    
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self stopAnimatingAlerts];
    CGFloat pageWidth = alertsScrollView.frame.size.width;
    int page = floor((alertsScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    //alertsPageControl.currentPage = page;
    
    if (0 == page%2)
    {
        //NSLog(@"page number %d %ld",page,(long)alertsPageControl.currentPage);
      //          alertsPageControl.currentPage = alertsPageControl.currentPage+1;
    }


}

- (CGSize)sizeForNumberOfPages:(NSInteger)pageCount
{
    return CGSizeMake(2, 2);
}
- (void)numberOfPagesInPagingView:(NSArray *)objectsArray
{
    int noPages = (int)[objectsArray count];
    if(1>=noPages)
    {
        alertsPageControl.hidden=true;
        alertsPageControl.tag=1;
    }
    else
    {
        alertsPageControl.hidden=false;
        int pageShareRatio=1;
        if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && noPages>18)
        {
            if(0==noPages%18)
            {
                pageShareRatio=noPages/18;
            }
            else
                pageShareRatio=(noPages-(noPages%18))/18+1;
        }
        alertsPageControl.numberOfPages=noPages/pageShareRatio;
        alertsPageControl.tag=pageShareRatio;
    }
}
-(void)stopAnimatingAlerts
{
    [myTimer invalidate];
    myTimer = nil;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)Alertsloaded
{
    
    UINavigationController *navController = (UINavigationController *)[[[[[[UIApplication sharedApplication] windows] objectAtIndex:0] rootViewController] childViewControllers] objectAtIndex:0];
    
    if ([navController.visibleViewController isKindOfClass:[SplashViewController_iPad class]]) {
        alertsView.hidden = YES;
    }
        //alertsView.hidden =NO;
        alertsArray = [[AlertsModel sharedDataSource] allAlerts];
        [self createAlertsPageControl];
        [self numberOfPagesInPagingView:alertsArray];
   
    
    
}
-(void)AlertsLoadFailure
{
    alertsArray = [[AlertsModel sharedDataSource] allAlerts];
    [self createAlertsPageControl];
    [self numberOfPagesInPagingView:alertsArray];
    alertsView.hidden = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)viewDidDisappear:(BOOL)animated
{
    if (dropDown.isOpened)
    {
        [dropDown hideDropDown:bigMenuBtn];
        dropDown=nil;
    }
    [myTimer invalidate];
    myTimer=nil;

}
- (void)makeMenuVisibleOrDisable:(id)sender
{
    if(dropDown == nil)
    {
        if(!modalView)
        {
            modalView = [[UIControl alloc] initWithFrame:self.view.bounds];
            [modalView addTarget:self action:@selector(dismissModel:) forControlEvents:UIControlEventTouchUpInside];
        }
        modalView.frame=CGRectMake(0, 0, self.view.bounds.size.width-52, self.view.bounds.size.height);
        modalView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;//self.view.autoresizingMask;
        modalView.backgroundColor=[UIColor clearColor];
        [self.view addSubview:modalView];
        
        CGFloat f = 300;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :nil :@"down"];
        dropDown.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin;
        dropDown.delegate = self;
        
    }
    else
    {
        [dropDown hideDropDown:sender];
        [modalView removeFromSuperview];
        dropDown=nil;
        
    }

}
- (IBAction)tappedOnMenu:(id)sender
{
    [self makeMenuVisibleOrDisable:sender];
}

-(IBAction)dismissModel:(id)sender
{
    [dropDown hideDropDown:bigMenuBtn];
    [modalView removeFromSuperview];
    dropDown=nil;
}


- (void)closePopUp{
    [dropDown hideDropDown:bigMenuBtn];
    
}
//- (void)dropDownHidden // Using for only when legal search finder
//{
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
//
//    UINavigationController *navController = (UINavigationController *)[[[[[[UIApplication sharedApplication] windows] objectAtIndex:0] rootViewController] childViewControllers] objectAtIndex:0];
//    UIViewController *visibleViewConroller = navController.visibleViewController;
//    LegalSearchDetailVC_iPad* toVC = [storyboard instantiateViewControllerWithIdentifier:@"LegalSearchDetailVC_iPad"];
//
//    if ([visibleViewConroller class] == [toVC class])
//    {
//        dropDown.hidden = YES;
//        legalSearchView.hidden = NO;
//    }
//}
//
- (void)didSelectedManuItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];

    UINavigationController *navController = (UINavigationController *)[[[[[[UIApplication sharedApplication] windows] objectAtIndex:0] rootViewController] childViewControllers] objectAtIndex:0];

    //NSLog(@"    navController.visibleViewController %@",    navController.visibleViewController);
    
    UIViewController *visibleViewConroller = navController.visibleViewController;
    
    switch (indexPath.row) {
        case 0:
        {
            
            HomeViewController_iPad* toVC = [storyboard instantiateViewControllerWithIdentifier:@"HomeViewController_iPad"];
            
            if ([visibleViewConroller class] != [toVC class])
            {
                [navController pushViewController:toVC animated:YES];
            }
        }
            break;
        case 1:
        {
            dropDown.hidden = YES;
            legalSearchView.hidden = NO;
            if (!legalAdvancedSearchView.isHidden) {
                    legalAdvancedSearchView.hidden =YES;
            }
            

        }
            break;
        case 2:
        {
            ImmigrationNews_iPadVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"ImmigrationNews_iPadVC"];
            if ([visibleViewConroller class] != [toVC class])
            {
                [navController pushViewController:toVC animated:YES];
            }
        }
            break;
        case 3:
        {
            TrainingsAndEventsVC_iPad* toVC = [storyboard instantiateViewControllerWithIdentifier:@"TrainingsAndEventsVC_iPad"];
            if ([visibleViewConroller class] != [toVC class])
            {
                [navController pushViewController:toVC animated:YES];
            }
        }
            break;
        case 4:
        {
            CommunityPoll_iPadVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"CommunityPoll_iPadVC"];
            if ([visibleViewConroller class] != [toVC class])
            {
                [navController pushViewController:toVC animated:YES];
            }
        }
            break;
        case 5:
        {
            AboutImmigoVC_iPad* toVC = [storyboard instantiateViewControllerWithIdentifier:@"AboutImmigoVC_iPad"];
            if ([visibleViewConroller class] != [toVC class])
            {
                [navController pushViewController:toVC animated:YES];
            }
        }
            break;

        default:
            break;
    }
    
    [dropDown hideDropDown:bigMenuBtn];
    dropDown=nil;
    [self reloadAlerts];
    [modalView removeFromSuperview];

    
}

- (IBAction)legalHelpFInderAdvancedSearchHeadingTapped:(id)sender{
    
    legalSearchView.hidden = YES;
    legalAdvancedSearchView.hidden =YES;
    dropDown.hidden = NO;
    if (locationOn)
    {
        // Turn off the location manager to save power.
        [locationManager stopUpdatingLocation];
    }
    [self makeMenuVisibleOrDisable:bigMenuBtn];
}
- (IBAction)advancedSearchBackButtonTapped:(id)sender{
    
    legalAdvancedSearchView.hidden =YES;
    legalSearchView.hidden =NO;
}


-(IBAction)moveToAdvancedSearch:(id)sender{
    
    legalSearchView.hidden =YES;
    legalAdvancedSearchView.hidden =NO;
    advancedSearchZipCodeTf.text =zipCodeField.text;
    
    advancedSearchZipCodeTf.userInteractionEnabled = !currentLocationSwitch.isOn;
    
    [advacnedSearchCurrentLocationSwitch setOn:currentLocationSwitch.isOn];
    aolTf.text =tolaTf.text =languageTf.text =@"";
}

- (IBAction)legalHelpFinderHeadingTapped:(id)sender
{
    legalSearchView.hidden = YES;
    dropDown.hidden = NO;
    if (locationOn)
    {
        // Turn off the location manager to save power.
        [locationManager stopUpdatingLocation];
    }
    [self makeMenuVisibleOrDisable:bigMenuBtn];
}

- (IBAction)currentLocationTapped:(id)sender
{
    if ([sender isOn])
    {
        
        //NSLog(@" lat: %f",locationManager.location.coordinate.latitude);
        //NSLog(@" lon: %f",locationManager.location.coordinate.longitude);
        
        zipCodeField.text = @"";
        zipCodeField.userInteractionEnabled = NO;
        
        if (locationManager == nil)
        {
            locationManager = [[CLLocationManager alloc] init];
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
            locationManager.delegate = self;
        }
        
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [locationManager requestWhenInUseAuthorization];
        }

        [locationManager startUpdatingLocation];
        
        locationOn = YES;
    }
    else
    {
        zipCodeField.userInteractionEnabled = YES;
    }


}

-(IBAction)advancedSearchCurrentLocationTapped:(id)sender{
    
    if ([sender isOn])
    {
        
        advancedSearchZipCodeTf.text = @"";
        advancedSearchZipCodeTf.userInteractionEnabled = NO;
        
        if (locationManager == nil)
        {
            locationManager = [[CLLocationManager alloc] init];
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
            locationManager.delegate = self;
        }
        [locationManager startUpdatingLocation];
        
        locationOn = YES;
    }
    else
    {
        advancedSearchZipCodeTf.userInteractionEnabled = YES;
    }

    
}
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if([CLLocationManager locationServicesEnabled])
    {
        
        //NSLog(@"Location Services Enabled");
        
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Immigo"
                                                            message:@"Failed to get your location as location services are disabled at the device level."
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Immigo"
                                                        message:@"Failed to get your location as location services are disabled for IMMIGO application."
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    currentLocationSwitch.on = NO;
    advacnedSearchCurrentLocationSwitch.on =NO;
    [locationManager stopUpdatingLocation];
    zipCodeField.userInteractionEnabled = YES;
    advancedSearchZipCodeTf.userInteractionEnabled =YES;
   

}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation * currentLocation = (CLLocation *)[locations lastObject];
    NSLog(@"Location: %@", currentLocation);
    if (currentLocation != nil)
    {
        currentLattitude = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
        currentLongitude = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
    }
    [locationManager stopUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil)
    {
        currentLattitude = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
        currentLongitude = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
    }
    [locationManager stopUpdatingLocation];
    
}

- (IBAction)homeButtonTapped:(id)sender
{
    legalSearchView.hidden = YES;
    legalAdvancedSearchView.hidden =YES;
    dropDown.hidden = NO;
    if (locationOn)
    {
        // Turn off the location manager to save power.
        [locationManager stopUpdatingLocation];
    }
    [self makeMenuVisibleOrDisable:bigMenuBtn];
}

- (IBAction)zipcodeTapped:(id)sender
{
    [zipCodeField resignFirstResponder];
    
    
    
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Legal Help Finder" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    if ([zipCodeField.text length]==0 && ![currentLocationSwitch isOn] )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Legal Help Finder" message:@"You must enter a zip code or use your current location to search for organizations." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alert show];
    }else if (([zipCodeField.text length]<5 || [zipCodeField.text isEqual:@"00000"]) && ![currentLocationSwitch isOn])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Legal Help Finder" message:@"You must enter a valid zip code or use your current location to search for organizations." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alert show];
        
    }
    else
    {
        dropDown.hidden = NO;
        legalSearchView.hidden = YES;

        //NSLog(@"lattitude %@ and longitude %@",currentLattitude,currentLongitude);
        [[LegalFinderModel sharedDataSource] fetchLegalFindersWithZipCode:zipCodeField.text OrWithCurrentLocationLattitude:currentLattitude AndLongitude:currentLongitude];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
        UINavigationController *navController = (UINavigationController *)[[[[[[UIApplication sharedApplication] windows] objectAtIndex:0] rootViewController] childViewControllers] objectAtIndex:0];
        UIViewController *visibleViewConroller = navController.visibleViewController;

        LegalSearchListVC_iPad* toVC = [storyboard instantiateViewControllerWithIdentifier:@"LegalSearchDetailVC_iPad"];
        toVC.zipCode = zipCodeField.text;
        toVC.coordinates = CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude);
        [DataDownloader sharedDownloader].delegate = (id)toVC;

        if ([visibleViewConroller class] != [toVC class])
        {
            [navController pushViewController:toVC animated:YES];
        }
        else
        {
            [navController pushViewController:toVC animated:NO];
        }
    }

    
    
}
#pragma mark - UItextfield delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    
    NSInteger insertDelta = string.length - range.length;
    if (textField.text.length + insertDelta >
        5)         {            return NO;
        
    }
    
    return ([string isEqualToString:@""] || (([string stringByTrimmingCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].length > 0) && newLength <= 5) ) ;
   // return (newLength > 5 || ![self isNumeric:string]) ? NO : YES;
}


-(BOOL)isNumeric:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}



-(IBAction)advancedSearchSearchButtonTapped:(id)sender{
    
    // get languageIDs
    NSArray *languageIds;
    NSString *languageString;
    if ([languageTf.text length]==1)
    {
        // code to write for when field exists single value
    }else
    {
        if (languageTf.text && languageTf.text.length>0) {
            languageIds = [languageTf.text componentsSeparatedByString:@","];
            
            for (int i=0; i<languageIds.count; i++)
            {
                if (!languageString)
                {
                    languageString = [[LanguageModel sharedDataSource] getIdForValue:[languageIds objectAtIndex:0]];
                }
                else
                {
                    NSString *string1 = [NSString stringWithFormat:@",%@",[[LanguageModel sharedDataSource] getIdForValue:[languageIds objectAtIndex:i]]];
                    languageString = [languageString stringByAppendingString:string1];
                }
            }
        }
        
    }
    
    
    
    // get Area of laws
    
    NSArray *aolIds;
    NSString *aolString;
    if ([aolTf.text length]==1)
    {
        // code to write for when field exists single value
    }else
    {
        
        if (aolTf.text && aolTf.text.length>0) {
            aolIds = [aolTf.text componentsSeparatedByString:@","];
            
            for (int i=0; i<aolIds.count; i++)
            {
                if (!aolString)
                {
                    aolString = [[AOLModel sharedDataSource] getLegalIdForValue:[aolIds objectAtIndex:0]];
                }
                else
                {
                    NSString *string1 = [NSString stringWithFormat:@",%@",[[AOLModel sharedDataSource] getLegalIdForValue:[aolIds objectAtIndex:i]]];
                    aolString = [aolString stringByAppendingString:string1];
                }
            }
        }
    }
    
    
    // get legal assiatance
    
    NSArray *legalAssitanceIds;
    NSString *legalAssistanceString;
    if ([tolaTf.text length]==1)
    {
        // code to write for when field exists single value
    }else
    {
        
        if (tolaTf.text && tolaTf.text.length>0) {
            legalAssitanceIds = [tolaTf.text componentsSeparatedByString:@","];
            
            for (int i=0; i<legalAssitanceIds.count; i++)
            {
                if (!legalAssistanceString)
                {
                    legalAssistanceString = [[LegalAssistanceModel sharedDataSource] getIdForValue:[legalAssitanceIds objectAtIndex:i]];
                }
                else
                {
                    NSString *string1 = [NSString stringWithFormat:@",%@",[[LegalAssistanceModel sharedDataSource] getIdForValue:[legalAssitanceIds objectAtIndex:i]]];
                    legalAssistanceString = [legalAssistanceString stringByAppendingString:string1];
                }
            }
        }
        
    }
    
    
    [Common logMainEventsToGoogleAnalytics:@"LegalHelpFinder Advanced Search" withAction:@"LegalHelpFinder Advanced Search tapped" andLabel:@"" and:0];
    
    
    if ([advancedSearchZipCodeTf.text length]==0 && ![advacnedSearchCurrentLocationSwitch isOn] )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Legal Help Finder" message:@"You must enter a zip code or use your current location and select at least one of the filters to search for organizations." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alert show];
    }else if (([advancedSearchZipCodeTf.text length]<5 || [advancedSearchZipCodeTf.text  isEqual:@"00000"]) && ![advacnedSearchCurrentLocationSwitch isOn])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Legal Help Finder" message:@"You must enter a zip code or use your current location and select at least one of the filters to search for organizations." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alert show];
        
    }
    else if (languageTf.text.length == 0 && aolTf.text.length == 0 && tolaTf.text.length == 0){
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Legal Help Finder" message:@"Please select at least one of the filters to search for organizations or tap Basic Search." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alert show];
        
    }
    
    else
    {
        
        [[LegalFinderModel sharedDataSource] fetchLegalFindersWithZipCode:advancedSearchZipCodeTf.text OrWithCurrentLocationLattitude:currentLattitude AndLongitude:currentLongitude withLanguages:languageString withAreasOfLaws:aolString withLegalAssistance:legalAssistanceString];
        
        
        UIStoryboard *storyboard;
        if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        else
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
        UINavigationController *navController = (UINavigationController *)[[[[[[UIApplication sharedApplication] windows] objectAtIndex:0] rootViewController] childViewControllers] objectAtIndex:0];
        UIViewController *visibleViewConroller = navController.visibleViewController;
        LegalSearchListVC_iPad* toVC = [storyboard instantiateViewControllerWithIdentifier:@"LegalSearchDetailVC_iPad"];
        toVC.zipCode = advancedSearchZipCodeTf.text;
        toVC.isFromAdvacnedSearch =YES;
        toVC.coordinates = CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude);
        [DataDownloader sharedDownloader].delegate = (id)toVC;
        
        if ([visibleViewConroller class] != [toVC class])
        {
            [navController pushViewController:toVC animated:YES];
        }
        else
        {
            [navController pushViewController:toVC animated:NO];
        }

    }

}
- (IBAction)advancedSearchTapped:(id)sender
{
    [zipCodeField resignFirstResponder];
    
    [Common logMainEventsToGoogleAnalytics:@"LegalHelpFinder Search" withAction:@"LegalHelpFinder Search tapped" andLabel:@"" and:0];
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Legal Help Finder" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    if ([zipCodeField.text length]==0 && ![currentLocationSwitch isOn] )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Legal Help Finder" message:@"You must enter a zip code or use your current location to search for organizations." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alert show];
    }else if (([zipCodeField.text length]<5 || [zipCodeField.text isEqual:@"00000"]) && ![currentLocationSwitch isOn])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Legal Help Finder" message:@"You must enter a valid zip code or use your current location to search for organizations." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Legal Help Finder" message:@"You must enter a valid zip code or use your current location to search for organizations." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        //        [alert show];
    }
    else
    {
        //NSLog(@"lattitude %@ and longitude %@",currentLattitude,currentLongitude);
        [[LegalFinderModel sharedDataSource] fetchLegalFindersWithZipCode:zipCodeField.text OrWithCurrentLocationLattitude:currentLattitude AndLongitude:currentLongitude];
        UIStoryboard *storyboard;
        if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        else
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
        UINavigationController *navController = (UINavigationController *)[[[[[[UIApplication sharedApplication] windows] objectAtIndex:0] rootViewController] childViewControllers] objectAtIndex:0];
        UIViewController *visibleViewConroller = navController.visibleViewController;
        LegalSearchListVC_iPad* toVC = [storyboard instantiateViewControllerWithIdentifier:@"LegalSearchDetailVC_iPad"];
        toVC.zipCode = zipCodeField.text;
        toVC.coordinates = CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude);
        [DataDownloader sharedDownloader].delegate = (id)toVC;
        
        if ([visibleViewConroller class] != [toVC class])
        {
            [navController pushViewController:toVC animated:YES];
        }
        else
        {
            [navController pushViewController:toVC animated:NO];
        }
    }

}

- (IBAction)aolTapped:(UIButton *)sender {
    
    
    if ([popoverController isPopoverVisible]) {
        [popoverController dismissPopoverAnimated:YES];
    }
     [dataArray removeAllObjects];
    dataArray = [[[AOLModel sharedDataSource] allAreas] mutableCopy];
    
    NSArray *existingValues;
    if ([aolTf.text length]>0)
    {
        existingValues = [aolTf.text componentsSeparatedByString:@","];
    }else
    {
        existingValues = nil;
    }
    NSArray *array =[NSArray arrayWithArray:existingValues];
    
    if ([array count]>0)
    {
        [selectedIndexes removeAllObjects];
        [selectedValues removeAllObjects];
        for (int i=0; i<array.count; i++)
        {
            NSUInteger index= [dataArray indexOfObject:[array objectAtIndex:i]];
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:0];
            [selectedIndexes addObject:indexpath];
            [selectedValues addObject:[dataArray objectAtIndex:index]];
        }
    }
    
    if (!pickerTable)
    {
        pickerTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 44, 280.0f, 355.0f)
                                                   style:UITableViewStylePlain];
        pickerTable.delegate = self;
        pickerTable.dataSource = self;
        pickerTable.rowHeight = 40;
    }
    pickerTable.rowHeight = 60;
    CGRect frame=pickerTable.frame;
    frame.size.height =355.0f;
    [pickerTable setFrame:frame];
    pickerTable.tag =1;
    pickerTable.tableHeaderView=nil;
  
    
    
    
    UIViewController *viewController = [[UIViewController alloc] init];
    viewController.view.frame = CGRectMake(0, 0, 280, 400);
    UIView *titleView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, pickerTable.frame.size.width, 44)];
    UIButton *doneButton =[[UIButton alloc] initWithFrame:CGRectMake(titleView.frame.size.width-60, 7, 50, 30)];
    [doneButton addTarget:self action:@selector(doneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [titleView setBorderForSides:UIBorderBottom withColor:[UIColor lightGrayColor] andWidth:0.5f];
    [doneButton setTitleColor:[UIColor colorWithRed:36/255.0 green:71/255.0 blue:113/255.0 alpha:1.0] forState:UIControlStateNormal];
    [doneButton setBackgroundColor:[UIColor clearColor]];
    [doneButton.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
//    [[doneButton layer] setCornerRadius:2.0f];
//    
//    [[doneButton layer] setMasksToBounds:YES];
//    
//    [[doneButton layer] setBorderWidth:1.0f];
  //  doneButton.layer.borderColor = [UIColor blueColor].CGColor;
    
    
    [titleView addSubview:doneButton];
    [titleView setBackgroundColor:[UIColor whiteColor]];
    [viewController.view setBackgroundColor:[UIColor whiteColor]];
    [viewController.view addSubview:pickerTable];
    [viewController setPreferredContentSize:viewController.view.frame.size];
    [viewController.view addSubview:titleView];
    popoverController = [[UIPopoverController alloc] initWithContentViewController:viewController];
    [popoverController presentPopoverFromRect:sender.bounds
                                       inView:sender
                     permittedArrowDirections:UIPopoverArrowDirectionUp
                                     animated:YES];
    popoverController.delegate =self;
    [pickerTable reloadData];
    [pickerTable setContentOffset:CGPointZero animated:YES];
   
    
}


-(IBAction)doneButtonPressed:(id)sender{
    [self.popoverController dismissPopoverAnimated:YES];
    [self passValuesToField:selectedValues];
    [selectedIndexes removeAllObjects];
    [selectedValues removeAllObjects];
    if (isSearchEnabled) {
        isSearchEnabled =NO;
    }
    return;
}
- (IBAction)legalAssistanceTapped:(UIButton*)sender {
    
    if ([popoverController isPopoverVisible]) {
        [popoverController dismissPopoverAnimated:YES];
    }
    
     [dataArray removeAllObjects];
    dataArray = [[[LegalAssistanceModel sharedDataSource] allObjects] mutableCopy];
    
    NSArray *existingValues;
    if ([tolaTf.text length]>0)
    {
        existingValues = [tolaTf.text componentsSeparatedByString:@","];
    }else
    {
        existingValues = nil;
    }
    NSArray *array =[NSArray arrayWithArray:existingValues];
    
    if ([array count]>0)
    {
        [selectedIndexes removeAllObjects];
        [selectedValues removeAllObjects];
        for (int i=0; i<array.count; i++)
        {
            NSUInteger index= [dataArray indexOfObject:[array objectAtIndex:i]];
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:0];
            [selectedIndexes addObject:indexpath];
            [selectedValues addObject:[dataArray objectAtIndex:index]];
        }
    }

    
    if (!pickerTable)
    {
        pickerTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 44, 280.0f, 156.0f)
                                                   style:UITableViewStylePlain];
        pickerTable.delegate = self;
        pickerTable.dataSource = self;
        pickerTable.rowHeight = 40;
    }
    pickerTable.rowHeight = 60;
    CGRect frame=pickerTable.frame;
    frame.size.height =156.0f;
    [pickerTable setFrame:frame];
    pickerTable.tag =2;
    pickerTable.tableHeaderView=nil;
    
    
    UIViewController *viewController = [[UIViewController alloc] init];
    viewController.view.frame = CGRectMake(0, 0, 280, 200.0f);
    UIView *titleView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, pickerTable.frame.size.width, 44)];
    UIButton *doneButton =[[UIButton alloc] initWithFrame:CGRectMake(titleView.frame.size.width-60, 7, 50, 30)];
    [doneButton addTarget:self action:@selector(doneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [titleView setBorderForSides:UIBorderBottom withColor:[UIColor lightGrayColor] andWidth:0.5f];
    [doneButton setTitleColor:[UIColor colorWithRed:36/255.0 green:71/255.0 blue:113/255.0 alpha:1.0] forState:UIControlStateNormal];
    [doneButton setBackgroundColor:[UIColor clearColor]];
    [doneButton.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    //    [[doneButton layer] setCornerRadius:2.0f];
    //
    //    [[doneButton layer] setMasksToBounds:YES];
    //
    //    [[doneButton layer] setBorderWidth:1.0f];
    //  doneButton.layer.borderColor = [UIColor blueColor].CGColor;
    
    
    [titleView addSubview:doneButton];
    [titleView setBackgroundColor:[UIColor whiteColor]];
    [viewController.view setBackgroundColor:[UIColor whiteColor]];
    [viewController.view addSubview:titleView];
    [viewController.view addSubview:pickerTable];
    [viewController setPreferredContentSize:viewController.view.frame.size];
    popoverController = [[UIPopoverController alloc] initWithContentViewController:viewController];
    popoverController.delegate =self;
    [popoverController presentPopoverFromRect:sender.bounds
                                       inView:sender
                     permittedArrowDirections:UIPopoverArrowDirectionUp
                                     animated:YES];
    [pickerTable reloadData];
    [pickerTable setContentOffset:CGPointZero animated:YES];

}

- (IBAction)languagesTapped:(UIButton *)sender {
    
    if ([popoverController isPopoverVisible]) {
        [popoverController dismissPopoverAnimated:YES];
    }
   
  [dataArray removeAllObjects];
  dataArray =[[[LanguageModel sharedDataSource] allObjects] mutableCopy];
    isSearchEnabled =NO;
    NSArray *existingValues;
    if ([languageTf.text length]>0)
    {
        existingValues = [languageTf.text componentsSeparatedByString:@","];
    }else
    {
        existingValues = nil;
    }
    NSArray *array =[NSArray arrayWithArray:existingValues];
    
    if ([array count]>0)
    {
        [selectedIndexes removeAllObjects];
        [selectedValues removeAllObjects];
        for (int i=0; i<array.count; i++)
        {
            NSUInteger index= [dataArray indexOfObject:[array objectAtIndex:i]];
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:0];
            [selectedIndexes addObject:indexpath];
            [selectedValues addObject:[dataArray objectAtIndex:index]];
        }
    }
    
    if (!pickerTable)
    {
        pickerTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 44, 280.0f, 356.0f)
                                                   style:UITableViewStylePlain];
        pickerTable.delegate = self;
        pickerTable.dataSource = self;
        pickerTable.rowHeight = 40;
        
        
    }
    pickerTable.rowHeight = 40;
    CGRect frame=pickerTable.frame;
    frame.size.height =356.0f;
    [pickerTable setFrame:frame];
    pickerTable.tag =3;
    UISearchBar *searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, 280, 45)];
    //temp.tintColor = [UIColor redColor];
    searchBar.backgroundImage = [[UIImage alloc] init];
    searchBar.tintColor = [UIColor blackColor];
    searchBar.showsCancelButton=NO;
    searchBar.autocorrectionType=UITextAutocorrectionTypeNo;
    searchBar.autocapitalizationType=UITextAutocapitalizationTypeNone;
    searchBar.delegate=self;
    searchBar.placeholder = @"Search Languages";
    
    for (UIView *view in searchBar.subviews){
        if ([view isKindOfClass: [UITextField class]]) {
            UITextField *tf = (UITextField *)view;
            tf.delegate = self;
            break;
        }
    }
    
    pickerTable.tableHeaderView=searchBar;
    UIViewController *viewController = [[UIViewController alloc] init];
    viewController.view.frame = CGRectMake(0, 0, 280, 400.0f);
    UIView *titleView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, pickerTable.frame.size.width, 44)];
    UIButton *doneButton =[[UIButton alloc] initWithFrame:CGRectMake(titleView.frame.size.width-60, 7, 50, 30)];
    [doneButton addTarget:self action:@selector(doneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [titleView setBorderForSides:UIBorderBottom withColor:[UIColor lightGrayColor] andWidth:0.5f];
    [doneButton setTitleColor:[UIColor colorWithRed:36/255.0 green:71/255.0 blue:113/255.0 alpha:1.0] forState:UIControlStateNormal];
    [doneButton setBackgroundColor:[UIColor clearColor]];
    [doneButton.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    //    [[doneButton layer] setCornerRadius:2.0f];
    //
    //    [[doneButton layer] setMasksToBounds:YES];
    //
    //    [[doneButton layer] setBorderWidth:1.0f];
    //  doneButton.layer.borderColor = [UIColor blueColor].CGColor;
    
    
    [titleView addSubview:doneButton];
    [titleView setBackgroundColor:[UIColor whiteColor]];
    [viewController.view setBackgroundColor:[UIColor whiteColor]];
    [viewController.view addSubview:titleView];
    [viewController.view addSubview:pickerTable];
    [viewController setPreferredContentSize:viewController.view.frame.size];
    popoverController = [[UIPopoverController alloc] initWithContentViewController:viewController];
    popoverController.delegate =self;
    [popoverController presentPopoverFromRect:sender.bounds
                                       inView:sender
                     permittedArrowDirections:UIPopoverArrowDirectionUp
                                     animated:YES];
    [pickerTable reloadData];
    [pickerTable setContentOffset:CGPointZero animated:YES];


}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    
    NSUInteger count =isSearchEnabled ? [searchFilteredArray count]:[dataArray count];
    
    if (count<=0)
    {
        if (!infoTextLabel)
            infoTextLabel = [[UILabel alloc] init];
        infoTextLabel.frame = CGRectMake(aTableView.frame.origin.x, 40, aTableView.frame.size.width, 40);//childView.frame;
        infoTextLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        infoTextLabel.backgroundColor = [UIColor clearColor];
        infoTextLabel.textAlignment = NSTextAlignmentCenter;
        infoTextLabel.textColor = UIColorFromRGB(0x70706f);
        infoTextLabel.font = [UIFont fontWithName:FONT_IPAD size:14.32f];//[UIFont boldSystemFontOfSize:16.0];
        infoTextLabel.text = NSLocalizedString(@"No results found", @"ERRORNOTRAINITEMS");
        [aTableView addSubview:infoTextLabel];
    }
    else{
        [infoTextLabel removeFromSuperview];
        infoTextLabel = nil;
    }
    return  count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    NSString *title =  isSearchEnabled? [searchFilteredArray objectAtIndex:indexPath.row]:[dataArray objectAtIndex:indexPath.row];

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.backgroundColor = [UIColor whiteColor];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.font = [UIFont fontWithName:@"OpenSans" size:14.0f];    // Configure the cell.
    cell.textLabel.text =title;
    
    if([selectedValues containsObject:title])
    {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    } else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([selectedIndexes containsObject:indexPath])
    {
        [selectedIndexes removeObject:indexPath];
        [selectedValues removeObject:isSearchEnabled? [searchFilteredArray objectAtIndex:indexPath.row]:[dataArray objectAtIndex:indexPath.row]];
    } else
    {
        [selectedIndexes addObject:indexPath];
        [selectedValues addObject:isSearchEnabled? [searchFilteredArray objectAtIndex:indexPath.row]:[dataArray objectAtIndex:indexPath.row]];
    }
    [tableView reloadData];
}

-(void)passValuesToField:(NSArray *)values
{
    NSSet* mySetWithUniqueItems= [NSSet setWithArray: values];
    NSString *string=@"";
    values =[[mySetWithUniqueItems allObjects]  sortedArrayUsingSelector:@selector(compare:)];
    string = [values componentsJoinedByString:@","];
    //NSLog(@"array %@",values);
    switch (pickerTable.tag) {
        case 1:
        {
            aolTf.text =@"";
            aolTf.text =string;
        }
            break;
        case 2:
        {
            tolaTf.text =@"";
            tolaTf.text =string;
        }
            break;
        case 3:
        {
            languageTf.text =@"";
            languageTf.text =string;
        }
            break;
            
        default:
            break;
    }
    
}

/* Called on the delegate when the popover controller will dismiss the popover. Return NO to prevent the dismissal of the view.
 */
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    
    return YES;
}

/* Called on the delegate when the user has taken action to dismiss the popover. This is not called when -dismissPopoverAnimated: is called directly.
 */
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    
    NSLog(@"Dismissing Popover");
    [self passValuesToField:selectedValues];
    [selectedIndexes removeAllObjects];
    [selectedValues removeAllObjects];
    if (isSearchEnabled) {
        isSearchEnabled =NO;
    }
}

//searchbar change characters
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@". "]) {
        return NO;
    }
    return YES;
}
//search button was tapped
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self handleSearch:searchBar];
}

//user tapped on the cancel button
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    NSLog(@"User canceled search");
    [searchBar resignFirstResponder];
    if (isSearchEnabled) {
        [selectedIndexes removeAllObjects];
        [selectedValues removeAllObjects];
    }
    isSearchEnabled =NO;
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
      if([searchText length] == 0 && searchBar.text.length == 0)
    {
        isSearchEnabled =NO;
        [searchBar performSelector: @selector(resignFirstResponder)
                        withObject: nil
                        afterDelay: 0.1];
    }
}

//user finished editing the search text
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [self handleSearch:searchBar];
}

- (void)handleSearch:(UISearchBar *)searchBar {
    
    _searchString = [searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([_searchString isEqualToString:@""]) {
        [searchBar resignFirstResponder];
    }
    [searchBar resignFirstResponder];
    if (_searchString.length >0) {
        isSearchEnabled=true;
    }
    //check what was passed as the query String and get rid of the keyboard
    NSLog(@"User searched for %@", searchBar.text);
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self contains[cd] %@", _searchString];
    NSArray *foundPersonArray = [dataArray filteredArrayUsingPredicate:predicate];
    searchFilteredArray=[foundPersonArray mutableCopy];
    
    if ([_searchString isEqualToString:@""]) {
        isSearchEnabled=false;
        
    }
    [pickerTable reloadData];
    [searchBar resignFirstResponder];
    
}

//- (void)searchBarCancelButtonClicked:(UISearchBar *) aSearchBar {
//    [aSearchBar resignFirstResponder];
//}
//
//- (BOOL)textFieldShouldClear:(UITextField *)textField {
//    //if we only try and resignFirstResponder on textField or searchBar,
//    //the keyboard will not dissapear (at least not on iPad)!
//    [self performSelector:@selector(searchBarCancelButtonClicked:) withObject:self.searchBar afterDelay: 0.1];
//    return YES;
//}
@end
