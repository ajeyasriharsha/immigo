//
//  CommunityPoll_iPadVC.m
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "CommunityPoll_iPadVC.h"
#import "CommunityPollTileView_iPad.h"
#import "Common.h"
#import "CommunityPollDetailVC_iPad.h"
#import "AppDelegate.h"

@interface CommunityPoll_iPadVC (){
    
     GridView *itemsGrid;
    __unsafe_unretained IBOutlet UIImageView *logo;
    __unsafe_unretained IBOutlet UIButton *backButton;
    
    IBOutlet UILabel *headingLabel;
    NSArray *pollQuestions;
    __weak IBOutlet UIImageView *dividerImageView;
    IBOutlet UIImageView *scrollIndicatorImgView;

}

@end

@implementation CommunityPoll_iPadVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    headingLabel.font = [UIFont fontWithName:FONT_IPAD size:22.0f];
    //headingLabel.textColor= UIColorFromRGB(UI_TRAINING_HEADER_TEXT_COLOR_IPAD);
    headingLabel.textColor =[UIColor lightGrayColor];
    UIImage *divider = [UIImage imageNamed:@"menuTitleDivider"];
    dividerImageView.image = [divider stretchableImageWithLeftCapWidth:divider.size.width/2 topCapHeight:divider.size.height/2];

    
    itemsGrid = [[GridView alloc] initWithFrame:CGRectMake(-20, 150, self.view.frame.size.width+10, self.view.frame.size.height-300)];
    itemsGrid.backgroundColor = [UIColor clearColor];
    itemsGrid.gridViewDelegate = self;
    itemsGrid.tileSizePortrait = CGSizeMake(250,100);
    itemsGrid.tileSizeLandScape = CGSizeMake(350,100);
    itemsGrid.tiles_gap = CGSizeMake(78, 10);
    [itemsGrid reloadData];
    itemsGrid.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    
    [self.view addSubview:itemsGrid];

    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate]refreshFakeViewController];

    [[DataDownloader sharedDownloader] setDelegate:self];
    [[PollQuestionsModel sharedDataSource] fetchPollQuestionsForDevice:[[NSUserDefaults standardUserDefaults] valueForKey:IMMIGO_GUID]];
    
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
}

-(BOOL)canShowScrollIndicator{
    CGFloat gridViewFrameHeight =itemsGrid.frame.size.height;
    float scrollContentSizeHeight = itemsGrid.contentScrollView.contentSize.height;
    return scrollContentSizeHeight > gridViewFrameHeight;
}

#pragma mark - DataDownloader delegate
- (void) pollQuestionsDownloadedSuccessfully
{
    pollQuestions = [[PollQuestionsModel sharedDataSource] allObjects];
    [itemsGrid reloadData];
}
-(void) pollQuestionsLoadedFailure
{
    pollQuestions = [[PollQuestionsModel sharedDataSource] allObjects];
    [itemsGrid reloadData];
}
#pragma Mark #gridView delgate methods

- (int)numberOfTilesIngridView:(GridView *)gridView
{
    return pollQuestions.count;
}
- (UIView *)gridView:(GridView *)gridView viewAtIndex:(int)index
{
   PollQuestions *pollQuestion = [pollQuestions objectAtIndex:index];
    CommunityPollTileView_iPad *favTileView = [[CommunityPollTileView_iPad alloc] initWithFrame:CGRectMake(0, 0, itemsGrid.tileSizeLandScape.width, itemsGrid.tileSizeLandScape.height)];
    [favTileView setPollQuestion:pollQuestion];
    return favTileView;

}

-(void)gridView:(GridView*) gridView didSelectTileAtIndex:(int) index
{
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    
    CommunityPollDetailVC_iPad* toVC = [storyboard instantiateViewControllerWithIdentifier:@"CommunityPollDetailVC_iPad"];
    toVC.pollQuestionIndex = index;
    [self.navigationController pushViewController:toVC animated:YES];
    
    
}
-(void)gridView:(GridView*) gridView didSelectedTile:(UIView*) view
{
    
}
-(void)gridViewshouldChangeScrollIndicators:(BOOL)toEnd{
    
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    scrollIndicatorImgView.image =toEnd?[UIImage imageNamed:@"scrollIndicator_up"]:[UIImage imageNamed:@"scrollIndicator_down"];
}

- (void)gridRefresh
{
    [itemsGrid reloadData];
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
}
-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
