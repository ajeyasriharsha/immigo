//
//  TrainingTileView_iPad.m
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "TrainingTileView_iPad.h"
#import "NSDate+Helper.h"
#import "Categories.h"

#import "Common.h"
@implementation TrainingTileView_iPad



-(void)customIntialization
{
    {
        self.clipsToBounds = YES;
        self.backgroundColor  =[UIColor clearColor];
//        self.layer.borderColor  =[UIColor grayColor].CGColor;
//        self.layer.borderWidth = 1.0f;
    }
    
    {
        topicTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, self.frame.size.width-30, self.frame.size.height*0.6)];
        topicTitle.autoresizingMask =UIViewAutoresizingFlexibleWidth;
        topicTitle.backgroundColor = [UIColor clearColor];
        topicTitle.numberOfLines = 2;
        topicTitle.font = [UIFont fontWithName:FONT_IPAD size:18.0f];
        topicTitle.textColor = [UIColor darkGrayColor];
        
        
        subtopicTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, topicTitle.frame.size.height+topicTitle.frame.origin.y, self.frame.size.width-30, self.frame.size.height*0.3)];
        subtopicTitle.autoresizingMask =UIViewAutoresizingFlexibleWidth;
        subtopicTitle.backgroundColor = [UIColor clearColor];
        subtopicTitle.alpha = 1;

        subtopicTitle.font = [UIFont fontWithName:FONT_SEMIBOLD size:10.0f];
        subtopicTitle.textColor = [UIColor redColor];
        
        UIImage *buttonBg = [UIImage imageNamed:@"arrow_unselected"];
        arrowImage =[[UIImageView alloc] initWithImage:buttonBg highlightedImage:buttonBg];
        arrowImage.frame =CGRectMake(self.frame.size.width-20, 0, 20, self.frame.size.height);
        arrowImage.contentMode =UIViewContentModeScaleAspectFit;
        arrowImage.autoresizingMask =UIViewAutoresizingFlexibleLeftMargin;
        
        
        seperatorimageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-1, self.frame.size.width, 1)];
        UIImage *textFiledBackGround = [UIImage imageNamed:@"menuTitleDivider"];
        seperatorimageView.image = [textFiledBackGround stretchableImageWithLeftCapWidth:textFiledBackGround.size.width/2 topCapHeight:textFiledBackGround.size.height/2];
        
        //        imgaeView.image = [image Stre];
        seperatorimageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        seperatorimageView.backgroundColor =[UIColor clearColor];


        [self addSubview:topicTitle];
        [self addSubview:subtopicTitle];
        [self addSubview:seperatorimageView];
        [self addSubview:seperatorimageView];
        [self addSubview:arrowImage];
//        [self bringSubviewToFront:topicTitle];
//        [self bringSubviewToFront:subtopicTitle];
        
    }
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        [self customIntialization];
    }
    return self;
}
-(void)setEvent:(Event *)event
{
    _event =event;
    
    NSString *address =@"";
    if ([_event.locationCity length]>0)
    {
        address = _event.locationCity;
        address = [address stringByAppendingString:@","];
    }
    if ([_event.locationState length]>0) {
        address = [address stringByAppendingString:event.locationState];
        
    }
    
    address = [address trimmedString];
    NSRange range = [event.title rangeOfString:@"^\\s*" options:NSRegularExpressionSearch];
    NSString *trimmedTitle = [event.title stringByReplacingCharactersInRange:range withString:@""];
    NSString *title = [address length] > 35 ? [[address substringToIndex:35] stringByAppendingString:@"..."] :address;
    
    NSDate *displayDate =[self getDateFromString:[event.startDate displayDateFormat] withTime:event.dailyStartTime];
    NSString *displayDateString = [NSDate stringFromDate:displayDate withFormat:@"YYYY-MM-dd"];
    
    
  //  cell.topicTitle.text = [NSString stringWithFormat:@"%@ | %@",title,[displayDateString displayDateFormat]];
    
//    cell.topicSubTitle.text = [NSString stringWithFormat:@"%@",trimmedTitle];

    subtopicTitle.text= [NSString stringWithFormat:@"%@ | %@",title,[displayDateString displayDateFormat]];
//    CGSize labelSize = [topicTitle.text sizeWithFont:topicTitle.font constrainedToSize:CGSizeMake(300, 300000) lineBreakMode:NSLineBreakByWordWrapping];
//    topicTitle.frame =CGRectMake(topicTitle.frame.origin.x, topicTitle.frame.origin.y, topicTitle.frame.size.width, labelSize.height);
    topicTitle.text = [NSString stringWithFormat:@"%@",trimmedTitle];
    //[self setNeedsDisplay];
    
    
}
- (NSDate*)getDateFromString:(NSString *)dateString withTime:(NSString *)time {
    
    NSDateFormatter *reDateFormatter = [[NSDateFormatter alloc] init];
    [reDateFormatter setDateFormat:@"MMM d, yyyy"];
    NSDate *date = [reDateFormatter dateFromString:dateString];
    NSInteger hourVal;
    NSInteger minuteVal;
    NSCalendar *calendar=[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components=[calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    NSArray* stringComponents=[time componentsSeparatedByString:@":"];
    NSString *eventTme =[stringComponents objectAtIndex:1];
    
    if (eventTme!=nil) {
        
        if ([eventTme rangeOfString:@"pm" options:NSCaseInsensitiveSearch].location == NSNotFound)
        {
            
            hourVal=[[stringComponents objectAtIndex:0] integerValue];
            minuteVal=[[stringComponents objectAtIndex:1] integerValue];
        }
        else
        {
            hourVal=[[stringComponents objectAtIndex:0] integerValue]+12;
            minuteVal=[[stringComponents objectAtIndex:1] integerValue];
            // //NSLog(@"Event time is Night");
        }
    }
    else{
        hourVal=9;
        minuteVal=0;
        
    }
    
    
    [components setHour:hourVal];
    [components setMinute:minuteVal];
    NSDate *finalDate = [calendar dateFromComponents:components];
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:finalDate];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:finalDate];
    NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:finalDate];
    
    return destinationDate;
}
-(Event *)event
{
    return _event;
}


@end
