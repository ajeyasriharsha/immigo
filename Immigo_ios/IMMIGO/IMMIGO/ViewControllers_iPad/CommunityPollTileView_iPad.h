//
//  CommunityPollTileView_iPad.h
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PollQuestionsModel.h"

@interface CommunityPollTileView_iPad : UIView {
    
    UILabel *topicTitle;
    UILabel *subtopicTitle;
    UILabel *votedLabel;
    UIImageView *seperatorimageView;
    UIImageView *arrowImage;
    PollQuestions *_pollQuestion;
    UIImageView *bg;
}

-(void)setPollQuestion:(PollQuestions *)selectedPollQuestion;
-(PollQuestions *)pollQuestion;
@end
