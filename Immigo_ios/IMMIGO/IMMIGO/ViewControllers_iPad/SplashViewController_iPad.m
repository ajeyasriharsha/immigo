//
//  SplashViewController_iPad.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/23/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "SplashViewController_iPad.h"
#import "Common.h"
#import "HomeViewController_iPad.h"
#import "FakeViewController_iPad.h"


@interface SplashViewController_iPad ()
{
    IBOutlet UIImageView *splash;
}

@end

@implementation SplashViewController_iPad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//-(UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.navigationController.view.hidden = YES;

    UIImage *image;
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        image = ([[UIDevice currentDevice].systemVersion floatValue] < 7.0) ? [UIImage imageNamed:@"Default-Portrait-Old"]:[UIImage imageNamed:@"Default_iPad_Portrait"];
    }
    else
    {
        image = ([[UIDevice currentDevice].systemVersion floatValue] < 7.0) ? [UIImage imageNamed:@"Default-Landscape-Old"]:[UIImage imageNamed:@"Default_iPad_Landscape"];
    }
    splash.image = image;
    
//    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)
//    {
//        [self setNeedsStatusBarAppearanceUpdate];
//    }
    [self performSelector:@selector(pushToHome) withObject:nil afterDelay:PARTNERS_DELAY_TIME];
}
-(void)viewWillAppear:(BOOL)animated
{

    [super viewWillAppear:animated];
    [self showFakeController:NO];
    self.navigationController.navigationBarHidden = YES;
}

-(void)showFakeController:(BOOL)show{
    
    FakeViewController_iPad *root =  (FakeViewController_iPad*)self.navigationController.parentViewController;

    if (show) {
        
        root.bigMenuBtn.hidden=false;
        root.alertsView.hidden=false;
        root.homeButton.hidden =false;
        
        
        
    }
    else {
        root.bigMenuBtn.hidden=true;
        root.alertsView.hidden=true;
        root.homeButton.hidden =true;
        
    }
    
    
}
-(void)viewDidDisappear:(BOOL)animated{
    
   [super viewDidDisappear:animated];
   [self showFakeController:YES];

}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    

}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait ||interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}


- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration
{
    UIImage *image;
    
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        image = ([[UIDevice currentDevice].systemVersion floatValue] < 7.0) ? [UIImage imageNamed:@"Default-Portrait-Old"]:[UIImage imageNamed:@"Default_iPad_Portrait"];
    }
    else
    {
        image = ([[UIDevice currentDevice].systemVersion floatValue] < 7.0) ? [UIImage imageNamed:@"Default-Landscape-Old"]:[UIImage imageNamed:@"Default_iPad_Landscape"];
    }
    splash.image = image;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)pushToHome
{
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    HomeViewController_iPad* toVC = [storyboard instantiateViewControllerWithIdentifier:@"HomeViewController_iPad"];
    toVC.fromSplash = YES;
    [self.navigationController pushViewController:toVC animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
