//
//  HomeViewController_iPad.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/23/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PartnersPullableView-Ipad.h"
@interface HomeViewController_iPad : UIViewController<PullableViewDelegate,PartnersPullableViewDelegate_iPad> {
    
    
    PullableView *bottompullRightView;
    BOOL _fromSplash;


}
@property (nonatomic)BOOL fromSplash;
@end
