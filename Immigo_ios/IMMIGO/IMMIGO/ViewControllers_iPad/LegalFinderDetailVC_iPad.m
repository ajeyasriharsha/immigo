//
//  LegalFinderDetailVC_iPad.m
//  IMMIGO
//
//  Created by pradeep ISPV on 6/9/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "LegalFinderDetailVC_iPad.h"
#import "LegalFinder.h"
#import "LegalFinderModel.h"
#import "Categories.h"
#import <MessageUI/MessageUI.h>
#import "Common.h"
#import "MTPopupWindow.h"
#import "AppDelegate.h"

@interface LegalFinderDetailVC_iPad ()<MFMailComposeViewControllerDelegate,MTPopupWindowDelegate>
{
    IBOutlet UIScrollView *detailScrollView;
    LegalFinder *selectedFinder;
    
    IBOutlet UIButton *mapButton;
    IBOutlet MKMapView *detailMapView;
    
    IBOutlet UIImageView *bottomDivider;
    IBOutlet UILabel *screenHeaderName;

    IBOutlet UIButton *fullscreen;
    IBOutlet UIView *leftParentView;
    
    CGRect mapViewP_OriginalFrame;
    CGRect leftParentP_OriginalFrame;
    CGRect mapViewL_OriginalFrame;
    CGRect leftParentL_OriginalFrame;

    BOOL isMapInFullView;
    
    CGFloat mapX_BeforeFull;
    CGFloat leftX_BeforeFull;
    
    MKPointAnnotation *point;
}

@end

@implementation LegalFinderDetailVC_iPad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate]refreshFakeViewController];
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        //NSLog(@"frame change in portrait left %@  -------------  map %@",NSStringFromCGRect(leftParentView.frame),NSStringFromCGRect(legalMapView.frame));
        
        CGRect leftFrame = leftParentView.frame;
        leftFrame.origin.x = 50.0f;
        leftFrame.size.width = 317.0f;
        leftFrame.size.height = 685.0f;
        leftParentView.frame = leftFrame;
        
        
        CGRect mapViewFrame = detailMapView.frame;
        mapViewFrame.origin.x = 399.0f;
        mapViewFrame.size.width = 350.0f;
        mapViewFrame.size.height = 685.0f;
        detailMapView.frame = mapViewFrame;
        
        mapViewP_OriginalFrame = detailMapView.frame;
        leftParentP_OriginalFrame = leftParentView.frame;
        
    }
    else if (UIInterfaceOrientationIsLandscape(orientation))
    {
        CGRect leftFrame = leftParentView.frame;
        leftFrame.origin.x = 50.0f;
        leftFrame.size.width = 324.0f;
        leftFrame.size.height = 491.0f;
        leftParentView.frame = leftFrame;
        
        
        CGRect mapViewFrame = detailMapView.frame;
        mapViewFrame.origin.x = 406.0f;
        mapViewFrame.size.width = 606.0f;
        mapViewFrame.size.height = 491.0f;
        detailMapView.frame = mapViewFrame;
        
        mapViewL_OriginalFrame = detailMapView.frame;
        leftParentL_OriginalFrame = leftParentView.frame;
        //NSLog(@"frame change in landscpe left %@  -------------  map %@",NSStringFromCGRect(leftParentView.frame),NSStringFromCGRect(legalMapView.frame));
    }
    mapX_BeforeFull = detailMapView.frame.origin.x;
    leftX_BeforeFull = leftParentView.frame.origin.x;
    
    [self dropPins];

}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
//    
//    if (UIInterfaceOrientationIsPortrait(orientation))
//    {
//        //NSLog(@"frame change in portrait left %@  -------------  map %@",NSStringFromCGRect(leftParentView.frame),NSStringFromCGRect(legalMapView.frame));
//        
//        CGRect leftFrame = leftParentView.frame;
//        leftFrame.origin.x = 50.0f;
//        leftFrame.size.width = 317.0f;
//        leftFrame.size.height = 685.0f;
//        leftParentView.frame = leftFrame;
//        
//        
//        CGRect mapViewFrame = detailMapView.frame;
//        mapViewFrame.origin.x = 399.0f;
//        mapViewFrame.size.width = 350.0f;
//        mapViewFrame.size.height = 685.0f;
//        detailMapView.frame = mapViewFrame;
//        
//        mapViewP_OriginalFrame = detailMapView.frame;
//        leftParentP_OriginalFrame = leftParentView.frame;
//        
//    }
//    else if (UIInterfaceOrientationIsLandscape(orientation))
//    {
//        CGRect leftFrame = leftParentView.frame;
//        leftFrame.origin.x = 50.0f;
//        leftFrame.size.width = 324.0f;
//        leftFrame.size.height = 491.0f;
//        leftParentView.frame = leftFrame;
//        
//        
//        CGRect mapViewFrame = detailMapView.frame;
//        mapViewFrame.origin.x = 406.0f;
//        mapViewFrame.size.width = 606.0f;
//        mapViewFrame.size.height = 491.0f;
//        detailMapView.frame = mapViewFrame;
//        
//        mapViewL_OriginalFrame = detailMapView.frame;
//        leftParentL_OriginalFrame = leftParentView.frame;
//        //NSLog(@"frame change in landscpe left %@  -------------  map %@",NSStringFromCGRect(leftParentView.frame),NSStringFromCGRect(legalMapView.frame));
//    }
//    mapX_BeforeFull = detailMapView.frame.origin.x;
//    leftX_BeforeFull = leftParentView.frame.origin.x;
//    
//    [self dropPins];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    selectedFinder = [[LegalFinderModel sharedDataSource] selectedFinder];

    UIImage *dividerImage = [UIImage imageNamed:@"menuTitleDivider"];
    bottomDivider.image = [dividerImage stretchableImageWithLeftCapWidth:dividerImage.size.width/2 topCapHeight:dividerImage.size.height/2];

    screenHeaderName.textColor = [UIColor grayColor];
    screenHeaderName.font = [UIFont fontWithName:FONT_IPAD size:22.0f];

    //*********** Start Detail creation *************//
    
    CGFloat headingsGap = 8.0f;
    CGPoint cursor = CGPointMake(0,0);
    CGSize size = CGSizeMake(detailScrollView.frame.size.width, detailScrollView.frame.size.height);
    
    {
        UILabel *newsTitle = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, 40.0f)];
        newsTitle.text = [selectedFinder.finderName trimmedString];
        newsTitle.font = [UIFont fontWithName:FONT_LIGHT size:17.50f];
        newsTitle.textColor= UIColorFromRGB(UI_TRAINING_DESC_LABEL_TEXT_COLOR_IPAD);
        newsTitle.numberOfLines = 0;
        
        CGSize newsTitleSize = [newsTitle sizeOfMultiLineLabel];
        CGRect newsTitleFrame = newsTitle.frame;
        newsTitleFrame.size.height = newsTitleSize.height;
        newsTitle.frame = newsTitleFrame;
        
        [detailScrollView addSubview:newsTitle];
        
        cursor.y +=newsTitle.frame.size.height+headingsGap;
    }
    //    { // location label
    //        UILabel *locationLbl = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, 20.0f)];
    //        locationLbl.text = @"Location:";
    //        locationLbl.font = [UIFont fontWithName:@"OpenSans-SemiBold" size:13.08f];
    //
    //        [detailScrollView addSubview:locationLbl];
    //        cursor.y +=locationLbl.frame.size.height;
    //    }
    NSString *address = @"";
    if ([selectedFinder.finderAddress.street length]>0)
    {
        address = selectedFinder.finderAddress.street;
        address = [address stringByAppendingString:@","];
    }
    if ([selectedFinder.finderAddress.city length]>0)
    {
        address = [address stringByAppendingString:selectedFinder.finderAddress.city];
        address = [address stringByAppendingString:@","];
    }
    if ([selectedFinder.finderAddress.state length]>0)
    {
        address = [address stringByAppendingString:selectedFinder.finderAddress.state];
        address = [address stringByAppendingString:@" "];
    }
    if (selectedFinder.finderAddress.zipcode >0)
    {
        address = [address stringByAppendingString:selectedFinder.finderAddress.zipcode];
    }
    
    { // address label
        
        UIImage *image = [UIImage imageNamed:@"Location"];
        {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(cursor.x-2, cursor.y+3, image.size.width, image.size.height)];
            imageView.image = image;
            [detailScrollView addSubview:imageView];
            cursor.x +=imageView.frame.size.width;
        }
        
        UILabel *addressLbl = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width-image.size.width, 40.0f)];
        addressLbl.text = [address trimmedString];
        addressLbl.font = [UIFont fontWithName:FONT_REGULAR size:15.7f];
        addressLbl.numberOfLines = 0;
        addressLbl.backgroundColor = [UIColor clearColor];
        CGSize newsTitleSize = [addressLbl sizeOfMultiLineLabel];
        CGRect newsTitleFrame = addressLbl.frame;
        newsTitleFrame.size.height = newsTitleSize.height;
        addressLbl.frame = newsTitleFrame;
        
        [detailScrollView addSubview:addressLbl];
        cursor.x = 0.0f;
        cursor.y +=addressLbl.frame.size.height+headingsGap;
    }
    if ([selectedFinder.contact.website length]>0 || [selectedFinder.contact.email length]>0 || [selectedFinder.contact.phone length]>0 ||[selectedFinder.contact.tollFreePhone length]>0)
    { // contacts label
        UILabel *contactLbl = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, 40.0f)];
        contactLbl.text =NSLocalizedString(@"Contact Information:", @"Contact Information:") ;
        contactLbl.font = [UIFont fontWithName:FONT_SEMIBOLD size:15.08f];
        contactLbl.numberOfLines = 0;
        
        [detailScrollView addSubview:contactLbl];
        cursor.y +=contactLbl.frame.size.height;
    }
    /*
     @property (nonatomic,strong) NSString *website;
     @property (nonatomic,strong) NSString *email;
     @property (nonatomic,strong) NSString *phone;
     @property (nonatomic,strong) NSString *tollFreePhone;
     */
    if ([selectedFinder.contact.website length]>0)
    { // Website Button
        
        UIImage *image = [UIImage imageNamed:@"url link_iPad"];
        {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(cursor.x+4, cursor.y+2, image.size.width, image.size.height)];
            imageView.image = image;
            [detailScrollView addSubview:imageView];
            cursor.x +=imageView.frame.size.width+4;
        }
        
        UILabel *addressLbl = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x+4, cursor.y, size.width-image.size.width, 40.0f)];
        addressLbl.text = [selectedFinder.contact.website trimmedString];
        addressLbl.font = [UIFont fontWithName:FONT_REGULAR size:15.7f];
        addressLbl.numberOfLines = 0;
        addressLbl.backgroundColor = [UIColor clearColor];
        addressLbl.textColor = [UIColor blueColor];
        CGSize newsTitleSize = [addressLbl sizeOfMultiLineLabel];
        CGRect newsTitleFrame = addressLbl.frame;
        newsTitleFrame.size.height = newsTitleSize.height;
        addressLbl.frame = newsTitleFrame;

        UIButton *websiteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        websiteBtn.frame = CGRectMake(cursor.x+4, cursor.y, size.width-image.size.width-4, addressLbl.frame.size.height);
        [websiteBtn addTarget:self action:@selector(websiteTapped:) forControlEvents:UIControlEventTouchUpInside];
//        [websiteBtn setTitle:[selectedFinder.contact.website trimmedString] forState:UIControlStateNormal];
//        [websiteBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        //        [websiteBtn setImage:[UIImage imageNamed:@"url_icon"] forState:UIControlStateNormal];
        websiteBtn.titleLabel.font = [UIFont fontWithName:FONT_REGULAR size:15.6f];
        websiteBtn.contentMode = UIViewContentModeLeft;
        websiteBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        websiteBtn.titleLabel.numberOfLines = 3;
        [detailScrollView addSubview:addressLbl];
        [detailScrollView addSubview:websiteBtn];
        cursor.x = 0.0f;
        cursor.y +=websiteBtn.frame.size.height+6;
    }
    if ([selectedFinder.contact.email length]>0)
    { // email Button
        UIImage *image = [UIImage imageNamed:@"mail_iPad"];
        {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(cursor.x+4, cursor.y+3, image.size.width, image.size.height)];
            imageView.image = image;
            [detailScrollView addSubview:imageView];
            cursor.x +=imageView.frame.size.width+4;
        }
        UILabel *addressLbl = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x+4, cursor.y, size.width-image.size.width, 40.0f)];
        addressLbl.text = [selectedFinder.contact.email trimmedString];
        addressLbl.font = [UIFont fontWithName:FONT_REGULAR size:15.7f];
        addressLbl.numberOfLines = 0;
        addressLbl.backgroundColor = [UIColor clearColor];
        CGSize newsTitleSize = [addressLbl sizeOfMultiLineLabel];
        CGRect newsTitleFrame = addressLbl.frame;
        newsTitleFrame.size.height = newsTitleSize.height;
        addressLbl.frame = newsTitleFrame;
        addressLbl.textColor = [UIColor blueColor];
        UIButton *emailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        emailBtn.frame = CGRectMake(cursor.x+4, cursor.y, size.width-image.size.width-4, addressLbl.frame.size.height);
        [emailBtn addTarget:self action:@selector(emailBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
//        [emailBtn setTitle:[selectedFinder.contact.email trimmedString] forState:UIControlStateNormal];
//        [emailBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        emailBtn.titleLabel.font = [UIFont fontWithName:FONT_REGULAR size:15.6f];
        emailBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
        [detailScrollView addSubview:addressLbl];
        [detailScrollView addSubview:emailBtn];
        cursor.x = 0.0f;
        cursor.y +=emailBtn.frame.size.height+6;
    }
    if ([selectedFinder.contact.phone length]>0)
    { // phone Button
        UIImage *image = [UIImage imageNamed:@"phone_iPad"];
        {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(cursor.x+4, cursor.y+3, image.size.width, image.size.height)];
            imageView.image = image;
            [detailScrollView addSubview:imageView];
            cursor.x +=imageView.frame.size.width+4;
        }
        
        UIButton *phoneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        phoneBtn.frame = CGRectMake(cursor.x+4, cursor.y, size.width-image.size.width-4, 25.0f);
        [phoneBtn addTarget:self action:@selector(phoneBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
        [phoneBtn setTitle:[[selectedFinder.contact.phone trimmedString] frameContactNumber] forState:UIControlStateNormal];
        [phoneBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        phoneBtn.titleLabel.font = [UIFont fontWithName:FONT_REGULAR size:15.6f];
        phoneBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [detailScrollView addSubview:phoneBtn];
        cursor.x = 0.0f;
        cursor.y +=phoneBtn.frame.size.height;
    }
    //    if ([selectedFinder.contact.tollFreePhone length]>0)
    //    { // tollfree Button
    //        UIButton *tollFreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //        tollFreeBtn.frame = CGRectMake(cursor.x, cursor.y, size.width, 25.0f);
    //        [tollFreeBtn addTarget:self action:@selector(tollFreeBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    //        [tollFreeBtn setTitle:selectedFinder.contact.tollFreePhone forState:UIControlStateNormal];
    //        [tollFreeBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    //        tollFreeBtn.titleLabel.font = [UIFont fontWithName:@"OpenSans-semiBold" size:14.6f];
    //        tollFreeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //        [detailScrollView addSubview:tollFreeBtn];
    //        cursor.y +=tollFreeBtn.frame.size.height;
    //    }
    
    detailScrollView.contentSize = CGSizeMake(detailScrollView.frame.size.width, cursor.y+30);

    // Do any additional setup after loading the view.
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
										 duration:(NSTimeInterval)duration
{
    
    if (UIInterfaceOrientationIsPortrait(interfaceOrientation))
    {
        //NSLog(@"frame change in portrait left %@  -------------  map %@",NSStringFromCGRect(leftParentView.frame),NSStringFromCGRect(legalMapView.frame));
        
        if (isMapInFullView)
        {
            CGRect mapViewFrame = detailMapView.frame;
            mapViewFrame.origin.x = 50.0f;
            mapViewFrame.size.width = 704.0f;
            mapViewFrame.size.height = 685.0f;
            detailMapView.frame = mapViewFrame;
        }
        else
        {
            CGRect leftFrame = leftParentView.frame;
            leftFrame.origin.x = leftX_BeforeFull;
            leftFrame.size.width = 317.0f;
            leftFrame.size.height = 685.0f;
            leftParentView.frame = leftFrame;
            
            
            CGRect mapViewFrame = detailMapView.frame;
            mapViewFrame.size.width = 350.0f;
            mapViewFrame.size.height = 685.0f;
            detailMapView.frame = mapViewFrame;
        }
        mapViewP_OriginalFrame = CGRectMake(399, 133, 350, 685);
        leftParentP_OriginalFrame = CGRectMake(50, 133, 317, 685);
        
    }
    else
    {
        if (isMapInFullView)
        {
            CGRect mapViewFrame = detailMapView.frame;
            mapViewFrame.origin.x = 50.0f;
            mapViewFrame.size.width = 960.0f;
            mapViewFrame.size.height = 491.0f;
            detailMapView.frame = mapViewFrame;
        }
        else
        {
            CGRect leftFrame = leftParentView.frame;
            leftFrame.origin.x = leftX_BeforeFull;
            leftFrame.size.width = 324.0f;
            leftFrame.size.height = 491.0f;
            leftParentView.frame = leftFrame;
            
            
            CGRect mapViewFrame = detailMapView.frame;
            mapViewFrame.size.width = 606.0f;
            mapViewFrame.size.height = 491.0f;
            detailMapView.frame = mapViewFrame;
            mapViewL_OriginalFrame = detailMapView.frame;
            leftParentL_OriginalFrame = leftParentView.frame;
        }
        mapViewL_OriginalFrame = CGRectMake(406, 133, 606, 491);
        leftParentL_OriginalFrame = CGRectMake(50, 133, 324, 491);
        
        //NSLog(@"frame change in landscpe left %@  -------------  map %@",NSStringFromCGRect(leftParentView.frame),NSStringFromCGRect(legalMapView.frame));
    }
    //    mapViewOriginalFrame = legalMapView.frame;
    //    leftParentOriginalFrame = leftParentView.frame;
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)
    {
        [detailMapView showAnnotations:[NSArray arrayWithObject:point] animated:NO];
    }
}
- (IBAction)tappedOnFullscreen:(UIButton *)sender
{
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    //    if (UIInterfaceOrientationIsPortrait(orientation))
    
    sender.selected = !sender.selected;
    
    if (sender.selected)
    {
        CGRect newmapViewFrame;CGRect newleftParentFrame;
        
        NSTimeInterval animationDuration = 0.5f;/* determine length of animation */;
        
        CGFloat x = 50.0f;//mapViewOriginalFrame.origin.x-leftParentOriginalFrame.size.width;
        
        if (UIInterfaceOrientationIsPortrait(orientation))
        {
            newmapViewFrame = CGRectMake(x, mapViewP_OriginalFrame.origin.y, 704.0f, 685.0f)/* determine what the frame size should be */;
            
            newleftParentFrame = CGRectMake(-leftParentP_OriginalFrame.size.width, leftParentP_OriginalFrame.origin.y, leftParentP_OriginalFrame.size.width, leftParentP_OriginalFrame.size.height)/* determine what the frame size should be */;
        }else
        {
            newmapViewFrame = CGRectMake(x, mapViewL_OriginalFrame.origin.y, 960.0f, 491.0f)/* determine what the frame size should be */;
            
            newleftParentFrame = CGRectMake(-leftParentL_OriginalFrame.size.width, leftParentL_OriginalFrame.origin.y, leftParentL_OriginalFrame.size.width, leftParentL_OriginalFrame.size.height)/* determine what the frame size should be */;
        }
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:animationDuration];
        
        detailMapView.frame = newmapViewFrame;
        leftParentView.frame = newleftParentFrame;
        
        [UIView commitAnimations];
        
        isMapInFullView = YES;
        
    }else
    {
        CGRect newmapViewFrame;CGRect newleftParentFrame;
        
        NSTimeInterval animationDuration = 0.5f;/* determine length of animation */;
        
        if (UIInterfaceOrientationIsPortrait(orientation))
        {
            newmapViewFrame = CGRectMake(mapViewP_OriginalFrame.origin.x, mapViewP_OriginalFrame.origin.y, mapViewP_OriginalFrame.size.width, mapViewP_OriginalFrame.size.height)/* determine what the frame size should be */;
            
            newleftParentFrame = CGRectMake(leftParentP_OriginalFrame.origin.x, leftParentP_OriginalFrame.origin.y, leftParentP_OriginalFrame.size.width, leftParentP_OriginalFrame.size.height)/* determine what the frame size should be */;
        }else
        {
            newmapViewFrame = CGRectMake(mapViewL_OriginalFrame.origin.x, mapViewL_OriginalFrame.origin.y, mapViewL_OriginalFrame.size.width, mapViewL_OriginalFrame.size.height)/* determine what the frame size should be */;
            
            newleftParentFrame = CGRectMake(leftParentL_OriginalFrame.origin.x, leftParentL_OriginalFrame.origin.y, leftParentL_OriginalFrame.size.width, leftParentL_OriginalFrame.size.height)/* determine what the frame size should be */;
        }
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:animationDuration];
        
        detailMapView.frame = newmapViewFrame;
        leftParentView.frame = newleftParentFrame;
        
        [UIView commitAnimations];
        
        isMapInFullView = NO;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)websiteTapped:(id)sender
{
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Legal Help Finder" message:@"NOINTERNET_ERROR_MESSAGE" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    NSString *linkUrl = [selectedFinder.contact.website trimmedString];
    //    NSURL *myURL;
    if ([linkUrl hasPrefix:@"http://"] || [linkUrl hasPrefix:@"https://"])
    {
        
    }
    else
    {
        linkUrl = [NSString stringWithFormat:@"http://%@",linkUrl];
    }
    
    MTPopupWindow *popup = [[MTPopupWindow alloc] init];
    popup.delegate = self;
    popup.fileName = linkUrl;
    [popup show];

//    LegalFinderUrlVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"LegalFinderUrlVC"];
//    toVC.urlAddress = [selectedFinder.contact.website trimmedString];
//    [self.navigationController pushViewController:toVC animated:YES];
    
}
-(IBAction)emailBtnTapped:(id)sender
{
//    if (![Common currentNetworkStatus])
//    {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Legal Help Finder" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alertView show];
//        return;
//    }

    if ([MFMailComposeViewController canSendMail])
	{
		// set the sendTo address
		NSMutableArray *recipients = [[NSMutableArray alloc] initWithCapacity:1];
		[recipients addObject:selectedFinder.contact.email];
		
		MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
		controller.mailComposeDelegate = self;
        //		[controller setSubject:@""];
		
		[controller setToRecipients:recipients];
		
		if ([self respondsToSelector:@selector(presentViewController:animated:completion:)])
        {
            
            
            UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
            UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
            titlView.image = titleLogo;
            controller.navigationItem.titleView = titlView;
            [[controller navigationBar] sendSubviewToBack:titlView];
            [[controller navigationBar] setTintColor:[UIColor blueColor]];
            /*
             [[controller navigationBar] setTintColor:[UIColor whiteColor]];
             UIImage *image = [UIImage imageNamed: @"logo.png"];
             UIImageView * iv = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,320,42)];
             iv.image = image;
             iv.contentMode = UIViewContentModeCenter;
             [[[controller viewControllers] lastObject] navigationItem].titleView = iv;
             [[controller navigationBar] sendSubviewToBack:iv];
             */
            [self presentViewController:controller animated:YES completion:nil];
            
            
        }
	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Legal Help Finder" message:@"No email account is setup to send mail." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles: nil];
		[alert show];
		
	}
    
}
-(IBAction)phoneBtnTapped:(id)sender
{
    //NSLog(@"Phone Tapped");
    if ([[selectedFinder.contact.phone trimmedString] length]>0)
    {
       
        if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        {
            
            NSString *fianlphoneNumber = [[selectedFinder.contact.phone trimmedString] frameContactNumber];
            NSString *phoneNumDecimalsOnly = [[fianlphoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
            NSString *phoneNumber = [@"telprompt://" stringByAppendingString:phoneNumDecimalsOnly];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
            
        } else {
            
            UIAlertView *warning =[[UIAlertView alloc] initWithTitle:@"Legal Help Finder" message:NSLocalizedString(@"DEVICE_NOT_SUPPORTED_ERROR", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
            
            [warning show];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	switch (result)
	{
		case MFMailComposeResultCancelled:
		{
			break;
		}
		case MFMailComposeResultSaved:
		{
			break;
		}
		case MFMailComposeResultSent:
		{
			break;
		}
		case MFMailComposeResultFailed:
		{
			break;
		}
		default:
		{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"EMAIL_TITLE", @"") message:@"Email Failed" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles: nil];
			[alert show];
            
		}
			break;
	}
	if ([self respondsToSelector:@selector(dismissModalViewControllerAnimated:)]) {
        [self performSelector:@selector(dismissModalViewControllerAnimated:) withObject:[NSNumber numberWithBool:YES]];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
	
	
}


//- (IBAction)mapIconTapped:(UIButton *)sender
//{
//    sender.selected = !sender.selected;
//    if ([sender isSelected])
//    {
//        [sender setImage:[UIImage imageNamed:@"listIcon"] forState:UIControlStateNormal];
//        [UIView beginAnimations:nil context:nil];
//        [UIView setAnimationDuration:0.6];
//        
//        [UIView setAnimationDelegate: self];
//        //    [UIView setAnimationDidStopSelector:
//        //     @selector(animationFinished:finished:context:)];
//        detailScrollView.hidden = YES;
//        detailMapView.hidden = NO;
//        
//        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
//                               forView:detailMapView cache:YES];
//        [UIView commitAnimations];
//        
//        [self dropPins];
//    }
//    else
//    {
//        [sender setImage:[UIImage imageNamed:@"map_icon"] forState:UIControlStateNormal];
//        [UIView beginAnimations:nil context:nil];
//        [UIView setAnimationDuration:0.6];
//        
//        [UIView setAnimationDelegate: self];
//        //    [UIView setAnimationDidStopSelector:
//        //     @selector(animationFinished:finished:context:)];
//        detailScrollView.hidden = NO;
//        detailMapView.hidden = YES;
//        
//        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
//                               forView:detailScrollView cache:YES];
//        [UIView commitAnimations];
//        
//    }
//}
#pragma mark - mapview Delegates
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    mapButton.selected = NO;
    [mapButton setImage:[UIImage imageNamed:@"map_icon"] forState:UIControlStateNormal];
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.6];
//    
//    [UIView setAnimationDelegate: self];
//    //    [UIView setAnimationDidStopSelector:
//    //     @selector(animationFinished:finished:context:)];
//    detailScrollView.hidden = NO;
//    detailMapView.hidden = YES;
//    
//    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
//                           forView:detailScrollView cache:YES];
//    [UIView commitAnimations];
}
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *SFAnnotationIdentifier = @"mapView";
    MKAnnotationView *flagAnnotationView = [detailMapView dequeueReusableAnnotationViewWithIdentifier:SFAnnotationIdentifier];
    
    if (flagAnnotationView == nil)
    {
        MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                                        reuseIdentifier:SFAnnotationIdentifier];
        annotationView.canShowCallout = YES;
        annotationView.backgroundColor = [UIColor clearColor];
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        [rightButton setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
        [rightButton addTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
//        annotationView.rightCalloutAccessoryView = rightButton;
        UIImage *flagImage = [UIImage imageNamed:@"pin_iPad.png"];
        annotationView.image = flagImage;
        //        if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)
        //        {
        annotationView.rightCalloutAccessoryView.tintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"arrow"]];
        //        }
        annotationView.opaque = NO;
        
        
        //change her
        return annotationView;
    }
    else
    {
        flagAnnotationView.annotation = annotation;
    }
    return flagAnnotationView;
}
- (void)showAnnotations:(NSArray *)annotations animated:(BOOL)animated
{
    
}
-(void)dropPins
{
    
    //Create your annotation
    point = [[MKPointAnnotation alloc] init];
    // Set your annotation to point at your coordinate
    point.coordinate = selectedFinder.finderAddress.coordinate;
    point.title = selectedFinder.finderName;

    //Drop pin on map
    [detailMapView addAnnotation:point];
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)
    {
        [detailMapView showAnnotations:[NSArray arrayWithObject:point] animated:YES];
    }
    [detailMapView selectAnnotation:point animated:YES];
}


@end
