//
//  CommunityPollDetailVC_iPad.m
//  IMMIGO
//
//  Created by pradeep ISPV on 6/10/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "CommunityPollDetailVC_iPad.h"
#import "Common.h"
#import "PollQuestionsModel.h"
#import "DataDownloader.h"
#import "AppDelegate.h"

@interface CommunityPollDetailVC_iPad ()<DataDownloaderDelegate>
{
    UIScrollView *scrollView;
    BOOL flipped;
    
    UILabel *questionText;
    int numberOfOptions;
    
    NSArray *QArray;
    NSUInteger currentQuestionIndex;
    
//    UIButton *submitButton;
//    UIButton *nextButton;
    
    BOOL optionSelcted;
    
    int answerNumber;
    
    BOOL submitSelected;
    IBOutlet UIImageView *bottomDivider;
    IBOutlet UILabel *screenHeaderName;
    IBOutlet UIButton *submitPollButton;
    IBOutlet UIImageView *scrollIndicatorImgView;

}
//@property (nonatomic, strong) AMGProgressView *progressBar;
@end

@implementation CommunityPollDetailVC_iPad
@synthesize pollQuestionIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(CGRect)frameForLabel:(UILabel *)label WithText:(NSString *)text
{
    CGSize maximumLabelSize = CGSizeMake(label.frame.size.width, FLT_MAX);
    CGSize expectedLabelSize = [text sizeWithFont:label.font constrainedToSize:maximumLabelSize lineBreakMode:label.lineBreakMode];
    //adjust the label the the new height.
    CGRect newFrame = label.frame;
    newFrame.size.height = expectedLabelSize.height<44.0f ? 44.0f :expectedLabelSize.height;
    //    label.frame = newFrame;
    
    return newFrame;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate]refreshFakeViewController];
    BOOL  canShowScrollIndicator=[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !canShowScrollIndicator;

}


-(BOOL)canShowScrollIndicator{
    CGFloat webViewFrameHeight =scrollView.frame.size.height;
    float scrollContentSizeHeight = scrollView.contentSize.height;
    return scrollContentSizeHeight > webViewFrameHeight;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    screenHeaderName.font = [UIFont fontWithName:FONT_IPAD size:22.0f];
    
    UIImage *divider = [UIImage imageNamed:@"menuTitleDivider"];
    bottomDivider.image = [divider stretchableImageWithLeftCapWidth:divider.size.width/2 topCapHeight:divider.size.height/2];

    
    [[DataDownloader sharedDownloader] setDelegate:self];
    
    QArray = [[PollQuestionsModel sharedDataSource] allObjects];//[[NSMutableArray alloc] initWithObjects:@"Question1",@"Question2",@"Question3",@"Question4",@"Question5",@"Question6",@"Question7",@"Question8", nil];
    currentQuestionIndex = pollQuestionIndex;
    

    UIImage *btnBg = [UIImage imageNamed:@"pollsubmitButton_iPad"];
    [submitPollButton setBackgroundImage:[btnBg stretchableImageWithLeftCapWidth:btnBg.size.width/2 topCapHeight:btnBg.size.height/2] forState:UIControlStateNormal];
    submitPollButton.contentMode = UIViewContentModeRedraw;
    [submitPollButton setTitle:@"SUBMIT" forState:UIControlStateNormal];
    [submitPollButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    submitPollButton.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:12.8];
    [submitPollButton addTarget:self action:@selector(submitButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

    [self customIntializationWithPollQuestion:[QArray objectAtIndex:currentQuestionIndex]];
    
    BOOL  canShowScrollIndicator=[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !canShowScrollIndicator;

    // Do any additional setup after loading the view.
}
- (NSArray *)filterThePercentageForPollQuestion:(PollQuestions *)pollQuestion
{
    int count = pollQuestion.optionsCount;
    
    
    NSMutableArray *optionArr = [NSMutableArray arrayWithCapacity:count];
    
    if (pollQuestion.answerOne)
    {
        [optionArr addObject:[NSString stringWithFormat:@"%d",pollQuestion.answerOnePercentage]];
    }
    if (pollQuestion.answerTwo)
    {
        [optionArr addObject:[NSString stringWithFormat:@"%d",pollQuestion.answerTwoPercentage]];
    }
    if (pollQuestion.answerThree)
    {
        [optionArr addObject:[NSString stringWithFormat:@"%d",pollQuestion.answerThreePercentage]];
    }
    if (pollQuestion.answerFour)
    {
        [optionArr addObject:[NSString stringWithFormat:@"%d",pollQuestion.answerFourPercentage]];
    }
    if (pollQuestion.answerFive)
    {
        [optionArr addObject:[NSString stringWithFormat:@"%d",pollQuestion.answerFivePercentage]];
    }
    if (pollQuestion.answerSix)
    {
        [optionArr addObject:[NSString stringWithFormat:@"%d",pollQuestion.answerSixPercentage]];
    }
    
    
    return [NSArray arrayWithArray:optionArr];
}
- (NSArray *)filterTheOptionTextForPollQuestion:(PollQuestions *)pollQuestion
{
    int count = pollQuestion.optionsCount;
    
    
    NSMutableArray *optionArr = [NSMutableArray arrayWithCapacity:count];
    
    if (pollQuestion.answerOne)
    {
        [optionArr addObject:pollQuestion.answerOne];
    }
    if (pollQuestion.answerTwo)
    {
        [optionArr addObject:pollQuestion.answerTwo];
    }
    if (pollQuestion.answerThree)
    {
        [optionArr addObject:pollQuestion.answerThree];
    }
    if (pollQuestion.answerFour)
    {
        [optionArr addObject:pollQuestion.answerFour];
    }
    if (pollQuestion.answerFive)
    {
        [optionArr addObject:pollQuestion.answerFive];
    }
    if (pollQuestion.answerSix)
    {
        [optionArr addObject:pollQuestion.answerSix];
    }
    
    
    return [NSArray arrayWithArray:optionArr];
}

- (NSArray *)filterTheKeyForPollQuestion:(PollQuestions *)pollQuestion
{
    int count = pollQuestion.optionsCount;
    
    
    NSMutableArray *optionArr = [NSMutableArray arrayWithCapacity:count];
    
    if (pollQuestion.answerOne)
    {
        [optionArr addObject:[NSString stringWithFormat:@"1"]];
    }
    if (pollQuestion.answerTwo)
    {
        [optionArr addObject:[NSString stringWithFormat:@"2"]];
    }
    if (pollQuestion.answerThree)
    {
        [optionArr addObject:[NSString stringWithFormat:@"3"]];
    }
    if (pollQuestion.answerFour)
    {
        [optionArr addObject:[NSString stringWithFormat:@"4"]];
    }
    if (pollQuestion.answerFive)
    {
        [optionArr addObject:[NSString stringWithFormat:@"5"]];
    }
    if (pollQuestion.answerSix)
    {
        [optionArr addObject:[NSString stringWithFormat:@"6"]];
    }
    
    
    return [NSArray arrayWithArray:optionArr];
}

#pragma mark- CutomIntialization
-(void)customIntializationWithPollQuestion:(PollQuestions *)pollQuestion
{
    
    NSArray *percentageArr = [self filterThePercentageForPollQuestion:pollQuestion];
    NSArray *objArr = [self filterTheOptionTextForPollQuestion:pollQuestion];
    NSArray *keysArr = [self filterTheKeyForPollQuestion:pollQuestion];
    
    heading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
    {
        self.view.clipsToBounds = YES;
    }
    optionSelcted = NO;
    float optionsGap = 5.0f;
    
    CGRect totalFrame = pollQuestionView.frame;
    CGPoint cursor = CGPointMake(0,0);
    CGSize size = CGSizeMake(totalFrame.size.width, totalFrame.size.height);
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, size.height)];
    scrollView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin |UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    
    
    if (!questionText)
        questionText = [[UILabel alloc] initWithFrame:CGRectMake(1, 5, size.width-2, 20)];
    questionText.lineBreakMode = NSLineBreakByWordWrapping;
    questionText.font = [UIFont fontWithName:FONT_LIGHT size:17.5f];
    questionText.frame = [self frameForLabel:questionText WithText:pollQuestion.question];
    questionText.numberOfLines =0;
    questionText.textColor = UIColorFromRGB(0xe61e28);
    questionText.backgroundColor  = [UIColor clearColor];
    questionText.text = pollQuestion.question;
    questionText.autoresizingMask =  UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
    
    [scrollView addSubview:questionText];
    
    
    UIImage *offIcon = [UIImage imageNamed:@"checkbox"];
    UIImage *onIcon = [UIImage imageNamed:@"checkbox_selected"];
    
    cursor.y =questionText.frame.size.height+optionsGap;
    cursor.x = 1;
    int percentage=0;
    
    NSString *answerOption = nil;
    numberOfOptions = pollQuestion.optionsCount;
    
    CGFloat scrollViewHeight= scrollView.contentSize.height;
    for (int i=1; i<=numberOfOptions; i++)
    {
        percentage = 0;
        answerOption = nil;
        //NSLog(@"anser optins %@",objArr);
        answerOption = [objArr objectAtIndex:i-1];
        if ([percentageArr count]>0)
        {
            percentage = [[percentageArr objectAtIndex:i-1] intValue];
        }

        CGRect titleLabelFrame = [pollQuestion.answerStatus isEqual:@"Yes"] ? CGRectMake(cursor.x, cursor.y+3, size.width*0.90, 40) : CGRectMake(20+offIcon.size.width, cursor.y+3, size.width*0.90, 40);
        UILabel *option1;
        if (!option1)
            option1 = [[UILabel alloc] initWithFrame:titleLabelFrame];
        option1.lineBreakMode = NSLineBreakByWordWrapping;
        option1.font = [UIFont fontWithName:FONT_LIGHT size:17.5];
        option1.frame = [self frameForLabel:option1 WithText:answerOption];
        option1.textColor = UIColorFromRGB(0x5d5d5d);
        option1.numberOfLines =0;
        option1.autoresizingMask =  UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
        option1.backgroundColor  = [UIColor clearColor];
        option1.text = answerOption;
        option1.tag = 10*i;
        option1.textAlignment = NSTextAlignmentLeft;
        
        
        UIButton *optionButton1= [UIButton buttonWithType:UIButtonTypeCustom];
        optionButton1.frame = CGRectMake(0, cursor.y, size.width, option1.frame.size.height+6);
        optionButton1.imageEdgeInsets = UIEdgeInsetsMake(0,+05, 0, 0);
        [optionButton1 addTarget:self action:@selector(optionSelected:) forControlEvents:UIControlEventTouchUpInside];
        [optionButton1 setImage:offIcon forState:UIControlStateNormal];
        [optionButton1 setImage:onIcon forState:UIControlStateHighlighted];
        [optionButton1 setImage:onIcon forState:UIControlStateSelected];
        optionButton1.titleLabel.backgroundColor = [UIColor redColor];
        optionButton1.tag = 100*[[keysArr objectAtIndex:i-1] intValue];
        //NSLog(@"optionButton tag Values %d",[[keysArr objectAtIndex:i-1] intValue]);
        optionButton1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        optionButton1.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
        optionButton1.imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    
        optionButton1.hidden = [pollQuestion.answerStatus isEqual:@"Yes"] ? YES:NO;
        
        [scrollView addSubview:optionButton1];
        [scrollView addSubview:option1];
        
        
        UILabel *percentageLbl = [[UILabel alloc] initWithFrame:CGRectMake(option1.frame.origin.x+option1.frame.size.width+1, option1.frame.origin.y, size.width-(option1.frame.origin.x+option1.frame.size.width+1), option1.frame.size.height)];
        //        percentageLbl.text = [NSString stringWithFormat:@"%.02f%%",self.progressBar.progress];
        percentageLbl.text = [NSString stringWithFormat:@"%d%%",percentage];
        //        percentageLbl.text = [NSString stringWithFormat:@"%d%%",self.progressBar.progress];
        percentageLbl.tag = 10000*i;
        percentageLbl.textColor = (pollQuestion.answerId == i) ? UIColorFromRGB(UI_BLUE_TEXT_COLOR):UIColorFromRGB(0xb8b8b8);
        percentageLbl.textAlignment = NSTextAlignmentRight;
        percentageLbl.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        percentageLbl.font = [UIFont fontWithName:FONT_SEMIBOLD size:14.49];
        percentageLbl.backgroundColor  =[UIColor clearColor];
        percentageLbl.hidden = [pollQuestion.answerStatus isEqual:@"Yes"] ? NO:YES;
        percentageLbl.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleBottomMargin;
        [scrollView addSubview:percentageLbl];
        
        
        cursor.y +=optionButton1.frame.size.height+optionsGap;
        UIProgressView *progressBar = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
        progressBar.progress = ((float)percentage/100.0f);
        progressBar.tag = 1000*i;
        progressBar.progressTintColor = UIColorFromRGB(0x67b600);
        progressBar.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
        progressBar.frame = CGRectMake(cursor.x, cursor.y-2, size.width-2, 2);

        progressBar.hidden = [pollQuestion.answerStatus isEqual:@"Yes"] ? NO:YES;

        UIImage *divider = [UIImage imageNamed:@"pollDivider_iPad"];
        UIImageView *dividerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width-2, divider.size.height)];
        dividerImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
        dividerImageView.image = [divider stretchableImageWithLeftCapWidth:divider.size.width/2 topCapHeight:divider.size.height];
        [scrollView addSubview: progressBar];
        [scrollView addSubview:dividerImageView];

        
        if (numberOfOptions == i) {
            scrollViewHeight = optionButton1.frame.origin.y +optionButton1.frame.size.height +90;
        }
    }
    scrollView.contentSize = CGSizeMake(totalFrame.size.width, scrollViewHeight);
    [pollQuestionView addSubview:scrollView];
    
    if (currentQuestionIndex  == QArray.count-1 && [pollQuestion.answerStatus isEqual:@"Yes"])
    {
        submitPollButton.hidden = YES;
    }
    else if ([pollQuestion.answerStatus isEqual:@"Yes"])
    {
        [submitPollButton setTitle:@"NEXT QUESTION" forState:UIControlStateNormal];
    }
    else
    {
        [submitPollButton setTitle:@"SUBMIT" forState:UIControlStateNormal];
    }
    
    submitPollButton.userInteractionEnabled = YES;
}
#pragma mark - PollQuestions Delegate Methods
-(void)pollQuestionsSubmittedSuccessfully
{
    [submitPollButton setTitle:@"NEXT QUESTION" forState:UIControlStateNormal];
    if (currentQuestionIndex  == QArray.count-1)
    {
        submitPollButton.hidden = YES;
    }

    QArray = nil;
    QArray = [[PollQuestionsModel sharedDataSource] allObjects];
    [self flipAnimation];
    [self setApperience];
    
}

#pragma mark - Animation Method
-(void)flipAnimation
{
    
    CATransition *applicationLoadViewIn = [CATransition animation];
    [applicationLoadViewIn setDelegate:self];
    [applicationLoadViewIn setDuration:1.5f];
    [applicationLoadViewIn setType:@"flip"];
    if(flipped)
    {
        [applicationLoadViewIn setSubtype:kCATransitionFromRight];
    }
    else
    {
        [applicationLoadViewIn setSubtype:kCATransitionFromLeft];
    }
    [applicationLoadViewIn setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    //    [self.layer addAnimation:applicationLoadViewIn forKey:NULL];
    [[pollQuestionView layer] addAnimation:applicationLoadViewIn forKey:kCATransitionReveal];
    
}
-(void) setApperience
{
    [[scrollView subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if (flipped)
    {
        [self customIntializationWithPollQuestion:[QArray objectAtIndex:currentQuestionIndex]];
        if (submitSelected)
        {
            submitSelected = NO;
        }
    }
    else
    {
        if (currentQuestionIndex == QArray.count-1)
        {
            //            submitButton.hidden = nextButton.hidden =  YES;
            submitPollButton.hidden = YES;
            return;
        }
        else
        {
            currentQuestionIndex++;
            [self customIntializationWithPollQuestion:[QArray objectAtIndex:currentQuestionIndex]];
        }
    }
}

////    [self customIntializationWithStr:@"Question1"];
//-(IBAction)nextButtonTapped:(id)sender
//{
//    //    for(UIView *subview in [scrollView subviews]) {
//    //        [subview removeFromSuperview];
//    //    }
//    [[scrollView subviews]
//     makeObjectsPerformSelector:@selector(removeFromSuperview)];
//    
//    flipped = NO;
//    [self setApperience];
//    [self slideAnimationIsLeft:NO];
//}
-(IBAction)submitButtonTapped:(UIButton *)sender
{
    if (submitSelected)
        return;
    
    if ([sender.titleLabel.text isEqualToString:@"SUBMIT"])
    {
        if (![Common currentNetworkStatus])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"POLL" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
            [alertView show];
            return;
        }
        if (!optionSelcted)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"POLL" message:@"Please make an answer choice." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
            [alert show];
            return;
        }
        submitSelected = YES;
        PollQuestions *question = [QArray objectAtIndex:currentQuestionIndex];
        
        [[PollQuestionsModel sharedDataSource]submitPollQuestionAnswerWithQuestionId:question.key AnswerId:answerNumber AndDeviceId:[[NSUserDefaults standardUserDefaults] valueForKey:IMMIGO_GUID]];
        flipped=!flipped;
        submitPollButton.userInteractionEnabled = NO;
    }
    else
    {
        [[scrollView subviews]
         makeObjectsPerformSelector:@selector(removeFromSuperview)];

        [submitPollButton setTitle:@"SUBMIT" forState:UIControlStateNormal];
        
        flipped = NO;
        [self setApperience];
        [self slideAnimationIsLeft:NO];
    }

    
}
- (void) slideAnimationIsLeft:(BOOL)isleftDirection
{
    CATransition *applicationLoadViewIn = [CATransition animation];
    [applicationLoadViewIn setDuration:0.3f];
    [applicationLoadViewIn setType:kCATransitionPush];
    if(isleftDirection)
        [applicationLoadViewIn setSubtype:kCATransitionFromLeft];
    else
        [applicationLoadViewIn setSubtype:kCATransitionFromRight];
    flipped = NO;
    [[pollQuestionView layer] addAnimation:applicationLoadViewIn forKey:kCATransitionReveal];
    
}
-(IBAction)optionSelected:(id)sender
{
    optionSelcted = YES;
    UIButton *selectedButton = (UIButton *)sender;
    
    //    selectedButton.selected =!selectedButton.selected;
    
    for (UIView *view in [scrollView subviews])
    {
        if ([view isKindOfClass:[UIButton class]])
        {
            UIButton *button = (UIButton *)view;
            if (button.tag != selectedButton.tag && [button isSelected])
            {
                button.selected = NO;
            }
            else if (button.tag == selectedButton.tag && ![button isSelected])
            {
                button.selected = YES;
                answerNumber = (int)button.tag/100;
                //NSLog(@"button selected tag %ld",(long)button.tag);
            }
        }
    }
    
}

-(void)pollQuestionsSubmittedFailure
{
    
    //NSLog(@"Problem in Submitting the Poll Question");
}


# pragma mark Scrollview Delegate

- (void)scrollViewDidScroll:(UIScrollView *)optionsscrollView
{
    
    BOOL toEnd =NO;
    if (optionsscrollView.contentOffset.y >= (optionsscrollView.contentSize.height - optionsscrollView.frame.size.height)) {
        //reach bottom
        NSLog(@"we are at the endddd");
        toEnd =YES;
    }
    
    if (optionsscrollView.contentOffset.y < 0){
        //reach top
        NSLog(@"we are at the top");
        toEnd =NO;
    }
    
    if (optionsscrollView.contentOffset.y >= 0 && optionsscrollView.contentOffset.y < (optionsscrollView.contentSize.height - optionsscrollView.frame.size.height)){
        //not top and not bottom
    }
    
    BOOL shouldShowScrollIndicator =[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !shouldShowScrollIndicator;
    scrollIndicatorImgView.image =toEnd?[UIImage imageNamed:@"scrollIndicator_up"]:[UIImage imageNamed:@"scrollIndicator_down"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    BOOL  canShowScrollIndicator=[self canShowScrollIndicator];
    scrollIndicatorImgView.hidden = !canShowScrollIndicator;
    
}
- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
