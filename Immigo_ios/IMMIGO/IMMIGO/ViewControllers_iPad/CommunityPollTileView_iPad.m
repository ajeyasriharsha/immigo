//
//  CommunityPollTileView_iPad.m
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "CommunityPollTileView_iPad.h"
#import "Common.h"
@implementation CommunityPollTileView_iPad

-(void)customIntialization
{
    {
        self.clipsToBounds = YES;
        self.backgroundColor  =[UIColor clearColor];
        //        self.layer.borderColor  =[UIColor grayColor].CGColor;
        //        self.layer.borderWidth = 1.0f;
    }
    
    {
        topicTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, self.frame.size.width-30, self.frame.size.height*0.8)];
        topicTitle.autoresizingMask =UIViewAutoresizingFlexibleWidth;
        topicTitle.backgroundColor = [UIColor clearColor];
        topicTitle.numberOfLines = 2;
        topicTitle.font = [UIFont fontWithName:FONT_IPAD size:18.0f];
       
        UIImage *buttonBg = [UIImage imageNamed:@"arrow_unselected"];
        arrowImage =[[UIImageView alloc] initWithImage:buttonBg highlightedImage:buttonBg];
        arrowImage.frame =CGRectMake(self.frame.size.width-20, 0, 20, self.frame.size.height);
        arrowImage.contentMode =UIViewContentModeScaleAspectFit;
        arrowImage.autoresizingMask =UIViewAutoresizingFlexibleLeftMargin;
        
        seperatorimageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-1, self.frame.size.width, 1)];
        UIImage *textFiledBackGround = [UIImage imageNamed:@"menuTitleDivider"];
        seperatorimageView.image = [textFiledBackGround stretchableImageWithLeftCapWidth:textFiledBackGround.size.width/2 topCapHeight:textFiledBackGround.size.height/2];
        
        //        imgaeView.image = [image Stre];
        seperatorimageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        seperatorimageView.backgroundColor =[UIColor clearColor];

        
        votedLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width-150, self.frame.size.height*0.8, 150, self.frame.size.height*0.2)];
        votedLabel.backgroundColor = [UIColor clearColor];
//        votedLabel.font = [UIFont fontWithName:FONT_REGULAR size:10.0f];
        votedLabel.font = [UIFont fontWithName:FONT_REGULAR size:13.0];
        votedLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        votedLabel.textAlignment = NSTextAlignmentRight;
        votedLabel.textColor = UIColorFromRGB(0xe61e28);

        
        [self addSubview:arrowImage];
        [self addSubview:topicTitle];
        [self addSubview:seperatorimageView];
        [self addSubview:votedLabel];
     
        
    }
}


-(void)setPollQuestion:(PollQuestions *)pollSelectedQuestion{
    
    _pollQuestion =pollSelectedQuestion;
    topicTitle.text = _pollQuestion.question;
//    topicTitle.textColor = [pollSelectedQuestion.answerStatus isEqual:@"Yes"]? [UIColor colorWithRed:(81.0f/255.0f) green:(183.0f/255.0f) blue:(22.0f/255.0f) alpha:1]:[UIColor darkGrayColor];

    topicTitle.textColor = [UIColor darkGrayColor];

//    if ([pollSelectedQuestion.answerStatus isEqual:@"Yes"])
    
//    {
//        self.layer.shadowColor = [UIColor blackColor].CGColor;
//        self.layer.shadowOpacity = 0.5f;
//        self.layer.shadowRadius = 0.8f;
//        self.layer.shadowOffset = CGSizeMake(-0.5, 0.5);
//
//    }
    votedLabel.text = [pollSelectedQuestion.answerStatus isEqual:@"Yes"]?@"Voted":@"";

}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        [self customIntialization];
    }
    return self;
}
-(PollQuestions *)pollQuestion
{
    return _pollQuestion;
}

@end
