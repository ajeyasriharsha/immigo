//
//  HomeViewController_iPad.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/23/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "HomeViewController_iPad.h"
#import "Common.h"
#import "ImmigrationBasicsModel.h"
#import "DataDownloader.h"
#import "ImmigrationSubTopicsVC_iPad.h"
#import "TrainingsAndEventsVC_iPad.h"
#import "FakeViewController_iPad.h"
#import "PartnersPullableView-Ipad.h"
#import "AppDelegate.h"
#import "Alerts.h"
#import "AlertsModel.h"

@interface HomeViewController_iPad () <DataDownloaderDelegate>
{
    NSArray *topicsArray;

    IBOutlet UILabel *screenHeadingLabel;
    IBOutlet UITableView *topicsTabel;
    
    NSIndexPath *selectedIndexpath;
    IBOutlet UIButton *dropDownBtn;
    IBOutlet UIImageView *headerDivider;
    IBOutlet UIImageView *bottomDivider;
}
- (IBAction)tappedOnMenu:(id)sender;
@end

@implementation HomeViewController_iPad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)stopAnimatingPartnersView
{
    
}
-(void)pullableView:(PullableView *)pView didChangeState:(BOOL)opened
{
    
}
-(void)setFromSplash:(BOOL)fromSplash
{
    _fromSplash = fromSplash;
}
-(BOOL)fromSplash
{
    return _fromSplash;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImage *dividerImage = [UIImage imageNamed:@"menuTitleDivider"];
    bottomDivider.image = [dividerImage stretchableImageWithLeftCapWidth:dividerImage.size.width/2 topCapHeight:dividerImage.size.height/2];
//    self.navigationController.view.hidden = NO;
    
    [[DataDownloader sharedDownloader] setDelegate:self];
    [[ImmigrationBasicsModel sharedDataSource] fetchTopics];

    dropDownBtn.titleLabel.font = [UIFont fontWithName:FONT_IPAD size:18.0f];
    screenHeadingLabel.font = [UIFont fontWithName:FONT_IPAD size:22.0f];
    topicsTabel.rowHeight = 50.0f;
    
    [dropDownBtn setTitle:@"IMMIGRATION BASICS" forState:UIControlStateNormal];
    UIImage *image = [UIImage imageNamed:@"headerDivider"];
    headerDivider.image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    
    screenHeadingLabel.textColor = [UIColor lightGrayColor];
    
    topicsTabel.tableFooterView = [UIView new];
    
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate]refreshFakeViewController];
    
    [self createPartnersView];
    [bottompullRightView setOpened:_fromSplash animated:YES];
    if (_fromSplash)
    {
        FakeViewController_iPad *root =  (FakeViewController_iPad*)self.navigationController.parentViewController;
        [root tappedOnMenu:root.bigMenuBtn];
        [self performSelector:@selector(closePartner) withObject:self afterDelay:5.0f];
    }
    if (selectedIndexpath) {
        
        UITableViewCell* theCell = [topicsTabel cellForRowAtIndexPath:selectedIndexpath];
        theCell.textLabel.textColor = [UIColor darkGrayColor];
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    
        [super viewDidAppear:animated];
   

}
-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    _fromSplash = NO;
    [bottompullRightView removeFromSuperview];
}
-(void)closePartner
{
    [bottompullRightView setOpened:NO animated:YES];
    
}
-(void)createPartnersView
{
    CGSize viewFrame = CGSizeZero;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        
        if (UIInterfaceOrientationIsPortrait(orientation))
        {
            viewFrame = CGSizeMake(768, 1004);
        }
        else
        {
            viewFrame = CGSizeMake(1024,748);
        }
        UIImage *parternsImg = [UIImage imageNamed:@"partners_iPad"];
        UIImageView *parternsImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, parternsImg.size.width, parternsImg.size.height)];
        bottompullRightView = [[PartnersPullableView_Ipad alloc] initWithFrame:CGRectMake(0, viewFrame.width, viewFrame.width-40, parternsImageView.frame.size.height)];
        parternsImageView.image = parternsImg;
        bottompullRightView.backgroundColor = [UIColor clearColor];
        
        if (UIInterfaceOrientationIsPortrait(orientation))
        {
            
            bottompullRightView.openedCenter = CGPointMake(viewFrame.width/2+20,viewFrame.width+65);
            
            bottompullRightView.closedCenter = CGPointMake(bottompullRightView.openedCenter.x+(bottompullRightView.frame.size.width-parternsImg.size.width), bottompullRightView.openedCenter.y);
            
        }
        else
        {
            
            
            bottompullRightView.openedCenter = CGPointMake(viewFrame.width/2+20,
                                                           ((viewFrame.height -160)-(bottompullRightView.frame.size.height/2)+50));
            
            bottompullRightView.closedCenter = CGPointMake(viewFrame.width+bottompullRightView.openedCenter.x-(parternsImg.size.width)*2-10, bottompullRightView.openedCenter.y);
            
            
        }
        
        bottompullRightView.center = bottompullRightView.closedCenter;
        
        bottompullRightView.animationDuration =0.9f;
        bottompullRightView.animate = YES;
        
        bottompullRightView.handleView.frame = CGRectMake(0, 0, parternsImageView.frame.size.width, bottompullRightView.frame.size.height);
        [bottompullRightView.handleView addSubview:parternsImageView];
        [bottompullRightView bringSubviewToFront:bottompullRightView.handleView];
        bottompullRightView.delegate = self;
        
        
        [self.view addSubview:bottompullRightView];
    }
    else{
        
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        NSLog(@"iPad Screen Size %@",NSStringFromCGSize(iOSDeviceScreenSize));

//        if (UIInterfaceOrientationIsPortrait(orientation))
//        {
//            viewFrame = CGSizeMake(768, 1044);
//        }
//        else
//        {
//            viewFrame = CGSizeMake(1024,768);
//        }
        UIImage *parternsImg = [UIImage imageNamed:@"partners_iPad"];
        UIImageView *parternsImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, parternsImg.size.width, parternsImg.size.height)];
        bottompullRightView = [[PartnersPullableView_Ipad alloc] initWithFrame:CGRectMake(0, iOSDeviceScreenSize.width, iOSDeviceScreenSize.width-40, parternsImageView.frame.size.height)];
        parternsImageView.image = parternsImg;
        bottompullRightView.backgroundColor = [UIColor clearColor];
        
        if (UIInterfaceOrientationIsPortrait(orientation))
        {
            
            bottompullRightView.openedCenter = CGPointMake(iOSDeviceScreenSize.width/2+20,iOSDeviceScreenSize.width+85);
            
            bottompullRightView.closedCenter = CGPointMake(bottompullRightView.openedCenter.x+(bottompullRightView.frame.size.width-parternsImg.size.width), bottompullRightView.openedCenter.y);
            
        }
        else
        {
            
            
            bottompullRightView.openedCenter = CGPointMake(iOSDeviceScreenSize.width/2+20,
                                                           ((iOSDeviceScreenSize.height -160)-(bottompullRightView.frame.size.height/2)+50));
            
            bottompullRightView.closedCenter = CGPointMake(iOSDeviceScreenSize.width+bottompullRightView.openedCenter.x-(parternsImg.size.width)*2-10, bottompullRightView.openedCenter.y);
            
            
        }
        
        bottompullRightView.center = bottompullRightView.closedCenter;
        NSLog(@"Center in %@ Orientation  is %@",UIInterfaceOrientationIsPortrait(orientation)?@"Portrait":@"Landscape",NSStringFromCGPoint(bottompullRightView.center));
        
        bottompullRightView.animationDuration =0.9f;
        bottompullRightView.animate = YES;
        
        bottompullRightView.handleView.frame = CGRectMake(0, 0, parternsImageView.frame.size.width, bottompullRightView.frame.size.height);
        NSLog(@"Pullable View Frame %@",NSStringFromCGRect(bottompullRightView.frame));
        [bottompullRightView.handleView addSubview:parternsImageView];
        [bottompullRightView bringSubviewToFront:bottompullRightView.handleView];
        bottompullRightView.delegate = self;
        
        
        [self.view addSubview:bottompullRightView];
        
    }
    
    
   


    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Data Downloader Delegate methods
-(void)immigrationBasicsLoadedFailure
{
    topicsArray = [[ImmigrationBasicsModel sharedDataSource] allItems];
    [topicsTabel reloadData];
}
-(void)immigrationBasicsDownloadedSuccessfully
{
    topicsArray = [[ImmigrationBasicsModel sharedDataSource] allItems];
    [topicsTabel reloadData];
}

#pragma mark - Tableview delegate and Data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return topicsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Topic *topic =[topicsArray objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    cell.textLabel.text = topic.topicTitle;//[NSString stringWithFormat:@"TopicTopicTopicTopicTopicTopic %ld",indexPath.row+1];
    cell.textLabel.font = [UIFont fontWithName:FONT_IPAD size:22.0f];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    if (indexPath == selectedIndexpath)
    {
        cell.textLabel.textColor =UIColorFromRGB(UI_BLUE_TEXT_COLOR);
    }
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (selectedIndexpath)
    {
        UITableViewCell* theCell = [tableView cellForRowAtIndexPath:selectedIndexpath];
        theCell.textLabel.textColor = [UIColor darkGrayColor];
    }//else
    selectedIndexpath = indexPath;
    {
        UITableViewCell* theCell = [tableView cellForRowAtIndexPath:indexPath];
        theCell.textLabel.textColor = UIColorFromRGB(UI_BLUE_TEXT_COLOR);
    }

    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    UITableViewCell* theCell = [tableView cellForRowAtIndexPath:indexPath];
    theCell.textLabel.textColor = UIColorFromRGB(UI_BLUE_TEXT_COLOR);

    FakeViewController_iPad *root =  (FakeViewController_iPad*)self.navigationController.parentViewController;
    [root.dropDown hideDropDown:root.bigMenuBtn];
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    ImmigrationSubTopicsVC_iPad* toVC = [storyboard instantiateViewControllerWithIdentifier:@"ImmigrationSubTopicsVC_iPad"];
    toVC.topic = [topicsArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:toVC animated:YES];
    
    
}

/*
-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
//    UIView* result = [super hitTest:point withEvent:event];
//    if (result)
//        return result;
//    for (UIView* sub in [self.subviews reverseObjectEnumerator]) {
//        CGPoint pt = [self convertPoint:point toView:sub];
//        result = [sub hitTest:pt withEvent:event];
//        if (result)
//            return result;
//    }
//    return nil;
}
 */
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)tappedOnMenu:(id)sender
{
    
}

#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait ||interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

- (void)willAnimateFirstHalfOfRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    
//    [[[[UIApplication sharedApplication] windows] objectAtIndex:0] setWindowLevel:UIWindowLevelAlert];

}
- (void)didAnimateFirstHalfOfRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [bottompullRightView removeFromSuperview];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
//    [[[[[UIApplication sharedApplication]windows] objectAtIndex:1] rootViewController] view].backgroundColor = [UIColor clearColor];
//        [[[[[UIApplication sharedApplication]windows] objectAtIndex:1] rootViewController] view].alpha = 0.3f;
//        [[[[UIApplication sharedApplication] windows] objectAtIndex:0] setWindowLevel:UIWindowLevelNormal];
    
    [self createPartnersView];

}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
//    [[[[[UIApplication sharedApplication]windows] objectAtIndex:1] rootViewController] view].backgroundColor = [UIColor whiteColor];
//    //NSLog(@"windows %@",[[UIApplication sharedApplication] windows]);
}


@end
