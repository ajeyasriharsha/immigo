//
//  ParternsPullableView.h
//  Immigo
//
//  Created by pradeep ISPV on 2/27/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "PullableView.h"
#import "MTPopupWindow.h"
#import "PartnersLandscapeView.h"

@protocol PartnersPullableViewDelegate <NSObject>

@optional
-(void)stopAnimatingPartnersView;

@end

@interface PartnersPullableView : PullableView <UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate,MTPopupWindowDelegate>
{
    UIWebView *partnersWebView;
    UIButton *closeButton;
    UIActivityIndicatorView *activityController;
    
}
@property (nonatomic,strong) id <PartnersPullableViewDelegate> partnersDelegate;

@property (nonatomic,strong) PartnersLandscapeView *partnerLandscapeView;
@end
