//Om Sri Sai Ram
//  GridView.m
//  SAT Speed Test
//
//  Created by PrasadBabu KN on 5/12/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "GridView.h"
#import <QuartzCore/QuartzCore.h>
#import <Math.h>

@interface GridView (privateMembers)
- (void)addPullToRefreshHeader;
- (void)startLoading;
- (void)gridRefresh;
@end

@implementation GridView
@synthesize gridViewDelegate = _gridViewDelegate;
@synthesize tileSizePortrait = _tileSizePortrait;
@synthesize tileSizeLandScape = _tileSizeLandScape;
@synthesize tiles_gap = _tiles_gap;
@synthesize contentScrollView;

-(void)customIntialization 
{
    {
        self.clipsToBounds=true;    
        self.userInteractionEnabled = YES;
        // self.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        
        {
            contentScrollView  = [[UIScrollView alloc] init];
            contentScrollView.delegate = self;
            contentScrollView.frame=CGRectMake(0,0, self.frame.size.width, self.frame.size.height);
            contentScrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;//|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
            contentScrollView.backgroundColor= [UIColor clearColor];//[UIColor colorWithRed:74/255 green:72/255 blue:69/255 alpha:0.5];
            [self addSubview:contentScrollView];
            
            contentView = [[UIView alloc] init];
            contentView.frame=contentScrollView.frame;
            contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
            contentView.backgroundColor= [UIColor clearColor];//[UIColor colorWithRed:74/255 green:72/255 blue:69/255 alpha:0.5];
            [contentScrollView addSubview:contentView];
            
        }
        
    }
    
    _scrollDirection = SD_VERTICLE;
   // _tiles_gap = CGSizeMake(10,10);
   // _tileSizePortrait=CGSizeMake(200,400);
   // _tileSizeLandScape=CGSizeMake(400,200);
}

-(void)calulationOfTileSize
{
    containerWidth = contentScrollView.frame.size.width;
    containerHeight = contentScrollView.frame.size.height;
    CGSize suggestedSize;
    if(SD_VERTICLE==_scrollDirection)
    {
        suggestedSize=_tileSizePortrait;
    }
    else 
    {
        suggestedSize=_tileSizeLandScape;
    }
    
    noColumns=(containerWidth-_tiles_gap.width)/(suggestedSize.width+_tiles_gap.width);
    noColumns=noColumns<1?1:noColumns;
    noRows=(containerHeight-_tiles_gap.height)/(suggestedSize.height+_tiles_gap.height);
    noRows=noRows<1?1:noRows;
    componentSize=CGSizeMake((containerWidth-(_tiles_gap.width*(noColumns+1)))/noColumns, (containerHeight-(_tiles_gap.height*(noRows+1)))/noRows);
    
}

-(CGRect) rectForViewAtIndex:(int) index
{
    CGRect rect=CGRectMake(0, 0, componentSize.width, componentSize.height);
    switch (_scrollDirection) 
    {
        case SD_VERTICLE:
        {
            rect.origin.x=(index%noColumns)*(componentSize.width+_tiles_gap.width)+_tiles_gap.width;
            rect.origin.y=(index/noColumns)*(componentSize.height+_tiles_gap.height)+_tiles_gap.height;
            return rect;
            break;
        }
        case SD_HORIZANTAL:
        {
            rect.origin.x=(index/noRows)*(componentSize.width+_tiles_gap.width)+_tiles_gap.width;
            rect.origin.y=(index%noRows)*(componentSize.height+_tiles_gap.height)+_tiles_gap.height;
            return rect;
            break;
        }
    }
    return rect;
}

-(void)adjustScrollContentSize
{
    int numberofTiles = [_gridViewDelegate numberOfTilesIngridView:self];
    if(numberofTiles>0)
        numberofTiles--;
    CGSize contentSize;
    if(_scrollDirection==SD_VERTICLE)
    {
        CGFloat height=_tiles_gap.height  + componentSize.height + ((_tiles_gap.height+componentSize.height) * (numberofTiles/noColumns));
        if(height<containerHeight)
            height=containerHeight;
        contentSize=CGSizeMake(contentScrollView.frame.size.width, height);
    }
    else 
    {
        CGFloat width=_tiles_gap.width + ((componentSize.width+_tiles_gap.width) * (numberofTiles/noRows));
        if(width<containerWidth)
            width=containerWidth+1;
        contentSize=CGSizeMake(width,contentScrollView.frame.size.height);
    }
    contentScrollView.contentSize=contentSize;
    contentView.frame=CGRectMake(0, 0, contentSize.width, contentSize.height);
    
}

-(void)reloadData
{
    for(UIView *view in [contentView subviews])
    {
        [view removeFromSuperview];
    }
    int numberofTiles = [_gridViewDelegate numberOfTilesIngridView:self];
    [self calulationOfTileSize];
    for(int index=0;index<numberofTiles;index++)
    {
        UIView *tileView=[_gridViewDelegate gridView:self viewAtIndex:index];
        tileView.tag=index+1;
        tileView.frame=[self rectForViewAtIndex:index];
        UITapGestureRecognizer *tapView=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapedOnTile:)];
        [tileView addGestureRecognizer:tapView];
        tapView=nil;
//        if([tileView isKindOfClass:[TopicView class]])
//        {
//            ((TopicView*)tileView).topicViewdelegate=self;
//        }
        
        //        tileView.autoresizingMask=UIViewAutoresizingNone;//UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [contentView addSubview:tileView];
        //        tileView.layer.borderColor=[UIColor grayColor].CGColor;
        //        tileView.layer.borderWidth=2;
        //        tileView.layer.masksToBounds=true;
        //        tileView.layer.shadowColor= [UIColor colorWithRed:(77/255) green:(69/255) blue:(55/255) alpha:0.5].CGColor;
        //        tileView.layer.shadowOffset=CGSizeMake(-1,2);
        //        tileView.layer.shadowRadius=1.0f;
        //        tileView.layer.shadowOpacity=0.5;
        //        tileView.layer.masksToBounds=false;
        
        // tileView.layer.cornerRadius = UI_CORNER_RADIUS_BIG;
        
        
        tileView.layer.transform=CATransform3DTranslate(CATransform3DIdentity, 0.0f, 0.0f, -400.0f);
    }
    [self adjustScrollContentSize];
}

-(void) tapedOnTile:(UITapGestureRecognizer*)sender
{
    if(sender.state == UIGestureRecognizerStateEnded)
    {
        [self layoutSubviews];
        if([sender.view.gestureRecognizers count]>0)
        {
            UIGestureRecognizer *gesture=[sender.view.gestureRecognizers objectAtIndex:0];
            gesture.enabled=false;
        }
        int index = [sender.view tag]-1;
        if (_gridViewDelegate && [_gridViewDelegate respondsToSelector:@selector(gridView:didSelectTileAtIndex:)]) 
        {
            [_gridViewDelegate gridView:self didSelectTileAtIndex:index];
        }
        if (_gridViewDelegate && [_gridViewDelegate respondsToSelector:@selector(gridView:didSelectedTile:)]) 
        {
            [_gridViewDelegate gridView:self didSelectedTile:sender.view];
        }
    }
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    int numberofTiles = [_gridViewDelegate numberOfTilesIngridView:self];
    [self calulationOfTileSize];
    for(int index=0;index<numberofTiles;index++)
    {
        UIView *tileView=[contentView viewWithTag:index+1];
        tileView.frame=[self rectForViewAtIndex:index];
        {
            UIGestureRecognizer *gesture=[tileView.gestureRecognizers objectAtIndex:0];
            gesture.enabled=true;
        }
    }
    [self adjustScrollContentSize];
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customIntialization];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customIntialization];
    }
    return self;
}

- (void)gridRefresh 
{
    if (!_needPullToRefresh) {
        return;
    }
    
    // This is just a demo. Override this method with your custom reload action.
    // Don't forget to call stopLoading at the end.
    [self performSelector:@selector(stopLoading) withObject:nil afterDelay:2.0];
    if (_gridViewDelegate && [_gridViewDelegate respondsToSelector:@selector(gridRefresh)]) 
    {
        [_gridViewDelegate gridRefresh];
    }
    
}

-(UIView*) viewAtIndex:(NSInteger)index
{
    int tag=index+1;
    return [contentView viewWithTag:tag];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    BOOL toEnd =NO;
    if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
        //reach bottom
        NSLog(@"we are at the endddd");
        toEnd =YES;
    }
    
    if (scrollView.contentOffset.y < 0){
        //reach top
        NSLog(@"we are at the top");
        toEnd =NO;
    }
    
    if (scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y < (scrollView.contentSize.height - scrollView.frame.size.height)){
        //not top and not bottom
    }
    if (_gridViewDelegate && [_gridViewDelegate respondsToSelector:@selector(gridViewshouldChangeScrollIndicators:)]) {
        [_gridViewDelegate gridViewshouldChangeScrollIndicators:toEnd];
    }
    
//
//    if (scrollView.contentOffset.y == roundf(scrollView.contentSize.height-scrollView.frame.size.height)) {
//        NSLog(@"we are at the endddd");
//        
//        //Call your function here...
//        
//        
//        
//    }
}

@end
