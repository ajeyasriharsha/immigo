//
//  CustomNavBar.m
//  customNavBar
//
//  Created by Jake Rocheleau on 1/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CustomNavBar.h"
#import <objc/runtime.h>
#import "UIImage+Utilities.h"

const CGFloat VFSNavigationBarHeightIncrease = 58.f;

#define kAppNavBarHeight 58.0
@implementation CustomNavBar
@synthesize navigationBarBackgroundImage, navigationController;
- (void) didMoveToSuperview {
    if( [self respondsToSelector: @selector(setBackgroundImage:forBarMetrics:)]) {
        //iOS5.0 and above has a system defined method -> use it
        //[[UINavigationBar appearance] setBackgroundImage:[[UIImage imageNamed:@"partners"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)] forBarMetrics:UIBarMetricsDefault];

        [self setBackgroundImage: [[UIImage imageNamed:@"Nav_Background"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)] forBarMetrics:UIBarMetricsDefault];
    }

}
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (void)drawRect:(CGRect)rect {
	
    
    CGRect frame = [self frame];
    frame.size.height = 58.0f;
    self.backgroundColor =[UIColor yellowColor];
    [self setFrame:frame];

    UIImage *image = [UIImage imageNamed: @"Nav_Background"];
    [image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    
    //for iOS5
    [self setBackgroundImage:[UIImage imageNamed: @"Nav_Background"] forBarMetrics:UIBarMetricsDefault];

    return;
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        frame.size.height = 58.0f;
        //[self setBackgroundColor:[UIColor clearColor]];
        
        /*
        UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo.png"];
        UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
        titlView.image = titleLogo;
        titlView.contentMode = UIViewContentModeCenter;
        titlView.clipsToBounds = YES;
        titlView.contentMode = UIViewContentModeTop;
        [self setBackgroundColor:[UIColor whiteColor]];
        [self.topItem.titleView setBackgroundColor:[UIColor clearColor]];
        self.topItem.titleView = titlView;
        */
        
//        UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
//        [backView setBackgroundColor:[UIColor clearColor]];
//        UIImage *image = [UIImage imageNamed:@"Nav_immigo"];
//        UIImageView *titleImageView = [[UIImageView alloc] initWithImage:image];
//        titleImageView.frame = CGRectMake(0, 5,backView.frame.size.width , backView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
//        [backView addSubview:titleImageView];
//        titleImageView.contentMode = UIViewContentModeTop;
//        self.topItem.titleView = backView;
    }
//    [self setBackgroundImage:[UIImage imageNamed:@"TopTabTitleBg"] forBarMetrics:UIBarMetricsDefault];
    
    [self setFrame:frame];
//    UIImage *imagetoSet =[self imageWithImage:[UIImage imageNamed:@"Nav_Background"] scaledToSize:CGSizeMake(frame.size.width, frame.size.height)];
    
    UIImage *imagetoSet =[UIImage imageNamed:@"Nav_Background"];
    [self setBackgroundImage:imagetoSet forBarMetrics:UIBarMetricsDefault];

//    self.backgroundColor = [UIColor colorWithPatternImage: [imagetoSet resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(self.frame.size.width, self.frame.size.height) interpolationQuality:kCGInterpolationHigh]];

}



- (CGSize)sizeThatFits:(CGSize)size
{
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0 )
    {
        return CGSizeMake(self.superview.bounds.size.width, 58.0f);
    }
    
    return CGSizeMake(self.superview.bounds.size.width, 44);
     //[self setTitleVerticalPositionAdjustment:-10 forBarMetrics:UIBarMetricsDefault];
}

/*
- (void)layoutSubviews {
    static CGFloat yPosForArrow = -1;
    
    [super layoutSubviews];
    
    // There's no official way to reposition the back button's arrow under iOS 7. It doesn't shift with the title.
    // We have to reposition it here instead.
    
    for (UIView *view in self.subviews) {
        
        // The arrow is a class of type _UINavigationBarBackIndicatorView. We're not calling any private methods, so I think
        // this is fine for the AppStore...
        
        if ([NSStringFromClass([view class]) isEqualToString:@"_UINavigationBarBackIndicatorView"]) {
            CGRect frame = view.frame;
            
            if (yPosForArrow < 0) {
                
                // On the first layout we work out what the actual position should be by applying our offset to the default position.
                
                yPosForArrow = frame.origin.y + (44 - kAppNavBarHeight);
            }
            
            // Update the frame.
            
            frame.origin.y = yPosForArrow;
            view.frame = frame;
        }
    }}
*/
@end
