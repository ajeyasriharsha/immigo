//
//  CustomNavBar.h
//  customNavBar
//
//  Created by Jake Rocheleau on 1/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavBar : UINavigationBar
{
    IBOutlet UINavigationController* navigationController;
}
@property (nonatomic, retain) UIImageView *navigationBarBackgroundImage;
@property (nonatomic, retain) IBOutlet UINavigationController* navigationController;
@end
