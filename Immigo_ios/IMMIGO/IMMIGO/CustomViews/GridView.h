//Om Sri Sai Ram
//  GridView.h
//  SAT Speed Test
//
//  Created by PrasadBabu KN on 5/12/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum 
{
    SD_VERTICLE,
    SD_HORIZANTAL
}ScrollDirection;

@class GridView;

@protocol MARGridViewDelegate <NSObject>
@required
- (int)numberOfTilesIngridView:(GridView *)gridView;
- (UIView *)gridView:(GridView *)gridView viewAtIndex:(int)index;
@optional
-(void)gridView:(GridView*) gridView didSelectTileAtIndex:(int) index;
-(void)gridView:(GridView*) gridView didSelectedTile:(UIView*) view;
-(void)gridViewshouldChangeScrollIndicators:(BOOL)toEnd;
- (void)gridRefresh ;
@end

@interface GridView : UIView<UIScrollViewDelegate>
{
    id<MARGridViewDelegate>_gridViewDelegate;
    UIScrollView *contentScrollView;
    UIView *contentView;
    
    CGFloat containerWidth;
    CGFloat containerHeight;
    int noColumns;
    int noRows;
    
    CGSize _tileSizePortrait;
    CGSize _tileSizeLandScape;
    //    UIScrollView*contentView;
    ScrollDirection _scrollDirection;
    CGSize _tiles_gap;
    CGSize componentSize;
    //    NSMutableSet *_visibleViews;
    //    NSMutableSet *_reusableViews;
    
    BOOL isDragging;
    BOOL isLoading;
    UIImageView *refreshArrow;    
    
    bool _needPullToRefresh;
}
@property (nonatomic,strong) UIScrollView *contentScrollView;
@property (nonatomic) CGSize tileSizePortrait;
@property (nonatomic) CGSize tileSizeLandScape;

@property (nonatomic) CGSize tiles_gap;

@property (nonatomic, retain) IBOutlet id<MARGridViewDelegate> gridViewDelegate;
-(void)reloadData;

- (id)initWithFrame:(CGRect)frame;
-(UIView*) viewAtIndex:(NSInteger)index;

@end
