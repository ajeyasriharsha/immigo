//
//  OptionsView.h
//  Immigo
//
//  Created by pradeep ISPV on 3/14/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdvancedSearchVC.h"
@protocol optionsViewDelegate <NSObject>
@optional
-(void)passValuesToField:(NSArray *)values;
-(void)passValueToField:(NSString *)string;
-(void)closePopUP;
@end
@interface OptionsView : UIView <UITableViewDelegate,UITableViewDataSource,AdvancedSearchVCDelegate,UISearchBarDelegate>
{
    NSArray *optionsArr;
    NSMutableArray *selectedIndexes;
    NSString *popUpTitle;
    UITableView *optionsTbl;
    UILabel *titleLbl;
    UIButton *closeButton;
    
    NSString *existingValue;
    
    NSMutableArray *selectedValues;
    NSMutableArray *searchFilteredArray;
    BOOL isSerachEnabled;
    NSString *_searchString;
}
@property (nonatomic) id<optionsViewDelegate> delegate;
- (id)initWithFrame:(CGRect)frame withOptions:(NSArray *)options withTitle:(NSString *)title withValue:(NSString *)value;
- (id)initWithFrame:(CGRect)frame withOptions:(NSArray *)options withTitle:(NSString *)title withValues:(NSArray *)array;
@end
