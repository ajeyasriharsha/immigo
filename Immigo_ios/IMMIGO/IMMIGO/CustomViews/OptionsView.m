//
//  OptionsView.m
//  Immigo
//
//  Created by pradeep ISPV on 3/14/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "OptionsView.h"
#import <QuartzCore/QuartzCore.h>

@implementation OptionsView
@synthesize delegate;
-(void)customIntialization
{
    {
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor colorWithRed:(227.0f/255.0f) green:(38.0f/255.0f) blue:(45.0f/255.0f) alpha:1.0];
     
        
        
        [self.layer setShadowColor:[UIColor blackColor].CGColor];
        [self.layer setShadowOpacity:0.8];
        [self.layer setShadowRadius:3.0];
        [self.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    }
    self.autoresizingMask =UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleTopMargin;
    titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.frame.size.width-20, 40)];
    titleLbl.text = popUpTitle;
    titleLbl.backgroundColor = [UIColor clearColor];//[UIColor colorWithRed:(227.0f/255.0f) green:(38.0f/255.0f) blue:(45.0f/255.0f) alpha:1.0];
    titleLbl.textColor = [UIColor whiteColor];
    titleLbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:18.0f];
    titleLbl.textAlignment = NSTextAlignmentCenter;
    titleLbl.autoresizingMask =UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight   ;
    [self addSubview:titleLbl];
    
    closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.frame = CGRectMake(self.frame.size.width-30, 10, 30, 30);
    [closeButton setImage:[UIImage imageNamed:@"popUp_close"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:closeButton];

    optionsTbl = [[UITableView alloc] initWithFrame:CGRectMake(10, titleLbl.frame.origin.y+titleLbl.frame.size.height, self.frame.size.width-20, self.frame.size.height-titleLbl.frame.size.height-20) style:UITableViewStylePlain];
    optionsTbl.delegate = self;
    optionsTbl.dataSource = self;
    optionsTbl.backgroundColor = [UIColor clearColor];
    
    
    if ([titleLbl.text isEqualToString:@"LANGUAGE"]) {
        UISearchBar *searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, 320, 45)];
        //temp.tintColor = [UIColor redColor];
        searchBar.backgroundImage = [[UIImage alloc] init];
        searchBar.tintColor = [UIColor blackColor];
        searchBar.showsCancelButton=NO;
        searchBar.autocorrectionType=UITextAutocorrectionTypeNo;
        searchBar.autocapitalizationType=UITextAutocapitalizationTypeNone;
        searchBar.delegate=self;
        searchBar.placeholder = @"Search Languages";
        optionsTbl.tableHeaderView=searchBar;
    }
    
    
    
    [self addSubview:optionsTbl];
    
    if ([optionsTbl respondsToSelector:@selector(setSeparatorInset:)]) {
        [optionsTbl setSeparatorInset:UIEdgeInsetsZero];
    }
}





- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self customIntialization];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame withOptions:(NSArray *)options withTitle:(NSString *)title withValue:(NSString *)value
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        existingValue = value;
        popUpTitle = title;
        optionsArr = [NSArray arrayWithArray:options];
        selectedValues = [NSMutableArray array];
        selectedIndexes = [NSMutableArray array];
        [self customIntialization];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame withOptions:(NSArray *)options withTitle:(NSString *)title withValues:(NSArray *)array
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
//        existingValue = value;
        popUpTitle = title;
        optionsArr = [NSArray arrayWithArray:options];
        selectedValues = [NSMutableArray array];
        selectedIndexes = [NSMutableArray array];
        if ([array count]>0)
        {
            [selectedIndexes removeAllObjects];
            [selectedValues removeAllObjects];
            for (int i=0; i<array.count; i++)
            {
                NSUInteger index= [optionsArr indexOfObject:[array objectAtIndex:i]];
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:0];
                [selectedIndexes addObject:indexpath];
                [selectedValues addObject:[optionsArr objectAtIndex:index]];
            }
        }
        
        [self customIntialization];
    }
    return self;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    // This method has been called when u enter some text on search or Cancel the search.
    if([searchText isEqualToString:@""] || searchText==nil) {
        // Nothing to search, empty result.
        [searchBar resignFirstResponder];
        isSerachEnabled =NO;
        }
}

//search button was tapped
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self handleSearch:searchBar];
}

//user finished editing the search text
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [self handleSearch:searchBar];
}
//user tapped on the cancel button
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    NSLog(@"User canceled search");
    [searchBar resignFirstResponder];
    isSerachEnabled =NO;
}
//do our search on the remote server using HTTP request
- (void)handleSearch:(UISearchBar *)searchBar {
    isSerachEnabled=true;
    _searchString = [searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //check what was passed as the query String and get rid of the keyboard
    NSLog(@"User searched for %@", searchBar.text);
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self contains[cd] %@", searchBar.text];
    NSArray *foundPersonArray = [optionsArr filteredArrayUsingPredicate:predicate];
    searchFilteredArray=[foundPersonArray mutableCopy];
    
    if ([_searchString isEqualToString:@""]) {
        isSerachEnabled=false;
        
    }
    [optionsTbl reloadData];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  isSerachEnabled? [searchFilteredArray count]:[optionsArr count];//[optionsArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString *title =  isSerachEnabled? [searchFilteredArray objectAtIndex:indexPath.row]:[optionsArr objectAtIndex:indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = title;
    cell.contentView.backgroundColor = [UIColor whiteColor];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.textLabel.numberOfLines = 2;
    cell.textLabel.font = [UIFont fontWithName:@"OpenSans" size:14.0f];

    if([selectedIndexes containsObject:indexPath])
    {
		[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
	} else {
		[cell setAccessoryType:UITableViewCellAccessoryNone];
	}

    return cell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    
    if([selectedIndexes containsObject:indexPath])
    {
		[selectedIndexes removeObject:indexPath];
        [selectedValues removeObject:[optionsArr objectAtIndex:indexPath.row]];
	} else
    {
		[selectedIndexes addObject:indexPath];
        [selectedValues addObject:[optionsArr objectAtIndex:indexPath.row]];
	}
	[tableView reloadData];

}
- (void)layoutSubviews {
   
    [super layoutSubviews];
    titleLbl.frame  =CGRectMake(10, 10, self.frame.size.width-20, 40);
    optionsTbl.frame = CGRectMake(10, titleLbl.frame.origin.y+titleLbl.frame.size.height, self.frame.size.width-20, self.frame.size.height-titleLbl.frame.size.height-20);
    closeButton.frame = CGRectMake(self.frame.size.width-30, 10, 30, 30);

}


-(IBAction)close:(id)sender
{
    if (delegate && [delegate respondsToSelector:@selector(passValuesToField:)])
    {
        [delegate passValuesToField:selectedValues];
    }
    [delegate closePopUP];
}
-(void)closePopUpIfTouchOutSide
{
    if (delegate && [delegate respondsToSelector:@selector(passValuesToField:)])
    {
        [delegate passValuesToField:selectedValues];
    }
    [delegate closePopUP];
}
@end
