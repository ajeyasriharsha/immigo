//
//  ParternsPullableView.m
//  Immigo
//
//  Created by pradeep ISPV on 2/27/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "PartnersPullableView.h"
#import "partnersCell.h"
#import "Common.h"
////#import "KGModal.h"
//#import "RNBlurModalView.h"
#import <QuartzCore/QuartzCore.h>

@implementation PartnersPullableView
@synthesize partnersDelegate;

@synthesize partnerLandscapeView;


// custom intialization of parterners Tableview
-(void)customIntialization
{
    self.backgroundColor = [UIColor clearColor];
    {
//        self.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    }
    UIImage *parternsImg = [UIImage imageNamed:@"partners"];
    CGFloat  height = 0.0;
    height =self.frame.size.height-50;
//    if (self.frame.size.width == 320)
//    {
//        height =self.frame.size.height-50;
//    }else
//    {
//        height = self.frame.size.height-50;
//    }
//    
    UITableView *parternsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 30, self.frame.size.width, height)];
    parternsTableView.delegate = self;
    parternsTableView.dataSource = self;
    //parternsTableView.rowHeight = 90.0f;
    parternsTableView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [self addSubview:parternsTableView];
//    [parternsTableView setBackgroundColor:[UIColor darkGrayColor]];
    
    partnerLandscapeView = [[PartnersLandscapeView alloc] initWithFrame:CGRectMake(0, parternsImg.size.height, self.frame.size.width, height)];
    partnerLandscapeView.hidden = YES;
    [self addSubview:partnerLandscapeView];
}

-(IBAction)closeButtonTapped:(id)sender
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6];
	
    [UIView setAnimationDelegate: self];
    //    [UIView setAnimationDidStopSelector:
    //     @selector(animationFinished:finished:context:)];
    partnersWebView.hidden = closeButton.hidden =  YES;
	[partnersWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];

    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp
                           forView:partnersWebView cache:YES];
    [UIView commitAnimations];

}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        [self customIntialization];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *lbl = [[UILabel alloc] init];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:12.85f];
    lbl.textColor = UIColorFromRGB(0X124A7F);
    if (section ==1)
        lbl.text = NSLocalizedString(@"MADE_POSSIBLEBY_TITLE", "");//@"Made Possible By";
    else lbl.text = @"";

    return lbl;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 10.0f;//Partner logo alignment//JUHI
    }else if (section == 2)
        return 0.0f;
    return 20.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return 2;
    else if (section == 1)
        return 1;
    else
        return 1;
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone && (indexPath.row == 0 && indexPath.section == 2))
//    {
//        
//        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
//        
//        //----------------HERE WE SETUP FOR IPHONE 4/4s/iPod----------------------
//        
//        if(iOSDeviceScreenSize.height == 480)
//        {
//            return 90.0f;
//        }
//        else
//        {
//            return 130.0f;
//        }
//    }
//    switch (indexPath.section) {
//        case 0:
//            return 150;
//            break;
//            
//        default:
//            break;
//    }
//    if (indexPath.section == 0) {
//        if (indexPath.row == 0)
//                        {
//                            return 200;
//                        }
//else
//        return 90.0f;//Partner logo alignment//JUHI
//    }
//    switch (indexPath.section) {
//        case 0:
//        {
//            if (indexPath.row == 0)
//            {
//                return 200;
//            }
//            break;
//        }
//            default:
//            return 90;
//            break;
//    }
    
    return 90.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [cell setBackgroundColor:[UIColor redColor]];
//    cell.transform = CGAffineTransformMakeScale(0.8, 0.8);
//    [UIView animateWithDuration:0.5 animations:^{
//        cell.transform = CGAffineTransformIdentity;
//    }];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    partnersCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (!cell)
    {
        cell = [[partnersCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell" WithView:tableView];
    }
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
//    UIImage *image = [UIImage imageNamed:@"divider"];
//    UIImageView* separatorLineView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 99.0f, cell.frame.size.width, 1)];
//    separatorLineView.image =[image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
////    separatorLineView.backgroundColor = [UIColor clearColor]; // set color as you want.
//    [cell.contentView addSubview:separatorLineView];

    switch (indexPath.section) {
        case 0:
        {
            if (indexPath.row == 0)
            {
                cell.logoImg = [UIImage imageNamed:@"Lulac"];
//                separatorLineView.hidden = YES;
            }
            else if (indexPath.row == 1)
            {
                cell.logoImg = [UIImage imageNamed:@"immigrationAdv"];
//                separatorLineView.hidden = NO;
            }
            
        }
            break;
        case 1:
        {
            cell.logoImg = [UIImage imageNamed:@"powered"];
//            separatorLineView.hidden = NO;
        }
            break;
        case 2:
        {
                cell.logoImg = [UIImage imageNamed:@"verizon"];
//            separatorLineView.hidden = YES;
        }
            break;
        default:
            break;
    }
    
    return cell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        [_objects removeObjectAtIndex:indexPath.row];
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
//    }
//}

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"PARTNERS", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    if (partnersDelegate && [partnersDelegate respondsToSelector:@selector(stopAnimatingPartnersView)])
    {
        [partnersDelegate stopAnimatingPartnersView];
    }
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.6];
//	
//    [UIView setAnimationDelegate: self];
//    //    [UIView setAnimationDidStopSelector:
//    //     @selector(animationFinished:finished:context:)];
////    partnersWebView.hidden = closeButton.hidden =  NO;
//	
//    [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown
//                           forView:partnersWebView cache:YES];
//    [UIView commitAnimations];
    
    NSString *urlAddress = [NSString stringWithFormat:@"%@",[NSURL URLWithString:@"about:blank"]];
    
    switch (indexPath.section) {
        case 0:
        {
            if (indexPath.row == 0)
            {
                urlAddress = PARTNERS_NCLR_URL;
            }
            else if (indexPath.row == 1)
            {
                urlAddress = PARTNERS_IMMIGRATION_URL;
            }
            
        }
            break;
        case 1:
        {
            urlAddress = PARTNERS_VERIZON_URL;
        }
            break;
        case 2:
        {
            urlAddress = PARTNERS_PROBONO_URL;
        }
            break;
        default:
            break;
    }
    

    activityController.hidden = YES;
    MTPopupWindow *popup = [[MTPopupWindow alloc] init];
    popup.delegate = self;
    popup.fileName = urlAddress;
    [popup show];

    //Load the request in the UIWebView.
}
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [activityController startAnimating];
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked){
        if (![Common currentNetworkStatus])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"PARTNERS", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
            [alertView show];
            return NO;
        }
        
    }
    return YES;
    
    
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    //NSLog(@"error %@",[error debugDescription]);
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [activityController stopAnimating];
}
@end
