//
//  PlaceHolderField.m
//  Immigo
//
//  Created by pradeep ISPV on 3/24/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "PlaceHolderField.h"

@implementation PlaceHolderField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
//- (void) drawPlaceholderInRect:(CGRect)rect
//{
////    [[UIColor grayColor] setFill];
//    [self setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
//    [[self placeholder] drawInRect:CGRectMake(5, 5, rect.size.width, rect.size.height) withFont:[UIFont fontWithName:@"OpenSans-italic" size:14.32f]];
//    [self reloadInputViews];
//}
- (void)drawPlaceholderInRect:(CGRect)rect
{
    /*
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)
    {
        [self setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
        [[self placeholder] drawInRect:CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height) withFont:[UIFont fontWithName:@"OpenSans-italic" size:14.32f]];
    }
    else {
        
//        [[self placeholder] drawInRect:rect withFont:[UIFont fontWithName:@"OpenSans-italic" size:14.32f]];
    }
     */
    // Placeholder text color, the same like default
    UIColor *placeholderColor = [UIColor colorWithWhite:0.70 alpha:1];
    [placeholderColor setFill];
    
    // Get the size of placeholder text. We will use height to calculate frame Y position
    CGSize size = [self.placeholder sizeWithFont:self.font];
    
    // Vertically centered frame
    CGRect placeholderRect = CGRectMake(rect.origin.x, (rect.size.height - size.height)/2, rect.size.width, size.height);
    
    // Check if OS version is 7.0+ and draw placeholder a bit differently
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0) {
        
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.lineBreakMode = NSLineBreakByTruncatingTail;
        style.alignment = self.textAlignment;
        NSDictionary *attr = [NSDictionary dictionaryWithObjectsAndKeys:style,NSParagraphStyleAttributeName, [UIFont fontWithName:@"OpenSans-SemiboldItalic" size:14.32f], NSFontAttributeName, placeholderColor, NSForegroundColorAttributeName, nil];
        
        [self.placeholder drawInRect:placeholderRect withAttributes:attr];
        
        
    } else
    {
        [placeholderColor setFill];
        [self.placeholder drawInRect:placeholderRect withFont:self.font];
//        [self setValue:[UIFont fontWithName: @"OpenSans" size: 20] forKeyPath:@"_placeholderLabel.font"];
//        self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"placeholder" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"OpenSans-italic" size:14.32f]}];

//        self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Your Name" attributes:@{NSFontAttributeName:@"OpenSans-italic"}];
//        [self.placeholder drawInRect:placeholderRect
//                            withFont:self.font
//                       lineBreakMode:NSLineBreakByTruncatingTail
//                           alignment:self.textAlignment];
    }

}

@end
