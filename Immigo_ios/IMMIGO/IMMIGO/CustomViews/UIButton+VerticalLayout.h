//
//  UIButton+VerticalLayout.h
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 6/12/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (VerticalLayout)
- (void)centerVerticallyWithPadding:(float)padding;
- (void)centerVertically;
@end
