//
//  PartnersLandscapeView.m
//  IMMIGO
//
//  Created by pradeep ISPV on 7/3/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

/*
 
 verizon_landscpe
 immigAdv_landscape.png
 immigAdv_landscape@2x.png
 nclr_landscape.png
 nclr_landscape@2x.png
 powered_landscape.png
 powered_landscape@2x.png
 verizon_landscpe.png
 verizon_landscpe@2x.png
 
 */


#import "PartnersLandscapeView.h"
#import "Common.h"

@implementation PartnersLandscapeView

- (void)customIntializationWithFrame:(CGRect)frame
{
    self.clipsToBounds = YES;
    self.backgroundColor = [UIColor whiteColor];
    
    CGPoint cursor = CGPointMake(0, 0);
    CGSize size = CGSizeMake(self.frame.size.width, self.frame.size.height);
    
    UIButton *nclrButton = [UIButton  buttonWithType:UIButtonTypeCustom];
    nclrButton.frame = CGRectMake(cursor.x,cursor.y, size.width/2,size.height *0.40);
    [nclrButton setImage:[UIImage imageNamed:@"Lulac"] forState:UIControlStateNormal];
    [nclrButton addTarget:self action:@selector(nclrTapped:) forControlEvents:UIControlEventTouchUpInside];
    nclrButton.backgroundColor = [UIColor clearColor];
    [self addSubview:nclrButton];
    
    cursor.x += nclrButton.frame.size.width;
    
    UIButton *immigrationAdvButton = [UIButton  buttonWithType:UIButtonTypeCustom];
    immigrationAdvButton.frame = CGRectMake(cursor.x,cursor.y, size.width/2, size.height *0.40);
    [immigrationAdvButton setImage:[UIImage imageNamed:@"immigAdv_landscape"] forState:UIControlStateNormal];
    [immigrationAdvButton addTarget:self action:@selector(immigrationAdvButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    immigrationAdvButton.backgroundColor = [UIColor clearColor];
    [self addSubview:immigrationAdvButton];
    
    cursor.x = 0;
    cursor.y += nclrButton.frame.size.height;

    //Made Possible By
    UILabel *madePossibleByLbl = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, size.height*0.06)];
    madePossibleByLbl.text = NSLocalizedString(@"MADE_POSSIBLEBY_TITLE", @"");
    madePossibleByLbl.backgroundColor = [UIColor clearColor];
    madePossibleByLbl.textAlignment = NSTextAlignmentCenter;
    madePossibleByLbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:9.85f];
    madePossibleByLbl.textColor = UIColorFromRGB(UI_BLUE_TEXT_COLOR);
    [self addSubview:madePossibleByLbl];
    
    cursor.y +=madePossibleByLbl.frame.size.height;
    
    UIButton *verizonButton = [UIButton  buttonWithType:UIButtonTypeCustom];
    verizonButton.frame = CGRectMake(cursor.x,cursor.y, size.width/2, size.height *0.40);
//    verizonButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 15, 0);
    [verizonButton setImage:[UIImage imageNamed:@"verizon_landscpe"] forState:UIControlStateNormal];
    [verizonButton addTarget:self action:@selector(verizonButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    verizonButton.backgroundColor = [UIColor clearColor];
    [self addSubview:verizonButton];

    cursor.x += verizonButton.frame.size.width;

    UIButton *poweredByProbonoBtn = [UIButton  buttonWithType:UIButtonTypeCustom];
    poweredByProbonoBtn.frame = CGRectMake(cursor.x,cursor.y, size.width/2, size.height *0.40);
    [poweredByProbonoBtn setImage:[UIImage imageNamed:@"powered"] forState:UIControlStateNormal];
    [poweredByProbonoBtn addTarget:self action:@selector(poweredByProbonoBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    poweredByProbonoBtn.backgroundColor = [UIColor clearColor];
    [self addSubview:poweredByProbonoBtn];
    

}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        
        [self customIntializationWithFrame:frame];
    }
    return self;
}


-(IBAction)nclrTapped:(id)sender
{
    MTPopupWindow *popup = [[MTPopupWindow alloc] init];
    popup.delegate = self;
    popup.fileName = PARTNERS_NCLR_URL;
    [popup show];

}
-(IBAction)immigrationAdvButtonTapped:(id)sender
{
    MTPopupWindow *popup = [[MTPopupWindow alloc] init];
    popup.delegate = self;
    popup.fileName =PARTNERS_IMMIGRATION_URL;
    [popup show];
    
}
-(IBAction)verizonButtonTapped:(id)sender
{
    MTPopupWindow *popup = [[MTPopupWindow alloc] init];
    popup.delegate = self;
    popup.fileName = PARTNERS_VERIZON_URL;
    [popup show];
    
}
-(IBAction)poweredByProbonoBtnTapped:(id)sender
{
    MTPopupWindow *popup = [[MTPopupWindow alloc] init];
    popup.delegate = self;
    popup.fileName = PARTNERS_PROBONO_URL;
    [popup show];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
