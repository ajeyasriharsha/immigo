//
//  PartnersLandscapeView.h
//  IMMIGO
//
//  Created by pradeep ISPV on 7/3/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullableView.h"
#import "MTPopupWindow.h"

@interface PartnersLandscapeView : UIView <MTPopupWindowDelegate>

@end
