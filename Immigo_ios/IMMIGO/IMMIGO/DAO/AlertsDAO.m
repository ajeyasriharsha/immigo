//
//  AlertsDAO.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/2/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "AlertsDAO.h"
#import "AlertsModel.h"
#import "Author.h"
#import "Categories.h"
#import "Alerts.h"
#import "CoreDataBase.h"

@implementation AlertsDAO
+ (Alerts*)insertIfDoesNotExists:(NSDictionary *)responseDict
{
    /*
     
     Id: 25
     Headline: "H1"
     Link: "http://www.biol.ttu.edu/Biology%20Shared%20Files/Undergraduate%20Advising/doc2012/ZOOLOGY%20MAJOR%20CHECKLIST.pdf"
     Description: "Link to biology pdf"
     IsActive: true
     
     */
    if (!responseDict || [responseDict isEqual:[NSNull null]])
    {
        return nil;
    }
    
    NSInteger key = [[responseDict valueForKey:@"Id"] integerValue];
    
    Alerts *alert = [self objectWithID:key];
    if (!alert) {
        alert = [NSEntityDescription
                      insertNewObjectForEntityForName:TBL_ALERTS
                      inManagedObjectContext:[CoreDataBase managedObjectContext]];
        alert.key = key;
    }
    alert.isLatest = YES;
    alert.alertTitle =( [responseDict valueForKey:@"Headline"]&& [responseDict valueForKey:@"Headline"] != [NSNull null])?[responseDict valueForKey:@"Headline"]:@"";
    alert.link = ([responseDict valueForKey:@"Link"] && [responseDict valueForKey:@"Link"] != [NSNull null])?[responseDict valueForKey:@"Link"]:@"";
    alert.alertDiscription = ([responseDict valueForKey:@"Description"] && [responseDict valueForKey:@"Description"] != [NSNull null])?[responseDict valueForKey:@"Description"]:@"";
    alert.orderNo = [[responseDict valueForKey:@"OrderNo"] isEqual:[NSNull null]] ? 0:[[responseDict valueForKey:@"OrderNo"] integerValue];

    [CoreDataBase saveContext];
    return alert;
}
+ (Alerts*)objectWithID:(int)key
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_ALERTS];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %i", key];
    [fetch setPredicate:predicate];
    
    [fetch setReturnsObjectsAsFaults:NO];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count]) {
        return [fetchedItems objectAtIndex:0];
    }
    return nil;
}
+ (NSArray*)allObjects
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_ALERTS];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
}

+(void)markAsOldRecords
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_ALERTS];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(Alerts *alert in fetchedItems)
        {
            alert.isLatest=false;
        }
    }
    [CoreDataBase saveContext];
}

+(void)deleteOldRecords
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_ALERTS];
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isLatest == 0"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isLatest == %@", [NSNumber numberWithBool:NO]];
    [fetch setPredicate:predicate];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(Alerts *alert in fetchedItems)
        {
            //            if(!state.isLatest)
            {
                [[CoreDataBase managedObjectContext] deleteObject:alert];
            }
        }
    }
    [CoreDataBase saveContext];
}

@end
