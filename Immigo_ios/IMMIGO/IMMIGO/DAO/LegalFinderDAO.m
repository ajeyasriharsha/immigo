//
//  LegalFinderDAO.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/9/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "LegalFinderDAO.h"
#import "Categories.h"
#import "FinderContact.h"
#import "FinderAddress.h"
#import "LegalFinderModel.h"
#import "NSString+HTML.h"

@implementation LegalFinderDAO
+ (LegalFinder*)insertIfDoesNotExists:(NSDictionary*)responseDict
{
    if (!responseDict || [responseDict isEqual:[NSNull null]])
    {
        return nil;
    }
    LegalFinder *finder = [[LegalFinder alloc] init];
    FinderAddress *address = [[FinderAddress alloc] init];
    FinderContact *contact = [[FinderContact alloc] init];
    Author *author = [[Author alloc] init];
    
    finder.finderName = [[responseDict valueForKey:@"title"] valueForKey:@"text"] ? [[responseDict valueForKey:@"title"] valueForKey:@"text"]:@"";
    finder.content = [[responseDict valueForKey:@"content"] valueForKey:@"text"]? [[[responseDict valueForKey:@"content"] valueForKey:@"text"] kv_decodeHTMLCharacterEntities]:@"";
    
    address.street = [[[responseDict valueForKey:@"pbn:address"] valueForKey:@"street"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"pbn:address"] valueForKey:@"street"] valueForKey:@"text"]:@"";
    address.city = [[[responseDict valueForKey:@"pbn:address"] valueForKey:@"city"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"pbn:address"] valueForKey:@"city"] valueForKey:@"text"]:@"";
    address.state = [[[responseDict valueForKey:@"pbn:address"] valueForKey:@"state"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"pbn:address"] valueForKey:@"state"] valueForKey:@"text"]:@"";
    address.zipcode = [NSString stringWithFormat:@"%@",[[[responseDict valueForKey:@"pbn:address"] valueForKey:@"zipcode"] valueForKey:@"text"]]; //? [[[responseDict valueForKey:@"pbn:address"] valueForKey:@"zipcode"] valueForKey:@"text"]:@"";
    //NSLog(@"read it %@",address.zipcode);
    address.latitude = [[[responseDict valueForKey:@"pbn:address"] valueForKey:@"latitude"] valueForKey:@"text"];   //[[[[responseDict valueForKey:@"pbn:address"] valueForKey:@"latitude"] valueForKey:@"text"] unsi]];
    address.longitude = [[[responseDict valueForKey:@"pbn:address"] valueForKey:@"longitude"] valueForKey:@"text"];
    
    address.coordinate = CLLocationCoordinate2DMake([address.latitude doubleValue],[address.longitude doubleValue]);
    finder.finderAddress = address;
    
    author.name = [[[responseDict valueForKey:@"author"] valueForKey:@"name"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"author"] valueForKey:@"name"] valueForKey:@"text"]:@"";
    author.uri = [[[responseDict valueForKey:@"author"] valueForKey:@"uri"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"author"] valueForKey:@"uri"] valueForKey:@"text"]:@"";
    finder.author = author;

    contact.website = [[[responseDict valueForKey:@"pbn:contact"] valueForKey:@"website"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"pbn:contact"] valueForKey:@"website"] valueForKey:@"text"]:@"";
    contact.email = [[[responseDict valueForKey:@"pbn:contact"] valueForKey:@"email"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"pbn:contact"] valueForKey:@"email"] valueForKey:@"text"]:@"";
    contact.phone = [[[responseDict valueForKey:@"pbn:contact"] valueForKey:@"phone"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"pbn:contact"] valueForKey:@"phone"] valueForKey:@"text"]:@"";
//    contact.tollFreePhone = [[[responseDict valueForKey:@"pbn:contact"] valueForKey:@"tollfreephone"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"pbn:contact"] valueForKey:@"tollfreephone"] valueForKey:@"text"]:@"";
    //NSLog(@"Phone NUm in :%@",contact.phone);
    finder.contact = contact;

    //NSLog(@"finder name %@",finder.finderName);

    [[LegalFinderModel sharedDataSource] insertOrg:finder];
    return finder;
    
}
+ (LegalFinder*)objectWithID:(int)key
{
    LegalFinder *finder = nil;
    
    return finder;
}
+ (NSArray*)allObjects
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    return array;
}



@end
