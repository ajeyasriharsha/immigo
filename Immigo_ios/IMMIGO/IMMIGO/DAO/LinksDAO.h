//
//  LinksDAO.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/28/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Links.h"
@interface LinksDAO : NSObject
+ (Links*)insertIfDoesNotExists:(NSDictionary*)responseDict WithSubTopic:(SubTopic *)subTopic;
+ (Links*)objectWithID:(int)key AndSubTopicID:(int) subTopicKey;
+ (NSArray*)allObjects;

+(void)markAsOldRecords;
+(void)deleteOldRecords;

@end
