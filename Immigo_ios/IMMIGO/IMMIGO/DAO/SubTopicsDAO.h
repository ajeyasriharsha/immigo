//
//  SubTopicsDAO.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/28/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SubTopic.h"
#import "Links.h"
#import "CheckListItems.h"

@interface SubTopicsDAO : NSObject
+ (SubTopic*)insertIfDoesNotExists:(NSDictionary*)responseDict;
+ (SubTopic*)objectWithID:(int)key;
+ (NSArray*)allObjects;

+ (NSArray*)fetchSubTopicsForTopicId:(int)key;
+(void)markAsOldRecords;
+(void)deleteOldRecords;

@end
