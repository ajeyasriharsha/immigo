//
//  AlertsDAO.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/2/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Alerts.h"

@interface AlertsDAO : NSObject
+ (Alerts*)insertIfDoesNotExists:(NSDictionary *)responseDict;
+ (Alerts*)objectWithID:(int)key;
+ (NSArray*)allObjects;

+(void)markAsOldRecords;
+(void)deleteOldRecords;

@end
