//
//  SubTopicsDAO.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/28/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "SubTopicsDAO.h"
#import "Common.h"
#import "CoreDataBase.h"
#import "Categories.h"
#import "LinksDAO.h"
#import "CheckListItemsDAO.h"
/*
 
 @property (nonatomic, retain) NSNumber * subTopicId;
 @property (nonatomic, retain) NSString * subtopicTitle;
 @property (nonatomic, retain) NSString * textContent;
 @property (nonatomic, retain) Topic *topic;
 @property (nonatomic, retain) NSSet *links;
 @property (nonatomic, retain) NSSet *checkListItems;

 
 */
@implementation SubTopicsDAO
+ (SubTopic*)insertIfDoesNotExists:(NSDictionary*)responseDict
{
    if (!responseDict || [responseDict isEqual:[NSNull null]])
    {
        return nil;
    }
    
    NSInteger key = [[responseDict valueForKey:@"SubTopicId"] integerValue];
    
    SubTopic *subTopic = [self objectWithID:key];
    if (!subTopic) {
        subTopic = [NSEntityDescription
                    insertNewObjectForEntityForName:TBL_SUBTOPICS
                 inManagedObjectContext:[CoreDataBase managedObjectContext]];
        subTopic.subTopicId = key;
    }
    subTopic.isLatest = YES;
    subTopic.heading = ([responseDict valueForKey:@"ContentHeading"] == [NSNull null]) ? nil:[responseDict valueForKey:@"ContentHeading"];
    subTopic.subtopicTitle = [responseDict valueForKey:@"SubTopicTitle"];
    subTopic.contentType = [responseDict valueForKey:@"ContentTypeName"];
    subTopic.contentTypeId = [[responseDict valueForKey:@"ContentTypeId"] integerValue];
    subTopic.textContent = [responseDict valueForKey:@"TextContent"]?[responseDict valueForKey:@"TextContent"]:@"";
    NSArray *linksList = [responseDict getObjectForKey:@"LinkListItems"];
    if ([linksList count]>0)
    {
        for (NSDictionary *dictCourse in linksList)
        {
            Links *currentLink=[LinksDAO insertIfDoesNotExists:dictCourse WithSubTopic:subTopic];
            if(currentLink)
            {
                [subTopic addLinksObject:currentLink];
            }
        }
    }
    
    NSArray *listCheckListItems = [responseDict getObjectForKey:@"CheckListItems"];
    if ([listCheckListItems count]>0)
    {
        for (NSDictionary *dictCourse in listCheckListItems)
        {
            CheckListItems *currentCheckList =[CheckListItemsDAO insertIfDoesNotExists:dictCourse WithSubTopic:subTopic];
            if(currentCheckList)
            {
                [subTopic addCheckListItemsObject:currentCheckList];
            }
        }
    }
    [CoreDataBase saveContext];
    return subTopic;

}
+ (SubTopic*)objectWithID:(int)key
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_SUBTOPICS];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"subTopicId == %i", key];
    [fetch setPredicate:predicate];
    
    [fetch setReturnsObjectsAsFaults:NO];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count]) {
        return [fetchedItems objectAtIndex:0];
    }
    return nil;

}
+ (NSArray*)fetchSubTopicsForTopicId:(int)key
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_SUBTOPICS];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"topicId == %i", key];
    [fetch setPredicate:predicate];

    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
}

+ (NSArray*)allObjects
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_SUBTOPICS];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
}

+(void)markAsOldRecords
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_SUBTOPICS];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(SubTopic *topic in fetchedItems)
        {
            topic.isLatest=false;
        }
    }
    [CoreDataBase saveContext];
}

+(void)deleteOldRecords
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_SUBTOPICS];
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isLatest == 0"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isLatest == %@", [NSNumber numberWithBool:NO]];
    [fetch setPredicate:predicate];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(SubTopic *topic in fetchedItems)
        {
            //            if(!state.isLatest)
            {
                [[CoreDataBase managedObjectContext] deleteObject:topic];
            }
        }
    }
    [CoreDataBase saveContext];
}


@end
