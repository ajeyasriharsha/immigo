//
//  PollQuestionsDAO.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/28/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "PollQuestionsDAO.h"
#import "Categories.h"
#import "Common.h"
#import "CoreDataBase.h"

@implementation PollQuestionsDAO
+ (PollQuestions*)insertIfDoesNotExists:(NSDictionary*)responseDict
{
    //NSLog(@"response dict %@",responseDict);
    int numberOfOptions = 0;
    
    if (!responseDict || [responseDict isEqual:[NSNull null]])
    {
        return nil;
    }
    
    NSInteger key = [[responseDict valueForKey:@"Id"] integerValue];
    
    PollQuestions *pollQuestion = [self objectWithID:key];
    if (!pollQuestion) {
        pollQuestion = [NSEntityDescription
                     insertNewObjectForEntityForName:TBL_POLLQUESTIONS
                     inManagedObjectContext:[CoreDataBase managedObjectContext]];
        pollQuestion.key = key;
    }
    pollQuestion.isLatest = YES;
    pollQuestion.question = [responseDict valueForKey:@"Question"] ? [responseDict valueForKey:@"Question"] :@"";
    
    if ([responseDict valueForKey:@"AnswerOne"]!= [NSNull null])
    {
        pollQuestion.answerOne = [responseDict valueForKey:@"AnswerOne"];
        numberOfOptions++;
    }
    if ([responseDict valueForKey:@"AnswerTwo"] != [NSNull null])
    {
        pollQuestion.answerTwo = [responseDict valueForKey:@"AnswerTwo"];
        numberOfOptions++;
    }
    if ([responseDict valueForKey:@"AnswerThree"]!= [NSNull null])
    {
        pollQuestion.answerThree = [responseDict valueForKey:@"AnswerThree"];
        numberOfOptions++;
    }
    if ([responseDict valueForKey:@"AnswerFour"]!= [NSNull null])
    {
        pollQuestion.answerFour = [responseDict valueForKey:@"AnswerFour"];
        numberOfOptions++;
    }
    if ([responseDict valueForKey:@"AnswerFive"] != [NSNull null])
    {
        pollQuestion.answerFive = [responseDict valueForKey:@"AnswerFive"];
        numberOfOptions++;
    }
    if ([responseDict valueForKey:@"AnswerSix"] != [NSNull null])
    {
        pollQuestion.answerSix = [responseDict valueForKey:@"AnswerSix"];
        numberOfOptions++;
    }
    pollQuestion.optionsCount = numberOfOptions;
    
    pollQuestion.answerOnePercentage = ([responseDict valueForKey:@"AnswerOnePercentage"] != [NSNull null]) ? [[responseDict valueForKey:@"AnswerOnePercentage"] intValue]:0;
    pollQuestion.answerTwoPercentage = ([responseDict valueForKey:@"AnswerTwoPercentage"] != [NSNull null]) ?[[responseDict valueForKey:@"AnswerTwoPercentage"] intValue]:0;
    pollQuestion.answerThreePercentage = ([responseDict valueForKey:@"AnswerThreePercentage"] != [NSNull null]) ?[[responseDict valueForKey:@"AnswerThreePercentage"] intValue]:0;
    pollQuestion.answerFourPercentage = ([responseDict valueForKey:@"AnswerFourPercentage"] != [NSNull null]) ?[[responseDict valueForKey:@"AnswerFourPercentage"] intValue]:0;
    pollQuestion.answerFivePercentage = ([responseDict valueForKey:@"AnswerFivePercentage"] != [NSNull null]) ?[[responseDict valueForKey:@"AnswerFivePercentage"] intValue]:0;
    pollQuestion.answerSixPercentage = ([responseDict valueForKey:@"AnswerSixPercentage"] != [NSNull null]) ?[[responseDict valueForKey:@"AnswerSixPercentage"] intValue]:0;

    pollQuestion.answerStatus = [responseDict valueForKey:@"AnswerStatus"];
    pollQuestion.answerId = [[responseDict valueForKey:@"AnswerId"] isEqual:[NSNull null]] ? 0:[[responseDict valueForKey:@"AnswerId"] integerValue];

    pollQuestion.orderNo = [[responseDict valueForKey:@"OrderNo"] isEqual:[NSNull null]] ? 0:[[responseDict valueForKey:@"OrderNo"] integerValue];

    [CoreDataBase saveContext];
    return pollQuestion;
    
}
+ (PollQuestions*)objectWithID:(int)key
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_POLLQUESTIONS];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %i", key];
    [fetch setPredicate:predicate];
    
    [fetch setReturnsObjectsAsFaults:NO];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count]) {
        return [fetchedItems objectAtIndex:0];
    }
    
    return nil;
    
}
+ (NSArray*)allObjects
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_POLLQUESTIONS];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
}

+(void)markAsOldRecords
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_POLLQUESTIONS];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(PollQuestions *question in fetchedItems)
        {
            question.isLatest=false;
        }
    }
    [CoreDataBase saveContext];
}

+(void)deleteOldRecords
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_POLLQUESTIONS];
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isLatest == 0"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isLatest == %@", [NSNumber numberWithBool:NO]];
    [fetch setPredicate:predicate];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(PollQuestions *question in fetchedItems)
        {
            //            if(!state.isLatest)
            {
                [[CoreDataBase managedObjectContext] deleteObject:question];
            }
        }
    }
    [CoreDataBase saveContext];
}


@end
