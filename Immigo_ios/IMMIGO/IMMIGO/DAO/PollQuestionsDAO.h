//
//  PollQuestionsDAO.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/28/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PollQuestions.h"

@interface PollQuestionsDAO : NSObject
+ (PollQuestions*)insertIfDoesNotExists:(NSDictionary*)responseDict;
+ (PollQuestions*)objectWithID:(int)key;
+ (NSArray*)allObjects;


+(void)markAsOldRecords;
+(void)deleteOldRecords;

@end
