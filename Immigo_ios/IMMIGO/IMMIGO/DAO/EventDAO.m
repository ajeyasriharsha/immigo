//
//  EventDAO.m
//  IMMIGO
//
//  Created by pradeep ISPV on 3/27/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "EventDAO.h"
#import "EventsModel.h"
#import "Contact.h"
#import "Location.h"
#import "Categories.h"
#import "NSString+HTML.h"
#import "CoreDataBase.h"

@implementation EventDAO
+ (NSString *)linkFromArray:(NSArray *)array
{
//    NSString *sourceLink = nil;
    NSString *alternateLink = nil;
//    NSString *selfLink = nil;
    
    for (NSDictionary*dict in array)
    {
        NSString *key = [dict valueForKey:@"rel"];
//        if ([key isEqualToString:@"source"])
//        {
//            sourceLink = [dict valueForKey:@"href"];
//        }else
        if ([key isEqualToString:@"alternate"])
        {
            alternateLink = [dict valueForKey:@"href"];
        }
//        }else if ([key isEqualToString:@"self"])
//        {
//            selfLink = [dict valueForKey:@"href"];
//        }
    }
//    if (selfLink)
//    {
//        return selfLink;
//    }else if (sourceLink)
//    {
//        return sourceLink;
//    }else if (alternateLink)
//    {
        return alternateLink;
//    }
    
    return nil;
}

+ (Event*)insertIfDoesNotExists:(NSDictionary*)responseDict
{
    if (!responseDict || [responseDict isEqual:[NSNull null]])
    {
        return nil;
    }

    NSInteger key = [[[[[responseDict valueForKey:@"id"] valueForKey:@"text"] componentsSeparatedByString:@":"] objectAtIndex:2] integerValue];//[[responseDict valueForKey:@"Id"] integerValue];

    Event *event = [self objectWithID:key];
    if (!event)
    {
        event = [NSEntityDescription
                insertNewObjectForEntityForName:TBL_EVENTS
                inManagedObjectContext:[CoreDataBase managedObjectContext]];
        event.key = key;
    }
    event.isLatest = YES;
    event.title = [[responseDict valueForKey:@"title"] valueForKey:@"text"] ? [[responseDict valueForKey:@"title"] valueForKey:@"text"]:@"";
    event.content= [[responseDict valueForKey:@"content"] valueForKey:@"text"]? [[[responseDict valueForKey:@"content"] valueForKey:@"text"] kv_decodeHTMLCharacterEntities]:@"";
//    //NSLog(@"calss Name %@",[[responseDict valueForKey:@"link"] class]);
    if ([[responseDict valueForKey:@"link"] isKindOfClass:[NSArray class]])
    {
        NSArray *linkArray = [responseDict valueForKey:@"link"];
        
        for (int i =0; i<linkArray.count; i++)
        {
            if ([[[linkArray objectAtIndex:i] valueForKey:@"rel"] isEqualToString:@"alternate"])
            {
                event.link = [[linkArray objectAtIndex:i] valueForKey:@"href"];
            }
        }
    }else
    {
        event.link =[[responseDict valueForKey:@"link"] valueForKey:@"href"];

    }
    
    event.startDate= [[responseDict valueForKey:@"pbn:startDate"] valueForKey:@"text"];// displayDateFormat];
    event.endDate=[[responseDict valueForKey:@"pbn:endDate"] valueForKey:@"text"] ;//displayDateFormat];
    
//    event.dailyStartTime = [[[[[responseDict valueForKey:@"pbn:dailyStartTime"] valueForKey:@"text"] trimmedString] substringToIndex:5] get12HrStringFrom];//[NSString stringWithFormat:@"%.02f",startTime];
//    event.dailyEndTime = [[[[[responseDict valueForKey:@"pbn:dailyEndTime"] valueForKey:@"text"] trimmedString] substringToIndex:5] get12HrStringFrom];//[NSString stringWithFormat:@"%.02f",endTime];

    event.dailyStartTime = [[[[responseDict valueForKey:@"pbn:dailyStartTime"] valueForKey:@"text"] trimmedString] substringToIndex:5];//[NSString stringWithFormat:@"%.02f",startTime];
    event.dailyEndTime = [[[[responseDict valueForKey:@"pbn:dailyEndTime"] valueForKey:@"text"] trimmedString] substringToIndex:5];//[NSString stringWithFormat:@"%.02f",endTime];

    event.contactName = [[[responseDict valueForKey:@"pbn:contact"] valueForKey:@"name"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"pbn:contact"] valueForKey:@"name"] valueForKey:@"text"]:@"";
    event.contactOrganization = [[[responseDict valueForKey:@"pbn:contact"] valueForKey:@"organization"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"pbn:contact"] valueForKey:@"organization"] valueForKey:@"text"]:@"";
    event.contactEmail = [[[responseDict valueForKey:@"pbn:contact"] valueForKey:@"email"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"pbn:contact"] valueForKey:@"email"] valueForKey:@"text"]:@"";
    event.contactPhone = [[[responseDict valueForKey:@"pbn:contact"] valueForKey:@"phone"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"pbn:contact"] valueForKey:@"phone"] valueForKey:@"text"]:@"";
    //NSLog(@"Phone Number:%@",event.contactPhone);
    
    event.locationCountry = [[[responseDict valueForKey:@"pbn:location"] valueForKey:@"country"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"pbn:location"] valueForKey:@"country"] valueForKey:@"text"]:@"";
    event.locationCountryId = [[[responseDict valueForKey:@"pbn:location"] valueForKey:@"country"] valueForKey:@"id"] ? [[[responseDict valueForKey:@"pbn:location"] valueForKey:@"country"] valueForKey:@"id"] :@"";
    event.locationState = [[[responseDict valueForKey:@"pbn:location"] valueForKey:@"state"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"pbn:location"] valueForKey:@"state"] valueForKey:@"text"]:@"";
    event.locationStateId = [[[responseDict valueForKey:@"pbn:location"] valueForKey:@"state"] valueForKey:@"id"]? [[[responseDict valueForKey:@"pbn:location"] valueForKey:@"state"] valueForKey:@"id"]:@"";
    event.locationCity = [[[responseDict valueForKey:@"pbn:location"] valueForKey:@"city"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"pbn:location"] valueForKey:@"city"] valueForKey:@"text"]:@"";
    event.locationStreetAddress = [[[responseDict valueForKey:@"pbn:location"] valueForKey:@"streetaddress"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"pbn:location"] valueForKey:@"streetaddress"] valueForKey:@"text"]:@"";
    event.locationStreetAddress2 = [[[responseDict valueForKey:@"pbn:location"] valueForKey:@"streetaddress2"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"pbn:location"] valueForKey:@"streetaddress2"] valueForKey:@"text"]:@"";

    [CoreDataBase saveContext];
    return event;

}
+ (Event*)objectWithID:(int)key
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_EVENTS];
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %i && ANY subTopics.subTopicId == %i", key];
    //    [fetch setPredicate:predicate];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %i", key];
    [fetch setPredicate:predicate];
    
    [fetch setReturnsObjectsAsFaults:NO];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count]) {
        return [fetchedItems objectAtIndex:0];
    }
    return nil;
    
}
+ (NSArray*)allObjects
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_EVENTS];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
}

+(void)markAsOldRecords
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_EVENTS];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(Event *event in fetchedItems)
        {
            event.isLatest=false;
        }
    }
    [CoreDataBase saveContext];
}

+(void)deleteOldRecords
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_EVENTS];
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isLatest == 0"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isLatest == %@", [NSNumber numberWithBool:NO]];
    [fetch setPredicate:predicate];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(Event *event in fetchedItems)
        {
            //            if(!state.isLatest)
            {
                [[CoreDataBase managedObjectContext] deleteObject:event];
            }
        }
    }
    [CoreDataBase saveContext];
}

@end
