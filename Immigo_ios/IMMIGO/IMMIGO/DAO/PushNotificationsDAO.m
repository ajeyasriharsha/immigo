//
//  PushNotificationsDAO.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/14/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "PushNotificationsDAO.h"
#import "CoreDataBase.h"
#import "Common.h"
#import "Categories.h"

@implementation PushNotificationsDAO
+ (PushNotifications*)insertIfDoesNotExistsWithStatus:(BOOL)isEnabled
{
    
    NSInteger key = 1;
    
    PushNotifications *pushNotification = [self objectWithID:key];
    if (!pushNotification) {
        pushNotification = [NSEntityDescription
                        insertNewObjectForEntityForName:TBL_PUSHNOTIFICATIONS
                        inManagedObjectContext:[CoreDataBase managedObjectContext]];
        pushNotification.key = key;
    }
    pushNotification.isEnabled = isEnabled;
    
    //NSLog(@"in databse %@",isEnabled? @"YES": @"NO");
    [CoreDataBase saveContext];
    return pushNotification;
    
}
+ (PushNotifications*)objectWithID:(int)key
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_PUSHNOTIFICATIONS];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %i", key];
    [fetch setPredicate:predicate];
    
    [fetch setReturnsObjectsAsFaults:NO];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count]) {
        return [fetchedItems objectAtIndex:0];
    }
    
    return nil;
    
}
+(void)updateStatusWith:(BOOL)status
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_PUSHNOTIFICATIONS];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %i",1];
    [fetch setPredicate:predicate];

    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(PushNotifications *notification in fetchedItems)
        {
            notification.isEnabled = status;
        }
    }
    [CoreDataBase saveContext];
}

@end
