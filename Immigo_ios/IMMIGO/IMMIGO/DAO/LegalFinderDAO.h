//
//  LegalFinderDAO.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/9/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LegalFinder.h"

@interface LegalFinderDAO : NSObject
+ (LegalFinder*)insertIfDoesNotExists:(NSDictionary*)responseDict;
+ (LegalFinder*)objectWithID:(int)key;
+ (NSArray*)allObjects;


@end
