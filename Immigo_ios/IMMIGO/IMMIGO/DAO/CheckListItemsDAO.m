//
//  CheckListItemsDAO.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/28/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "CheckListItemsDAO.h"
#import "CoreDataBase.h"
#import "Categories.h"
#import "Common.h"
#import "SubTopic.h"

@implementation CheckListItemsDAO
+ (CheckListItems*)insertIfDoesNotExists:(NSDictionary*)responseDict WithSubTopic:(SubTopic *)subTopic
{
    if (!responseDict || [responseDict isEqual:[NSNull null]])
    {
        return nil;
    }
    
    NSInteger key = [[responseDict valueForKey:@"Id"] integerValue];
    
    CheckListItems *checklist = [self objectWithCheckListKey:(int)key AndSubTopicID:subTopic.subTopicId];
    if (!checklist) {
        checklist = [NSEntityDescription
                insertNewObjectForEntityForName:TBL_CHECKLISTITEMS
                inManagedObjectContext:[CoreDataBase managedObjectContext]];
        checklist.key = (int)key;
        checklist.isChecked = NO;
    }
    checklist.isLatest = YES;
    checklist.title = [responseDict valueForKey:@"Text"] ? [responseDict valueForKey:@"Text"] :@"";
    
    [CoreDataBase saveContext];
    return checklist;
    
}
+ (CheckListItems*)objectWithCheckListKey:(int)key AndSubTopicID:(int) subTopicKey
{
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_CHECKLISTITEMS];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %i && subTopics.subTopicId == %i", key,subTopicKey];
    [fetch setPredicate:predicate];
    
    [fetch setReturnsObjectsAsFaults:NO];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count]) {
        return [fetchedItems objectAtIndex:0];
    }

    return nil;
    
}
+ (NSArray*)allObjects
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_CHECKLISTITEMS];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
}


+(void)selectTheCheckListItemWithKey:(int)key AndSubTopicID:(int) subTopicKey IfSelected:(BOOL)selected
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_CHECKLISTITEMS];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %i && ANY subTopics.subTopicId == %i", key,subTopicKey];
    [fetch setPredicate:predicate];
    
    [fetch setReturnsObjectsAsFaults:NO];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    CheckListItems *item = [fetchedItems objectAtIndex:0];
    item.isChecked = selected;
    [CoreDataBase saveContext];

}
+(void)unSelectTheCheckListItemWithKey:(int)key AndSubTopicID:(int) subTopicKey
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_CHECKLISTITEMS];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %i && ANY subTopics.subTopicId == %i", key,subTopicKey];
    [fetch setPredicate:predicate];
    
    [fetch setReturnsObjectsAsFaults:NO];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    CheckListItems *item = [fetchedItems objectAtIndex:0];
    item.isChecked = NO;
    [CoreDataBase saveContext];
    
}

+(void)markAsOldRecords
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_CHECKLISTITEMS];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(CheckListItems *link in fetchedItems)
        {
            link.isLatest=false;
        }
    }
    [CoreDataBase saveContext];
}

+(void)deleteOldRecords
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_CHECKLISTITEMS];
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isLatest == 0"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isLatest == %@", [NSNumber numberWithBool:NO]];
    [fetch setPredicate:predicate];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(CheckListItems *link in fetchedItems)
        {
            //            if(!state.isLatest)
            {
                [[CoreDataBase managedObjectContext] deleteObject:link];
            }
        }
    }
    [CoreDataBase saveContext];
}

@end
