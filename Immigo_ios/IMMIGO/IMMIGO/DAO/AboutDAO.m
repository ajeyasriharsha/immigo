//
//  AboutDAO.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/2/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "AboutDAO.h"

@implementation AboutDAO
+ (NSString*)insertAboutIfDoesNotExists:(NSArray*)responseDict
{
    NSString *aboutContent=nil;
    
    aboutContent = [[responseDict objectAtIndex:0]valueForKey:@"BodyContent"];
    //NSLog(@"aboutString %@",aboutContent);
    
    [[AboutUsModel sharedDataSource] aboutUsString:aboutContent];
    return aboutContent;
}
+ (NSString*)insertPrivacyIfDoesNotExists:(NSArray*)responseDict
{
    NSString *privacyContent=nil;
    
    privacyContent = [[responseDict objectAtIndex:0] valueForKey:@"BodyContent"];
    //NSLog(@"aboutString %@",privacyContent);
    
    [[PrivacyModel sharedDataSource]privacyString:privacyContent];
    return privacyContent;

}
+ (NSString*)insertTermsIfDoesNotExists:(NSArray*)responseDict
{
    NSString *termsContent=nil;
    termsContent = [[responseDict objectAtIndex:0] valueForKey:@"BodyContent"];
    //NSLog(@"aboutString %@",termsContent);
    
    [[TermsModel sharedDataSource]termsString:termsContent];

    return termsContent;

}

@end
