//
//  CheckListItemsDAO.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/28/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CheckListItems.h"
@interface CheckListItemsDAO : NSObject
+ (CheckListItems*)insertIfDoesNotExists:(NSDictionary*)responseDict WithSubTopic:(SubTopic *)subTopic;
//+ (CheckListItems*)objectWithID:(int)key;
+ (NSArray*)allObjects;

+ (CheckListItems*)objectWithCheckListKey:(int)key AndSubTopicID:(int) subTopicKey;
+(void)selectTheCheckListItemWithKey:(int)key AndSubTopicID:(int) subTopicKey IfSelected:(BOOL)selected;
//+(void)unSelectTheCheckListItemWithKey:(int)key AndSubTopicID:(int) subTopicKey;


+(void)markAsOldRecords;
+(void)deleteOldRecords;

@end
