//
//  DisclaimerDAO.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/21/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "DisclaimerDAO.h"
#import "CoreDataBase.h"
#import "Categories.h"

@implementation DisclaimerDAO
+ (Disclaimer*)insertIfDoesNotExists:(NSArray *)responseDict
{
    
    /*
     
     NSString *termsContent=nil;
     termsContent = [[responseDict objectAtIndex:0] valueForKey:@"BodyContent"];
     //NSLog(@"aboutString %@",termsContent);
     
     [[TermsModel sharedDataSource]termsString:termsContent];
     
     return termsContent;

     */
    if (!responseDict || [responseDict isEqual:[NSNull null]])
    {
        return nil;
    }
    
    NSInteger key = [[[responseDict objectAtIndex:0] valueForKey:@"Id"] integerValue];
    
    Disclaimer *disclaimer = [self objectWithID:key];
    if (!disclaimer) {
        disclaimer = [NSEntityDescription
                  insertNewObjectForEntityForName:TBL_DISCLAIMER
                  inManagedObjectContext:[CoreDataBase managedObjectContext]];
        disclaimer.key = key;
    }
    
    disclaimer.content = [[responseDict objectAtIndex:0] valueForKey:@"PageContent"];//==[NSNull null] ? @"" :[[responseDict objectAtIndex:0] valueForKey:@"PageContent"];
    [CoreDataBase saveContext];
    return disclaimer;
}
+ (Disclaimer*)objectWithID:(int)key
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_DISCLAIMER];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %i", key];
    [fetch setPredicate:predicate];
    
    [fetch setReturnsObjectsAsFaults:NO];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count]) {
        return [fetchedItems objectAtIndex:0];
    }
    return nil;

}
+ (NSArray*)allObjects
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_DISCLAIMER];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;

}

@end
