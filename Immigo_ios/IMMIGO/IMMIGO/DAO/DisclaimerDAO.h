//
//  DisclaimerDAO.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/21/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Disclaimer.h"
@interface DisclaimerDAO : NSObject
+ (Disclaimer*)insertIfDoesNotExists:(NSArray *)responseDict;
+ (Disclaimer*)objectWithID:(int)key;
+ (NSArray*)allObjects;

@end
