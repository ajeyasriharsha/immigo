//
//  AboutImmigoDAO.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/21/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "AboutImmigoDAO.h"
#import "CoreDataBase.h"
#import "Categories.h"

@implementation AboutImmigoDAO
+ (About*)insertIfDoesNotExists:(NSArray *)responseDict
{
    
    /*
     
     NSString *termsContent=nil;
     termsContent = [[responseDict objectAtIndex:0] valueForKey:@"BodyContent"];
     //NSLog(@"aboutString %@",termsContent);
     
     [[TermsModel sharedDataSource]termsString:termsContent];
     
     return termsContent;
     
     */
    if (!responseDict || [responseDict isEqual:[NSNull null]])
    {
        return nil;
    }
    
    NSInteger key = [[[responseDict objectAtIndex:0] valueForKey:@"Id"] integerValue];
    
    About *about = [self objectWithID:key];
    if (!about) {
        about = [NSEntityDescription
                      insertNewObjectForEntityForName:TBL_ABOUT
                      inManagedObjectContext:[CoreDataBase managedObjectContext]];
        about.key = key;
    }
    
    about.content = [[responseDict objectAtIndex:0] valueForKey:@"PageContent"];//==[NSNull null] ? @"" :[[responseDict objectAtIndex:0] valueForKey:@"PageContent"];
    [CoreDataBase saveContext];
    return about;
}
+ (About*)objectWithID:(int)key
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_ABOUT];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %i", key];
    [fetch setPredicate:predicate];
    
    [fetch setReturnsObjectsAsFaults:NO];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count]) {
        return [fetchedItems objectAtIndex:0];
    }
    return nil;
    
}
+ (NSArray*)allObjects
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_ABOUT];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
    
}


@end
