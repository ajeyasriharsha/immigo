//
//  LinksDAO.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/28/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "LinksDAO.h"
#import "CoreDataBase.h"
#import "Categories.h"
#import "Common.h"
#import "SubTopic.h"
@implementation LinksDAO
+ (NSString *)flattenHTML:(NSString *)html
{
    
    NSScanner *theScanner;
    NSString *text = nil;
    theScanner = [NSScanner scannerWithString:html];
    
    while ([theScanner isAtEnd] == NO) {
        
        [theScanner scanUpToString:@"<a" intoString:NULL] ;
        
        [theScanner scanUpToString:@">" intoString:&text] ;
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    //
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return html;
}
+ (Links*)insertIfDoesNotExists:(NSDictionary*)responseDict WithSubTopic:(SubTopic *)subTopic
{
    if (!responseDict || [responseDict isEqual:[NSNull null]])
    {
        return nil;
    }
//    NSString *html = [self flattenHTML:[responseDict valueForKey:@"LinkUrl"]];
//    //NSLog(@"html %@",html);
//    
//    NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:@"(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))" options:NSRegularExpressionCaseInsensitive error:NULL];
//    
//    NSString *someString = [responseDict valueForKey:@"LinkUrl"];
//    
//    NSString *match = [someString substringWithRange:[expression rangeOfFirstMatchInString:someString options:NSMatchingCompleted range:NSMakeRange(0, [someString length])]];
//    
//    //NSLog(@"%@", match);
    NSInteger key = [[responseDict valueForKey:@"Id"] integerValue];
    
    Links *link = [self objectWithID:key AndSubTopicID:subTopic.subTopicId];
    if (!link) {
        link = [NSEntityDescription
                    insertNewObjectForEntityForName:TBL_LINKS
                    inManagedObjectContext:[CoreDataBase managedObjectContext]];
        link.key = key;
    }
    link.isLatest = YES;
    link.linkUrl = [responseDict valueForKey:@"LinkUrl"];
    link.linktext = [responseDict valueForKey:@"LinkText"];
    link.linkDescription = [responseDict valueForKey:@"LinkDescription"];
    [CoreDataBase saveContext];
    return link;
    
}
+ (Links*)objectWithID:(int)key AndSubTopicID:(int) subTopicKey
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_LINKS];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %i && ANY subTopics.subTopicId == %i", key];
//    [fetch setPredicate:predicate];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %i && subTopics.subTopicId == %i", key,subTopicKey];
    [fetch setPredicate:predicate];

    [fetch setReturnsObjectsAsFaults:NO];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count]) {
        return [fetchedItems objectAtIndex:0];
    }
    return nil;
    
}
+ (NSArray*)allObjects
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_LINKS];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
}

+(void)markAsOldRecords
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_LINKS];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(Links *link in fetchedItems)
        {
            link.isLatest=false;
        }
    }
    [CoreDataBase saveContext];
}

+(void)deleteOldRecords
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_LINKS];
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isLatest == 0"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isLatest == %@", [NSNumber numberWithBool:NO]];
    [fetch setPredicate:predicate];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(Links *link in fetchedItems)
        {
            //            if(!state.isLatest)
            {
                [[CoreDataBase managedObjectContext] deleteObject:link];
            }
        }
    }
    [CoreDataBase saveContext];
}

@end
