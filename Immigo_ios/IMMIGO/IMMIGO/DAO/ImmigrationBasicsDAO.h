//
//  ImmigrationBasicsDAO.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/28/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Topic.h"
#import "SubTopic.h"
#import "Links.h"
#import "CheckListItems.h"

@interface ImmigrationBasicsDAO : NSObject
{
    
}
+ (Topic*)insertIfDoesNotExists:(NSDictionary*)responseDict;
+ (Topic*)objectWithID:(int)key;
+ (NSArray*)allObjects;

+(void)markAsOldRecords;
+(void)deleteOldRecords;

@end
