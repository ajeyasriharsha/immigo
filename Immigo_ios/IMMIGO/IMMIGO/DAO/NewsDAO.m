//
//  NewsDAO.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/1/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "NewsDAO.h"
#import "NewsModel.h"
#import "Author.h"
#import "Categories.h"
#import "CoreDataBase.h"
#import "Common.h"
#import "AuthorDAO.h"

@implementation NewsDAO
+ (NSString *)linkFromArray:(NSArray *)array
{
    NSString *sourceLink = nil;
    NSString *alternateLink = nil;
    NSString *selfLink = nil;
    
    for (NSDictionary*dict in array)
    {
        NSString *key = [dict valueForKey:@"rel"];
        if ([key isEqualToString:@"source"])
        {
            sourceLink = [dict valueForKey:@"href"];
        }else if ([key isEqualToString:@"alternate"])
        {
            alternateLink = [dict valueForKey:@"href"];
        }else if ([key isEqualToString:@"self"])
        {
            selfLink = [dict valueForKey:@"href"];
        }
    }
    if (selfLink)
    {
        return selfLink;
    }else if (sourceLink)
    {
        return sourceLink;
    }else if (alternateLink)
    {
        return alternateLink;
    }
    
    return nil;
}

+ (News*)insertIfDoesNotExists:(NSDictionary*)responseDict
{
    if (!responseDict || [responseDict isEqual:[NSNull null]])
    {
        return nil;
    }
    NSInteger key = [[[[[responseDict valueForKey:@"id"] valueForKey:@"text"] componentsSeparatedByString:@":"] objectAtIndex:2] integerValue];//[[responseDict valueForKey:@"Id"] integerValue];
    
    News *news = [self objectWithID:key];
    if (!news)
    {
        news = [NSEntityDescription
                insertNewObjectForEntityForName:TBL_NEWS
                inManagedObjectContext:[CoreDataBase managedObjectContext]];
        news.key = key;
    }
    news.isLatest = YES;
    news.title = [[responseDict valueForKey:@"title"] valueForKey:@"text"] ? [[responseDict valueForKey:@"title"] valueForKey:@"text"]:@"";
    news.origanization = [[[responseDict valueForKey:@"pbn:originator"] valueForKey:@"organization"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"pbn:originator"] valueForKey:@"organization"] valueForKey:@"text"] :@"";
    news.content = [[responseDict valueForKey:@"content"] valueForKey:@"text"]? [[responseDict valueForKey:@"content"] valueForKey:@"text"]:@"";
    NSMutableArray *array = [NSMutableArray array];
    NSInteger linkCount = [[responseDict valueForKey:@"link"] count];
    for (int i = 0; i<linkCount; i++)
    {
        [array addObject:[[responseDict valueForKey:@"link"] objectAtIndex:i]];
    }
    
    news.newsLink = [self linkFromArray:array];
    news.date= [[responseDict valueForKey:@"pbn:date"] valueForKey:@"text"];
    
    
    
//    NSDictionary *dictAuthor = [responseDict getObjectForKey:@"author"];
//    if (dictAuthor || ![dictAuthor isEqual:[NSNull null]])
//    {
//        Author *author=[AuthorDAO insertIfDoesNotExists:dictAuthor WithNews:news];
//        if(author)
//        {
//            news.author = author;
//        }
//    }
    [CoreDataBase saveContext];
    
    return news;
}

+ (News*)objectWithID:(int)key
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_NEWS];
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %i && ANY subTopics.subTopicId == %i", key];
    //    [fetch setPredicate:predicate];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %i", key];
    [fetch setPredicate:predicate];
    
    [fetch setReturnsObjectsAsFaults:NO];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count]) {
        return [fetchedItems objectAtIndex:0];
    }
    return nil;
    
}
+ (NSArray*)allObjects
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_NEWS];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
}

+(void)markAsOldRecords
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_NEWS];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(News *news in fetchedItems)
        {
            news.isLatest=false;
        }
    }
    [CoreDataBase saveContext];
}

+(void)deleteOldRecords
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_NEWS];
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isLatest == 0"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isLatest == %@", [NSNumber numberWithBool:NO]];
    [fetch setPredicate:predicate];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(News *news in fetchedItems)
        {
            //            if(!state.isLatest)
            {
                [[CoreDataBase managedObjectContext] deleteObject:news];
            }
        }
    }
    [CoreDataBase saveContext];
}

@end
