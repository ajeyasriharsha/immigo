//
//  PrivacyDAO.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/21/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Privacy.h"

@interface PrivacyDAO : NSObject
+ (Privacy*)insertIfDoesNotExists:(NSArray *)responseDict;
+ (Privacy*)objectWithID:(int)key;
+ (NSArray*)allObjects;

@end
