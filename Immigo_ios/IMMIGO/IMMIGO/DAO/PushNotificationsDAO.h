//
//  PushNotificationsDAO.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/14/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PushNotifications.h"

@interface PushNotificationsDAO : NSObject
+ (PushNotifications*)insertIfDoesNotExistsWithStatus:(BOOL)isEnabled;
+ (PushNotifications*)objectWithID:(int)key;
+(void)updateStatusWith:(BOOL)status;
@end
