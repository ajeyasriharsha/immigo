//
//  PrivacyDAO.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/21/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "PrivacyDAO.h"
#import "CoreDataBase.h"
#import "Categories.h"

@implementation PrivacyDAO
+ (Privacy*)insertIfDoesNotExists:(NSArray *)responseDict
{
    
    /*
     
     NSString *termsContent=nil;
     termsContent = [[responseDict objectAtIndex:0] valueForKey:@"BodyContent"];
     //NSLog(@"aboutString %@",termsContent);
     
     [[TermsModel sharedDataSource]termsString:termsContent];
     
     return termsContent;
     
     */
    if (!responseDict || [responseDict isEqual:[NSNull null]])
    {
        return nil;
    }
    
    NSInteger key = [[[responseDict objectAtIndex:0] valueForKey:@"Id"] integerValue];
    
    Privacy *about = [self objectWithID:key];
    if (!about) {
        about = [NSEntityDescription
                 insertNewObjectForEntityForName:TBL_PRIVACY
                 inManagedObjectContext:[CoreDataBase managedObjectContext]];
        about.key = key;
    }
    
    about.content = [[responseDict objectAtIndex:0] valueForKey:@"PageContent"];//==[NSNull null] ? @"" :[[responseDict objectAtIndex:0] valueForKey:@"PageContent"];
    [CoreDataBase saveContext];
    return about;
}
+ (Privacy*)objectWithID:(int)key
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_PRIVACY];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %i", key];
    [fetch setPredicate:predicate];
    
    [fetch setReturnsObjectsAsFaults:NO];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count]) {
        return [fetchedItems objectAtIndex:0];
    }
    return nil;
    
}
+ (NSArray*)allObjects
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_PRIVACY];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
    
}

@end
