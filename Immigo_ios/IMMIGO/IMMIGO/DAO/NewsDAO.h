//
//  NewsDAO.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/1/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "News.h"

@interface NewsDAO : NSObject
+ (News*)insertIfDoesNotExists:(NSDictionary*)responseDict;
+ (News*)objectWithID:(int)key;
+ (NSArray*)allObjects;

+(void)markAsOldRecords;
+(void)deleteOldRecords;

@end
