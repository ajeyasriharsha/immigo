//
//  AboutImmigoDAO.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/21/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "About.h"

@interface AboutImmigoDAO : NSObject
+ (About*)insertIfDoesNotExists:(NSArray *)responseDict;
+ (About*)objectWithID:(int)key;
+ (NSArray*)allObjects;

@end
