//
//  EventDAO.h
//  IMMIGO
//
//  Created by pradeep ISPV on 3/27/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Enums.h"
#import "Event.h"

@interface EventDAO : NSObject
+ (Event*)insertIfDoesNotExists:(NSDictionary*)responseDict;
+ (Event*)objectWithID:(int)key;
+ (NSArray*)allObjects;

+(void)markAsOldRecords;
+(void)deleteOldRecords;


@end
