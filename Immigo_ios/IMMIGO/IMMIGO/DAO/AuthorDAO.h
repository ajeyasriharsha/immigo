//
//  AuthorDAO.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/8/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Author.h"

@interface AuthorDAO : NSObject
+ (Author*)insertIfDoesNotExists:(NSDictionary*)responseDict WithNews:(News *)news;
+ (Author*)objectWithID:(int)key AndNewsID:(int)newsKey;
+ (NSArray*)allObjects;

+(void)markAsOldRecords;
+(void)deleteOldRecords;

@end
