//
//  ImmigrationBasicsDAO.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/28/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "ImmigrationBasicsDAO.h"
#import "Common.h"
#import "CoreDataBase.h"
#import "Categories.h"
#import "SubTopicsDAO.h"


/*
 
 {
 "MainTopic": {
 "Id": "26",
 "Title": "Topic - 01",
 "SubTopics": {
 "SubTopic": [
 {
 "MainTopicId": "26",
 "Id": "261",
 "Title": "SubTopic - 01",
 "TextContent": "HtmlText",
 "ContentType": "1"
 },
 {
 "MainTopicId": "26",
 "Id": "262",
 "Title": " SubTopic - 02",
 "Links": {
 "link": [
 {
 "id": "1",
 "linkUrl": "www.google.com"
 },
 {
 "id": "2",
 "linkUrl": "www.yahoo.com"
 }
 ]
 },
 "ContentType": "2"
 },
 {
 "MainTopicId": "26",
 "Id": "263",
 "Title": " SubTopic - 03",
 "CheckListItems": {
 "CheckListItem": [
 title=@""
 {
 "id": "1",
 "TextContent": "Belongs to Asia ?"
 },
 {
 "id": "2",
 "TextContent": "Belongs to Africa ?"
 }
 ]
 },
 "ContentType": "3"
 }
 ]
 }
 }
 }
 
 */
@implementation ImmigrationBasicsDAO
+ (Topic*)insertIfDoesNotExists:(NSDictionary*)responseDict
{
    if (!responseDict || [responseDict isEqual:[NSNull null]])
    {
        return nil;
    }
    
    NSInteger key = [[responseDict valueForKey:@"TopicId"] integerValue];//Id//TopicId
    Topic *topic = [self objectWithID:key];
    if (!topic) {
        topic = [NSEntityDescription
                 insertNewObjectForEntityForName:TBL_TOPICS
                 inManagedObjectContext:[CoreDataBase managedObjectContext]];
        topic.topicId = key;
    }
    topic.isLatest = YES;
    topic.topicTitle = [responseDict valueForKey:@"TopicTitle"];//Title//TopicTitle
    topic.orderNo = [[responseDict valueForKey:@"OrderNo"] isEqual:[NSNull null]] ? 0:[[responseDict valueForKey:@"OrderNo"] integerValue];

    
    NSArray *listSubTopics;
    if ([responseDict hasValidJSONKey:@"SubTopics"]) {
        
        listSubTopics = [responseDict getObjectForKey:@"SubTopics"];
    }else if ([responseDict hasValidJSONKey:@"CivicEngagementsSubTopics"]){
        listSubTopics = [responseDict getObjectForKey:@"CivicEngagementsSubTopics"];
    }
    else if ([responseDict hasValidJSONKey:@"CivilOrganizationSubTopics"]){
        listSubTopics = [responseDict getObjectForKey:@"CivilOrganizationSubTopics"];
    }
    
    for (NSDictionary *dictCourse in listSubTopics)
    {
        SubTopic *subTopic=[SubTopicsDAO insertIfDoesNotExists:dictCourse];
        if(subTopic)
        {
            [topic addSubTopicsObject:subTopic];
        }
    }
    [CoreDataBase saveContext];
    return topic;
}
+ (Topic*)objectWithID:(int)key
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_TOPICS];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"topicId == %i", key];
    [fetch setPredicate:predicate];
    
    [fetch setReturnsObjectsAsFaults:NO];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count]) {
        return [fetchedItems objectAtIndex:0];
    }
    return nil;
}
+ (NSArray*)allObjects
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_TOPICS];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
}
+(void)markAsOldRecords
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_TOPICS];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(Topic *topic in fetchedItems)
        {
            topic.isLatest=false;
        }
    }
    [CoreDataBase saveContext];
}

+(void)deleteOldRecords
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_TOPICS];
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isLatest == 0"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isLatest == %@", [NSNumber numberWithBool:NO]];
    [fetch setPredicate:predicate];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(Topic *topic in fetchedItems)
        {
            //            if(!state.isLatest)
            {
                [[CoreDataBase managedObjectContext] deleteObject:topic];
            }
        }
    }
    [CoreDataBase saveContext];
}

@end
