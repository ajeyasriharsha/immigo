//
//  AboutDAO.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/2/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AboutUsModel.h"
#import "PrivacyModel.h"
#import "TermsModel.h"

@interface AboutDAO : NSObject
+ (NSString*)insertAboutIfDoesNotExists:(NSArray*)responseDict;
+ (NSString*)insertPrivacyIfDoesNotExists:(NSArray*)responseDict;
+ (NSString*)insertTermsIfDoesNotExists:(NSArray*)responseDict;

@end
