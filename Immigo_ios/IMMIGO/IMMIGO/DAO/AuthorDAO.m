//
//  AuthorDAO.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/8/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "AuthorDAO.h"
#import "CoreDataBase.h"
#import "Categories.h"
#import "News.h"

@implementation AuthorDAO
+ (Author*)insertIfDoesNotExists:(NSDictionary*)responseDict WithNews:(News *)news
{
    if (!responseDict || [responseDict isEqual:[NSNull null]])
    {
        return nil;
    }
    
    NSInteger key = [[responseDict valueForKey:@"Id"] integerValue];
    
    Author *author = [self objectWithID:key AndNewsID:news.key];
    if (!author) {
        author = [NSEntityDescription
                insertNewObjectForEntityForName:TBL_AUTHOR
                inManagedObjectContext:[CoreDataBase managedObjectContext]];
        author.key = key;
    }
    author.isLatest = YES;
    author.name = [[responseDict valueForKey:@"name"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"author"] valueForKey:@"name"] valueForKey:@"text"]:@"";
    author.uri = [[responseDict valueForKey:@"uri"] valueForKey:@"text"] ? [[[responseDict valueForKey:@"author"] valueForKey:@"uri"] valueForKey:@"text"]:@"";

    [CoreDataBase saveContext];
    return author;
    
}
+ (Author*)objectWithID:(int)key AndNewsID:(int)newsKey
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_AUTHOR];
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %i && ANY subTopics.subTopicId == %i", key];
    //    [fetch setPredicate:predicate];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %i && news.key == %i", key,newsKey];
    [fetch setPredicate:predicate];
    
    [fetch setReturnsObjectsAsFaults:NO];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count]) {
        return [fetchedItems objectAtIndex:0];
    }
    return nil;
    
}
+ (NSArray*)allObjects
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_AUTHOR];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
}

+(void)markAsOldRecords
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_AUTHOR];
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(Author *author in fetchedItems)
        {
            author.isLatest=false;
        }
    }
    [CoreDataBase saveContext];
}

+(void)deleteOldRecords
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_AUTHOR];
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isLatest == 0"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isLatest == %@", [NSNumber numberWithBool:NO]];
    [fetch setPredicate:predicate];
    
    NSError *fetchError;
    NSArray *fetchedItems = [[CoreDataBase managedObjectContext] executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(Author *author in fetchedItems)
        {
            //            if(!state.isLatest)
            {
                [[CoreDataBase managedObjectContext] deleteObject:author];
            }
        }
    }
    [CoreDataBase saveContext];
}

@end
