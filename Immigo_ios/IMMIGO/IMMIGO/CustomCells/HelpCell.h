//
//  HelpCell.h
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 17/02/16.
//  Copyright © 2016 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
