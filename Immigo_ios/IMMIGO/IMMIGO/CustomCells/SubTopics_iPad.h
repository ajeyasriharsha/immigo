//
//  SubTopics_iPad.h
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 6/10/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubTopics_iPad : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *subTopic;
@end
