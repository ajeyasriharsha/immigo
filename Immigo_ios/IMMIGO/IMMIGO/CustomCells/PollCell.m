//
//  PollCell.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/13/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "PollCell.h"

@implementation PollCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
