//
//  PollCell.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/13/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PollCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *pollQuestion;
@property (strong, nonatomic) IBOutlet UILabel *votedLbl;

@end
