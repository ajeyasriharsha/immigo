//
//  SearchListCell.m
//  Immigo
//
//  Created by pradeep ISPV on 3/11/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "SearchListCell.h"

@implementation SearchListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
