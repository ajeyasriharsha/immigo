//
//  partnersCell.m
//  Immigo
//
//  Created by pradeep ISPV on 2/27/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "partnersCell.h"

@implementation partnersCell
@synthesize partnerLogo;

-(void)customIntialization:(UITableView *)view
{

    {
        self.clipsToBounds = YES;
    }
    partnerLogo = [[UIImageView alloc] init];
    partnerLogo.frame = CGRectMake(30, 0, view.frame.size.width-60, 180.0f);
    partnerLogo.contentMode = UIViewContentModeScaleAspectFit;
    partnerLogo.backgroundColor = [UIColor clearColor];
    [self addSubview:partnerLogo];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self customIntialization:nil];
    }
    return self;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier WithView:(UITableView *)view
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        superView = view;
        [self customIntialization:view];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setLogoImg:(UIImage *)logoImg
{
    _logoImg = logoImg;
    
//    if ([logoImg isEqual:[UIImage imageNamed:@"nclr"]])
//    {
//        partnerLogo.frame  = CGRectMake((self.frame.size.width-_logoImg.size.width)/2, (110-_logoImg.size.height)/2, _logoImg.size.width, _logoImg.size.height);
//        partnerLogo.image =_logoImg;
//    }else
    {
//        partnerLogo.frame  = CGRectMake((self.frame.size.width-_logoImg.size.width)/2, (90-_logoImg.size.height)/2, _logoImg.size.width, _logoImg.size.height);
        CGRect frame = self.frame;
        frame.size.width = superView.frame.size.width;
        self.frame = frame;
        
        partnerLogo.frame  = CGRectMake((self.frame.size.width-_logoImg.size.width)/2, (90-_logoImg.size.height)/2, _logoImg.size.width, _logoImg.size.height);
        partnerLogo.image =_logoImg;
    }
}
-(UIImage *)logoImg
{
    return _logoImg;
}
@end
