//
//  SubTopics_iPad.m
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 6/10/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "SubTopics_iPad.h"
#import "Common.h"
@implementation SubTopics_iPad
@synthesize subTopic;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    //UIColorFromRGB(UI_BLUE_TEXT_COLOR);
    //[subTopic setHighlightedTextColor:UIColorFromRGB(UI_BLUE_TEXT_COLOR)];
    
    if (selected)
        self.subTopic.highlightedTextColor=UIColorFromRGB(UI_BLUE_TEXT_COLOR);
    else
        self.subTopic.highlightedTextColor=[UIColor lightGrayColor];

    // Configure the view for the selected state
}

@end
