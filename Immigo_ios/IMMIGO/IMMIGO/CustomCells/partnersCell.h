//
//  partnersCell.h
//  Immigo
//
//  Created by pradeep ISPV on 2/27/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface partnersCell : UITableViewCell
{
    UIImage *_logoImg;
    UITableView *superView;
}
@property (nonatomic,strong) UIImage *logoImg;
@property (nonatomic,strong) UIImageView *partnerLogo;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier WithView:(UITableView *)view;
@end
