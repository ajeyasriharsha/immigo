//
//  Contact.h
//  IMMIGO
//
//  Created by pradeep ISPV on 3/27/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *organization;
@property (nonatomic,strong) NSString *email;
@property (nonatomic,strong) NSString *phone;
@end
