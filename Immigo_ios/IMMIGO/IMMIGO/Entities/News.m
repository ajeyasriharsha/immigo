//
//  News.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/8/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "News.h"
#import "Author.h"


@implementation News

@dynamic content;
@dynamic date;
@dynamic newsLink;
@dynamic origanization;
@dynamic title;
@dynamic isLatest;
@dynamic key;
@dynamic author;

@end
