//
//  Alerts.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "Alerts.h"


@implementation Alerts

@dynamic alertDiscription;
@dynamic alertTitle;
@dynamic isLatest;
@dynamic key;
@dynamic link;
@dynamic orderNo;

@end
