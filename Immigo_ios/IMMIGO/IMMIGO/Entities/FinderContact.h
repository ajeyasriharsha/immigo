//
//  FinderContact.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/9/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//
/*
 
 <pbn:contact>
 <website>http://www.ufwfoundation.org</website>
 <email>info@ufwfoundation.org</email>
 <phone>(602) 889-0933</phone>
 <tollfreephone>1-877-881-8281</tollfreephone>
 </pbn:contact>
 */

#import <Foundation/Foundation.h>

@interface FinderContact : NSObject
@property (nonatomic,strong) NSString *website;
@property (nonatomic,strong) NSString *email;
@property (nonatomic,strong) NSString *phone;
@property (nonatomic,strong) NSString *tollFreePhone;
@end
