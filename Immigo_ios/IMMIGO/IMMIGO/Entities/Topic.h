//
//  Topic.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SubTopic;

@interface Topic : NSManagedObject

@property (nonatomic) BOOL isLatest;
@property (nonatomic, retain) NSString * topicDescription;
@property (nonatomic) int32_t topicId;
@property (nonatomic, retain) NSString * topicTitle;
@property (nonatomic) int32_t orderNo;
@property (nonatomic, retain) NSSet *subTopics;
@end

@interface Topic (CoreDataGeneratedAccessors)

- (void)addSubTopicsObject:(SubTopic *)value;
- (void)removeSubTopicsObject:(SubTopic *)value;
- (void)addSubTopics:(NSSet *)values;
- (void)removeSubTopics:(NSSet *)values;

@end
