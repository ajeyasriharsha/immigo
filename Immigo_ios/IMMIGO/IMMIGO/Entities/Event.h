//
//  Event.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/9/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Event : NSManagedObject

@property (nonatomic, retain) NSString * contactEmail;
@property (nonatomic, retain) NSString * contactName;
@property (nonatomic, retain) NSString * contactOrganization;
@property (nonatomic, retain) NSString * contactPhone;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSString * dailyEndTime;
@property (nonatomic, retain) NSString * dailyStartTime;
@property (nonatomic, retain) NSString * endDate;
@property (nonatomic) BOOL isLatest;
@property (nonatomic) int32_t key;
@property (nonatomic, retain) NSString * link;
@property (nonatomic, retain) NSString * locationCity;
@property (nonatomic, retain) NSString * locationCountry;
@property (nonatomic, retain) NSString * locationCountryId;
@property (nonatomic, retain) NSString * locationState;
@property (nonatomic, retain) NSString * locationStateId;
@property (nonatomic, retain) NSString * locationStreetAddress;
@property (nonatomic, retain) NSString * locationStreetAddress2;
@property (nonatomic, retain) NSString * startDate;
@property (nonatomic, retain) NSString * title;

@end
