//
//  Location.h
//  IMMIGO
//
//  Created by pradeep ISPV on 3/27/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Location : NSObject
@property (nonatomic,strong) NSString *city;
@property (nonatomic,strong) NSString *state;
@property (nonatomic,strong) NSString *stateId;
@property (nonatomic,strong) NSString *country;
@property (nonatomic,strong) NSString *countryId;
@property (nonatomic,strong) NSString *streetaddress;
@property (nonatomic,strong) NSString *streetaddress2;
@end
