//
//  SubTopic.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CheckListItems, Links, Topic;

@interface SubTopic : NSManagedObject

@property (nonatomic, retain) NSString * contentType;
@property (nonatomic) int32_t contentTypeId;
@property (nonatomic, retain) NSString * heading;
@property (nonatomic) BOOL isLatest;
@property (nonatomic) int32_t subTopicId;
@property (nonatomic, retain) NSString * subtopicTitle;
@property (nonatomic, retain) NSString * textContent;
@property (nonatomic) int32_t orderNo;
@property (nonatomic, retain) NSSet *checkListItems;
@property (nonatomic, retain) NSSet *links;
@property (nonatomic, retain) Topic *topic;
@end

@interface SubTopic (CoreDataGeneratedAccessors)

- (void)addCheckListItemsObject:(CheckListItems *)value;
- (void)removeCheckListItemsObject:(CheckListItems *)value;
- (void)addCheckListItems:(NSSet *)values;
- (void)removeCheckListItems:(NSSet *)values;

- (void)addLinksObject:(Links *)value;
- (void)removeLinksObject:(Links *)value;
- (void)addLinks:(NSSet *)values;
- (void)removeLinks:(NSSet *)values;

@end
