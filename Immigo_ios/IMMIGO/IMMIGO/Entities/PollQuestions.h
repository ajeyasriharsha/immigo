//
//  PollQuestions.h
//  IMMIGO
//
//  Created by pradeep ISPV on 6/1/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PollQuestions : NSManagedObject

@property (nonatomic, retain) NSString * answerFive;
@property (nonatomic) int32_t answerFivePercentage;
@property (nonatomic, retain) NSString * answerFour;
@property (nonatomic) int32_t answerFourPercentage;
@property (nonatomic) int32_t answerId;
@property (nonatomic, retain) NSString * answerOne;
@property (nonatomic) int32_t answerOnePercentage;
@property (nonatomic, retain) NSString * answerSix;
@property (nonatomic) int32_t answerSixPercentage;
@property (nonatomic, retain) NSString * answerStatus;
@property (nonatomic, retain) NSString * answerThree;
@property (nonatomic) int32_t answerThreePercentage;
@property (nonatomic, retain) NSString * answerTwo;
@property (nonatomic) int32_t answerTwoPercentage;
@property (nonatomic) BOOL isLatest;
@property (nonatomic) int32_t key;
@property (nonatomic) int32_t optionsCount;
@property (nonatomic) int32_t orderNo;
@property (nonatomic, retain) NSString * question;

@end
