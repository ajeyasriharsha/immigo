//
//  LegalFinder.h
//  Immigo
//
//  Created by pradeep ISPV on 3/25/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FinderAddress.h"
#import "Author.h"
#import "FinderContact.h"

@interface LegalFinder : NSObject
@property (nonatomic,strong) NSString *finderName;
@property (nonatomic,strong) NSString *content;
@property (nonatomic,strong) FinderAddress *finderAddress;
@property (nonatomic,strong) Author *author;
@property (nonatomic,strong) FinderContact *contact;
@end
