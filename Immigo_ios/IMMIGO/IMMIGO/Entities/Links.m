//
//  Links.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/19/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "Links.h"
#import "SubTopic.h"


@implementation Links

@dynamic isLatest;
@dynamic key;
@dynamic linkUrl;
@dynamic linktext;
@dynamic linkDescription;
@dynamic subTopics;

@end
