//
//  Topic.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "Topic.h"
#import "SubTopic.h"


@implementation Topic

@dynamic isLatest;
@dynamic topicDescription;
@dynamic topicId;
@dynamic topicTitle;
@dynamic orderNo;
@dynamic subTopics;

@end
