//
//  CheckListItems.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/3/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SubTopic;

@interface CheckListItems : NSManagedObject

@property (nonatomic) BOOL isChecked;
@property (nonatomic) int32_t key;
@property (nonatomic, retain) NSString * title;
@property (nonatomic) BOOL isLatest;
@property (nonatomic, retain) SubTopic *subTopics;

@end
