//
//  PollQuestions.m
//  IMMIGO
//
//  Created by pradeep ISPV on 6/1/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "PollQuestions.h"


@implementation PollQuestions

@dynamic answerFive;
@dynamic answerFivePercentage;
@dynamic answerFour;
@dynamic answerFourPercentage;
@dynamic answerId;
@dynamic answerOne;
@dynamic answerOnePercentage;
@dynamic answerSix;
@dynamic answerSixPercentage;
@dynamic answerStatus;
@dynamic answerThree;
@dynamic answerThreePercentage;
@dynamic answerTwo;
@dynamic answerTwoPercentage;
@dynamic isLatest;
@dynamic key;
@dynamic optionsCount;
@dynamic orderNo;
@dynamic question;

@end
