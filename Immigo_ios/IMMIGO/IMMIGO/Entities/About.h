//
//  About.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/21/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface About : NSManagedObject

@property (nonatomic) int32_t key;
@property (nonatomic, retain) NSString * content;

@end
