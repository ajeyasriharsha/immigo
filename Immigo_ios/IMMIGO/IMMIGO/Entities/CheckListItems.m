//
//  CheckListItems.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/3/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "CheckListItems.h"
#import "SubTopic.h"


@implementation CheckListItems

@dynamic isChecked;
@dynamic key;
@dynamic title;
@dynamic isLatest;
@dynamic subTopics;

@end
