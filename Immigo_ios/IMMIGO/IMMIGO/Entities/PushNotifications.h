//
//  PushNotifications.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/8/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PushNotifications : NSManagedObject

@property (nonatomic) int32_t key;
@property (nonatomic) BOOL isEnabled;

@end
