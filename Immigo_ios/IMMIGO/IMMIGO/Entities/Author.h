//
//  Author.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/1/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Author : NSObject
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *uri;

@end
