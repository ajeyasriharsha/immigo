//
//  Event.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/9/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "Event.h"


@implementation Event

@dynamic contactEmail;
@dynamic contactName;
@dynamic contactOrganization;
@dynamic contactPhone;
@dynamic content;
@dynamic dailyEndTime;
@dynamic dailyStartTime;
@dynamic endDate;
@dynamic isLatest;
@dynamic key;
@dynamic link;
@dynamic locationCity;
@dynamic locationCountry;
@dynamic locationCountryId;
@dynamic locationState;
@dynamic locationStateId;
@dynamic locationStreetAddress;
@dynamic locationStreetAddress2;
@dynamic startDate;
@dynamic title;

@end
