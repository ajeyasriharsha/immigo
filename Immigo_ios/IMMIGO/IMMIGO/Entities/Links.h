//
//  Links.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/19/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SubTopic;

@interface Links : NSManagedObject

@property (nonatomic) BOOL isLatest;
@property (nonatomic) int32_t key;
@property (nonatomic, retain) NSString * linkUrl;
@property (nonatomic, retain) NSString * linktext;
@property (nonatomic, retain) NSString * linkDescription;
@property (nonatomic, retain) SubTopic *subTopics;

@end
