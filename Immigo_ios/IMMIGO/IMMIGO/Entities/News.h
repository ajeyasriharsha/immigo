//
//  News.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/8/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Author;

@interface News : NSManagedObject

@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) NSString * newsLink;
@property (nonatomic, retain) NSString * origanization;
@property (nonatomic, retain) NSString * title;
@property (nonatomic) BOOL isLatest;
@property (nonatomic) int32_t key;
@property (nonatomic, retain) Author *author;

@end
