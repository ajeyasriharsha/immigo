//
//  Alerts.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Alerts : NSManagedObject

@property (nonatomic, retain) NSString * alertDiscription;
@property (nonatomic, retain) NSString * alertTitle;
@property (nonatomic) BOOL isLatest;
@property (nonatomic) int32_t key;
@property (nonatomic, retain) NSString * link;
@property (nonatomic) int32_t orderNo;

@end
