//
//  SubTopic.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/29/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "SubTopic.h"
#import "CheckListItems.h"
#import "Links.h"
#import "Topic.h"


@implementation SubTopic

@dynamic contentType;
@dynamic contentTypeId;
@dynamic heading;
@dynamic isLatest;
@dynamic subTopicId;
@dynamic subtopicTitle;
@dynamic textContent;
@dynamic orderNo;
@dynamic checkListItems;
@dynamic links;
@dynamic topic;

@end
