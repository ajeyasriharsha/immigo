//
//  FinderAddress.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/9/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

/*
 
 <pbn:address>
 <street>1440 East Washington St</street>
 <city>Phoenix</city>
 <state id="AZ">Arizona</state>
 <zipcode>85034</zipcode>
 <latitude>33.4382789</latitude>
 <longitude>-112.0178286</longitude>
 </pbn:address>

 
 */

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface FinderAddress : NSObject
@property (nonatomic,strong) NSString *street;
@property (nonatomic,strong) NSString *city;
@property (nonatomic,strong) NSString *state;
@property (nonatomic,strong) NSString *zipcode;
@property (nonatomic,strong) NSString *latitude;
@property (nonatomic,strong) NSString *longitude;
@property (nonatomic,assign) CLLocationCoordinate2D coordinate;
@end
