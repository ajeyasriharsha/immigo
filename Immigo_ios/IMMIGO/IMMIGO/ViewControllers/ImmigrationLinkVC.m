//
//  ImmigrationLinkVC.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/2/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "ImmigrationLinkVC.h"
#import "Topic.h"
#import "SubTopic.h"
#import "Links.h"
#import "LinksModel.h"
#import "Common.h"
#import "Categories.h"
#import "ImmigrationLinkDetailVC.h"
#import "UITextViewDisableSelection.h"
#import <QuartzCore/QuartzCore.h>
#import "FaceBookActivityProvider.h"
#import "LinkedInActivity.h"
@interface ImmigrationLinkVC ()
{
    UIScrollView *scrollView;
    IBOutlet UIView *bgView;
    
    IBOutlet UIButton *topHeading;
    IBOutlet UILabel *headingBg;
    IBOutlet UITableView *linksTable;
    
    NSArray *links;
    IBOutlet UITextViewDisableSelection *subTopicDesc;
    IBOutlet UITextViewDisableSelection *heading;
    
    UILabel *questionText;
}
@end

@implementation ImmigrationLinkVC
@synthesize subtopic = _subtopic;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)init
{
    self=[super init];
   
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    links = [[LinksModel sharedDataSource] fetchLinksForSubtopic:_subtopic];
    
    titleheading.backgroundColor=UIColorFromRGB(0Xc51039);
    titleheading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
    sepratorLine.backgroundColor=UIColorFromRGB(0Xc51039);
    
    //NSLog(@"This is Link VC");
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        //NSLog(@"runnig on < 7.0");
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"left arrow"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        UIImage *image = [UIImage imageNamed:@"question"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = rightItem;
        
        [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    subTopicDesc.backgroundColor = [UIColor clearColor];
    subTopicDesc.textColor = [UIColor darkGrayColor];
    
    subTopicDesc.text= @"AJ";
    heading.text =@"AJ";
    heading.textColor = [UIColor blackColor]; // VAMSHI
    
    
//    CGRect rect= subTopicDesc.frame;
//    rect.size.height+=10;
//    subTopicDesc.frame=rect;
    
//    self.navigationController.navigationBarHidden = NO;
//    UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//    UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//    titlView.image = titleLogo;
//    self.navigationItem.titleView = titlView;
    
    linksTable.rowHeight = 60.0f;
    
    if (_subtopic.heading)
    {
        questionText = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, self.view.frame.size.width-40, 20)];
        questionText.lineBreakMode = NSLineBreakByWordWrapping;
        questionText.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.91]; // [UIFont fontWithName:FONT_SEMIBOLD_ITALIC size:16.91]; // VAMSHI
        questionText.frame = [self frameForLabel:questionText WithText:_subtopic.heading];
        //    questionText.frame = [self frameForLabel:questionText WithText:@"dfhvdhjsfgsdh gdsgsdhj bghbhjdbhbdhsbgkjsdbg kjsdbgjkdsbgkj bdsjkgbdsjkgbsdkjbgsdkjbgkjsdbgkj dsgkjds bgkjdsbgj dbsgkjdsbgjksdb kgjbdskjgbdksjbg jksdbgkj bdsk jgbdskjbg kjdsbgkjdsbgkjsdbg jkbsdkjgbsdlkngpsdriehoisdojbgioewhtgiweknfgskjdanglngljdshaiepniohaong;ldsjaprje"];
        questionText.numberOfLines =0;
        questionText.textColor = UIColorFromRGB(0x212121); // VAMSHI //UIColorFromRGB(0x4c4c4c);
        questionText.backgroundColor  = [UIColor clearColor];
        questionText.text = _subtopic.heading;
        questionText.autoresizingMask =  UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
        questionText.textAlignment = NSTextAlignmentLeft;
    }
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    
    
}


-(NSString *)getformsHtmlContentsforPrint:(BOOL)allowPrint{
    
    NSArray *linksArray =links;
    NSMutableString *bodyHtml=[NSMutableString string];
    if (!allowPrint) {
        for (int i=0; i<linksArray.count; i++) {
            
            Links *link = [links objectAtIndex:i];
            NSString *linkText =link.linktext;
            if (_subtopic.heading && i ==0) {
                NSString *linkHtml =[NSString stringWithFormat:@"<p>%@ </p>",_subtopic.heading];
                [bodyHtml appendString:linkHtml];
            }
            NSString *linkHtml =[NSString stringWithFormat:@"<p><a href=\"%@\">%@</a></p>",link.linkUrl,linkText];
            [bodyHtml appendString:linkHtml];
            
        }
    }else{
        for (int i=0; i<linksArray.count; i++) {
            
            Links *link = [links objectAtIndex:i];
            NSString *linkText =link.linktext;
            
            if (_subtopic.heading && i ==0) {
                NSString *linkHtml =[NSString stringWithFormat:@"<p>%@ </p>",_subtopic.heading];
                [bodyHtml appendString:linkHtml];
            }
            NSString *linkHtml =[NSString stringWithFormat:@"<p>%@ - <a href=\" %@ /\"> %@</a> </p>",linkText,link.linkUrl,link.linkUrl];
            [bodyHtml appendString:linkHtml];
            
        }
        
    }
    if (!allowPrint) {
        NSURL *appstoreUrl = [NSURL URLWithString:APP_STORE_URL];
        NSString *mailSignature =[NSString stringWithFormat:@"<p> %@ <a href=\"%@\">%@</a></p>",NSLocalizedString(@"SENT_EMAIL_SIGNATURE", @""),appstoreUrl,[Common deviceName]];
        [bodyHtml appendString:mailSignature];
    }
    NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body> %@</body></html>",bodyHtml];
    return html;
}

-(IBAction)share:(id)sender{
    
    NSString *sharingContentForSocialMedia =[NSString stringWithFormat:@"%@/%@:",_subtopic.topic.topicTitle,_subtopic.subtopicTitle];
    
    NSString *mailCOntent =[self getformsHtmlContentsforPrint:NO];
    
    NSString *textToShare = mailCOntent;
    NSURL *myWebsite = [NSURL URLWithString:APP_STORE_COMMON_LINK];
    
    
    NSString *contentToPrint =[self getformsHtmlContentsforPrint:YES];
    UIMarkupTextPrintFormatter *htmlFormatter = [[UIMarkupTextPrintFormatter alloc]
                                                 initWithMarkupText:contentToPrint];
    htmlFormatter.startPage = 0;
    htmlFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
    NSArray *applicationActivities=nil;
    
//    if ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] integerValue] >= 6 && [[[[UIDevice currentDevice] systemVersion] substringToIndex:1] integerValue] <= 7) {
//        LinkedInActivity *activity =[[LinkedInActivity alloc] init];
//        activity.postUrl =[myWebsite absoluteString];
//        activity.postCommentText =sharingContentForSocialMedia;
//        applicationActivities =
//        @[
//          activity
//          ];
//        
//    }else if ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] integerValue] >= 8){
//        
//        applicationActivities =nil;
//    }

    
    FaceBookActivityProvider *activityItemProvider =
    [[FaceBookActivityProvider alloc] initWithPlaceholderItem:textToShare
                                                  twitterItem:sharingContentForSocialMedia andUrlForStore:[myWebsite absoluteString]];
    
    NSArray *objectsToShare = @[activityItemProvider,htmlFormatter,myWebsite];
    
    UIActivityViewController *controller =
    [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:applicationActivities];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,//@"com.linkedin.LinkedIn.ShareExtension",
                                   UIActivityTypePostToVimeo,
                                   UIActivityTypePostToTencentWeibo,
                                   UIActivityTypeMessage,
                                   UIActivityTypeCopyToPasteboard];
    
    controller.excludedActivityTypes = excludeActivities;
    NSString *appname =NSLocalizedString(@"APP_NAME", @"");
    NSString *subject =[NSString stringWithFormat:@"%@ - %@ - %@",appname,_subtopic.topic.topicTitle,_subtopic.subtopicTitle];
    
    [controller setValue:subject forKey:@"subject"];
    
    [self presentViewController:controller animated:YES completion:nil];
    
    [controller setCompletionHandler:^(NSString *activityType, BOOL completed)
     {
         if (completed)
         {
//             UIAlertView *objalert = [[UIAlertView alloc]initWithTitle:@"Immigo" message:@"Posting was successful" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//             [objalert show];
//             objalert = nil;
         }else
         {
//             UIAlertView *objalert = [[UIAlertView alloc]initWithTitle:@"Immigo" message:@"Posting was not successful" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//             [objalert show];
//             objalert = nil;
         }
     }];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //NSLog(@"view did appear");
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
   // [subTopicDesc disableAlginment];
    [self.navigationController setToolbarHidden:YES];
    [subTopicDesc enableObserver:NO onObject:subTopicDesc selector:@selector(contentSize)];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
  //  [subTopicDesc disableAlginment];
    
}
//- (void)dealloc
//{
//    //NSLog(@"links dealloc");
//    //[heading disableAlginment];
//    if (subTopicDesc) {
//            [subTopicDesc disableAlginment];
//    }
//}
-(CGRect)frameForTextView:(UITextView *)textView WithText:(NSString *)text
{
    CGSize maximumLabelSize = CGSizeMake(textView.frame.size.width, FLT_MAX);
    
    CGSize expectedLabelSize = [text sizeWithFont:textView.font constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByWordWrapping];
    //adjust the label the the new height.
    CGRect newFrame = textView.frame;
    newFrame.size.height = expectedLabelSize.height<44.0f ? 44.0f :expectedLabelSize.height;
    //    label.frame = newFrame;
    
    return newFrame;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self customizeNavigationBar];
    
    subTopicDesc.text = nil;
    subTopicDesc.text = [NSString stringWithFormat:@"%@ / %@",_subtopic.topic.topicTitle,_subtopic.subtopicTitle];
    subTopicDesc.font = [UIFont fontWithName:FONT_SEMIBOLD size:16]; // VAMSHI //[UIFont fontWithName:FONT_SEMIBOLD_ITALIC size:16.91f];
    subTopicDesc.textColor = UIColorFromRGB(0XC51039);  //VAMSHI //[UIColor darkGrayColor];

    
    [self.navigationController setToolbarHidden:YES];
    [self.navigationController.toolbar setBarStyle:UIBarStyleDefault];  //for example
//
    //set the toolbar buttons
    
    
    // Create a custom UIBarButtonItem with the button
    UIBarButtonItem *share = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(share:)];
    
    [self setToolbarItems:[NSArray arrayWithObjects:share,nil]];
    //new changes for textview center text
    [subTopicDesc enableObserver:YES onObject:subTopicDesc selector:@selector(contentSize)];
    
}


//-(CGRect)frameForTextView:(UITextView *)textView WithText:(NSString *)text
//{
//    CGSize maximumLabelSize = CGSizeMake(textView.frame.size.width, FLT_MAX);
//    CGSize expectedLabelSize = [text sizeWithFont:textView.font constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByWordWrapping];
//    //adjust the label the the new height.
//    CGRect newFrame = textView.frame;
//    newFrame.size.height = expectedLabelSize.height<44.0f ? 44.0f :expectedLabelSize.height;
//    //    label.frame = newFrame;
//
//    return newFrame;
//}

-(void)customizeNavigationBar
{
    
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    
//    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
//    [backView setBackgroundColor:[UIColor clearColor]];
//    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
//    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
//    [backView addSubview:titleImageView];
//    titleImageView.contentMode = UIViewContentModeCenter;
//    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        bgView.frame = viewFrame;
        
    }
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(CGRect)frameForLabel:(UILabel *)label WithText:(NSString *)text
{
    CGSize maximumLabelSize = CGSizeMake(label.frame.size.width, FLT_MAX);
    CGSize expectedLabelSize = [text sizeWithFont:label.font constrainedToSize:maximumLabelSize lineBreakMode:label.lineBreakMode];
    //adjust the label the the new height.
    CGRect newFrame = label.frame;
    newFrame.size.height = expectedLabelSize.height<44.0f ? 44.0f :expectedLabelSize.height;
    //    label.frame = newFrame;
    
    return newFrame;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (indexPath.row == 0 && _subtopic.heading != nil)
    {
        return [self frameForLabel:questionText WithText:_subtopic.heading].size.height+20;
    }
    else
    {
        return 60.0f;
    }
    return 60.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (_subtopic.heading != nil) ? links.count+1 : links.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    
    if (indexPath.row == 0 && _subtopic.heading !=nil)
    {
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.font = [UIFont fontWithName:FONT_SEMIBOLD_ITALIC size:16.91];
        cell.textLabel.numberOfLines =0;
        cell.textLabel.textColor = UIColorFromRGB(0X124A7F);  //VAMSHI //UIColorFromRGB(0x4c4c4c);
        cell.textLabel.backgroundColor  = [UIColor clearColor];
        cell.textLabel.text = _subtopic.heading;
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryView = nil;
    }
    else
    {
        
        Links *link = (_subtopic.heading == nil) ? [links objectAtIndex:indexPath.row]:[links objectAtIndex:indexPath.row-1];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.text = [link.linktext trimmedString];
        cell.detailTextLabel.text = [link.linkDescription trimmedString];
        cell.textLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:14.5];
        cell.detailTextLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:11.5];
        cell.detailTextLabel.textColor = [UIColor darkGrayColor];
        cell.textLabel.textColor = UIColorFromRGB(0X124A7F);  //VAMSHI //UIColorFromRGB(UI_TITLE_HEADER_COLOR);
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];
    }
        
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        [_objects removeObjectAtIndex:indexPath.row];
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
//    }
//}

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Basics" message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        return;  // @"Immigration Basics" // VAMSHI
    }

    if (indexPath.row == 0 && _subtopic.heading != nil)
    {
        return;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    Links *selectedLink = (_subtopic.heading == nil) ? [links objectAtIndex:indexPath.row]:[links objectAtIndex:indexPath.row-1];
    ImmigrationLinkDetailVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"ImmigrationLinkDetailVC"];
    toVC.linkUrl = selectedLink.linkUrl;
    [self.navigationController pushViewController:toVC animated:YES];
    
    
}

#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        bgView.frame = viewFrame;
    }
   // [subTopicDesc scrollRangeToVisible:NSMakeRange(0, 0)];
   // [heading scrollRangeToVisible:NSMakeRange(0, 0)];
    //    }
}

- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
@end
