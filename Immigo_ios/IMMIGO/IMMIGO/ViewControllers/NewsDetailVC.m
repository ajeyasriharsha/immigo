//
//  NewsDetailVC.m
//  Immigo
//
//  Created by pradeep ISPV on 3/10/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "NewsDetailVC.h"
#import "AboutUsVC.h"
#import "News.h"
#import "NewsModel.h"
#import "Categories.h"
#import "Common.h"

@interface NewsDetailVC ()<UIWebViewDelegate>
{
//    News *selectedNews;
    
//    IBOutlet UIScrollView *detailScrollView;
    IBOutlet UILabel *topBG;
    
//    IBOutlet UITextView *linksTextView;
    IBOutlet UITextView *titleView;
    IBOutlet UIWebView *contentWebView;
    IBOutlet UIActivityIndicatorView *activityView;
    __weak IBOutlet UIView *bgView;
    __weak IBOutlet UIButton *openInSafari;
}
@end

@implementation NewsDetailVC
@synthesize selectedNews;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    activityView.hidden = YES;
//    selectedNews = [[NewsModel sharedDataSource] selectedNews];
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        //NSLog(@"runnig on < 7.0");
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"left arrow"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        UIImage *image = [UIImage imageNamed:@"question"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = rightItem;
        
        
        [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];
        
    }

    heading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
    heading.backgroundColor=UIColorFromRGB(0Xc51039);//RedColor ThroughtAPP//JUHI
    {//navigation bar customization
        self.navigationController.navigationBarHidden = NO;
//        UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//        UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//        titlView.image = titleLogo;
//        self.navigationItem.titleView = titlView;
    }
    
    titleView.text = [selectedNews.title trimmedString];
    titleView.font = [UIFont fontWithName:FONT_REGULAR size:13.7f];
    
    [contentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:selectedNews.newsLink]]];

    /*
    for (UIView *scroll in [contentWebView subviews]) {
        //Set the zoom level.
        if ([scroll isKindOfClass:[UIScrollView class]])
        {
            UIScrollView *scrollVIew = (UIScrollView *)scroll;
            [scrollVIew setMinimumZoomScale:0.0];
            [scrollVIew setMaximumZoomScale:1.0];
            
           [scrollVIew setZoomScale:5.5f animated:YES];
        }
    }
*/
    /*
    CGPoint cursor = CGPointMake(20,3);
    CGSize size = CGSizeMake(self.view.frame.size.width-cursor.x-cursor.x, self.view.frame.size.height);

    
    UILabel *newsTitle = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, size.height)];
    newsTitle.text = [selectedNews.title trimmedString];
    newsTitle.font = [UIFont fontWithName:@"OpenSans" size:13.7f];
    newsTitle.numberOfLines = 0;
    
    CGSize newsTitleSize = [newsTitle sizeOfMultiLineLabel];
    CGRect newsTitleFrame = newsTitle.frame;
    newsTitleFrame.size.height = newsTitleSize.height;
    newsTitle.frame = newsTitleFrame;
    
    [detailScrollView addSubview:newsTitle];

    cursor.y +=newsTitle.frame.size.height;
    
    UILabel *linksHeading = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, 25.0f)];
    linksHeading.text = @"Links: ";
    linksHeading.font = [UIFont fontWithName:@"OpenSans-SemiBold" size:16.91f];
    [detailScrollView addSubview:linksHeading];
    
    cursor.y +=linksHeading.frame.size.height;
    
//    if (selectedNews.newsLinks.count >0)
//    {
//        for (int i = 0; i<selectedNews.newsLinks.count; i++)
//        {
//            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//            [button addTarget:self action:@selector(tappedOnNewsLinks:) forControlEvents:UIControlEventTouchUpInside];
//            [button setTitle:[NSString stringWithFormat:@"%d) %@",i+1,[selectedNews.newsLinks objectAtIndex:i]] forState:UIControlStateNormal];
//            button.tag = 100*i;
//            button.titleLabel.numberOfLines = 3;
//            [button.titleLabel setFont:[UIFont fontWithName:@"OpenSans" size:13.08f]];
//            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//            [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
//            button.frame = CGRectMake(cursor.x, cursor.y, size.width, 40.0f);
//            
//
//            [detailScrollView addSubview:button];
//            
//            cursor.y += button.frame.size.height+5;
//        }
//    }
//    if ([selectedNews.author.name length]>0)
//    {
//        UILabel *authorNameHeading = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, 25.0f)];
//        authorNameHeading.text = @"Author name: ";
//        authorNameHeading.font = [UIFont fontWithName:@"OpenSans-SemiBold" size:14.0f];
//        [detailScrollView addSubview:authorNameHeading];
//        
//        cursor.y += authorNameHeading.frame.size.height;
//        
//        UILabel *authorName = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, 30.0f)];
//        authorName.text = [selectedNews.author.name trimmedString];
//        authorName.numberOfLines = 2;
//        authorName.font = [UIFont fontWithName:@"OpenSans" size:14.0f];
//        [detailScrollView addSubview:authorName];
//
//        cursor.y += authorName.frame.size.height+2;
//    }
    if ([selectedNews.author.name length]>0)
    {
        UILabel *authorUriHeading = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, 25.0f)];
        authorUriHeading.text = @"Author:";
        authorUriHeading.font = [UIFont fontWithName:@"OpenSans-SemiBold" size:16.91f];
        [detailScrollView addSubview:authorUriHeading];
        
        cursor.y += authorUriHeading.frame.size.height;
        
        UIButton *authorUri = [UIButton buttonWithType:UIButtonTypeCustom];
        [authorUri addTarget:self action:@selector(tappedonAuthorUri:) forControlEvents:UIControlEventTouchUpInside];
        [authorUri setTitle:[selectedNews.author.name trimmedString] forState:UIControlStateNormal];
        [authorUri setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
        authorUri.frame = CGRectMake(cursor.x, cursor.y, size.width, 40.0f);
        authorUri.imageEdgeInsets = UIEdgeInsetsMake(0, authorUri.frame.size.width - [UIImage imageNamed:@"arrow"].size.width, 0, 0);
        authorUri.titleEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
        authorUri.tag = 1000;
        authorUri.titleLabel.numberOfLines = 3;
        [authorUri.titleLabel setFont:[UIFont fontWithName:@"OpenSans" size:13.08f]];
        authorUri.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [authorUri setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        
        CGSize newsTitleSize1 = [authorUri sizeOfMultiLineButton];
        CGRect newsTitleFrame1 = authorUri.frame;
        newsTitleFrame1.size.height = newsTitleSize1.height;
        authorUri.frame = newsTitleFrame1;

        [detailScrollView addSubview:authorUri];

        
        
//        UILabel *authorUri = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, 40.0f)];
//        authorUri.text = [selectedNews.author.uri trimmedString];
//        authorUri.numberOfLines = 2;
//        authorUri.font = [UIFont fontWithName:@"OpenSans" size:14.0f];
//        [detailScrollView addSubview:authorUri];
        
        cursor.y += authorUri.frame.size.height+2;
    }
    if ([selectedNews.date length]>0)
    {
        UILabel *dateHeading = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, 25.0f)];
        dateHeading.text = @"Date: ";
        dateHeading.font = [UIFont fontWithName:@"OpenSans-SemiBold" size:16.91f];
        [detailScrollView addSubview:dateHeading];
        
        cursor.y += dateHeading.frame.size.height;
        
        UILabel *date = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, 25.0f)];
        date.text = [selectedNews.date newsDateFormat];
        date.numberOfLines = 1;
        date.font = [UIFont fontWithName:@"OpenSans" size:13.08f]; // 2014-03-31T04:00:00Z
        [detailScrollView addSubview:date];
        
        cursor.y += date.frame.size.height+2;
    }

    detailScrollView.contentSize = CGSizeMake(detailScrollView.frame.size.width, cursor.y+50.0f);
    */
	// Do any additional setup after loading the view.
}
//-(IBAction)tappedOnNewsLinks:(UIButton *)sender
//{
//    UIStoryboard *storyboard;
//    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
//        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
//    else
//        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
//    
//    NewsUrlVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"NewsUrlVC"];
//    toVC.urlString = [sender.titleLabel.text substringFromIndex:3];
//    //NSLog(@"url %@",[sender.titleLabel.text substringFromIndex:3]);
//    [self.navigationController pushViewController:toVC animated:YES];
//
//}
//-(IBAction)tappedonAuthorUri:(UIButton *)sender
//{
//    UIStoryboard *storyboard;
//    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
//        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
//    else
//        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
//    
//    NewsUrlVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"NewsUrlVC"];
//    toVC.urlString =[selectedNews.author.uri trimmedString];
//    [self.navigationController pushViewController:toVC animated:YES];
//
//}

-(void)viewWillAppear:(BOOL)animated
{
    
    [self customizeNavigationBar];
    [super viewWillAppear:animated];
}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
    contentWebView.scrollView.minimumZoomScale = 0; // set similar to previous.
    contentWebView.scrollView.maximumZoomScale = 10; // set similar to previous.
}
-(IBAction)openInsafariTapped:(id)sender{
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"IMMIGRATION_NEWS_TITLE", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
        [alertView show];
        return ;
    }
    NSURL *requestUrl =[NSURL URLWithString:selectedNews.newsLink];
    [[UIApplication sharedApplication] openURL:requestUrl];
    
}
-(void)customizeNavigationBar{
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    
//    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
//    [backView setBackgroundColor:[UIColor clearColor]];
//    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
//    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
//    [backView addSubview:titleImageView];
//    titleImageView.contentMode = UIViewContentModeCenter;
//    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        bgView.frame = viewFrame;
        
    }
    
    
    
}
#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        bgView.frame = viewFrame;
    }
    
    //    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];
}
- (IBAction)twitterTapped:(id)sender
{
    
}
- (IBAction)facebookTapped:(id)sender
{
    
}
- (IBAction)linkedInTapped:(id)sender
{
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    activityView.hidden= NO;
    [activityView startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    webView.scrollView.delegate = self; // set delegate method of UISrollView
    webView.scrollView.maximumZoomScale = 10; // set as you want.
    webView.scrollView.minimumZoomScale = 0; // set as you want.
    activityView.hidden= YES;
    [activityView stopAnimating];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    activityView.hidden= YES;
    [activityView stopAnimating];
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked){
        if (![Common currentNetworkStatus])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"IMMIGRATION_NEWS_TITLE", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
            [alertView show];
            return NO;
        }
        
    }
    return YES;
    
    
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
@end
