//
//  ImmigrationSubTopicsVC.m
//  Immigo
//
//  Created by pradeep ISPV on 3/7/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "ImmigrationSubTopicsVC.h"
#import "ImmigrationDescVC.h"
#import "AboutUsVC.h"
#import "SubtopicsModel.h"
#import "ImmigrationCheckListVC.h"
#import "ImmigrationLinkVC.h"
#import "Common.h"
@interface ImmigrationSubTopicsVC ()
{
    IBOutlet UILabel *topicName;
    IBOutlet UILabel *topicHeading;
    
    __weak IBOutlet UIView *bgView;
    NSArray *subTopicsArray;
    IBOutlet UITableView *subTopicsTbl;
    
    
    ImmigrationDescVC* DescVC;
    ImmigrationLinkVC* linkVC;
}
@end

@implementation ImmigrationSubTopicsVC
@synthesize topic,headerTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        //NSLog(@"runnig on < 7.0");
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"left arrow"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        UIImage *image = [UIImage imageNamed:@"question"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = rightItem;
        
        [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];
    }
    heading.titleLabel.text =self.headerTitle;
    heading.backgroundColor=UIColorFromRGB(0Xc51039);
    heading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
    sepratorLine.backgroundColor=UIColorFromRGB(0Xc51039);
    
    topicHeading.textAlignment = NSTextAlignmentLeft;
    topicHeading.font = [UIFont fontWithName:FONT_SEMIBOLD size:16]; // VAMSHI //[UIFont fontWithName:FONT_SEMIBOLD size:16.91];
    topicHeading.textColor = UIColorFromRGB(0XC51039);// VAMSHI // [UIColor darkGrayColor];
//    topicHeading.text = @"TOPIC:";
    topicHeading.backgroundColor = [UIColor clearColor];

    topicName.textAlignment = NSTextAlignmentLeft;
    topicName.backgroundColor = [UIColor clearColor];
    topicName.font = [UIFont fontWithName:FONT_SEMIBOLD size:16]; // VAMSHI //[UIFont fontWithName:FONT_SEMIBOLD_ITALIC size:16.91];
    topicName.textColor = UIColorFromRGB(0XC51039);// VAMSHI //[UIColor darkGrayColor];
    topicName.text = topic.topicTitle;

    self.navigationController.navigationBarHidden = NO;
//    heading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
//    UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//    UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//    titlView.image = titleLogo;
//    self.navigationItem.titleView = titlView;
	// Do any additional setup after loading the view.
    
    subTopicsArray = [[SubtopicsModel sharedDataSource] fetchSubTopicsForTopic:topic];//[NSArray arrayWithObjects:@"Overview",@"Statutes",@"Forms",@"Intake Checklist",@"Document Checklist",@"Other Resources",nil];
}
-(void)viewWillAppear:(BOOL)animated
{
    //NSLog(@"subtopics vc");
    
    [self customizeNavigationBar];
    [super viewWillAppear:animated];
}

-(void)customizeNavigationBar{
    
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    
//    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
//    [backView setBackgroundColor:[UIColor clearColor]];
//    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
//    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
//    [backView addSubview:titleImageView];
//    titleImageView.contentMode = UIViewContentModeCenter;
//    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        bgView.frame = viewFrame;
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
//    
//    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(13, 0, headerView.frame.size.width*0.20, headerView.frame.size.height)];
//    lbl.textAlignment = NSTextAlignmentLeft;
//    lbl.font = [UIFont fontWithName:@"OpenSans" size:16.91];
//    lbl.textColor = [UIColor grayColor];
//    lbl.text = @"TOPIC:";
//    lbl.backgroundColor = [UIColor clearColor];
//    UILabel *lbl1 = [[UILabel alloc] initWithFrame:CGRectMake(lbl.frame.origin.x+lbl.frame.size.width, 0, headerView.frame.size.width*0.80, headerView.frame.size.height)];
//    lbl1.textAlignment = NSTextAlignmentLeft;
//    lbl1.backgroundColor = [UIColor clearColor];
//    lbl1.font = [UIFont fontWithName:@"OpenSans-Italic" size:16.91];
//    lbl1.textColor = [UIColor grayColor];
//    lbl1.text = @"Asylum";
//
//    [headerView addSubview:lbl];
//    [headerView addSubview:lbl1];
//    return headerView;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 40.0f;
//}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return subTopicsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SubTopic *subtopic = [subTopicsArray objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    cell.textLabel.text = subtopic.subtopicTitle;//[subTopicsArray objectAtIndex:indexPath.row];//[NSString stringWithFormat:@"SubTopic SubTopic SubTopic SubTopic SubTopic SubTopic SubTopic SubTopic SubTopic SubTopic %ld",indexPath.row+1];
    cell.textLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:14.5];
    cell.textLabel.textColor = UIColorFromRGB(0X124A7F);  //VAMSHI //[UIColor darkGrayColor];
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];

    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        [_objects removeObjectAtIndex:indexPath.row];
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
//    }
//}

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    
    SubTopic *selectedSubTopic = [subTopicsArray objectAtIndex:indexPath.row];
    
    if ([selectedSubTopic.contentType isEqual:@"Text"])
    {
         DescVC= [storyboard instantiateViewControllerWithIdentifier:@"ImmigrationDescVC"];
        DescVC.subtopic = selectedSubTopic;
        [self.navigationController pushViewController:DescVC animated:YES];
        
    }else if ([selectedSubTopic.contentType isEqual:@"Checklist"])
    {
        ImmigrationCheckListVC* checkListVC = [storyboard instantiateViewControllerWithIdentifier:@"ImmigrationCheckListVC"];
        checkListVC.subtopic = selectedSubTopic;
        [self.navigationController pushViewController:checkListVC animated:YES];
        
    }else if ([selectedSubTopic.contentType isEqual:@"Links"])
    {
        linkVC = [storyboard instantiateViewControllerWithIdentifier:@"ImmigrationLinkVC"];
        linkVC.subtopic = selectedSubTopic;
        [self.navigationController pushViewController:linkVC animated:YES];
    }
    
//    toVC.topicTitle = topic.topicTitle;
}
#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        bgView.frame = viewFrame;
    }
    
    //    }
}


- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
@end
