//
//  SplashViewController.h
//  Immigo
//
//  Created by pradeep ISPV on 2/27/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PartnersPullableView.h"

@interface SplashViewController : UIViewController <PullableViewDelegate>
{
    PartnersPullableView *pullUpView;

}
@end
