//
//  ImmigrationCheckListVC.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/2/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubTopic.h"

@interface ImmigrationCheckListVC : UIViewController
{
    SubTopic *_subtopic;
    IBOutlet UIButton *titleheading;
    IBOutlet UILabel *sepratorLine;
}
@property (nonatomic,strong) NSString *topicTitle;
@property (nonatomic,strong) NSString *subTopicTitle;

@property (nonatomic,strong) SubTopic *subtopic;

@end
