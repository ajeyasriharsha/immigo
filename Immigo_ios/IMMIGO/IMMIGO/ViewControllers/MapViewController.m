//
//  MapViewController.m
//  Immigo
//
//  Created by pradeep ISPV on 3/11/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "MapViewController.h"
#import "Common.h"

@interface MapViewController ()
{
    
    IBOutlet UIButton *heading;
}
@end

@implementation MapViewController
@synthesize resultsNear,areaName,listButton;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        NSLog(@"runnig on < 7.0");
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"home"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        UIImage *image = [UIImage imageNamed:@"question"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = rightItem;
        
    }

        heading.titleLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:16.785f];
    {
        self.navigationController.navigationBarHidden = NO;
        UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
        UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
        titlView.image = titleLogo;
        self.navigationItem.titleView = titlView;
        //        pollsTable.rowHeight = 60.0f;
    }
    {//map view related code
        legalMapView.showsUserLocation = YES;

    }
    resultsNear.textColor = UIColorFromRGB(UI_LIST_TEXT_COLOR);
    resultsNear.font = [UIFont fontWithName:@"OpenSans-SemiboldItalic" size:14.3f];
    areaName.font = [UIFont fontWithName:@"OpenSans" size:13.7f];
    
    areaName.text = @"500 Streetname Granville MI";
    
    UIImage *mapBg = [UIImage imageNamed:@"list_bg"];
    listButton.titleLabel.font = [UIFont fontWithName:@"OpenSans" size:13.7f];
    [listButton setBackgroundImage:[mapBg stretchableImageWithLeftCapWidth:mapBg.size.width/2 topCapHeight:mapBg.size.height/2] forState:UIControlStateNormal];
    [listButton setBackgroundImage:[mapBg stretchableImageWithLeftCapWidth:mapBg.size.width/2 topCapHeight:mapBg.size.height/2] forState:UIControlStateSelected];

	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)listButtontapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];

}
- (void)mapViewWillStartLoadingMap:(MKMapView *)mapView
{
    
}
- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
{
    
}

@end
