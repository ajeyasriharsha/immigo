//
//  CommunityPollVC.h
//  Immigo
//
//  Created by pradeep ISPV on 3/6/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PollQuestionsModel.h"
#import "DataDownloader.h"
#import "PollCell.h"
@interface CommunityPollVC : UIViewController <DataDownloaderDelegate>
{
    IBOutlet UITableView *pollsTable;
    
}
@end
