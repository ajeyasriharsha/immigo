//
//  TrainingsDetailVC.m
//  Immigo
//
//  Created by pradeep ISPV on 3/10/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "TrainingsDetailVC.h"
#import "AboutUsVC.h"
#import "EventsModel.h"
#import "Categories.h"
#import <MessageUI/MessageUI.h>
#import "Common.h"
#import "TrainingsDetailContentVC.h"
#import "NSDate+Helper.h"
#import "MyCalendar.h"
@interface TrainingsDetailVC () <MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>
{
    IBOutlet UIButton *addtoCal;
    IBOutlet UIButton *heading;
    IBOutlet UIWebView *contentWebView;
    IBOutlet UIScrollView *detailScrollView;
    IBOutlet UILabel *descLabel;
    __weak IBOutlet UIView *bgView;
    IBOutlet UIButton *contentBtn;
}
@end

@implementation TrainingsDetailVC
@synthesize selectedEvent,selectedEventLocation,eventStore,recursiveEventEndDate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //    selectedEvent = [EventsModel sharedDataSource].selectedEvent;
    //NSLog(@"event %@",selectedEvent.title);
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        //NSLog(@"runnig on < 7.0");
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"left arrow"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        UIImage *image = [UIImage imageNamed:@"question"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = rightItem;
        
        [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];
        
    }
    
    heading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
    contentBtn.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:14.785f];
    {//navigation bar customization
        self.navigationController.navigationBarHidden = NO;
//        UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//        UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//        titlView.image = titleLogo;
//        self.navigationItem.titleView = titlView;
    }
    
    UIImage *btnBg = [UIImage imageNamed:@"RedBtn_Bg"];
    [addtoCal setBackgroundImage:[btnBg stretchableImageWithLeftCapWidth:btnBg.size.width/2 topCapHeight:btnBg.size.height/2] forState:UIControlStateNormal];
    addtoCal.contentMode = UIViewContentModeRedraw;
    addtoCal.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.06f];
    //    addtoCal.imageEdgeInsets = UIEdgeInsetsMake(<#CGFloat top#>, <#CGFloat left#>, <#CGFloat bottom#>, <#CGFloat right#>);
    
    //webview related
    {
        [contentWebView loadHTMLString:selectedEvent.content baseURL:nil];
    }
    
    NSString *address = @"";
    if ([selectedEvent.locationStreetAddress length]>0)
    {
        address = selectedEvent.locationStreetAddress;
        address = [address stringByAppendingString:@","];
    }
    if ([selectedEvent.locationStreetAddress2 length]>0)
    {
        address = [address stringByAppendingString:selectedEvent.locationStreetAddress2];
        address = [address stringByAppendingString:@","];
    }
    if ([selectedEvent.locationCity length]>0)
    {
        address = [address stringByAppendingString:selectedEvent.locationCity];
        address = [address stringByAppendingString:@","];
    }
    if ([selectedEvent.locationState length]>0) {
        address = [address stringByAppendingString:selectedEvent.locationState];
        address = [address stringByAppendingString:@","];
    }
    if ([selectedEvent.locationCountry length]>0) {
        address = [address stringByAppendingString:selectedEvent.locationCountry];
    }
    
    //    trainingsPlace.text = address;
    //
    //    trainingDesc.font = [UIFont fontWithName:@"OpenSans-Regular" size:16.785f];
    //    trainingDesc.text = [NSString stringWithFormat:@"%@ \n%@",[selectedEvent.title trimmedString],[address trimmedString]];
    
    CGFloat headingsGap = 8.0f;
    CGPoint cursor = CGPointMake(20,7);
    CGSize size = CGSizeMake(self.view.frame.size.width-cursor.x-cursor.x, self.view.frame.size.height);
    
    {
        UILabel *newsTitle = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, 40.0f)];
        newsTitle.autoresizingMask =UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
        newsTitle.text = [selectedEvent.title trimmedString];
        newsTitle.font = [UIFont fontWithName:FONT_REGULAR size:14.9f];
        newsTitle.numberOfLines = 0;
        
        CGSize newsTitleSize = [newsTitle sizeOfMultiLineLabel];
        CGRect newsTitleFrame = newsTitle.frame;
        newsTitleFrame.size.height = newsTitleSize.height;
        newsTitle.frame = newsTitleFrame;
        
        newsTitle.backgroundColor = [UIColor clearColor];
        [detailScrollView addSubview:newsTitle];
        
        cursor.y +=newsTitle.frame.size.height+headingsGap;
    }
//    { // location label
//
//        UILabel *locationLbl = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, 20.0f)];
//        locationLbl.autoresizingMask =UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
//        locationLbl.text = @"Location:";
//        locationLbl.font = [UIFont fontWithName:@"OpenSans-SemiBold" size:13.08f];
//        locationLbl.backgroundColor = [UIColor clearColor];
//        [detailScrollView addSubview:locationLbl];
//        cursor.x = 20.0f;
//        cursor.y +=locationLbl.frame.size.height;
//    }
    
    { // address label
        UIImage *image = [UIImage imageNamed:@"location_icon"];
        {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(cursor.x-2, cursor.y+3, image.size.width, image.size.height)];
            imageView.image = image;
            [detailScrollView addSubview:imageView];
            cursor.x +=imageView.frame.size.width;
        }

        UILabel *addressLbl = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width-image.size.width-4, 40.0f)];
        addressLbl.autoresizingMask =UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
        addressLbl.text = [address trimmedString];
        addressLbl.font = [UIFont fontWithName:FONT_REGULAR size:13.7f];
        addressLbl.numberOfLines = 0;
        
        CGSize newsTitleSize = [addressLbl sizeOfMultiLineLabel];
        CGRect newsTitleFrame = addressLbl.frame;
        newsTitleFrame.size.height = newsTitleSize.height;
        addressLbl.frame = newsTitleFrame;
        self.selectedEventLocation=addressLbl.text;
        [detailScrollView addSubview:addressLbl];
        cursor.x = 20.0f;
        cursor.y +=addressLbl.frame.size.height+headingsGap;
    }
    { // timings label
        UILabel *timingsLbl = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, 20.0f)];
        timingsLbl.autoresizingMask =UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
        timingsLbl.text = @"Date and Time:";
        timingsLbl.font = [UIFont fontWithName:FONT_SEMIBOLD size:13.08f];
        timingsLbl.numberOfLines = 0;
        
        [detailScrollView addSubview:timingsLbl];
        cursor.y +=timingsLbl.frame.size.height;
    }
    { // time label
        UILabel *timeLbl = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, 40.0f)];
        timeLbl.autoresizingMask =UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
        if (selectedEvent.dailyStartTime && selectedEvent.dailyEndTime)
        {
            NSString *datesString;
            NSString *startDtfString;
            NSString *endDtfString;
            NSDate *startformatDate;
            NSDate *endFormatDate;
         //   NSString *timeZoneString =[[NSTimeZone systemTimeZone] localizedName:NSTimeZoneNameStyleGeneric locale:[NSLocale currentLocale]];
            NSString *timeZoneString=[[NSTimeZone systemTimeZone]name];
            
            NSString *startTime;
            NSString *endTime;
            if ([[selectedEvent.startDate displayDateFormat] isEqual:[selectedEvent.endDate displayDateFormat]])
            {
                startformatDate =[self getDateFromString:[selectedEvent.startDate displayDateFormat] withTime:selectedEvent.dailyStartTime];
                endFormatDate = [self getDateFromString:[selectedEvent.startDate displayDateFormat] withTime:selectedEvent.dailyEndTime];
                startDtfString = [NSDate stringFromDate:startformatDate];
                endDtfString =[NSDate stringFromDate:endFormatDate];
                NSString *dbDateString = [NSDate stringFromDate:startformatDate withFormat:@"YYYY-MM-dd"];
                datesString = [dbDateString displayDateFormat];
            }
            else
            {
              //  datesString = [NSString stringWithFormat:@"%@ - %@",[selectedEvent.startDate displayDateFormat],[selectedEvent.endDate displayDateFormat]];
                startformatDate =[self getDateFromString:[selectedEvent.startDate displayDateFormat] withTime:selectedEvent.dailyStartTime];
                endFormatDate = [self getDateFromString:[selectedEvent.endDate displayDateFormat] withTime:selectedEvent.dailyEndTime];
                startDtfString = [NSDate stringFromDate:startformatDate];
                endDtfString =[NSDate stringFromDate:endFormatDate];
                NSString *startDateString = [NSDate stringFromDate:startformatDate withFormat:@"YYYY-MM-dd"];
                NSString *endDateString = [NSDate stringFromDate:endFormatDate withFormat:@"YYYY-MM-dd"];
                
                //TODO:NEEd to vchange the dates and check the condition to disoply only one
                
                
                NSArray *allDatesBetween =[self getDatesBetweenTwoDates:startformatDate andEndDate:endFormatDate];
                if ([allDatesBetween count]>1){
                     datesString = [NSString stringWithFormat:@"%@ - %@",[startDateString displayDateFormat],[endDateString displayDateFormat]];
                    
                }else{
                    datesString = [NSString stringWithFormat:@"%@",[startDateString displayDateFormat]];
                    
                }
               

            }
            
         //   timeLbl.text = [NSString stringWithFormat:@"%@ (%@ - %@) (%@)",datesString,[selectedEvent.dailyStartTime get12HrStringFrom],[selectedEvent.dailyEndTime get12HrStringFrom],[NSString stringWithFormat:@"%@",timeZoneString]];
             startTime =[NSString stringWithFormat:@"%d:%d",[startformatDate hour],[startformatDate minute]];
             endTime =[NSString stringWithFormat:@"%d:%d",[endFormatDate hour],[endFormatDate minute]];
             timeLbl.text = [NSString stringWithFormat:@"%@ (%@ - %@) (%@) ",datesString,[startTime get12HrStringFrom],[endTime get12HrStringFrom],timeZoneString];
        }
        else
        {
            if ([[selectedEvent.startDate displayDateFormat] isEqual:[selectedEvent.endDate displayDateFormat]])
            {
                timeLbl.text = [NSString stringWithFormat:@"%@",[selectedEvent.startDate displayDateFormat]];
            }
            else
            {
                timeLbl.text = [NSString stringWithFormat:@"%@ - %@",[selectedEvent.startDate displayDateFormat],[selectedEvent.endDate displayDateFormat]];
            }
        }
        timeLbl.font = [UIFont fontWithName:FONT_REGULAR size:13.08f];
        timeLbl.frame = [self frameForLabel:timeLbl WithText:timeLbl.text];
        timeLbl.numberOfLines = 0;
        
        [detailScrollView addSubview:timeLbl];
        cursor.y +=timeLbl.frame.size.height+headingsGap;
    }
    if ([selectedEvent.contactName length]>0 || [selectedEvent.contactPhone length]>0 || [selectedEvent.contactEmail length]>0)
    {
        { // contact label
            UILabel *contactLbl = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, 20.0f)];
            contactLbl.autoresizingMask =UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
            contactLbl.text = @"Contact: ";
            contactLbl.font = [UIFont fontWithName:FONT_SEMIBOLD size:13.08f];
            contactLbl.numberOfLines = 0;
            
            [detailScrollView addSubview:contactLbl];
            cursor.y +=contactLbl.frame.size.height;
        }
        if ([selectedEvent.contactName length]>0)
        { // name label
            UILabel *nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, 25.0f)];
            nameLbl.autoresizingMask =UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
            nameLbl.text = [selectedEvent.contactName trimmedString];
            nameLbl.font = [UIFont fontWithName:FONT_REGULAR size:13.7f];
            nameLbl.numberOfLines = 0;
            
            CGSize newsTitleSize = [nameLbl sizeOfMultiLineLabel];
            CGRect newsTitleFrame = nameLbl.frame;
            newsTitleFrame.size.height = newsTitleSize.height;
            nameLbl.frame = newsTitleFrame;
            
            [detailScrollView addSubview:nameLbl];
            cursor.y +=nameLbl.frame.size.height;
        }
        if ([selectedEvent.contactEmail length]>0)
        {
            UIImage *image = [UIImage imageNamed:@"mail_icon"];
            {
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(cursor.x, cursor.y+3, image.size.width, image.size.height)];
                imageView.image = image;
                [detailScrollView addSubview:imageView];
                cursor.x +=imageView.frame.size.width+4;
            }

            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.autoresizingMask =UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
            [button addTarget:self action:@selector(tappedOnMail:) forControlEvents:UIControlEventTouchUpInside];
            [button setTitle:[selectedEvent.contactEmail trimmedString] forState:UIControlStateNormal];
            button.tag = 100;
            button.titleLabel.numberOfLines = 3;
            [button.titleLabel setFont:[UIFont fontWithName:FONT_REGULAR size:13.08f]];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            button.frame = CGRectMake(cursor.x, cursor.y, size.width-image.size.width-4, 25.0f);
            button.backgroundColor = [UIColor clearColor];
            
            [detailScrollView addSubview:button];
            cursor.x = 20.0f;
            cursor.y +=button.frame.size.height+3;
        }

        if ([selectedEvent.contactPhone length]>0)
        {
            UIImage *image = [UIImage imageNamed:@"phone_icon"];
            {
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(cursor.x, cursor.y+3, image.size.width, image.size.height)];
                imageView.image = image;
                [detailScrollView addSubview:imageView];
                cursor.x +=imageView.frame.size.width+4;
            }

            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.autoresizingMask =UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
            [button addTarget:self action:@selector(tappedOnPhone:) forControlEvents:UIControlEventTouchUpInside];
            //NSLog(@"phone %@",selectedEvent.contactPhone);
            NSString *phoneNumber = [selectedEvent.contactPhone trimmedString];
            [button setTitle:[phoneNumber frameContactNumber] forState:UIControlStateNormal];
            button.tag = 100;
            button.titleLabel.numberOfLines = 3;
            [button.titleLabel setFont:[UIFont fontWithName:FONT_REGULAR size:13.08f]];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            button.frame = CGRectMake(cursor.x, cursor.y, size.width-image.size.width-4, 25.0f);
            button.backgroundColor = [UIColor clearColor];
            [detailScrollView addSubview:button];
            cursor.x = 20.0f;
            cursor.y +=button.frame.size.height;
        }
        
    }
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.autoresizingMask =UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
        [button addTarget:self action:@selector(tappedOnContentDesc:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:NSLocalizedString(@"MOREINFORMATION_TITLE", @"") forState:UIControlStateNormal];
        button.tag = 100000;
        button.titleLabel.numberOfLines = 3;
        [button.titleLabel setFont:[UIFont fontWithName:FONT_REGULAR size:14.08f]];
        //            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [button setTitleColor:UIColorFromRGB(UI_TITLE_HEADER_COLOR) forState:UIControlStateNormal];
        button.frame = CGRectMake(cursor.x, cursor.y, size.width, 40.0f);
        button.backgroundColor = [UIColor clearColor];
        [detailScrollView addSubview:button];
        
        cursor.y +=button.frame.size.height;
    }
    
    detailScrollView.contentSize = CGSizeMake(detailScrollView.frame.size.width, cursor.y+30);
    
    descLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:13.08f];
    //    CGRect scrollViewFrame = detailScrollView.frame;
    //
    //    if (detailScrollView.contentSize.height < scrollViewFrame.size.height)
    //    {
    //        scrollViewFrame.size.height = detailScrollView.contentSize.height;
    //
    //        CGRect webViewFrame = contentWebView.frame;
    //        webViewFrame.origin.y = scrollViewFrame.size.height+1;
    //        contentWebView.frame = webViewFrame;
    //
    //    }
    
    
    
	// Do any additional setup after loading the view.
}


-(CGRect)frameForLabel:(UILabel *)label WithText:(NSString *)text
{
    CGSize maximumLabelSize = CGSizeMake(label.frame.size.width, FLT_MAX);
    CGSize expectedLabelSize = [text sizeWithFont:label.font constrainedToSize:maximumLabelSize lineBreakMode:label.lineBreakMode];
    //adjust the label the the new height.
    CGRect newFrame = label.frame;
    newFrame.size.height = expectedLabelSize.height<20.0f ? 20.0f :expectedLabelSize.height;
    //    label.frame = newFrame;
    
    return newFrame;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self customizeNavigationBar];
    
}
-(void)customizeNavigationBar{
    
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    
    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
    [backView setBackgroundColor:[UIColor clearColor]];
    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
    [backView addSubview:titleImageView];
    titleImageView.contentMode = UIViewContentModeCenter;
    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        bgView.frame = viewFrame;
        
    }
}
-(IBAction)tappedOnPhone:(UIButton *)sender
{
    //NSLog(@"Phone Tapped");
    if ([[selectedEvent.contactPhone trimmedString] length]>0)
    {
        if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        {
            
            NSString *fianlphoneNumber = [[selectedEvent.contactPhone trimmedString] frameContactNumber];
            NSString *phoneNumDecimalsOnly = [[fianlphoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
            
            NSString *phoneNumber = [@"telprompt://" stringByAppendingString:phoneNumDecimalsOnly];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
            
        } else {
            
            UIAlertView *warning =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"TRAININGS_AND_EVENTS_TITLE", @"") message:NSLocalizedString(@"DEVICE_NOT_SUPPORTED_ERROR", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
            
            [warning show];
        }
    }
}
-(IBAction)tappedOnMail:(UIButton *)sender
{
//    if (![Common currentNetworkStatus])
//    {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Trainings & Events" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alertView show];
//        return;
//    }
    //NSLog(@"MAIL Tapped");
    if ([MFMailComposeViewController canSendMail])
	{
		// set the sendTo address
		NSMutableArray *recipients = [[NSMutableArray alloc] initWithCapacity:1];
		[recipients addObject:selectedEvent.contactEmail];
		
		MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
		controller.mailComposeDelegate = self;
        //		[controller setSubject:@"Hi Its Wrkng"];
		
		[controller setToRecipients:recipients];
		
		if ([self respondsToSelector:@selector(presentViewController:animated:completion:)])
        {
            
            
//            UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//            UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//            titlView.image = titleLogo;
//            controller.navigationItem.titleView = titlView;
//            [[controller navigationBar] sendSubviewToBack:titlView];
            [[controller navigationBar] setTintColor:[UIColor blueColor]];
            /*
             [[controller navigationBar] setTintColor:[UIColor whiteColor]];
             UIImage *image = [UIImage imageNamed: @"logo.png"];
             UIImageView * iv = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,320,42)];
             iv.image = image;
             iv.contentMode = UIViewContentModeCenter;
             [[[controller viewControllers] lastObject] navigationItem].titleView = iv;
             [[controller navigationBar] sendSubviewToBack:iv];
             */
            [self presentViewController:controller animated:YES completion:nil];
        }
	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"TRAININGS_AND_EVENTS_TITLE", @"") message:NSLocalizedString(@"NOEMAIL_ACCOUNT_FOUND", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles: nil];
		[alert show];
		
	}
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	switch (result)
	{
		case MFMailComposeResultCancelled:
		{
			break;
		}
		case MFMailComposeResultSaved:
		{
			break;
		}
		case MFMailComposeResultSent:
		{
			break;
		}
		case MFMailComposeResultFailed:
		{
			break;
		}
		default:
		{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"TRAININGS_AND_EVENTS_TITLE", @"") message:NSLocalizedString(@"EMAIL_FAILED", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles: nil];
			[alert show];
            
		}
			break;
	}
	if ([self respondsToSelector:@selector(dismissModalViewControllerAnimated:)]) {
        [self performSelector:@selector(dismissModalViewControllerAnimated:) withObject:[NSNumber numberWithBool:YES]];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
	
	
}
#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        bgView.frame = viewFrame;
    }
    
    //    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];
}
-(IBAction)tappedOnContentDesc:(id)sender
{
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    TrainingsDetailContentVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"TrainingsDetailContentVC"];
    toVC.selectedEvent = selectedEvent;
    [self.navigationController pushViewController:toVC animated:YES];
    
}
- (IBAction)addToCalendarTapped:(id)sender
{
    //    //NSLog(@"Add to Calendar:%@",selectedEvent.location);
    //NSLog(@"Event Start Date:%@",selectedEvent.startDate);
    //NSLog(@"Event End Date:%@",selectedEvent.endDate);
    //NSLog(@"Event Start time:%@",selectedEvent.dailyStartTime);
    //NSLog(@"Event End time:%@",selectedEvent.dailyEndTime);
//    NSArray* stringComponents = [selectedEvent.dailyStartTime componentsSeparatedByString:@":"];
    //NSLog(@"Hour Component %@",[stringComponents objectAtIndex:0]);
    //NSLog(@"Minutes Component %@",[stringComponents objectAtIndex:1]);
    NSDate *startDate;
    NSDate *endDate;
    if ([selectedEvent.startDate displayDateFormat] .length>0) {
        startDate=[self getDateFromString:[selectedEvent.startDate displayDateFormat] withTime:selectedEvent.dailyStartTime isStartTime:YES];
    }
    if ([selectedEvent.endDate displayDateFormat].length>0) {
        endDate=[self getDateFromString:[selectedEvent.endDate displayDateFormat] withTime:selectedEvent.dailyEndTime isStartTime:NO];
    }
    NSArray *allDatesBetween =[self getDatesBetweenTwoDates:startDate andEndDate:endDate];
    if ([allDatesBetween count]>1) {
        NSDate *eventStartDate=[self getDateFromString:[selectedEvent.startDate displayDateFormat] withTime:selectedEvent.dailyStartTime isStartTime:YES];
        NSDate *eventEndDate=[self getDateFromString:[selectedEvent.startDate displayDateFormat] withTime:selectedEvent.dailyEndTime isStartTime:NO];
        self.recursiveEventEndDate =[self getDateFromString:[selectedEvent.endDate displayDateFormat] withTime:selectedEvent.dailyEndTime isStartTime:NO];
        [self addToCalendarEvent:eventStartDate andEndDtae:eventEndDate inLocation:self.selectedEventLocation withTitle:self.selectedEvent.title];
    }
    else {
        
        [self addToCalendarEvent:startDate andEndDtae:endDate inLocation:self.selectedEventLocation withTitle:self.selectedEvent.title];
    }
    
    
}

-(NSArray *)getDatesBetweenTwoDates:(NSDate *)startDate andEndDate:(NSDate *)endDate{
    
    NSMutableArray *dates = [NSMutableArray array];
    NSDate *curDate = startDate;
    while([curDate timeIntervalSince1970] <= [endDate timeIntervalSince1970]) //you can also use the earlier-method
    {
        [dates addObject:curDate];
        curDate = [NSDate dateWithTimeInterval:86400 sinceDate:curDate];//Adding one day to the Current Date
        
    }
    ////NSLog(@"Dates Between %@ and %@ are %@",startDate,endDate,dates);
    return dates;
}

-(NSDate*)getDateFromString:(NSString *)dateString withTime:(NSString *)time {
    
    NSDateFormatter *reDateFormatter = [[NSDateFormatter alloc] init];
    [reDateFormatter setDateFormat:@"MMM d, yyyy"];
    NSDate *date = [reDateFormatter dateFromString:dateString];
    NSInteger hourVal=0;
    NSInteger minuteVal=0;
    NSCalendar *calendar=[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components=[calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    NSArray* stringComponents=[time componentsSeparatedByString:@":"];
    NSString *eventTme =[stringComponents objectAtIndex:1];
    
    //    if ([eventTme length]>0)
    {
        
        if ([eventTme rangeOfString:@"pm" options:NSCaseInsensitiveSearch].location == NSNotFound)
        {
            //NSLog(@"Event time is Morning");
            
            hourVal=[[stringComponents objectAtIndex:0] integerValue];
            minuteVal=[[stringComponents objectAtIndex:1] integerValue];
        }
        else
        {
            hourVal=[[stringComponents objectAtIndex:0] integerValue]+12;
            minuteVal=[[stringComponents objectAtIndex:1] integerValue];
            //NSLog(@"Event time is Night");
        }
    }
    
    [components setHour:hourVal];
    [components setMinute:minuteVal];
    NSDate *finalDate = [calendar dateFromComponents:components];
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:finalDate];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:finalDate];
    NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:finalDate];
    
    return destinationDate;
}
/*
 Change With DAylight Savings Implemented
-(NSDate*)getDateFromString:(NSString *)dateString withTime:(NSString *)time {
    
    NSDateFormatter *reDateFormatter = [[NSDateFormatter alloc] init];
    [reDateFormatter setDateFormat:@"MMM d, yyyy"];
    NSDate *date = [reDateFormatter dateFromString:dateString];
    NSInteger hourVal=0;
    NSInteger minuteVal=0;
    NSCalendar *calendar=[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components=[calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    NSArray* stringComponents=[time componentsSeparatedByString:@":"];
    NSString *eventTme =[stringComponents objectAtIndex:1];
    
//    if ([eventTme length]>0)
    {
        
        if ([eventTme rangeOfString:@"pm" options:NSCaseInsensitiveSearch].location == NSNotFound)
        {
            //NSLog(@"Event time is Morning");
            
            hourVal=[[stringComponents objectAtIndex:0] integerValue];
            minuteVal=[[stringComponents objectAtIndex:1] integerValue];
        }
        else
        {
            hourVal=[[stringComponents objectAtIndex:0] integerValue]+12;
            minuteVal=[[stringComponents objectAtIndex:1] integerValue];
            //NSLog(@"Event time is Night");
        }
    }
    
    [components setHour:hourVal];
    [components setMinute:minuteVal];
    NSDate *finalDate = [calendar dateFromComponents:components];
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    
    int daylightOffset =  [[NSTimeZone localTimeZone] daylightSavingTimeOffset];
 
    
    NSInteger  gmtOffsetFinal;
    NSInteger currentGMTOffset;
    NSTimeInterval gmtInterval;
    gmtOffsetFinal =  [[NSTimeZone localTimeZone] secondsFromGMT];
    currentGMTOffset = [currentTimeZone secondsFromGMTForDate:finalDate];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:finalDate];
    if ([[NSTimeZone localTimeZone] isDaylightSavingTime]) {
        //Contains DST
        gmtInterval = (currentGMTOffset - gmtOffset)-daylightOffset;
    }else {
        
        gmtInterval = currentGMTOffset - gmtOffset;
    }
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:finalDate];

    return destinationDate;
}
 
 */
-(NSDate*)getDateFromString:(NSString *)dateString withTime:(NSString *)time isStartTime:(BOOL)isStart{
    
    NSDateFormatter *reDateFormatter = [[NSDateFormatter alloc] init];
    [reDateFormatter setDateFormat:@"MMM d, yyyy"];
    NSDate *date = [reDateFormatter dateFromString:dateString];
    NSInteger hourVal;
    NSInteger minuteVal;
    NSCalendar *calendar=[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components=[calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    NSArray* stringComponents=[time componentsSeparatedByString:@":"];
    NSString *eventTme =[stringComponents objectAtIndex:1];
    
    
    if (eventTme!=nil) {
        
        if ([eventTme rangeOfString:@"pm" options:NSCaseInsensitiveSearch].location == NSNotFound)
        {
            //NSLog(@"Event time is Morning");
            
            hourVal=[[stringComponents objectAtIndex:0] integerValue];
            minuteVal=[[stringComponents objectAtIndex:1] integerValue];
        }
        else
        {
            hourVal=[[stringComponents objectAtIndex:0] integerValue]+12;
            minuteVal=[[stringComponents objectAtIndex:1] integerValue];
            //NSLog(@"Event time is Night");
        }
    }
    else{
        
        if(isStart){
            
            hourVal=9;
            minuteVal=0;
        }
        else{
            
            hourVal=17;
            minuteVal=0;
        }
        
    }
    
    [components setHour:hourVal];
    [components setMinute:minuteVal];
    NSDate *finalDate = [calendar dateFromComponents:components];
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:finalDate];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:finalDate];
    NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:finalDate];

    /*
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"dd-MMM-yyyy hh:mm"];
    [dateFormatters setDateStyle:NSDateFormatterShortStyle];
    [dateFormatters setTimeStyle:NSDateFormatterShortStyle];
    [dateFormatters setDoesRelativeDateFormatting:YES];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    NSString * dateStr = [dateFormatters stringFromDate: destinationDate];
    //NSLog(@"DateString : %@", dateStr);
     */
    //NSLog(@"Required Final Date is :%@",destinationDate);
    return destinationDate;
    
}
-(void)addToCalendarEvent: (NSDate *)startDate andEndDtae:(NSDate *)endDate inLocation:(NSString *)eventLocation withTitle:(NSString *)eventTitle{
    
    self.eventStore = [[EKEventStore alloc] init];
    
    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        /* This code will run when uses has made his/her choice */
        
        if (error)
        {
            // display error message here
        }
        else if (!granted)
        {
            // display access denied error message here
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"ALLOW_CALENDAR_PERMISSIONS_ERROR", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", nil)  otherButtonTitles: nil] show];
                return ;
            });
        }
        else
        {
            // access granted
            
            
            EKSource *localSource = nil;
            EKCalendar *cal;
            NSString *calendarIdentifier = [[NSUserDefaults standardUserDefaults] valueForKey:@"my_calendar_identifier"];
            // when identifier exists, my calendar probably already exists
            // note that user can delete my calendar. In that case I have to create it again.
            if (calendarIdentifier) {
                
                for (EKCalendar *aCal in [eventStore calendarsForEntityType:EKEntityTypeEvent]) {
                    
                    if([aCal.calendarIdentifier isEqualToString:calendarIdentifier])
                    {
                        cal = aCal;
                        break;
                    }
                    //cal = [eventStore calendarWithIdentifier:calendarIdentifier];
                }
               // cal = [eventStore calendarWithIdentifier:calendarIdentifier];
            }
            if (!cal) {
                
                cal = [EKCalendar calendarForEntityType:EKEntityTypeEvent eventStore:eventStore];
                
                // set calendar name. This is what users will see in their Calendar app
                [cal setTitle:NSLocalizedString(@"APP_NAME", nil)];
                
                // find appropriate source type. I'm interested only in local calendars but
                // there are also calendars in iCloud, MS Exchange, ...
                // look for EKSourceType in manual for more options
                for (EKSource *source in eventStore.sources)
                {
                    if (source.sourceType == EKSourceTypeCalDAV && [source.title isEqualToString:@"iCloud"])
                    {
                        localSource = source;
                        cal.source =source;
                        break;
                    }
                }
                if (localSource == nil)
                {
                    for (EKSource *source in eventStore.sources) {
                        if (source.sourceType == EKSourceTypeLocal)
                        {
                            localSource = source;
                            cal.source =source;
                            break;
                        }
                    }
                }
                
                // save this in NSUserDefaults data for retrieval later
                NSString *calendarIdentifier = [cal calendarIdentifier];
                NSError *error = nil;
                BOOL saved = [eventStore saveCalendar:cal commit:YES error:&error];
                if (saved) {
                    // http://stackoverflow.com/questions/1731530/whats-the-easiest-way-to-persist-data-in-an-iphone-app
                    // saved successfuly, store it's identifier in NSUserDefaults
                    [[NSUserDefaults standardUserDefaults] setObject:calendarIdentifier forKey:@"my_calendar_identifier"];
                } else {
                    // unable to save calendar
                    //return NO;
                }
            }
            
            // this shouldn't happen
            if (!cal) {
                //return NO;
            }
            
            
            //calendar properties
            //NSLog(@"Calendar Properties %@", cal);
            
            //Add Event to Calendar
            //NSLog(@"Adding event!");
            EKEvent *event  = [EKEvent eventWithEventStore:eventStore];
            event.title     = eventTitle;
            event.calendar = cal;
            NSArray *allDatesBetween =[self getDatesBetweenTwoDates:startDate andEndDate:self.recursiveEventEndDate];
            if ([allDatesBetween count]>1) {
                EKRecurrenceEnd *recurrenceEnd =[EKRecurrenceEnd recurrenceEndWithEndDate:self.recursiveEventEndDate];
                EKRecurrenceRule *er =[[EKRecurrenceRule alloc] initRecurrenceWithFrequency:EKRecurrenceFrequencyDaily interval:1 end:recurrenceEnd];
                [event addRecurrenceRule:er];
                
            }
            event.calendar = cal;
            event.allDay = NO;
            event.startDate = startDate;
            event.endDate = endDate;
            event.location =eventLocation;
            
            NSError *error = nil;
            BOOL result = [eventStore saveEvent:event span:EKSpanThisEvent commit:YES error:&error];
            if (result) {
                //NSLog(@"Saved event to event store.");
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"SUCCESS_TITLE", @"") message:NSLocalizedString(@"ADD_EVENT_SUCCESS_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", nil) otherButtonTitles: nil];
                    [alert show];
                    return ;
                    
                });
            } else {
                NSLog(@"Error saving event: %@.", error);
                /*
                 dispatch_async(dispatch_get_main_queue(), ^{
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure!" message:@"Could Not Add Event" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                 [alert show];      return ;  });
                 */
            }
        }
        
        
    }];

}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
@end
