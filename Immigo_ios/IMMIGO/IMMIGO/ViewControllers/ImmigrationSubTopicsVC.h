//
//  ImmigrationSubTopicsVC.h
//  Immigo
//
//  Created by pradeep ISPV on 3/7/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Topic.h"

@interface ImmigrationSubTopicsVC : UIViewController
{
    IBOutlet UIButton *heading;
    IBOutlet UILabel *sepratorLine;
    
}
@property (nonatomic,retain) NSString *headerTitle;
@property (nonatomic,strong) Topic *topic;
@end
