//
//  TrainingsViewController.h
//  Immigo
//
//  Created by pradeep ISPV on 3/6/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import "ShareToSocialNetworkingSites.h"
@interface TrainingsViewController : UIViewController <SWTableViewCellDelegate>
{
    IBOutlet UIButton *heading;
    
    IBOutlet UITableView *trainingsTable;
    ShareToSocialNetworkingSites *shareObj;
}
@end
