//
//  MyNavigationController.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/30/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "MyNavigationController.h"

@interface MyNavigationController ()
{
    NSMutableArray *waitingList;
}
@end

@implementation MyNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- ( void )  navigationController    : ( UINavigationController* ) pNavigationController
            didShowViewController   : ( UIViewController*       ) pController
            animated                : ( BOOL                    ) pAnimated
{
    
    if ( [ waitingList count ] > 0 ) [ waitingList removeObjectAtIndex : 0 ];
    if ( [ waitingList count ] > 0 ) [ super pushViewController : [ waitingList objectAtIndex : 0 ] animated : YES ];
    
}


- ( void )  pushViewController  : ( UIViewController* ) pController
            animated            : ( BOOL ) pAnimated
{
    
    [ waitingList addObject : pController ];
    if ( [ waitingList count ] == 1 ) [ super pushViewController : [ waitingList objectAtIndex : 0 ] animated : YES ];
    
}

@end
