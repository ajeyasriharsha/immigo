//
//  AdvanedSearchMultiSelectVC.h
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/14/15.
//  Copyright (c) 2015 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol multiselectDelegate <NSObject>
@optional
-(void)passValuesToField:(NSArray *)values;
-(void)passValueToField:(NSString *)string;
-(void)closePopUP;
@end

@interface AdvanedSearchMultiSelectVC : UIViewController<UISearchBarDelegate>{
    
    UISearchBar *langsearchBar;
}
@property (nonatomic,strong) id<multiselectDelegate> delegate;
@property (nonatomic,strong) NSMutableArray *optionsArr;
@property (nonatomic,strong) NSMutableArray *searchFilteredArray;
@property (nonatomic,strong) NSMutableArray *selectedIndexes;
@property (nonatomic,strong) NSMutableArray *selectedValues;
@property (nonatomic,strong) NSString *popUpTitle;

@end
