//
//  SettingsVC.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/11/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum
{
    SL_ENGLISH,
    SL_SPANISH,
    SL_COUNT
}
SelectedLanguage;
@interface SettingsVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UIButton *heading;

}
@end
