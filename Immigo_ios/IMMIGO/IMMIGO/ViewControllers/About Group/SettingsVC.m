//
//  SettingsVC.m
//  IMMIGO
//
//  Created by pradeep ISPV on 4/11/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "SettingsVC.h"
#import "DCRoundSwitch.h"
#import "Common.h"
#import "RegisterDeviceModel.h"
#import "PushNotificationsDAO.h"
#import "PushNotificationsModel.h"
#import "APPConfig.h"
#import "DataDownloader.h"
#import "RCSwitchOnOff.h"
@interface SettingsVC () <DataDownloaderDelegate>
{
    IBOutlet UILabel *notificationLbl;
    IBOutlet UIImageView *notificationBg;
    //ON/OFF Switch Implemtation//JUHI
//    DCRoundSwitch *switchControl;
    RCSwitchOnOff *switchControl;
    
    __weak IBOutlet UIView *childView;
    IBOutlet UIButton *registerBtn;
    
    IBOutlet UITableView *languageTable;
    NSMutableArray *languagesArray;
    NSMutableArray *selectedLanaguage;
    BOOL serviceCalled;
    IBOutlet UIScrollView *contentScrollView;
    NSDictionary *mybooleans;
    NSIndexPath *lastSelectedIndexPath;
    

}
@property (nonatomic, retain) NSIndexPath* checkedIndexPath;

@end
//static BOOL isServiceCallMade =NO;
@implementation SettingsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)
    {    self.automaticallyAdjustsScrollViewInsets = NO;
        
    }
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"left arrow"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        
        [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
    }

    [languageTable setTintColor:UIColorFromRGB(0Xc51039)];
    notificationLbl.font = [UIFont fontWithName:@"OpenSans" size:14.5f];
    
    registerBtn.titleLabel.font = [UIFont fontWithName:@"OpenSans" size:14.5f];
    

    heading.titleLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:16.785f];
    heading.backgroundColor=UIColorFromRGB(0Xc51039);
    //    [privacyWebview loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"PrivacyNotice" ofType:@"html"]isDirectory:NO]]];
    {
//        self.navigationController.navigationBarHidden = NO;
//        UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//        UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//        titlView.image = titleLogo;
//        self.navigationItem.titleView = titlView;
        //        pollsTable.rowHeight = 60.0f;
    }
    CGFloat height = 36.0f;
    
    //ON/OFF Switch Implemtation//JUHI
//    switchControl = [[DCRoundSwitch alloc] initWithFrame:CGRectMake(notificationBg.frame.origin.x+notificationBg.frame.size.width-78,notificationBg.frame.origin.y+(notificationBg.frame.size.height-height)/2, 70, height)];
//
//    [switchControl addTarget:self action:@selector(tappedOnSwitch:) forControlEvents:UIControlEventValueChanged];
//    
//    switchControl.autoresizingMask= UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin;    switchControl.backgroundColor = [UIColor clearColor];
    

    
    switchControl = [[RCSwitchOnOff alloc] initWithFrame:CGRectMake(notificationBg.frame.origin.x+notificationBg.frame.size.width-85,notificationBg.frame.origin.y+3, 81, 36)];
    
    [switchControl addTarget:self action:@selector(tappedOnSwitch:) forControlEvents:UIControlEventValueChanged];
    
    switchControl.autoresizingMask= UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin;    switchControl.backgroundColor = [UIColor clearColor];

    [contentScrollView  addSubview:switchControl];

    [DataDownloader sharedDownloader].delegate = self;

    CALayer *layer = languageTable.layer;
    [layer setMasksToBounds:YES];
    [layer setCornerRadius: 4.0];
    [layer setBorderWidth:1.0];
    [layer setBorderColor:[[UIColor colorWithWhite: 0.8 alpha: 1.0] CGColor]];
    // Do any additional setup after loading the view.
    
    //Handling Language Table
    [contentScrollView setBackgroundColor:[UIColor clearColor]];
    languagesArray = [NSMutableArray arrayWithObjects:NSLocalizedString(@"English", @"English") ,NSLocalizedString(@"Spanish", @"Spanish") ,
                        nil];
    mybooleans =[NSMutableDictionary dictionary];
    selectedLanaguage =[NSMutableArray array];
    NSString *languageSelected = [BundleLocalization sharedInstance].language;//[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    
    if ([languageSelected isEqualToString:@"en"]) {
        languageSelected = NSLocalizedString(@"English", @"English");
    }else if ([languageSelected isEqualToString:@"es"]){
        languageSelected = NSLocalizedString(@"Spanish", @"Spanish");
    }

    if([languagesArray containsObject:languageSelected]){
    NSUInteger index =[languagesArray indexOfObject:languageSelected];
    
        NSIndexPath *indexpath =[NSIndexPath indexPathForRow:index inSection:0];
        self.checkedIndexPath =indexpath;
        
    }
    

}

#pragma mark UItableview data source

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 30.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *backGroundView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 30)];
    backGroundView.backgroundColor = [UIColor darkGrayColor];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.frame.size.width-10, 30)];
    //backGroundView.backgroundColor = [UIColor darkGrayColor];
    backGroundView.backgroundColor=UIColorFromRGB(0Xc51039);
    label.textColor = [UIColor whiteColor];
    label.text =NSLocalizedString(@"SELECT_LANGUAGE_TITLE", @"");
    label.font = [UIFont fontWithName:FONT_BOLD size:15.0f];
    [backGroundView addSubview:label];
    return backGroundView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 45.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    NSString *selectedLanguageTitle =languagesArray[indexPath.row];
    cell.textLabel.text =selectedLanguageTitle;
    cell.textLabel.font = [UIFont fontWithName:@"OpenSans" size:14.5f];
    if ([indexPath compare:self.checkedIndexPath] == NSOrderedSame)
    {
        //cell.accessoryType = UITableViewCellAccessoryCheckmark;
        UIImage *image =[UIImage imageNamed:@"tick_icon"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect frame = CGRectMake(0.0, 0.0, 30, 30);
        button.frame = frame;	// match the button's size with the image size
        [button setBackgroundImage:image forState:UIControlStateNormal];
        button.backgroundColor = [UIColor clearColor];
        cell.accessoryView = button;

    }
    else
    {
        cell.accessoryView = nil;
        //cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(![Common currentNetworkStatus])
        
        return;
    
    self.checkedIndexPath = indexPath;//    NSString *lang =languagesArray[indexPath.row];
    switch (indexPath.row) {
        case 0:

            [[NSNotificationCenter defaultCenter] postNotificationName:@"LanguageChanged" object:nil];
            [[BundleLocalization sharedInstance] setLanguage:@"en"];
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:DEVICE_REGISTER_SUCCESSFUL] isEqualToString:@"NO"])
            {
                PushNotifications *notification = [PushNotificationsDAO objectWithID:1];
                if ([[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY])
                {
                    
                    if ([self notificationServicesEnabled] && [Common currentNetworkStatus]) {
                        [[RegisterDeviceModel sharedDataSource] registerDeviceWithId:[[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY] AndType:@"iOS" AndPushNotificationStatus:notification.isEnabled AtAppLaunch:NO];
                        
                        NSLog(@"Making Service Call");
                    }
                    
                }
            }
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        case 1:
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:DEVICE_REGISTER_SUCCESSFUL] isEqualToString:@"NO"])
            {
                PushNotifications *notification = [PushNotificationsDAO objectWithID:1];
                if ([[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY])
                {
                    
                    if ([self notificationServicesEnabled] && [Common currentNetworkStatus]) {
                        [[RegisterDeviceModel sharedDataSource] registerDeviceWithId:[[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY] AndType:@"iOS" AndPushNotificationStatus:notification.isEnabled AtAppLaunch:NO];
                        
                        NSLog(@"Making Service Call");
                    }
                    
                }
            }

            [[NSNotificationCenter defaultCenter] postNotificationName:@"LanguageChanged" object:nil];
            [[BundleLocalization sharedInstance] setLanguage:@"es"];
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
            
        default:
            break;
    }
    [tableView reloadData];
}



- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    
    [self customizeNavigationBar];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:APP_LAUNCH] isEqualToString:@"YES"])
    {
        registerBtn.hidden = YES;
    }
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:DEVICE_REGISTER_SUCCESSFUL_APPLAUNCH] isEqualToString:@"NO"])
    {
        registerBtn.hidden = NO;
    }
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:DEVICE_REGISTER_SUCCESSFUL] isEqualToString:@"NO"])
    {
        registerBtn.hidden = YES;
        //NSLog(@"Calling NOT in app launch time--- deviceSubmitFailure");
        
        PushNotifications *notification = [PushNotificationsDAO objectWithID:1];
        if ([[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY])
        {
            
            if ([self notificationServicesEnabled] && [Common currentNetworkStatus]) {
                serviceCalled = YES;
                [[RegisterDeviceModel sharedDataSource] registerDeviceWithId:[[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY] AndType:@"iOS" AndPushNotificationStatus:notification.isEnabled AtAppLaunch:NO];
                
                NSLog(@"Making Service Call");
            }else{
                
                UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"APP_NAME", @"") message:@"Please enable Push Notifications for Oportunidad" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil, nil];
                [alert show];
            }
            
           
        }
    }
    

    PushNotifications *notification = [PushNotificationsDAO objectWithID:1];
    if ([self notificationServicesEnabled]) {
        switchControl.userInteractionEnabled = YES;
        if (notification.isEnabled)
        {
            switchControl.on = YES;
        }
        else
        {
            switchControl.on = NO;
        }
    }else{
        //OFF ENABLED CONDITION
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:DEVICE_REGISTER_SUCCESSFUL_APPLAUNCH] isEqualToString:@"NO"])
            registerBtn.hidden = YES;
        switchControl.userInteractionEnabled = NO;
        if (notification.isEnabled)
        {
            switchControl.on = NO;
        }
        else
        {
            switchControl.on = NO;
        }
        
    }
    CGRect frame = registerBtn.isHidden ? notificationBg.frame : registerBtn.frame;
    CGFloat y = frame.origin.y +frame.size.height +30;
    
    [languageTable setFrame:CGRectMake(languageTable.frame.origin.x, y, CGRectGetWidth(languageTable.frame), CGRectGetHeight(languageTable.frame))];
    /*
    UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    if(types & UIRemoteNotificationTypeAlert)
    {
        switchControl.userInteractionEnabled = YES;
        if (notification.isEnabled)
        {
            switchControl.on = YES;
        }
        else
        {
            switchControl.on = NO;
        }
    }
    else
    {////OFF enabled condition
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:DEVICE_REGISTER_SUCCESSFUL_APPLAUNCH] isEqualToString:@"NO"])
            registerBtn.hidden = YES;
        switchControl.userInteractionEnabled = NO;
        if (notification.isEnabled)
        {
            switchControl.on = NO;
        }
        else
        {
            switchControl.on = NO;
        }
    }
    */
    
}

-(BOOL)notificationServicesEnabled {
    BOOL isEnabled = NO;
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(currentUserNotificationSettings)]){
        UIUserNotificationSettings *notificationSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
        
        if (!notificationSettings || (notificationSettings.types == UIUserNotificationTypeNone)) {
            isEnabled = NO;
        } else {
            isEnabled = YES;
        }
    } else {
        UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if (types & UIRemoteNotificationTypeAlert) {
            isEnabled = YES;
        } else{
            isEnabled = NO;
        }
    }
    
    return isEnabled;
}
-(void)customizeNavigationBar
{
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    
//    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
//    [backView setBackgroundColor:[UIColor clearColor]];
//    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
//    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
//    [backView addSubview:titleImageView];
//    titleImageView.contentMode = UIViewContentModeCenter;
//    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = childView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        childView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = childView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        childView.frame = viewFrame;
        
    }
    
}
#pragma mark Orientation handling

-(void)viewWillLayoutSubviews{
    [languageTable setScrollEnabled:NO];
    CGFloat height = 30.0f;
    [switchControl setFrame:CGRectMake(notificationBg.frame.origin.x+notificationBg.frame.size.width-85,notificationBg.frame.origin.y+3, 81, 36)];
    [super viewWillLayoutSubviews];
    NSInteger maxY = 0;
    
    CGFloat scrollViewHeight = 0.0f;
    for (UIView* view in contentScrollView.subviews)
    {
        if (!view.hidden)
        {
            CGFloat y = view.frame.origin.y;
            CGFloat h = view.frame.size.height;
            if (y + h > scrollViewHeight)
            {
                scrollViewHeight = h + y;
            }
        }
    }
    scrollViewHeight =languageTable.frame.origin.y +languageTable.frame.size.height+50;
    [contentScrollView setContentSize:(CGSizeMake(contentScrollView.frame.size.width, scrollViewHeight))];
    
    return;
    for (UIView* subview in contentScrollView.subviews)
    {
        if (CGRectGetMaxY(subview.frame) > maxY)
        {
            maxY = CGRectGetMaxY(subview.frame);
        }
    }
    maxY += 10;
    [contentScrollView setContentSize:CGSizeMake(contentScrollView.frame.size.width, maxY)];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = childView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        childView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = childView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        childView.frame = viewFrame;
    }
    
    //    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}//ON/OFF Switch Implemtation//JUHI
-(IBAction)tappedOnSwitch:(RCSwitchOnOff*)sender
{
    if ([sender isOn])
    {
        
        NSLog(@"Switch is turned on");
        if (serviceCalled)
            return;
        [[PushNotificationsModel sharedDataSource] updatePushNotificationStatus:YES];
        [[RegisterDeviceModel sharedDataSource] registerDeviceWithId:[[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY] AndType:@"iOS" AndPushNotificationStatus:YES AtAppLaunch:NO];
    }else
    {
        
        NSLog(@"Switch is turned on");
        if (![self notificationServicesEnabled]) {
            return;
            NSLog(@"Switch is turned on");
        }
        [[PushNotificationsModel sharedDataSource] updatePushNotificationStatus:NO];
        [[RegisterDeviceModel sharedDataSource] registerDeviceWithId:[[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY] AndType:@"iOS" AndPushNotificationStatus:NO AtAppLaunch:NO];
    }

}
- (IBAction)registerDevice:(id)sender
{
    
    if ([self notificationServicesEnabled]) {
        [[RegisterDeviceModel sharedDataSource] registerDeviceWithId:[[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY] AndType:@"iOS" AndPushNotificationStatus:YES AtAppLaunch:NO];

    }else{
        
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"APP_NAME", @"") message:@"Please enable Push Notifications for Oportunidad" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

@end
