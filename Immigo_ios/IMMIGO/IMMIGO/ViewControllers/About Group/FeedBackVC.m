//
//  FeedBackVC.m
//  Immigo
//
//  Created by pradeep ISPV on 3/14/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

// immigo@immigrationadvocates.org

#import "FeedBackVC.h"
#import "Categories.h"
#import "Common.h"
#import <MessageUI/MessageUI.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "Constants.h"
@interface FeedBackVC () <MFMailComposeViewControllerDelegate>
{
    IBOutlet UIBarButtonItem *homeButton;
    
    IBOutlet UIButton *heading;
    IBOutlet UIButton *cancelBtn;
    IBOutlet UIScrollView *optionsScrollView;
    IBOutlet UIView *childView;
    IBOutlet TPKeyboardAvoidingScrollView *scrollView;
    id activeField;
    IBOutlet UIButton *optionBtn;
    IBOutlet UIView *feedBackWishView;
    IBOutlet UILabel *wishlabel;
}
@end

@implementation FeedBackVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    feedBackWishView.hidden  = YES;
    [scrollView contentSizeToFit];
    [[DataDownloader sharedDownloader] setDelegate:self];
    optionBtn.titleLabel.font = [UIFont fontWithName:FONT_REGULAR size:9.785f];
    optionBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    // you probably want to center it
    [optionBtn setTitle:[NSString stringWithFormat:@"%@",NSLocalizedString(@"I would like to receive communications from Oportunidad\n partners", @"") ]  forState: UIControlStateNormal];

//    I would like to receive communications from Oportunidad \n partners
    optionBtn.selected = YES;

    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"left arrow"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        UIImage *image = [UIImage imageNamed:@"question"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = rightItem;
        
        [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];
    }

    wishlabel.textColor = [UIColor darkGrayColor];
    wishlabel.font = [UIFont fontWithName:FONT_REGULAR size:16.785f];
    
    heading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
    heading.backgroundColor=UIColorFromRGB(0Xc51039);
    yourMessageTextView.font = [UIFont fontWithName:FONT_SEMIBOLD_ITALIC size:12.785f];
    yourMessageTextView.textColor = [UIColor grayColor];
    yourMessageTextView.layer.borderWidth = 1.0f;
    yourMessageTextView.layer.borderColor = [UIColorFromRGB(0XB9B9B9) CGColor];
    yourMessageTextView.layer.cornerRadius = 2.0f;
    
    nameTf.font = [UIFont fontWithName:FONT_SEMIBOLD_ITALIC size:12.785f];
    nameTf.textColor = [UIColor grayColor];
    
    emailTF.font = [UIFont fontWithName:FONT_SEMIBOLD_ITALIC size:12.785f];
    emailTF.textColor = [UIColor grayColor];
    
    phoneTf.font = [UIFont fontWithName:FONT_SEMIBOLD_ITALIC size:12.785f];
    phoneTf.textColor = [UIColor grayColor];
    
    
   // UIImage *btnBg = [UIImage imageNamed:@"RedBtn_Bg"];
    //[submitBtn setBackgroundImage:[btnBg stretchableImageWithLeftCapWidth:btnBg.size.width/2 topCapHeight:btnBg.size.height/2] forState:UIControlStateNormal];
    [submitBtn setBackgroundColor:UIColorFromRGB(0X114171)];  //VAMSH];
    //JUHI
    submitBtn.layer.cornerRadius = 5.0f;
    submitBtn.contentMode = UIViewContentModeRedraw;

    //[cancelBtn setBackgroundImage:[btnBg stretchableImageWithLeftCapWidth:btnBg.size.width/2 topCapHeight:btnBg.size.height/2] forState:UIControlStateNormal];
    [cancelBtn setBackgroundColor:UIColorFromRGB(0X114171)];  //VAMSH];
    //JUHI
    cancelBtn.layer.cornerRadius = 5.0f;
    cancelBtn.contentMode = UIViewContentModeRedraw;

    {
        self.navigationController.navigationBarHidden = NO;
//        UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//        UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//        titlView.image = titleLogo;
//        self.navigationItem.titleView = titlView;
        //        pollsTable.rowHeight = 60.0f;
    }

	// Do any additional setup after loading the view.
    
    yourMessageTextView.text = NSLocalizedString(@"Your Message", @""); // VAMSHI 
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (IBAction)sendFeedback:(id)sender
{
    {
        if ([nameTf.text length]==0 || [[nameTf.text trimmedString] length]==0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Feedback", @"Feedback")  message:NSLocalizedString(@"Name is a required field.", @"")  delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
            [alert show];
            
        }else if ([emailTF.text length]==0 || [[emailTF.text trimmedString] length]==0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Feedback", @"Feedback") message:NSLocalizedString(@"Email address is a required field.", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
            [alert show];
        }else if ([yourMessageTextView.text length]==0 || [[yourMessageTextView.text trimmedString] length]==0 || [yourMessageTextView.text isEqualToString: NSLocalizedString(@"Your Message", @"") ])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Feedback", @"Feedback") message:NSLocalizedString(@"Message is a required field.", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
            [alert show];
        }else if(([emailTF.text length]>0 || [[emailTF.text trimmedString] length]>0) && ([yourMessageTextView.text length]>0 || [[yourMessageTextView.text trimmedString] length]>0  || ![yourMessageTextView.text isEqualToString:NSLocalizedString(@"Your Message", @"")]) && ([nameTf.text length]>0 || [[nameTf.text trimmedString] length]>0))
        {
            if (![self validateEmailWithString:emailTF.text])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"EMAIL_TITLE", @"")  message:NSLocalizedString(@"Please enter valid email address.",@"") delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil]; // VAMSHI
                [alert show];
                return;
            }
            if (![Common currentNetworkStatus])
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Feedback", @"Feedback") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
                [alertView show];
                return;
            }

            
            [[FeedbackModel sharedDataSource] submitFeedBackWithDeviceId:[[NSUserDefaults standardUserDefaults] valueForKey:IMMIGO_GUID] AndUserName:nameTf.text AndEmailId:emailTF.text AndPhoneNumber:phoneTf.text AndFeedback:yourMessageTextView.text AndStatus:optionBtn.selected];
            
            //I would like to receive communications from Immigo partners
            NSString *messageBody = [NSString stringWithFormat:@"<!DOCTYPE html> <html> <head> <style> table,th,td { border:1px solid black; border-collapse:collapse; } th,td { padding:5px; } </style> </head> <body> <table style=\"width:300px\"> <tr> <th>Name</th> <td>%@</td> </tr> <tr> <th>Email</th> <td>%@</td> </tr> <tr> <th>Phone Number</th> <td>%@</td> </tr> <tr> <th>Your Feedback</th> <td>%@</td> </tr><tr> <th>I would like to receive communications from Oportunidad partners</th> <td>%@</td> </tr> </table> </body> </html>",nameTf.text,emailTF.text,phoneTf.text,yourMessageTextView.text,optionBtn.selected ? @"YES":@"NO"];
            
            
            if ([MFMailComposeViewController canSendMail])
            {
                // set the sendTo address
                NSMutableArray *recipients = [[NSMutableArray alloc] initWithCapacity:1];
                [recipients addObject:@"immigo@immigrationadvocates.org"];
                
                MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
                controller.mailComposeDelegate = self;
                [controller setMessageBody:messageBody isHTML:YES];
                [controller setSubject:@"Immigo feedback"];
                
                [controller setToRecipients:recipients];
                
                if ([self respondsToSelector:@selector(presentViewController:animated:completion:)])
                {
//                    UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//                    UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//                    titlView.image = titleLogo;
//                    controller.navigationItem.titleView = titlView;
//                    [[controller navigationBar] sendSubviewToBack:titlView];
                    [[controller navigationBar] setTintColor:[UIColor blueColor]];
                    /*
                     [[controller navigationBar] setTintColor:[UIColor whiteColor]];
                     UIImage *image = [UIImage imageNamed: @"logo.png"];
                     UIImageView * iv = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,320,42)];
                     iv.image = image;
                     iv.contentMode = UIViewContentModeCenter;
                     [[[controller viewControllers] lastObject] navigationItem].titleView = iv;
                     [[controller navigationBar] sendSubviewToBack:iv];
                     */
                    [self presentViewController:controller animated:YES completion:nil];
                    [nameTf setText:@""];
                    [phoneTf setText:@""];
                    [emailTF setText:@""];
                    [yourMessageTextView setText:@""];

                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:NSLocalizedString(@"NOEMAIL_ACCOUNT_FOUND", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles: nil];
                [alert show];
                
            }
        }
    }
    
   
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    // register for keyboard notifications

    activeField = textView;
    if ([textView.textColor isEqual:[UIColor grayColor]] || [yourMessageTextView.text isEqualToString:NSLocalizedString(@"Your Message", @"")])
    {
        textView.text= @"";
        textView.textColor = [UIColor darkGrayColor];
        
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
}

- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        if ([yourMessageTextView.text length]==0 || [[yourMessageTextView.text trimmedString] length]==0)
        {
            yourMessageTextView.text = NSLocalizedString(@"Your Message", @"");
            textView.textColor = [UIColor grayColor];
        }
        
        return NO;
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    activeField = textField;
   [textField resignFirstResponder];
    
    
    /*
    if ([textField isEqual:nameTf])
    {
        [nameTf resignFirstResponder];
        [emailTF becomeFirstResponder];
    }else if ([textField isEqual:emailTF])
    {
        [emailTF resignFirstResponder];
        [phoneTf becomeFirstResponder];
    }else if ([textField isEqual:phoneTf])
    {
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(keyboardWillShow)
//                                                     name:UIKeyboardWillShowNotification
//                                                   object:nil];
//        
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(keyboardWillHide)
//                                                     name:UIKeyboardWillHideNotification
//                                                   object:nil];

        activeField = yourMessageTextView;
        
        [phoneTf performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0.1];
       [yourMessageTextView performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1];
//        [phoneTf resignFirstResponder];

    }
     
     */
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == phoneTf)
    {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 10 || ![self isNumeric:string]) ? NO : YES;
    }
    return YES;
}

-(BOOL)isNumeric:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}

- (void)viewWillAppear:(BOOL)animated
{
//    optionsScrollView.contentSize = CGSizeMake(320, 200);
//    [optionsScrollView scrollsToTop];
//    
     [self customizeNavigationBar];


}
-(void)viewDidAppear:(BOOL)animated{
   // [self customizeNavigationBar];
    [super viewDidAppear:animated];
}
-(void)customizeNavigationBar{
    
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    
//    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
//    [backView setBackgroundColor:[UIColor clearColor]];
//    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
//    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
//    [backView addSubview:titleImageView];
//    titleImageView.contentMode = UIViewContentModeCenter;
//    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = childView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        childView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = childView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        childView.frame = viewFrame;
        
    }
    
    
}

#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = childView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        childView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = childView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        childView.frame = viewFrame;
    }
    
    //    }
}


- (void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
     
                                                  object:nil];
    
    //    [self.view endEditing:YES];
    
}

- (void)viewDidUnload
{
    scrollView=nil;
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Feedback Datadownloader delegate methods
-(void)feedBackSubmittedSuccessfully
{
    [scrollView scrollsToTop];
    yourMessageTextView.text = nameTf.text = phoneTf.text = emailTF.text = [NSString string];
    feedBackWishView.hidden  = NO;
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"FeedBack" message:@"Feedback submitted successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//    [alert show];

}
-(void)feedBackSubmittedFailure
{
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"FeedBack" message:@"Sorry! Feedback is not submitted." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//    [alert show];
//
    
}
#pragma mark - MFMail Composer Delegate methods
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	switch (result)
	{
		case MFMailComposeResultCancelled:
		{
			break;
		}
		case MFMailComposeResultSaved:
		{
			break;
		}
		case MFMailComposeResultSent:
		{
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Email sent successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//			[alert show];
			break;
		}
		case MFMailComposeResultFailed:
		{
			break;
		}
		default:
		{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"EMAIL_TITLE", @"") message:@"Email Failed" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles: nil];
			[alert show];
            
		}
			break;
	}
	if ([self respondsToSelector:@selector(dismissModalViewControllerAnimated:)]) {
        [self performSelector:@selector(dismissModalViewControllerAnimated:) withObject:[NSNumber numberWithBool:YES]];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
	
	
}

- (IBAction)optionbuttonTapped:(UIButton *)sender
{
    sender.selected =! sender.selected;
}


@end
