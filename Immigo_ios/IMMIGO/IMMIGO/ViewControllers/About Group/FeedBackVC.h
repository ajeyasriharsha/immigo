//
//  FeedBackVC.h
//  Immigo
//
//  Created by pradeep ISPV on 3/14/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataDownloader.h"
#import "FeedbackModel.h"


@interface FeedBackVC : UIViewController <UITextViewDelegate,UITextFieldDelegate,DataDownloaderDelegate>
{
    IBOutlet UITextField *nameTf;
    
    IBOutlet UITextField *phoneTf;
    IBOutlet UITextField *emailTF;
    IBOutlet UITextView *yourMessageTextView;
    IBOutlet UIButton *submitBtn;
     BOOL keyboardVisible;
}
- (IBAction)backButtonAction:(id)sender;
@end
