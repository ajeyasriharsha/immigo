//
//  PrivacyVC.m
//  Immigo
//
//  Created by pradeep ISPV on 3/14/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "PrivacyVC.h"
#import "PrivacyModel.h"
#import "DataDownloader.h"
#import "PrivacyDAO.h"
#import "APPConfig.h"
#import "Common.h"

@interface PrivacyVC () <DataDownloaderDelegate,UIScrollViewDelegate>
{
    
    IBOutlet UIButton *heading;
    __weak IBOutlet UIView *bgView;
    BOOL clickedOnLink;
    NSString *htmlString;
    IBOutlet UIButton *safariButton;
    NSString *linkToOpenInSafari;

}
@end

@implementation PrivacyVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    clickedOnLink =NO;
    if (isAPPOnline)
    {
        //NSLog(@"online");
    }
    else
    {
        //NSLog(@"Offline");
    }
    
    [DataDownloader sharedDownloader].delegate = self;
    [[PrivacyModel sharedDataSource] getPrivacyPolicyContent];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)
    {    self.automaticallyAdjustsScrollViewInsets = NO;
        
    }

    heading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
    heading.backgroundColor=UIColorFromRGB(0Xc51039);
//    [privacyWebview loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"PrivacyNotice" ofType:@"html"]isDirectory:NO]]];
    {
//        self.navigationController.navigationBarHidden = NO;
//        UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//        UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//        titlView.image = titleLogo;
//        self.navigationItem.titleView = titlView;
        //        pollsTable.rowHeight = 60.0f;
    }
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"left arrow"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        
        [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
    }

	// Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    [self customizeNavigationBar];
    if (!linkToOpenInSafari) {
        safariButton.hidden = YES;
    }else{
        
        safariButton.hidden = NO;
    }}
-(void)customizeNavigationBar{
    
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    
//    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
//    [backView setBackgroundColor:[UIColor clearColor]];
//    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
//    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
//    [backView addSubview:titleImageView];
//    titleImageView.contentMode = UIViewContentModeCenter;
//    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        bgView.frame = viewFrame;
        
    }
    
    
}
#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        bgView.frame = viewFrame;
    }
//    [privacyWebview reload];
    
    
    if (!clickedOnLink) {
        
        
        if (!htmlString || [htmlString isKindOfClass:[NSNull class]])
        {
            [privacyWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
        }else{
 //      NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body><p><font face=\"opensans\" color=\"black\" size=\"3.5\">%@</font></p></body></html>",htmlString];
      NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><head><style>p.padding {padding-left: 2cm;}</style></head><body style=\"margin-top: 0px; margin-left: 20px; margin-right: 20px;\"><p ><font face=\"opensans\" color=\"black\" size=\"3.5\">%@</font></p></body></html>",htmlString];

       [privacyWebview loadHTMLString:html baseURL:nil];
        }
    }

    //    }
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
   
    webView.scrollView.delegate = self; // set delegate method of UISrollView
    webView.scrollView.maximumZoomScale = 10; // set as you want.
    webView.scrollView.minimumZoomScale = 0; // set as you want.
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{   clickedOnLink =NO;
    //NSLog(@"Error : %@",error);
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    /*
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        return;
    }
     */
    
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked){
        clickedOnLink =YES;
        
        if ([request.URL.scheme isEqualToString:@"tel"])
        {
            safariButton.hidden = YES;
            return YES;
        }
        
        else if ([request.URL.scheme isEqualToString:@"mailto"])
        {
            safariButton.hidden = YES;
            return YES;
        }
        else{
            if (![Common currentNetworkStatus])
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Privacy" message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
                [alertView show];
                return NO;
            }
            if (linkToOpenInSafari)
                linkToOpenInSafari = nil;
            safariButton.hidden = NO;
            linkToOpenInSafari =[request.URL absoluteString];
            if ([linkToOpenInSafari hasPrefix:@"http://"] || [linkToOpenInSafari hasPrefix:@"https://"])
            {
                
            }
            else
            {
                linkToOpenInSafari = [NSString stringWithFormat:@"http://%@",linkToOpenInSafari];
            }

            
        }
        
    }
    return YES;
    
    
}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
    privacyWebview.scrollView.minimumZoomScale = 0; // set similar to previous.
    privacyWebview.scrollView.maximumZoomScale = 10; // set similar to previous.
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)tappedOnSafari:(id)sender
{
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Privacy" message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        return ;
    }
    NSURL *requestUrl =[NSURL URLWithString:linkToOpenInSafari];
    [[UIApplication sharedApplication] openURL:requestUrl];
}
-(void)privacyDownloadedSuccessfully
{
    Privacy *privacy = [[PrivacyDAO allObjects] objectAtIndex:0];
    htmlString = [[PrivacyDAO objectWithID:privacy.key] content];
  //  NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body><p><font face=\"opensans\" color=\"black\" size=\"3.5\">%@</font></p></body></html>",htmlString];
    NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><head><style>p.padding {padding-left: 2cm;}</style></head><body style=\"margin-top: 0px; margin-left: 20px; margin-right: 20px;\"><p ><font face=\"opensans\" color=\"black\" size=\"3.5\">%@</font></p></body></html>",htmlString];

    [privacyWebview loadHTMLString:html baseURL:nil];
}
-(void)privacyLoadedFailure
{
    Privacy *privacy = [[PrivacyDAO allObjects] objectAtIndex:0];
    htmlString = [[PrivacyDAO objectWithID:privacy.key] content];
   
    if (!htmlString || [htmlString isKindOfClass:[NSNull class]])
    {
        [privacyWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    }else{
 //       NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body><p><font face=\"opensans\" color=\"black\" size=\"3.5\">%@</font></p></body></html>",htmlString];
        NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><head><style>p.padding {padding-left: 2cm;}</style></head><body style=\"margin-top: 0px; margin-left: 20px; margin-right: 20px;\"><p ><font face=\"opensans\" color=\"black\" size=\"3.5\">%@</font></p></body></html>",htmlString];

        [privacyWebview loadHTMLString:html baseURL:nil];

        
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
}
- (BOOL)prefersStatusBarHidden {
    return NO;
}

@end
