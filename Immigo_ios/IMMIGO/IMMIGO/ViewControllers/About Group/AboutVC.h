//
//  AboutVC.h
//  Immigo
//
//  Created by pradeep ISPV on 3/14/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataDownloader.h"
@interface AboutVC : UIViewController <DataDownloaderDelegate>
{
    IBOutlet UIWebView *aboutWebView;
    
}
@end
