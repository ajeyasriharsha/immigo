//
//  DisclaimerVC.h
//  Immigo
//
//  Created by pradeep ISPV on 3/14/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisclaimerVC : UIViewController
{
    
    IBOutlet UIWebView *disclaimerWebview;
}
@end
