//
//  ImmigrationBasicsVC.h
//  Immigo
//
//  Created by pradeep ISPV on 3/4/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ImmigrationBasicsVC : UIViewController
{
    IBOutlet UIButton *heading;
    
}
- (IBAction)backButtonAction:(id)sender;
- (IBAction)questionMarkTapped:(id)sender;
@property (nonatomic,strong) NSString *headerTitle;
@property (nonatomic,strong) UIImage *headerImage;
@property (nonatomic,assign) TP_TopicType selTopicType;

@end
