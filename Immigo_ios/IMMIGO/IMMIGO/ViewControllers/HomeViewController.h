//
//  ViewController.h
//  Immigo
//
//  Created by pradeep ISPV on 2/27/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PartnersPullableView.h"

@interface HomeViewController : UIViewController <PullableViewDelegate>
{
    PartnersPullableView *pullUpView;
    IBOutlet UIScrollView *alertsScrollView;
    IBOutlet UIPageControl *alertsPageControl;
    
    IBOutlet UIButton *basicsBtn;
    IBOutlet UIButton *finderBtn;
    IBOutlet UIButton *newsBtn;
    IBOutlet UIButton *civicEngagementBtn;
    IBOutlet UIButton *govtresourceBtn;
    IBOutlet UIButton *aboutBtn;
    
    BOOL _fromSplash;
    
}
@property (nonatomic)BOOL fromSplash;
@end
