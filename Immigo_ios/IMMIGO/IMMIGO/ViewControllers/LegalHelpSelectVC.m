//
//  LegalHelpSelectVC.m
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 17/02/16.
//  Copyright © 2016 pradeep ISPV. All rights reserved.
//

#import "LegalHelpSelectVC.h"
#import "HelpCell.h"
#import "LegalHelpFinderVC.h"
#import "Common.h"
@interface LegalHelpSelectVC (){
    
    IBOutlet UIButton *heading;
    
    NSMutableArray *legalFindersArray;
    IBOutlet UIView *bgView;
    IBOutlet UITableView *optionsTable;

    
}

@end

@implementation LegalHelpSelectVC


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
       [self customizeNavigationBar];
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    //    if ([zipCode length]>0)
    //    {
    //        // Turn off the location manager to save power.
    //        [locationManager stopUpdatingLocation];
    //    }
}


-(void)customizeNavigationBar{
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        bgView.frame = viewFrame;
        
    }
    
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *button1;
    UIButton *button;
    {
        if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
        {
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            UIImage *image1 = [UIImage imageNamed:@"left arrow"];
            button1 = [UIButton buttonWithType:UIButtonTypeCustom];
            [button1 setImage:image1 forState:UIControlStateNormal];
            [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
            
            // Create a custom UIBarButtonItem with the button
            UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
            self.navigationItem.leftBarButtonItem = leftItem;
            
            UIImage *image = [UIImage imageNamed:@"question"];
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setImage:image forState:UIControlStateNormal];
            [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
            button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
            
            // Create a custom UIBarButtonItem with the button
            UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
            self.navigationItem.rightBarButtonItem = rightItem;
            
            [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
            [button setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];
            
        }
    }
    
    [optionsTable setSeparatorColor:[UIColor grayColor]];

    self.navigationController.navigationBarHidden = NO;
    heading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];    // Do any additional setup after loading the view.
    heading.backgroundColor=UIColorFromRGB(0Xc51039);
    optionsTable.tableFooterView = [UIView new];

    
}

#pragma mark - Tableview delegate and Data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    HelpCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HelpCell"];
    
    if (!cell)
    {
        cell = (HelpCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HelpCell"];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    cell.textLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:14.5];
    cell.textLabel.textColor = UIColorFromRGB(0X124A7F);  //VAMSHI
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = NSLocalizedString(@"Find a LULAC Council", nil);
            break;
        case 1:
            cell.textLabel.text = NSLocalizedString(@"Find IMMIGRATION Help", nil);
            break;

            
        default:
            break;
    }
    return cell;
}
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
//    cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"TopicCell_BG"]];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    switch (indexPath.row) {
        case 0:{
            UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Yet to be Implemented", nil) message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case 1:{
            LegalHelpFinderVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"LegalHelpFinderVC"];
            [self.navigationController pushViewController:toVC animated:YES];

            break;
        }

            
        default:
            break;
    }
    //[self.navigationController pushViewController:toVC animated:YES];
    
}
-(void)pushToViewControllerWithIdentifier:(NSString *)identifier //Pass a identifier Used in Storyboard for viewController
{
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    UIViewController* toVC = [storyboard instantiateViewControllerWithIdentifier:identifier];
    [self.navigationController pushViewController:toVC animated:YES];
    
}
#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        bgView.frame = viewFrame;
    }
    
    //    }
}
- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
