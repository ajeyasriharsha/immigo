//
//  LegalHelpFinderVC.h
//  Immigo
//
//  Created by pradeep ISPV on 3/6/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface LegalHelpFinderVC : UIViewController <CLLocationManagerDelegate>
{
   // CLLocationManager *locationManager;
    
    CLLocation *selectedLocation;
    
}
//@property (nonatomic,strong)     CLLocationManager *locationManager;
@property (nonatomic,strong) CLLocation *selectedLocation;
- (IBAction)backButtonAction:(id)sender;
- (IBAction)questionMarkTapped:(id)sender;
- (IBAction)advanceSearchTapped:(id)sender;
- (IBAction)searchButtonTapped:(id)sender;

@end
