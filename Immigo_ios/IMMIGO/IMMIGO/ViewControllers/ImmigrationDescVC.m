//
//  ImmigrationDescVC.m
//  Immigo
//
//  Created by pradeep ISPV on 3/7/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "ImmigrationDescVC.h"
#import "AboutUsVC.h"
#import "Topic.h"
#import "Common.h"
#import "UITextViewDisableSelection.h"
#import "FaceBookActivityProvider.h"
#import "LinkedInActivity.h"
#import "FacebookActivity.h"
#import "UICustomActionSheet.h"
#import "ShareOptionsViewController.h"
@interface ImmigrationDescVC () <UIScrollViewDelegate,LinkedINActivityDelegate>
{
    IBOutlet UILabel *topicHeading;
    UIActivityIndicatorView *activityController;
    IBOutlet UILabel *topicName;
    IBOutlet UITextViewDisableSelection *subTopicDesc;
    __weak IBOutlet UIView *bgView;
    IBOutlet UITextViewDisableSelection *heading;
    IBOutlet UIButton *safariButton;
    NSString *linkToOpenInSafari;
    BOOL _isClicked;
    UICustomActionSheet *customsheet;

}

@end

@implementation ImmigrationDescVC
@synthesize topicTitle,subTopicTitle,subtopic,dividerLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)init
{
    self=[super init];
    return self;
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController setToolbarHidden:YES];
    [heading enableObserver:NO onObject:heading selector:@selector(contentSize)];
    [subTopicDesc enableObserver:NO onObject:subTopicDesc selector:@selector(contentSize)];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
//    [heading disableAlginment];
//    [subTopicDesc disableAlginment];
    
    //if (SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(@"9")){
        
    
    //}
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
//    [heading addObserver:self forKeyPath:@"contentSize" options:(NSKeyValueObservingOptionNew) context:NULL];
//    [subTopicDesc addObserver:self forKeyPath:@"contentSize" options:(NSKeyValueObservingOptionNew) context:NULL];
    titleheading.backgroundColor=UIColorFromRGB(0Xc51039);
   titleheading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
    dividerLabel.backgroundColor=UIColorFromRGB(0Xc51039);
    
    //NSLog(@"desc VC");
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        //NSLog(@"runnig on < 7.0");
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"left arrow"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        UIImage *image = [UIImage imageNamed:@"question"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = rightItem;
        
        [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    topicHeading.textAlignment = NSTextAlignmentLeft;
    topicHeading.font = [UIFont fontWithName:FONT_SEMIBOLD size:16];
    topicHeading.textColor = heading.textColor =[UIColor darkGrayColor];
    topicHeading.backgroundColor = [UIColor clearColor];
    
    subTopicDesc.backgroundColor = [UIColor clearColor];
    subTopicDesc.font = heading.font = [UIFont fontWithName:FONT_SEMIBOLD size:16]; // VAMSHI //[UIFont fontWithName:FONT_SEMIBOLD_ITALIC size:16];
    subTopicDesc.textColor = UIColorFromRGB(0XC51039);  //VAMSHI //[UIColor darkGrayColor];
    subTopicDesc.text = [NSString stringWithFormat:@"%@ / %@",subtopic.topic.topicTitle,subtopic.subtopicTitle];
    
    CGRect frame = subTopicWebView.frame;
    activityController = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    activityController.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [subTopicWebView addSubview:activityController];

    self.navigationController.navigationBarHidden = NO;
//    UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//    UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//    titlView.image = titleLogo;
//    self.navigationItem.titleView = titlView;

    
    if ([subtopic.heading length]>0)
    {
        CGFloat height = heading.frame.size.height;
//        CGRect frame = heading.frame;
//        frame  = [self frameForTextView:heading WithText:subtopic.heading];
//        heading.frame = frame;

        CGFloat diff = height-heading.frame.size.height;
        
        self.dividerLabel.frame = CGRectMake(0, subTopicDesc.frame.origin.y+subTopicDesc.frame.size.height, self.dividerLabel.frame.size.width, self.dividerLabel.frame.size.height);
        
        CGRect frame1 = subTopicWebView.frame;
        frame1.origin.y -= diff;
        frame1.size.height +=diff;
        subTopicWebView.frame = frame1;
        
        

        heading.text =subtopic.heading;
        
    }
    else
    {
        heading.hidden = YES;
        CGRect frame = subTopicWebView.frame;
        frame.origin.y -= heading.frame.size.height;
        frame.size.height +=heading.frame.size.height;
        subTopicWebView.frame = frame;
    }
    
    
 //   NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body><p><font face=\"opensans\" color=\"black\" size=\"3.5\">%@</font></p></body></html>",subtopic.textContent];
    NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><head><style>p.padding {padding-left: 2cm;}</style></head><body style=\"margin-top: 0px; margin-left: 20px; margin-right: 20px;\"><p ><font face=\"opensans\" color=\"black\" size=\"3.5\">%@</font></p></body></html>",subtopic.textContent];


    [subTopicWebView loadHTMLString:html baseURL:nil];
   

//    [heading alignToTop];
    // [subTopicDesc alignToTop];
    
    //new changes for textview center text
    
  
    
     
}

-(NSString *)getHtmlContentisFromPrint:(BOOL)fromPrint{
    
    
    NSMutableString *bodyHtml=[NSMutableString string];
    
    if (subtopic.heading.length) {
            NSString *linkHtml =[NSString stringWithFormat:@"<p> %@ </p>",subtopic.heading];
            [bodyHtml appendString:linkHtml];
    }
    NSString *linkHtml =[NSString stringWithFormat:@"<p> %@ </p>",subtopic.textContent];
    [bodyHtml appendString:linkHtml];
    
    if (!fromPrint) {
        NSURL *appstoreUrl = [NSURL URLWithString:APP_STORE_URL];
        NSString *mailSignature =[NSString stringWithFormat:@"<p> %@ <a href=\"%@\">%@</a></p>",NSLocalizedString(@"SENT_EMAIL_SIGNATURE", @""),appstoreUrl,[Common deviceName]];
        [bodyHtml appendString:mailSignature];
    }
    NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body> %@</body></html>",bodyHtml];
    return html;
}


-(IBAction)showActionSheet:(id)sender{
    
    NSString * storyboardName = @"Main_iPhone";
    NSString * viewControllerID = @"ShareOptionsViewController";
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    ShareOptionsViewController *   multiSelectVC = (ShareOptionsViewController *)[storyboard instantiateViewControllerWithIdentifier:viewControllerID];
    
    if ([[UIDevice currentDevice].systemVersion integerValue] >= 8)
    {
        //For iOS 8
        multiSelectVC.providesPresentationContextTransitionStyle = YES;
        multiSelectVC.definesPresentationContext = YES;
        multiSelectVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    }
    else
    {
        //For iOS 7
        multiSelectVC.modalPresentationStyle = UIModalPresentationCurrentContext;
    }
    [self presentViewController:multiSelectVC animated:YES completion:nil];


   
}
-(IBAction)share:(id)sender{
    
    NSString *sharingContentForSocialMedia =[NSString stringWithFormat:@"%@/%@:",subtopic.topic.topicTitle,subtopic.subtopicTitle];

    NSString *html = [self getHtmlContentisFromPrint:NO];
    
    NSString *textToShare = html;
    NSURL *myWebsite = [NSURL URLWithString:APP_STORE_COMMON_LINK];
    
    NSString *printContent =[self getHtmlContentisFromPrint:YES];
    UIMarkupTextPrintFormatter *htmlFormatter = [[UIMarkupTextPrintFormatter alloc]
                                                 initWithMarkupText:printContent];
    htmlFormatter.startPage = 0;
    htmlFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
    
//    NSMutableArray *applicationActivities=[NSMutableArray array];
//    
//    if ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] integerValue] >= 6 && [[[[UIDevice currentDevice] systemVersion] substringToIndex:1] integerValue] <= 7) {
//        LinkedInActivity *activity =[[LinkedInActivity alloc] init];
//        activity.postUrl =[myWebsite absoluteString];
//        activity.postCommentText =sharingContentForSocialMedia;
//        activity.delegate =self;
//        applicationActivities =
//        @[
//          activity
//          ];
//
//    }else if ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] integerValue] >= 8){
//        
//        applicationActivities =[NSMutableArray array];
//    }
    
    FaceBookActivityProvider *activityItemProvider =
    [[FaceBookActivityProvider alloc] initWithPlaceholderItem:textToShare
                                                  twitterItem:sharingContentForSocialMedia andUrlForStore:[myWebsite absoluteString]];
    
    NSArray *objectsToShare = @[activityItemProvider,htmlFormatter,myWebsite];
    
    UIActivityViewController *controller =
    [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,//@"com.linkedin.LinkedIn.ShareExtension",
                                   UIActivityTypePostToVimeo,
                                   UIActivityTypePostToTencentWeibo,
                                   UIActivityTypeMessage,
                                   UIActivityTypeCopyToPasteboard];
    
    controller.excludedActivityTypes = excludeActivities;
    NSString *appname =NSLocalizedString(@"APP_NAME", @"");
    NSString *subject =[NSString stringWithFormat:@"%@ - %@ - %@",appname,subtopic.topic.topicTitle,subtopic.subtopicTitle];
    
    [controller setValue:subject forKey:@"subject"];
    
    [self presentViewController:controller animated:YES completion:nil];
    

    if([[UIApplication sharedApplication] respondsToSelector:(@selector(setCompletionWithItemsHandler:))]){
        [controller setCompletionWithItemsHandler:^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
            NSLog(@"completed dialog - activity: %@ - finished flag: %d", activityType, completed);
            if(!completed){
                // Out of scope of question
            }
        }];
    }else{
        [controller setCompletionHandler:^(NSString *activityType, BOOL completed) {
            if(!completed){
                // Out of scope of question
            }
        }];
    }

    
    
    
}

-(void)linkedInButtonAction{
    
    shareObj = [[ShareToSocialNetworkingSites alloc] init];
    [shareObj linkedInLogIn:@"" andText:@"" fromViewContoller:self];
}
- (CGFloat)measureHeightOfUITextView:(UITextView *)textView
{
    if ([textView respondsToSelector:@selector(snapshotViewAfterScreenUpdates:)])
    {
        // This is the code for iOS 7. contentSize no longer returns the correct value, so
        // we have to calculate it.
        //
        // This is partly borrowed from HPGrowingTextView, but I've replaced the
        // magic fudge factors with the calculated values (having worked out where
        // they came from)
        
        CGRect frame = textView.bounds;
        
        // Take account of the padding added around the text.
        
        UIEdgeInsets textContainerInsets = textView.textContainerInset;
        UIEdgeInsets contentInsets = textView.contentInset;
        
        CGFloat leftRightPadding = textContainerInsets.left + textContainerInsets.right + textView.textContainer.lineFragmentPadding * 2 + contentInsets.left + contentInsets.right;
        CGFloat topBottomPadding = textContainerInsets.top + textContainerInsets.bottom + contentInsets.top + contentInsets.bottom;
        
        frame.size.width -= leftRightPadding;
        frame.size.height -= topBottomPadding;
        
        NSString *textToMeasure = textView.text;
        if ([textToMeasure hasSuffix:@"\n"])
        {
            textToMeasure = [NSString stringWithFormat:@"%@-", textView.text];
        }
        
        // NSString class method: boundingRectWithSize:options:attributes:context is
        // available only on ios7.0 sdk.
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
        
        NSDictionary *attributes = @{ NSFontAttributeName: textView.font, NSParagraphStyleAttributeName : paragraphStyle };
        
        CGRect size = [textToMeasure boundingRectWithSize:CGSizeMake(CGRectGetWidth(frame), MAXFLOAT)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:attributes
                                                  context:nil];
        
        CGFloat measuredHeight = ceilf(CGRectGetHeight(size) + topBottomPadding);
        return measuredHeight;
    }
    else
    {
        return textView.contentSize.height;
    }
}
-(CGRect)frameForTextView:(UITextView *)textView WithText:(NSString *)text
{
    CGSize maximumLabelSize = CGSizeMake(textView.frame.size.width, FLT_MAX);
    
    CGSize expectedLabelSize = [text sizeWithFont:textView.font constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByWordWrapping];
    //adjust the label the the new height.
    CGRect newFrame = textView.frame;
    newFrame.size.height = expectedLabelSize.height>44.0f ? 44.0f :expectedLabelSize.height;
    //    label.frame = newFrame;
    
    return newFrame;
}

-(void)viewWillAppear:(BOOL)animated
{
   
    [super viewWillAppear:animated];
    
    if (!linkToOpenInSafari) {
        safariButton.hidden = YES;
    }else{
        
        safariButton.hidden = NO;
    }
    [self customizeNavigationBar];
    [self.navigationController setToolbarHidden:YES];
    [self.navigationController.toolbar setBarStyle:UIBarStyleDefault];  //for example
    
    //set the toolbar buttons
    
    
    // Create a custom UIBarButtonItem with the button
    
    UIBarButtonItem *fixedSpace =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                  target:nil
                                                  action:nil];
//    fixedSpace.width = 10;
    UIButton *shareButton =[UIButton buttonWithType:UIButtonTypeCustom];
    shareButton.frame = CGRectMake(0 ,0,30,30);
    [shareButton setImage:[UIImage imageNamed:@"sharingiconnew"] forState:UIControlStateNormal];
    [shareButton addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *share = [[UIBarButtonItem alloc] initWithCustomView:shareButton];
    
    [self setToolbarItems:[NSArray arrayWithObjects:fixedSpace,share,nil]];
    
    
    
    //Adding Proper Code For Vertical Allignment Issue Fix
        [heading enableObserver:YES onObject:heading selector:@selector(contentSize)];
        [subTopicDesc enableObserver:YES onObject:subTopicDesc selector:@selector(contentSize)];
    
//    if (![subTopicDesc.text isEqualToString:@"AJ"] && subTopicDesc.text.length>0) {
//        NSString *stringToBeAssigned =subTopicDesc.text;
//        [subTopicDesc setText:@"AJ"];
//        [subTopicDesc setText:stringToBeAssigned];
//    }
//    if (![heading.text isEqualToString:@"AJ"] && heading.text.length>0) {
//        NSString *stringToBeAssigned =heading.text;
//        [heading setText:@"AJ"];
//        [heading setText:stringToBeAssigned];
//    }
}
-(void)customizeNavigationBar{
    
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    
//    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
//    [backView setBackgroundColor:[UIColor clearColor]];
//    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
//    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
//    [backView addSubview:titleImageView];
//    titleImageView.contentMode = UIViewContentModeCenter;
//    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        bgView.frame = viewFrame;
        
    }

    
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        bgView.frame = viewFrame;
    }
    
    //    }
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked){
        
        if ([request.URL.scheme isEqualToString:@"tel"])
        {
            safariButton.hidden = YES;
            return YES;
        }
        
        else if ([request.URL.scheme isEqualToString:@"mailto"])
        {
            safariButton.hidden = YES;
            return YES;
        }
        else{
            if (![Common currentNetworkStatus])
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"IMMIGRATION_BASICS_TITLE", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
                [alertView show];
                return NO;
            }
            if (linkToOpenInSafari)
                linkToOpenInSafari = nil;
            safariButton.hidden = NO;
            linkToOpenInSafari =[request.URL absoluteString];
            if ([linkToOpenInSafari hasPrefix:@"http://"] || [linkToOpenInSafari hasPrefix:@"https://"])
            {
                
            }
            else
            {
                linkToOpenInSafari = [NSString stringWithFormat:@"http://%@",linkToOpenInSafari];
            }

            
        }
        

    }
    return YES;
    
    
}
- (IBAction)tappedOnSafari:(id)sender
{
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ABOUT_IMMIGO_TITLE", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
        [alertView show];
        return ;
    }
    NSURL *requestUrl =[NSURL URLWithString:linkToOpenInSafari];
    [[UIApplication sharedApplication] openURL:requestUrl];
}
- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];
}
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [activityController startAnimating];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [activityController stopAnimating];
    //NSLog(@"error %@",[error debugDescription]);
}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
    subTopicWebView.scrollView.minimumZoomScale = 0; // set similar to previous.
    subTopicWebView.scrollView.maximumZoomScale = 10; // set similar to previous.
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    subTopicWebView.scrollView.delegate = self; // set delegate method of UISrollView
    subTopicWebView.scrollView.maximumZoomScale = 10; // set as you want.
    subTopicWebView.scrollView.minimumZoomScale = 0; // set as you want.

    [activityController stopAnimating];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
@end
