//
//  MapViewController.h
//  Immigo
//
//  Created by pradeep ISPV on 3/11/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController <MKMapViewDelegate>
{
    IBOutlet MKMapView *legalMapView;
    
}
@property (strong, nonatomic) IBOutlet UILabel *resultsNear;
@property (strong, nonatomic) IBOutlet UILabel *areaName;
@property (strong, nonatomic) IBOutlet UIButton *listButton;
- (IBAction)listButtontapped:(id)sender;

@end
