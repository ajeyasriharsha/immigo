//
//  AdvancedSearchVC.h
//  Immigo
//
//  Created by pradeep ISPV on 3/6/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
@protocol AdvancedSearchVCDelegate <NSObject>
@optional
-(void)closePopUpIfTouchOutSide;
@end

@interface AdvancedSearchVC : UIViewController
{
    IBOutlet UIScrollView *optionsScrollView;
    IBOutlet UIView *childView;
    UITextField *activeField;
    BOOL keyboardVisible;
    CGPoint offset;
    
}
@property (nonatomic,strong) CLLocation *selectedLocation;
@property (nonatomic) CLLocationCoordinate2D coordinates;
@property (nonatomic) BOOL isSwitchOn;
@property (nonatomic,strong)  UITextField *activeField;
@property (nonatomic) id<AdvancedSearchVCDelegate> delegate;
@property (nonatomic,strong) NSString *zipCode;

@property(weak, nonatomic) UIView *activeTextView;
- (IBAction)tappedOnAreaOfLaw:(id)sender;
- (IBAction)tappedOnLegalAssistance:(id)sender;
- (IBAction)tappedOnLanguage:(id)sender;

- (IBAction)advanceSearchTapped:(id)sender;
@end
