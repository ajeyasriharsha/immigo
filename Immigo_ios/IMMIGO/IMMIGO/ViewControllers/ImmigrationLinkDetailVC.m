//
//  ImmigrationLinkDetailVC.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/3/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "ImmigrationLinkDetailVC.h"
#import "Common.h"
#import "ImmigrationLinkVC.h"

@interface ImmigrationLinkDetailVC ()<UIScrollViewDelegate>
{
    
    IBOutlet UIView *bgView;
    IBOutlet UIWebView *linkWebView;
    IBOutlet UIButton *topHeading;
    UIActivityIndicatorView *activityView;
    __weak IBOutlet UIButton *safariButton;
    
    IBOutlet UIButton *titleheading;
    
    
}
@end

@implementation ImmigrationLinkDetailVC
@synthesize linkUrl;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    titleheading.backgroundColor=UIColorFromRGB(0Xc51039);
    titleheading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        //NSLog(@"runnig on < 7.0");
        
        
       
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"left arrow"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        UIImage *image = [UIImage imageNamed:@"question"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = rightItem;
        
        [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }

//    self.navigationController.navigationBarHidden = NO;
//    UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//    UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//    titlView.image = titleLogo;
//    self.navigationItem.titleView = titlView;

    activityView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, linkWebView.frame.size.width, linkWebView.frame.size.height)];
    activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    activityView.autoresizingMask = UIViewAutoresizingNone;
    //    activityView.tintColor = [UIColor blueColor];
    [linkWebView addSubview:activityView];
    activityView.hidden = YES;
    
    
    /*
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlPredic = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    BOOL isValidURL = [urlPredic evaluateWithObject:linkUrl];
    
    if(isValidURL)
    {
        linkUrl = [linkUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *websiteUrl = [NSURL URLWithString:linkUrl];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
        [linkWebView loadRequest:urlRequest];    // Do any additional setup after loading the view.
    }
    else
    {
        //NSLog(@"URL IS NOT VALID.");
//        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[linkUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] ]];
    }
*/
    NSURL *myURL;
    if ([linkUrl hasPrefix:@"http://"] || [linkUrl hasPrefix:@"https://"]) {
        myURL = [NSURL URLWithString:linkUrl];
    } else {
        myURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",linkUrl]];
    }
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:myURL];
    [linkWebView loadRequest:urlRequest];    // Do any additional setup after loading the view.

    

    
    /*
    [self.navigationController setToolbarHidden:NO];
    [self.navigationController.toolbar setBarStyle:UIBarStyleDefault];  //for example
    
    //set the toolbar buttons
    
    
    // Create a custom UIBarButtonItem with the button
    UIBarButtonItem *share = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(share:)];

    [self setToolbarItems:[NSArray arrayWithObjects:share,nil]];
    */
    
}

-(IBAction)share:(id)sender{
    
    NSString *textToShare = @"Look at this awesome website for aspiring iOS Developers!";
    NSURL *myWebsite = [NSURL URLWithString:@"http://www.codingexplorer.com/"];
    
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo,
                                   UIActivityTypePostToTencentWeibo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
    
    [activityVC setCompletionHandler:^(NSString *activityType, BOOL completed)
     {
         if (completed)
         {
             UIAlertView *objalert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"APP_NAME", @"") message:NSLocalizedString(@"POST_SUCCESSFUL", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
             [objalert show];
             objalert = nil;
         }else
         {
             UIAlertView *objalert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"APP_NAME", @"") message:NSLocalizedString(@"POST_FAILUER_ERROR", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
             [objalert show];
             objalert = nil;
         }
     }];

    
}
- (BOOL)webView:(UIWebView *)wv shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    activityView.hidden = NO;
    [activityView startAnimating];

    if (navigationType == UIWebViewNavigationTypeLinkClicked){
        
        
        if ([request.URL.scheme isEqualToString:@"tel"])
        {
            
            return YES;
        }
        
        else if ([request.URL.scheme isEqualToString:@"mailto"])
        {
            return YES;
        }
        
        else {
            
            if (![Common currentNetworkStatus])
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"DISCLAIMER_TITLE", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
                [alertView show];
                return NO;
            }
            // Determine if we want the system to handle it.
            NSURL *url = request.URL;
            if (![url.scheme isEqual:@"http"] && ![url.scheme isEqual:@"https"]) {
                if ([[UIApplication sharedApplication]canOpenURL:url]) {
                    [[UIApplication sharedApplication]openURL:url];
                    return NO;
                }
            }
        }
        
        
    }
    
    return YES;
}
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    activityView.hidden = NO;
    [activityView startAnimating];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    webView.scrollView.delegate = self; // set delegate method of UISrollView
    webView.scrollView.maximumZoomScale = 10; // set as you want.
    webView.scrollView.minimumZoomScale = 0; // set as you want.

    [activityView stopAnimating];
    activityView.hidden = YES;
}


- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale; // scale between minimum and maximum. called after any 'bounce' animations

{
    linkWebView.scrollView.minimumZoomScale = 0; // set similar to previous.
    linkWebView.scrollView.maximumZoomScale = 10; // set similar to previous.
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    //NSLog(@"Error : %@",error);
    
    [activityView stopAnimating];
    activityView.hidden = YES;
    
    
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"IMMIGRATION_BASICS_TITLE", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
        [alertView show];
        return;
    }
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [self customizeNavigationBar];
    [super viewWillAppear:animated];
}
-(void)customizeNavigationBar{
    
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    
//    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
//    [backView setBackgroundColor:[UIColor clearColor]];
//    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
//    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
//    [backView addSubview:titleImageView];
//    titleImageView.contentMode = UIViewContentModeCenter;
//    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        bgView.frame = viewFrame;
        
    }
    
    
}
-(IBAction)openInSafari:(id)sender{
    
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"IMMIGRATION_BASICS_TITLE", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
        [alertView show];
        return ;
    }
    NSURL *requestUrl =[NSURL URLWithString:linkUrl];
    [[UIApplication sharedApplication] openURL:requestUrl];
    
}
- (IBAction)backButtonAction:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];
}
#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        bgView.frame = viewFrame;
    }
    
    //    }
}

-(void)viewWillDisappear:(BOOL)animated {
    
     [self.navigationController setToolbarHidden:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
@end
