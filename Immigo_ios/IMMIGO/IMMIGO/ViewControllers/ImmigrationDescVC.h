//
//  ImmigrationDescVC.h
//  Immigo
//
//  Created by pradeep ISPV on 3/7/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubTopic.h"
#import "ShareToSocialNetworkingSites.h"

@interface ImmigrationDescVC : UIViewController
{
    IBOutlet UIWebView *subTopicWebView;
    ShareToSocialNetworkingSites *shareObj;
    IBOutlet UIButton *titleheading;
    IBOutlet UILabel *sepratorLine;

    
}
@property (nonatomic,strong) NSString *topicTitle;
@property (nonatomic,strong) NSString *subTopicTitle;
@property (weak, nonatomic) IBOutlet UILabel *dividerLabel;

@property (nonatomic,strong) SubTopic *subtopic;
@end
