//
//  ImmigrationNewsVC.m
//  Immigo
//
//  Created by pradeep ISPV on 3/6/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "ImmigrationNewsVC.h"
#import "NewsDetailVC.h"
#import "AboutUsVC.h"
#import "NewsCell.h"
#import "NewsModel.h"
#import "Categories.h"
#import "Common.h"
#import "AppDelegate.h"

@interface ImmigrationNewsVC ()<DataDownloaderDelegate>
{
    NSArray *newsList;
    __weak IBOutlet UIView *bgView;
}
@end

@implementation ImmigrationNewsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [Common logEventToGoogleAnalytics:@"Immigo ImmigrationNews Screen"];
//    [Common logMainEventsToGoogleAnalytics:@"Immigo ImmigrationNews Screen" withAction:@"Immigo ImmigrationNews Screen" andLabel:@"" and:0];
    shareObj = [[ShareToSocialNetworkingSites alloc] init];
    [[DataDownloader sharedDownloader] setDelegate:self];
    [[DataDownloader sharedDownloader] cancelAllRequests];
        [[NewsModel sharedDataSource] fetchNews];


    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        //NSLog(@"runnig on < 7.0");
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"left arrow"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        UIImage *image = [UIImage imageNamed:@"question"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = rightItem;
        
        [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];
    }

    heading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
    heading.backgroundColor=UIColorFromRGB(0Xc51039);//RedColor ThroughtAPP//JUHI
    self.navigationController.navigationBarHidden = NO;
//    UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//    UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//    titlView.image = titleLogo;
//    self.navigationItem.titleView = titlView;
    newsTable.rowHeight = 58.0f;

	// Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    
	
    [super viewWillAppear:animated];
    [self customizeNavigationBar];

}
-(void)customizeNavigationBar{
    
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    
//    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
//    [backView setBackgroundColor:[UIColor clearColor]];
//    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
//    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
//    [backView addSubview:titleImageView];
//    titleImageView.contentMode = UIViewContentModeCenter;
//    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        
        bgView.frame = viewFrame;
        
    }else {
        
        CGRect viewFrame = bgView.frame;
        // navigationBar.frame.origin.y//
        viewFrame.origin.y = 20+frame.size.height;
        bgView.frame = viewFrame;
        
    }
    
    
    
}
#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        viewFrame.size.height = (viewFrame.size.width == 320) ? 408:504;
        bgView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        bgView.frame = viewFrame;
    }
    
    //    }
}


-(void)NewsLoaded
{
    newsList = [[NewsModel sharedDataSource] allNewsItems];
    

    [newsTable reloadData];
}
-(void)NewsLoadFailure
{
    newsList = [[NewsModel sharedDataSource] allNewsItems];
    [newsTable reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];
}
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UILabel *lbl = [[UILabel alloc] init];
//    lbl.textAlignment = NSTextAlignmentLeft;
//    lbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
//    lbl.textColor = [UIColor grayColor];
//    lbl.text = @"  TOPIC: Asylum";
//    return lbl;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 40.0f;
//}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [newsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    News *news = [newsList objectAtIndex:indexPath.row];
    
    
    SWTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];//[requestsTbl dequeueReusableCellWithIdentifier:@"requestCell" forIndexPath:indexPath];
    [cell setCellHeight:cell.frame.size.height];
    cell.containingTableView = tableView;
    cell.rightUtilityButtons = [self rightButtons];
    cell.delegate = self;

//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
//    
//    if (!cell)
//    {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
//    }
    
    
    NSRange range = [news.origanization rangeOfString:@"^\\s*" options:NSRegularExpressionSearch];
    NSString *trimmedTitle = [news.origanization stringByReplacingCharactersInRange:range withString:@""];
    
    NSString *title = [trimmedTitle length] > 35 ? [[trimmedTitle substringToIndex:35] stringByAppendingString:@"..."] :trimmedTitle;
    /*cell.topicTitle.text = [NSString stringWithFormat:@"%@ | %@",title,[news.date listDateFormat]];

//    cell.topicTitle.text = [news.author.name trimmedString];
    cell.topicSubTitle.text = [news.title trimmedString];
    cell.topicSubTitle.numberOfLines = 2;
    
    cell.topicTitle.font = [UIFont fontWithName:FONT_SEMIBOLD size:8.0];
    cell.topicTitle.textColor = [UIColor redColor];
    cell.topicSubTitle.font = [UIFont fontWithName:FONT_SEMIBOLD size:11.5];
    cell.topicTitle.textColor = UIColorFromRGB(0X6A6A6A); // VAMSHI // UIColorFromRGB(UI_TRAINING_HEADER_TEXT_COLOR);
    cell.topicSubTitle.textColor = UIColorFromRGB(0X124A7F); // VAMSHI // UIColorFromRGB(UI_TRAINING_DESC_TEXT_COLOR);
     */
    
//    cell.topicSubTitle.text = [NSString stringWithFormat:@"%@ | %@",title,[news.date listDateFormat]];
    cell.topicSubTitle.text = [NSString stringWithFormat:@"%@ - %@",title,[news.date listDateFormat]]; // VAMSHI
    
    //    cell.topicTitle.text = [news.author.name trimmedString]; //topicSubTitle //topicTitle
    cell.topicTitle.text = [news.title trimmedString];
    cell.topicTitle.numberOfLines = 2;
    
    cell.topicSubTitle.font = [UIFont fontWithName:FONT_SEMIBOLD size:8.0];
    cell.topicSubTitle.textColor = [UIColor redColor];
    cell.topicTitle.font = [UIFont fontWithName:FONT_SEMIBOLD size:11.5];
    cell.topicSubTitle.textColor = UIColorFromRGB(0X6A6A6A); // VAMSHI // UIColorFromRGB(UI_TRAINING_HEADER_TEXT_COLOR);
    cell.topicTitle.textColor = UIColorFromRGB(0X124A7F); // VAMSHI // UIColorFromRGB(UI_TRAINING_DESC_TEXT_COLOR);
//    cell.accVIew = [[UIImageView alloc] initWithImage:];
    [cell.accView setImage:[UIImage imageNamed:@"share_icon"] forState:UIControlStateNormal];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        [_objects removeObjectAtIndex:indexPath.row];
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
//    }
//}

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"IMMIGRATION_NEWS_TITLE", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
        [alertView show];
    }
    else
    {
        UIStoryboard *storyboard;
        if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        else
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
        NewsDetailVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"NewsDetailVC"];
        toVC.selectedNews = [newsList objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:toVC animated:YES];
    }

}
- (NSArray *)rightButtons
{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    
    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[UIImage imageNamed:@"linkedin_icon"]];
    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[UIImage imageNamed:@"twitter_icon"]];
    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[UIImage imageNamed:@"facebook_icon"]];

    return leftUtilityButtons;
}

#pragma mark - SWTableViewDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"IMMIGRATION_NEWS_TITLE", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
        [alertView show];
        return ;
    }
    NSIndexPath *indexPath = [newsTable indexPathForCell:cell];
    News *news = [newsList objectAtIndex:indexPath.row];
    
    switch (index)
    {
        case 0:
        {
            //NSLog(@"linkedin was pressed");
            [shareObj linkedInLogIn:news.newsLink andText:news.title fromViewContoller:self];
            //[userRequests objectAtIndex:indexPath.row];
            //            [[ContactsModel sharedDataSource] processFriendRequest:indexedReq withStatus:CRS_ACCEPTED]; 9538883912
        }
            break;
        case 1:
        {
            //NSLog(@"twitter was pressed");
            [shareObj tweetTapped:news.newsLink andText:news.title fromViewContoller:self];
            //            [[ContactsModel sharedDataSource] processFriendRequest:indexedReq withStatus:CRS_DENIED];
        }
            break;
        case 2:
        {
            //NSLog(@"fb was pressed");
            [shareObj fbShareBtnTapped:news.newsLink andText:news.title fromViewContoller:self];
            //            [[ContactsModel sharedDataSource] processFriendRequest:indexedReq withStatus:CRS_BLOCKED];
        }
            break;
        default:
            break;
    }
    //    if (indexPath)
    //    {
    //        [userRequests removeObjectAtIndex:indexPath.row];
    //        [requestsTbl deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    //    }
    //
}
/*
 - (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
 switch (index) {
 case 0:
 {
 //NSLog(@"More button was pressed");
 UIAlertView *alertTest = [[UIAlertView alloc] initWithTitle:@"Hello" message:@"More more more" delegate:nil cancelButtonTitle:@"cancel" otherButtonTitles: nil];
 [alertTest show];
 
 [cell hideUtilityButtonsAnimated:YES];
 break;
 }
 case 1:
 {
 // Delete button was pressed
 NSIndexPath *cellIndexPath = [requestsTbl indexPathForCell:cell];
 
 [userRequests[cellIndexPath.section] removeObjectAtIndex:cellIndexPath.row];
 [requestsTbl deleteRowsAtIndexPaths:@[cellIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
 break;
 }
 default:
 break;
 }
 }
 */
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    return YES;
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
@end
