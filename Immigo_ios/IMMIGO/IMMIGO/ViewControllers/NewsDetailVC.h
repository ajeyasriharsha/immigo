//
//  NewsDetailVC.h
//  Immigo
//
//  Created by pradeep ISPV on 3/10/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "News.h"

@interface NewsDetailVC : UIViewController<UIScrollViewDelegate>
{
    IBOutlet UILabel *newsSubtitle;
    IBOutlet UIButton *heading;
    IBOutlet UIWebView *newsDetailWebView;
}

@property (nonatomic,strong) News *selectedNews;
@end
