//
//  TrainingsDetailVC.h
//  Immigo
//
//  Created by pradeep ISPV on 3/10/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import <EventKit/EventKit.h>

@interface TrainingsDetailVC : UIViewController
{
    IBOutlet UILabel *trainingTitle;
    IBOutlet UILabel *trainingsPlace;
    IBOutlet UITextView *trainingDesc;
    
    Event *selectedEvent;
}
-(NSDate*)getDateFromString:(NSString *)dateString withTime:(NSString *)time isStartTime:(BOOL)isStart;
//-(void)setEvent:(NSString *)title withStartDate:(NSDate *)startDate withEndDate:(NSDate *)endDate withResecheduling:(BOOL)rescheduling completion:(void (^)(void))completionBlock;
@property (nonatomic,strong) Event *selectedEvent;
@property (nonatomic,retain) NSString *selectedEventLocation;
@property (nonatomic, retain) EKEventStore *eventStore;
@property (nonatomic, retain) NSDate *recursiveEventEndDate;
@end
