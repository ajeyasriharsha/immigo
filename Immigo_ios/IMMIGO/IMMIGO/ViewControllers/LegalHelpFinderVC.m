//
//  LegalHelpFinderVC.m
//  Immigo
//
//  Created by pradeep ISPV on 3/6/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "LegalHelpFinderVC.h"
#import "AboutUsVC.h"
#import "DCRoundSwitch.h"
#import "Common.h"
#import "PlaceHolderField.h"
#import "AdvancedSearchVC.h"
#import "LegalSearchListVC.h"
#import "LegalFinderModel.h"
#import "AppDelegate.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "Categories.h"
#import "FTLocationManager.h"
#import "INTULocationManager.h"
#import "RCSwitchOnOff.h"
@interface LegalHelpFinderVC ()
{
    IBOutlet UILabel *subHeading;
    
   // IBOutlet PlaceHolderField *selectState;
    IBOutlet UIButton *heading;
    IBOutlet UIButton *searchButton;
    IBOutlet UITextField *currentLocField;
    IBOutlet UIView *bgView;
    IBOutlet UIImageView *current_BG;
    IBOutlet UIImageView *zip_BG;
    IBOutlet UIButton *advancedSrcBtn;
    IBOutlet PlaceHolderField *searchField;
    IBOutlet UIScrollView *optionsScrollView;
    IBOutlet TPKeyboardAvoidingScrollView *scrollView;
//    DCRoundSwitch *switchControl;//ON/OFF Switch Implemtation//JUHI
    RCSwitchOnOff *switchControl;
    
    NSString *currentLattitude;
    NSString *currentLongitude;
    
    
  //  BOOL locationOn;
}
@property (assign, nonatomic) INTULocationRequestID locationRequestID;
@end

@implementation LegalHelpFinderVC
//@synthesize locationManager;
@synthesize selectedLocation;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [scrollView contentSizeToFit];
    [Common logEventToGoogleAnalytics:@"Immigo LegalHelpFinder Screen"];
    
    optionsScrollView.contentSize = CGSizeMake(320,320);
    heading.backgroundColor=UIColorFromRGB(0Xc51039);
    //selectState.placeholder = @"Select State";
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        //NSLog(@"runnig on < 7.0");
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"left arrow"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        UIImage *image = [UIImage imageNamed:@"question"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = rightItem;
        
        
        [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];
        
        
    }
    
    
    currentLocField.font = searchField.font = [UIFont fontWithName:FONT_SEMIBOLD size:14.32f];
    
    self.navigationController.navigationBarHidden = NO;
    {
        UIImage *bgIm = [UIImage imageNamed:@"TextField_BG"];
        current_BG.image = zip_BG.image = [bgIm stretchableImageWithLeftCapWidth:bgIm.size.width/2 topCapHeight:bgIm.size.height/2];
    }
    searchButton.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:14.91];
    advancedSrcBtn.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:12.85f];
    [advancedSrcBtn setTitleColor:UIColorFromRGB(0XC51039) forState:UIControlStateNormal];
    
    heading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
    subHeading.font = [UIFont fontWithName:FONT_EXTRA_BOLD size:12.00f]; //[UIFont fontWithName:FONT_ITALIC size:13.0f];  // VAMSHI
    
//    UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//    UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//    titlView.image = titleLogo;
//    self.navigationItem.titleView.backgroundColor =titlView.backgroundColor= [UIColor clearColor];
//    self.navigationItem.titleView = titlView;
    
//    UIImage *btnBg = [UIImage imageNamed:@"RedBtn_Bg"];        // VAMSHI
    [searchButton setBackgroundColor:UIColorFromRGB(0X114171)];  //VAMSHI
//    [searchButton setBackgroundImage:[btnBg stretchableImageWithLeftCapWidth:btnBg.size.width/2 topCapHeight:btnBg.size.height/2] forState:UIControlStateNormal];
    searchButton.contentMode = UIViewContentModeRedraw;
    
    //ON/OFF Switch Implemtation//JUHI
    
//    switchControl = [[DCRoundSwitch alloc] initWithFrame:CGRectMake(current_BG.frame.origin.x+current_BG.frame.size.width-78, currentLocField.frame.origin.y, 70, 30)];
//    [switchControl addTarget:self action:@selector(tappedOnSwitch:) forControlEvents:UIControlEventValueChanged];
//    switchControl.on = NO;
//    switchControl.autoresizingMask= UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin;// | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin;//UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
//    switchControl.backgroundColor = [UIColor clearColor];
    
    switchControl = [[RCSwitchOnOff alloc] initWithFrame:CGRectMake(current_BG.frame.origin.x+current_BG.frame.size.width-83, currentLocField.frame.origin.y-3, 81, 36)];
    [switchControl addTarget:self action:@selector(tappedOnSwitch:) forControlEvents:UIControlEventValueChanged];
    switchControl.on = NO;
    switchControl.autoresizingMask= UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin;// | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin;//UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;

    [scrollView  addSubview:switchControl];
    
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [self customizeNavigationBar];
    [super viewWillAppear:animated];
}
-(void)customizeNavigationBar{
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    
//    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
//    [backView setBackgroundColor:[UIColor clearColor]];
//    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
//    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
//    [backView addSubview:titleImageView];
//    titleImageView.contentMode = UIViewContentModeCenter;
//    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        bgView.frame = viewFrame;
        
    }
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
//    if (locationOn)
//    {
//        // Turn off the location manager to save power.
//        [locationManager stopUpdatingLocation];
//    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        bgView.frame = viewFrame;
    }
    
    //    }
}

#pragma mark - UItextfield delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 5 || ![self isNumeric:string]) ? NO : YES;
}

//-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    if([self isNumeric:string])
//        return TRUE;
//    else
//        return FALSE;
//}

-(BOOL)isNumeric:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}

- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];
}
//ON/OFF Switch Implemtation//JUHI
-(IBAction)tappedOnSwitch:(RCSwitchOnOff *)sender
{
    if ([sender isOn])
    {
        
        searchField.text = @"";
        searchField.userInteractionEnabled = NO;
        
        
        /*
        //  Get FTLocationManager singleton instance
        FTLocationManager *locationManager = [FTLocationManager sharedManager];
        
        [locationManager updateLocationWithCompletionHandler:^(CLLocation *location, NSError *error, BOOL locationServicesDisabled) {
            
            NSString *outputText;
            if (error)
            {
                
               
                //  Often cause of error is that Location services are disabled for this app
                //  BOOL passed to the completion handler is YES if this is the cause of error
                if(locationServicesDisabled) {
                    outputText = [NSString stringWithFormat:@"Failed to get your location as location services are disabled for IMMIGO application."];
                } else {
                    outputText = [NSString stringWithFormat:@"Failed to get your location as location services are disabled for IMMIGO application."];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [switchControl setOn:NO];
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Immigo"
                                                                    message:outputText
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                });
                
                
                
            }
            else {
                outputText = [NSString stringWithFormat:@"Received CLLocation: %@", location];
                selectedLocation =location;
                currentLattitude = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
                currentLongitude = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
            }
            
            NSLog(@"%@", outputText);
            
        }];
         
         
         */
        
        __weak __typeof(self) weakSelf = self;
        __block NSString *outputText = nil;
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
      self.locationRequestID=  [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyNeighborhood
                                                                    timeout:10
                                                       delayUntilAuthorized:YES
                                                                      block:
                                  ^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                      __typeof(weakSelf) strongSelf = weakSelf;
                                      
                                      if (status == INTULocationStatusSuccess) {
                                          // achievedAccuracy is at least the desired accuracy (potentially better)
                                         
                                          strongSelf.selectedLocation =currentLocation;
                                          currentLattitude = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
                                          currentLongitude = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
                                          
                                      }
                                      else if (status == INTULocationStatusServicesDisabled) {
                                        //  User has turned off location services device-wide (for all apps) from the system Settings app.
                                          outputText = [NSString stringWithFormat:NSLocalizedString(@"LOCATION_FAILURE_ERROR", @"")];
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"APP_NAME", @"")
                                                                                              message:outputText
                                                                                             delegate:nil
                                                                                    cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"")
                                                                                    otherButtonTitles:nil];
                                              [alert show];
                                              
                                              [switchControl setOn:NO];
                                          });
                                          
                                          
                                          
                                      }else if (status == INTULocationStatusServicesDenied){
                                         /** User has explicitly denied this app permission to access location services. */
                                          outputText = [NSString stringWithFormat:NSLocalizedString(@"LOCATION_PERMISSIONS_DENIED_ERROR", @"")];
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"APP_NAME", @"")
                                                                                              message:outputText
                                                                                             delegate:nil
                                                                                    cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"")
                                                                                    otherButtonTitles:nil];
                                              [alert show];
                                              
                                              [switchControl setOn:NO];
                                          });
                                          
                                      }
                                      else if (status == INTULocationStatusTimedOut) {
                                          // You may wish to inspect achievedAccuracy here to see if it is acceptable, if you plan to use currentLocation
                                          [switchControl setOn:NO];
                                      }
                                      else {
                                          // An error occurred
                                          [switchControl setOn:NO];
                                          
                                      }
                                      
                                  }];


    }
    else
    {
        searchField.userInteractionEnabled = YES;
    }
}



- (IBAction)advanceSearchTapped:(id)sender
{
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    AdvancedSearchVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"AdvancedSearchVC"];
    NSLog(@"Latitude:%@ and Longitude:%@",currentLattitude,currentLongitude);
    toVC.zipCode = searchField.text;
    toVC.coordinates =selectedLocation.coordinate;
    toVC.selectedLocation =selectedLocation;
    toVC.isSwitchOn =switchControl.isOn;
    [self.navigationController pushViewController:toVC animated:YES];
    
}

- (IBAction)searchButtonTapped:(id)sender
{
    [Common logMainEventsToGoogleAnalytics:@"LegalHelpFinder Search" withAction:@"LegalHelpFinder Search tapped" andLabel:@"" and:0];
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"LEGAL_HELP_FINDER_TITLE", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    if ([searchField.text length]==0 && ![switchControl isOn] )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"LEGAL_HELP_FINDER_TITLE", @"") message:NSLocalizedString(@"ENTER_ZIPCODE_ERROR", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alert show];
    }else if (([searchField.text length]<5 || [searchField.text isEqual:@"00000"]) && ![switchControl isOn])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Legal Help Finder" message:NSLocalizedString(@"ENTER_ZIPCODE_USE_CURRENT_LOCATION", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
        [alert show];
        
        }
    else
    {
        NSLog(@"lattitude %@ and longitude %@",currentLattitude,currentLongitude);
        [[LegalFinderModel sharedDataSource] fetchLegalFindersWithZipCode:searchField.text OrWithCurrentLocationLattitude:currentLattitude AndLongitude:currentLongitude];
        UIStoryboard *storyboard;
        if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        else
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
        LegalSearchListVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"LegalSearchListVC"];
        
        toVC.zipCode = searchField.text;
        toVC.coordinates = selectedLocation.coordinate;
        [DataDownloader sharedDownloader].delegate = (id)toVC;
        [self.navigationController pushViewController:toVC animated:YES];
    }
}


-(void)pushToViewControllerWithIdentifier:(NSString *)identifier //Pass a identifier Used in Storyboard for viewController
{
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    UIViewController* toVC = [storyboard instantiateViewControllerWithIdentifier:identifier];
    [self.navigationController pushViewController:toVC animated:YES];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //    searchField.font = [UIFont fontWithName:@"OpenSans-italic" size:14.32f];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
@end
