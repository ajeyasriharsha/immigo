//
//  TrainingsDetailContentVC.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/2/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"

@interface TrainingsDetailContentVC : UIViewController

@property (nonatomic,strong) Event *selectedEvent;
@end
