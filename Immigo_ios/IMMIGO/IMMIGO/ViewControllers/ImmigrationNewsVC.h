//
//  ImmigrationNewsVC.h
//  Immigo
//
//  Created by pradeep ISPV on 3/6/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import "DataDownloader.h"
#import "ShareToSocialNetworkingSites.h"

@interface ImmigrationNewsVC : UIViewController <SWTableViewCellDelegate>
{
    ShareToSocialNetworkingSites *shareObj;
    
    IBOutlet UIButton *heading;
    IBOutlet UITableView *newsTable;
}
@end
