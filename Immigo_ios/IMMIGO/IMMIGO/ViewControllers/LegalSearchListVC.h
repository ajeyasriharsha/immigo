//
//  LegalSearchListVC.h
//  Immigo
//
//  Created by pradeep ISPV on 3/11/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface LegalSearchListVC : UIViewController
{
    IBOutlet UILabel *resultsNearLabel;
    
    IBOutlet UILabel *searchAreaName;
    IBOutlet UIButton *mapButton;
    IBOutlet UITableView *searchList;
    
    
}

@property (nonatomic) CLLocationCoordinate2D coordinates;
@property (nonatomic,strong) NSString *zipCode;
@property (nonatomic,assign) BOOL isFromAdvancedSearch;
- (IBAction)mapButtonTapped:(id)sender;
@end
