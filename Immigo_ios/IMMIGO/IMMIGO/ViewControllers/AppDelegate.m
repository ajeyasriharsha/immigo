//
//  AppDelegate.m
//  Immigo
//
//  Created by pradeep ISPV on 2/27/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "AppDelegate.h"
#import "Common.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"
#import "APPConfig.h"
#import "RegisterDeviceModel.h"
#import "PushNotificationsDAO.h"
#import <QuartzCore/QuartzCore.h>
#import "FakeViewController_iPad.h"


#import "DataDownloader.h"
#import "SettingsVC.h"
#import "AboutImmigoVC_iPad.h"
#import "SplashViewController_iPad.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>


@interface AppDelegate ()
{
    Reachability *reachability;
    
    UIViewController *viewController;
}
@property (nonatomic,strong)     UIWindow* topWindow;
void uncaughtExceptionHandler(NSException *exception);
@end


/** Google Analytics configuration constants **/
static NSString *const kGAPropertyId =@"UA-51399355-1";// Placeholder property ID;
static NSString *const kTrackingPreferenceKey = @"allowTracking";
static BOOL const kGADryRun = NO;/*The dryRun flag should be set YES, whenever you are testing or debugging an implementation and do not want test data to appear in your Google Analytics reports.*/
static int const kGADispatchPeriod = 3;

@interface AppDelegate()
- (void)initializeGoogleAnalytics;
@end

@implementation AppDelegate
@synthesize tracker,topWindow;

void uncaughtExceptionHandler(NSException *exception)
{
    //NSLog(@"CRASH: %@", exception);
    //NSLog(@"Stack Trace: %@", [exception callStackSymbols]);
    // Internal error reporting
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
   #ifdef DEBUG
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
#endif

    

    // Override point for customization after application launch.
    //Get guid only on first launch and save it in userdefaults for Google Analytics
    if (![[NSUserDefaults standardUserDefaults] valueForKey:IMMIGO_GUID])
    {
        NSString *GUID = [Common generateGUID];
        [[NSUserDefaults standardUserDefaults] setValue:GUID forKey:IMMIGO_GUID];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
//    if (![[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY])
//    {
//        [self RegisterForAPNSWithForce:false];
//    }
    
    [self initializeGoogleAnalytics];
    [Fabric with:@[CrashlyticsKit]];
    

    //    [[Crashlytics sharedInstance] setDebugMode:YES];
    

    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if (![gestureRecognizer.view isKindOfClass:[UIButton class]])
    {
        return NO;
    }
    return YES;
}

-(IBAction)singleTapWebView:(id)sender
{
    //NSLog(@"tapped");
}

-(void)RegisterDeviceWithNotificationWithToken:(NSString *)token
{
    UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    UIRemoteNotificationType typesset = (UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge);
    if((types & typesset) == typesset)
    {
        [[RegisterDeviceModel sharedDataSource] registerDeviceWithId:token AndType:@"iOS" AndPushNotificationStatus:YES AtAppLaunch:YES];
        [PushNotificationsDAO insertIfDoesNotExistsWithStatus:YES];
    }
    else
    {
        [[RegisterDeviceModel sharedDataSource] registerDeviceWithId:token AndType:@"iOS" AndPushNotificationStatus:NO AtAppLaunch:YES];
        [PushNotificationsDAO insertIfDoesNotExistsWithStatus:NO];
    }

}

-(void) customiseNavigationBar :(UINavigationController *)navCtrlr{
    
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    
    UIViewController *rootViewController = navCtrlr;
    // You now have in rootViewController the view with your "Hello world" label and go button.
    
    // Get the navigation controller of this view controller with:
    UINavigationController *navigationController = (UINavigationController *)rootViewController;
    UINavigationBar *navigationBar = [navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
//    UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//    UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//    titlView.image = titleLogo;
//    titlView.contentMode = UIViewContentModeTop;
//    navigationController.navigationItem.titleView = titlView;
//    navigationController.navigationItem.titleView.backgroundColor = [UIColor clearColor];
//    
//    UIView *view = navigationController.navigationItem.titleView;
//    CGRect frame1 = view.frame;
//    frame1.size.height = 58.0f;
//    frame1.size.width = 120.0f;
//    [view setFrame:frame1];
    
    [navigationController.navigationItem rightBarButtonItem].imageInsets = UIEdgeInsetsMake(-24, 0, 0, 0);
    [navigationController.navigationItem leftBarButtonItem].imageInsets = UIEdgeInsetsMake(-24, 0, 0, 0);
    
}

#pragma mark - initializeGoogleAnalytics Method
- (void)initializeGoogleAnalytics
{
    [[GAI sharedInstance] setDispatchInterval:kGADispatchPeriod];
    [[GAI sharedInstance] setDryRun:kGADryRun];
    self.tracker = [[GAI sharedInstance] trackerWithTrackingId:kGAPropertyId];
    
    //    [MGMATSettings logEventToGoogleAnalytics:@"Pocket GMAT iOS App Opened"];
    [Common logMainEventsToGoogleAnalytics:@"Immigo iOS App Opened" withAction:@"Immigo iOS App Opened" andLabel:@"" and:0];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
//    [self.window endEditing:YES]; //REsign All first responders
    
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [Common logMainEventsToGoogleAnalytics:@"Immigo iOS App Closed" withAction:@"Immigo iOS App Closed" andLabel:@"" and:0];

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(deviceOrientationDidChange:) name:UIDeviceOrientationDidChangeNotification  object:nil];
    
    isInForeground = true;
    
    [self reloadSplash];
    
    [self performSelector:@selector(removeSplash) withObject:nil afterDelay:APP_TIMEFORSPLASH_FOREGROUND];

     */
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

/*
- (void)reloadSplash
{
    UIImage *image;
    CGRect windowRect;
    
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    
    UIInterfaceOrientation statusOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if (UIInterfaceOrientationIsPortrait(statusOrientation))
    {
        NSLog(@"portrain");
        //load the portrait view
        if([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad)
        {
            image = ([[UIDevice currentDevice].systemVersion floatValue] < 7.0) ? [UIImage imageNamed:@"Default-Portrait-Old"]:[UIImage imageNamed:@"Default_iPad_Portrait"];
            windowRect = CGRectMake(0, 0, 768, 1024);
        }
        else
        {
            image = (iOSDeviceScreenSize.height == 480)? [UIImage imageNamed:@"LaunchPortrait"] :[UIImage imageNamed:@"LaunchPortrait-5"];
            windowRect = (iOSDeviceScreenSize.height == 480)? CGRectMake(0, 0, 320, 480):CGRectMake(0, 0, 320, 568);
        }
    }
    else //if (statusOrientation == UIDeviceOrientationLandscapeLeft || statusOrientation == UIDeviceOrientationLandscapeRight)
    {
        NSLog(@"landscape");
        //load the landscape view
        if([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad)
        {
            image = ([[UIDevice currentDevice].systemVersion floatValue] < 7.0) ? [UIImage imageNamed:@"Default-Landscape-Old"]:[UIImage imageNamed:@"Default_iPad_Landscape"];
            windowRect = CGRectMake(0, 0, 1024, 768);
        }
        else
        {
            image = (iOSDeviceScreenSize.height == 480)? [UIImage imageNamed:@"landscape_480"] :[UIImage imageNamed:@"landscape_568"];
            windowRect = (iOSDeviceScreenSize.height == 480)? CGRectMake(0, 0, 480, 320):CGRectMake(0, 0, 568, 320);
        }
        
    }
    
    if (splashChildView == nil)
    {
        splashChildView = [[UIView alloc] initWithFrame:windowRect];
        splashChildView.backgroundColor = [UIColor redColor];
        foregroundSplashView = [[UIImageView alloc] initWithFrame:splashChildView.frame];
        foregroundSplashView.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight;
        [splashChildView addSubview:foregroundSplashView];
        
        [self.window.rootViewController.view addSubview:splashChildView];
    }
    else
    {
        CGRect rect = splashChildView.frame;
        rect =  windowRect;
        splashChildView.frame = rect;
    }
    foregroundSplashView.image = image;

}
- (void)deviceOrientationDidChange:(NSNotification *)notification
{
    if (!isInForeground)
        return;
    
    [self reloadSplash];

}
- (void)removeSplash
{
    isInForeground = false;
    [splashChildView removeFromSuperview];
    splashChildView = nil;
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}




*/



- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [self RegisterForAPNSWithForce:false];
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
#pragma mark APNS related methods and delegates...
-(void)RegisterForAPNSWithForce:(bool)forceRegister
{
    NSString *deviceToken=[[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY];
    if (deviceToken)
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:DEVICE_REGISTER_SUCCESSFUL_APPLAUNCH];
    }

    if ([deviceToken rangeOfString:@"-"].length >0)
    {
        forceRegister = true;
    }
    if(forceRegister || deviceToken==nil || [deviceToken length]==0)
    {
        
//        //-- Set Notification
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
        {
            // iOS 8 Notifications
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
        else
        {
            // iOS < 8 Notifications
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert |UIRemoteNotificationTypeNewsstandContentAvailability| UIRemoteNotificationTypeSound)];
        }

        
 //       [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeNewsstandContentAvailability | UIRemoteNotificationTypeSound)];
    }
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *deviceTokenString = [NSString stringWithFormat:@"%@",[deviceToken description]];
    
    NSString *newString = [deviceTokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
    newString = [newString stringByReplacingOccurrencesOfString:@"<" withString:@""];
    newString = [newString stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    [[NSUserDefaults standardUserDefaults] setValue:newString forKey:APNS_DEVICE_TOKEN_KEY];
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:APP_LAUNCH];
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:DEVICE_REGISTER_SUCCESSFUL_APPLAUNCH];
    [[NSUserDefaults standardUserDefaults] synchronize];

//    AboutImmigoVC_iPad *settings = [[AboutImmigoVC_iPad alloc] init];
    [PushNotificationsDAO insertIfDoesNotExistsWithStatus:YES];
//    [DataDownloader sharedDownloader].delegate = (id)settings;
    
    [[RegisterDeviceModel sharedDataSource] registerDeviceWithId:newString AndType:@"iOS" AndPushNotificationStatus:YES AtAppLaunch:YES];

}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    [PushNotificationsDAO insertIfDoesNotExistsWithStatus:NO];
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:APP_LAUNCH];
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:DEVICE_REGISTER_SUCCESSFUL_APPLAUNCH];
    [[NSUserDefaults standardUserDefaults] synchronize];

//    if ([[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY])
//        [[RegisterDeviceModel sharedDataSource] registerDeviceWithId:[[NSUserDefaults standardUserDefaults] valueForKey:APNS_DEVICE_TOKEN_KEY] AndType:@"iOS" AndPushNotificationStatus:NO AtAppLaunch:YES];

    UIAlertView* errorAlert= [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"NOTIFICATION_REGISTER_ERROR", @"")  message:[error localizedFailureReason] delegate:nil cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE","") otherButtonTitles:nil, nil];
    [errorAlert show];
    errorAlert=nil;
    
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:APNS_DEVICE_TOKEN_KEY];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    if(types & UIRemoteNotificationTypeAlert)
    {
        UIAlertView* notiAlert= [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"APP_NAME", @"")  message:[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] delegate:nil cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil, nil];
        [notiAlert show];
        notiAlert=nil;
    }
    //NSLog(@"NOTIFICATION %@", [[userInfo valueForKey:@"aps"] valueForKey:@"alert"]);
//    [UIApplication sharedApplication].applicationIconBadgeNumber = [userInfo valueForKey:@"badge"];

//    [[NotificationsModel sharedDataSource] didReceiveRemoteNotification:userInfo];
//    [UIApplication sharedApplication].applicationIconBadgeNumber=[[NotificationsDAO allObjects] count];
//    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

-(void)refreshFakeViewController
{

    FakeViewController_iPad *root =  (FakeViewController_iPad*) [[[[UIApplication sharedApplication] windows] objectAtIndex:0] rootViewController]; //navigationController.parentViewController;
    
    if (![root.legalSearchView isHidden])
    {
        root.legalSearchView.hidden = YES;
        if ([root.zipCodeField.text length]>0)
        {
            root.zipCodeField.text = [NSString string];
        }
        if ([root.zipCodeField isFirstResponder])
        {
            [root.zipCodeField resignFirstResponder];
        }
    }
    
    if (![root.legalAdvancedSearchView isHidden]) {
        root.legalAdvancedSearchView.hidden = YES;
    }
}

/*
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}
 */
@end
