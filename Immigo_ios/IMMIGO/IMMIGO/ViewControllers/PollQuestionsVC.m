//
//  PollQuestionsVC.m
//  Immigo
//
//  Created by pradeep ISPV on 3/10/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "PollQuestionsVC.h"
#import "AboutUsVC.h"
#import "Common.h"
#import "PollQuestionsModel.h"
#import "DataDownloader.h"

@interface PollQuestionsVC () <DataDownloaderDelegate>
{
    UIScrollView *scrollView;
    BOOL flipped;
    
    UILabel *questionText;
    int numberOfOptions;
    
    NSArray *QArray;
    NSUInteger currentQuestionIndex;
    
    UIButton *submitButton;
    UIButton *nextButton;
    
    BOOL optionSelcted;
    
    int answerNumber;
    
    BOOL submitSelected;
}
@property (nonatomic, strong) AMGProgressView *progressBar;
@end

@implementation PollQuestionsVC
@synthesize pollQuestionIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(CGRect)frameForLabel:(UILabel *)label WithText:(NSString *)text
{
    CGSize maximumLabelSize = CGSizeMake(label.frame.size.width, FLT_MAX);
    CGSize expectedLabelSize = [text sizeWithFont:label.font constrainedToSize:maximumLabelSize lineBreakMode:label.lineBreakMode];
    //adjust the label the the new height.
    CGRect newFrame = label.frame;
    newFrame.size.height = expectedLabelSize.height<44.0f ? 44.0f :expectedLabelSize.height;
    //    label.frame = newFrame;
    
    return newFrame;
}


-(void)viewWillAppear:(BOOL)animated
{

    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (frame.size.height != 58.0f)
    {
        [self customizeNavigationBar];
    }
    [super viewWillAppear:animated];
}
-(void)customizeNavigationBar
{
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    
    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
    [backView setBackgroundColor:[UIColor clearColor]];
    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
    [backView addSubview:titleImageView];
    titleImageView.contentMode = UIViewContentModeCenter;
    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = pollQuestionView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        pollQuestionView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = pollQuestionView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
//        viewFrame.size.height = (viewFrame.size.height == 480) ? 416 : 504;
        pollQuestionView.frame = viewFrame;
        
    }
    
}

- (NSArray *)filterThePercentageForPollQuestion:(PollQuestions *)pollQuestion
{
    int count = pollQuestion.optionsCount;
    
    
    NSMutableArray *optionArr = [NSMutableArray arrayWithCapacity:count];
    
    if (pollQuestion.answerOne)
    {
        [optionArr addObject:[NSString stringWithFormat:@"%d",pollQuestion.answerOnePercentage]];
    }
    if (pollQuestion.answerTwo)
    {
        [optionArr addObject:[NSString stringWithFormat:@"%d",pollQuestion.answerTwoPercentage]];
    }
    if (pollQuestion.answerThree)
    {
        [optionArr addObject:[NSString stringWithFormat:@"%d",pollQuestion.answerThreePercentage]];
    }
    if (pollQuestion.answerFour)
    {
        [optionArr addObject:[NSString stringWithFormat:@"%d",pollQuestion.answerFourPercentage]];
    }
    if (pollQuestion.answerFive)
    {
        [optionArr addObject:[NSString stringWithFormat:@"%d",pollQuestion.answerFivePercentage]];
    }
    if (pollQuestion.answerSix)
    {
        [optionArr addObject:[NSString stringWithFormat:@"%d",pollQuestion.answerSixPercentage]];
    }
    
    
    return [NSArray arrayWithArray:optionArr];
}
- (NSArray *)filterTheOptionTextForPollQuestion:(PollQuestions *)pollQuestion
{
    int count = pollQuestion.optionsCount;
    
    
    NSMutableArray *optionArr = [NSMutableArray arrayWithCapacity:count];
    
    if (pollQuestion.answerOne)
    {
        [optionArr addObject:pollQuestion.answerOne];
    }
    if (pollQuestion.answerTwo)
    {
        [optionArr addObject:pollQuestion.answerTwo];
    }
    if (pollQuestion.answerThree)
    {
        [optionArr addObject:pollQuestion.answerThree];
    }
    if (pollQuestion.answerFour)
    {
        [optionArr addObject:pollQuestion.answerFour];
    }
    if (pollQuestion.answerFive)
    {
        [optionArr addObject:pollQuestion.answerFive];
    }
    if (pollQuestion.answerSix)
    {
        [optionArr addObject:pollQuestion.answerSix];
    }
    
    
    return [NSArray arrayWithArray:optionArr];
}

- (NSArray *)filterTheKeyForPollQuestion:(PollQuestions *)pollQuestion
{
    int count = pollQuestion.optionsCount;
    
    
    NSMutableArray *optionArr = [NSMutableArray arrayWithCapacity:count];
    
    if (pollQuestion.answerOne)
    {
        [optionArr addObject:[NSString stringWithFormat:@"1"]];
    }
    if (pollQuestion.answerTwo)
    {
        [optionArr addObject:[NSString stringWithFormat:@"2"]];
    }
    if (pollQuestion.answerThree)
    {
        [optionArr addObject:[NSString stringWithFormat:@"3"]];
    }
    if (pollQuestion.answerFour)
    {
        [optionArr addObject:[NSString stringWithFormat:@"4"]];
    }
    if (pollQuestion.answerFive)
    {
        [optionArr addObject:[NSString stringWithFormat:@"5"]];
    }
    if (pollQuestion.answerSix)
    {
        [optionArr addObject:[NSString stringWithFormat:@"6"]];
    }
    
    
    return [NSArray arrayWithArray:optionArr];
}

#pragma mark- CutomIntialization
-(void)customIntializationWithPollQuestion:(PollQuestions *)pollQuestion
{
 
    
    NSArray *percentageArr = [self filterThePercentageForPollQuestion:pollQuestion];
    NSArray *objArr = [self filterTheOptionTextForPollQuestion:pollQuestion];
    NSArray *keysArr = [self filterTheKeyForPollQuestion:pollQuestion];

    optionSelcted = NO;
    if ([pollQuestion.answerStatus isEqual:@"Yes"])
    {
        flipped  = YES;
    }else
    {
    }
    heading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
    {
        self.view.clipsToBounds = YES;
    }
    
    float optionsGap = 5.0f;
    
    CGRect totalFrame = pollQuestionView.frame;
    CGPoint cursor = CGPointMake(totalFrame.origin.x, heading.frame.origin.y+heading.frame.size.height);
    CGSize size = CGSizeMake(totalFrame.size.width, totalFrame.size.height-20);
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, size.height)];
     scrollView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin |UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    
    if (!questionText)
        questionText = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, self.view.frame.size.width-40, 20)];
    questionText.lineBreakMode = NSLineBreakByWordWrapping;
    questionText.font = [UIFont fontWithName:FONT_SEMIBOLD_ITALIC size:14.32];
    questionText.frame = [self frameForLabel:questionText WithText:pollQuestion.question];
    questionText.numberOfLines =0;
    questionText.textColor = UIColorFromRGB(0x4c4c4c);
    questionText.backgroundColor  = [UIColor clearColor];
    questionText.text = pollQuestion.question;
    questionText.autoresizingMask =  UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
    questionText.textAlignment = NSTextAlignmentCenter;
    
    UILabel *questionTextBG;
    if (!questionTextBG)
        questionTextBG= [[UILabel alloc] initWithFrame:CGRectMake(0, 0, size.width, questionText.frame.size.height+20)];
    questionTextBG.backgroundColor = [UIColor lightGrayColor];
    questionTextBG.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
    questionTextBG.alpha = 0.3f;
    
    [scrollView addSubview:questionTextBG];
    [scrollView addSubview:questionText];

    
    UIImage *optionBG = [UIImage imageNamed:@"pollOptionBG"] ;
    UIImage *offIcon = [UIImage imageNamed:@"unselected_icon"];
    UIImage *onIcon = [UIImage imageNamed:@"tick_icon"];
    
    cursor.y =questionTextBG.frame.size.height+optionsGap;
    cursor.x = 4;
    int percentage=0;
    
    NSString *answerOption = nil;
    numberOfOptions = pollQuestion.optionsCount;
    
    CGFloat scrollViewHeight= scrollView.contentSize.height;
    for (int i=1; i<=numberOfOptions; i++)
    {
        percentage = 0;
        answerOption = nil;
//        if (i==1)
//        {
//            percentage = pollQuestion.answerOnePercentage;
////            answerOption = pollQuestion.answerOne;
//        }else if (i==2)
//        {
//            percentage = pollQuestion.answerTwoPercentage;
////            answerOption = pollQuestion.answerTwo;
//        }else if (i==3)
//        {
//            percentage = pollQuestion.answerThreePercentage;
////            answerOption = pollQuestion.answerThree;
//        }else if (i==4)
//        {
//            percentage = pollQuestion.answerFourPercentage;
////            answerOption = pollQuestion.answerFour;
//        }else if (i==5)
//        {
//            percentage = pollQuestion.answerFivePercentage;
////            answerOption = pollQuestion.answerFive;
//        }else if (i==6)
//        {
//            percentage = pollQuestion.answerSixPercentage;
////            answerOption = pollQuestion.answerSix;
//        }

        //NSLog(@"anser optins %@",objArr);
        answerOption = [objArr objectAtIndex:i-1];
        if ([percentageArr count]>0)
        {
            percentage = [[percentageArr objectAtIndex:i-1] intValue];
        }
        /*
        UILabel *option1;
        if (!option1)
            option1 = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x+13, cursor.y+3, size.width*0.75, size.height*0.25)];
        option1.lineBreakMode = NSLineBreakByWordWrapping;
        option1.font = [UIFont fontWithName:@"OpenSans-Semibold" size:14.49];
        option1.frame = [self frameForLabel:option1 WithText:answerOption];
        option1.textColor = UIColorFromRGB(0x5d5d5d);
        option1.numberOfLines =0;
        option1.autoresizingMask =  UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
        option1.backgroundColor  = [UIColor clearColor];
        option1.text = answerOption;
        option1.tag = 10*i;
        option1.textAlignment = NSTextAlignmentLeft;
        
//        CGFloat buttonHeight = option1.frame.size.height<44.0 ? 44.0f:option1.frame.size.height;
        
        
        UIButton *optionButton1= [UIButton buttonWithType:UIButtonTypeCustom];
        optionButton1.frame = CGRectMake(5, cursor.y, self.view.frame.size.width-10, option1.frame.size.height+6);
//        optionButton1.imageEdgeInsets = UIEdgeInsetsMake(0, optionButton1.frame.size.width-onIcon.size.width-50, 0, 0);
        optionButton1.contentEdgeInsets = UIEdgeInsetsMake(0,0, 0, 5);
        [optionButton1 addTarget:self action:@selector(optionSelected:) forControlEvents:UIControlEventTouchUpInside];
        [optionButton1 setBackgroundImage:[optionBG stretchableImageWithLeftCapWidth:optionBG.size.width/2 topCapHeight:optionBG.size.height/2] forState:UIControlStateNormal];
        [optionButton1 setImage:offIcon forState:UIControlStateNormal];
        [optionButton1 setImage:onIcon forState:UIControlStateHighlighted];
        [optionButton1 setImage:onIcon forState:UIControlStateSelected];
        optionButton1.titleLabel.backgroundColor = [UIColor clearColor];
        optionButton1.tag = 100*i;
        optionButton1.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
//        optionButton1.imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        optionButton1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
*/
        /*
         option1 = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x+13, cursor.y+3, size.width*0.75, size.height*0.25)];
         */
        CGRect titleLabelFrame = [pollQuestion.answerStatus isEqual:@"Yes"] ? CGRectMake(cursor.x+13, cursor.y+3, size.width*0.75, size.height*0.25) : CGRectMake(20+offIcon.size.width, cursor.y+3, size.width*0.8, size.height*0.25);
        UILabel *option1;
        if (!option1)
            option1 = [[UILabel alloc] initWithFrame:titleLabelFrame];
        option1.lineBreakMode = NSLineBreakByWordWrapping;
        option1.font = [UIFont fontWithName:FONT_SEMIBOLD size:14.49];
        option1.frame = [self frameForLabel:option1 WithText:answerOption];
        option1.textColor = UIColorFromRGB(0x5d5d5d);
        option1.numberOfLines =0;
        option1.autoresizingMask =  UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
        option1.backgroundColor  = [UIColor clearColor];
        option1.text = answerOption;
        option1.tag = 10*i;
        option1.textAlignment = NSTextAlignmentLeft;
        
        
        UIButton *optionButton1= [UIButton buttonWithType:UIButtonTypeCustom];
        optionButton1.frame = CGRectMake(10, cursor.y, self.view.frame.size.width-20, option1.frame.size.height+6);
        optionButton1.imageEdgeInsets = UIEdgeInsetsMake(0,+05, 0, 0);
        [optionButton1 addTarget:self action:@selector(optionSelected:) forControlEvents:UIControlEventTouchUpInside];
        [optionButton1 setBackgroundImage:[optionBG stretchableImageWithLeftCapWidth:optionBG.size.width/2 topCapHeight:optionBG.size.height/2] forState:UIControlStateNormal];
        [optionButton1 setImage:offIcon forState:UIControlStateNormal];
        [optionButton1 setImage:onIcon forState:UIControlStateHighlighted];
        [optionButton1 setImage:onIcon forState:UIControlStateSelected];
        optionButton1.titleLabel.backgroundColor = [UIColor clearColor];
        optionButton1.tag = 100*[[keysArr objectAtIndex:i-1] intValue];
        //NSLog(@"optionButton tag Values %d",[[keysArr objectAtIndex:i-1] intValue]);
        optionButton1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        optionButton1.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
        optionButton1.imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;

        
        optionButton1.hidden = [pollQuestion.answerStatus isEqual:@"Yes"] ? YES:NO;
        self.progressBar = [[AMGProgressView alloc] initWithFrame:optionButton1.frame];
        self.progressBar.gradientColors = @[[UIColor colorWithRed:(193.0f/255.0f) green:(245.0f/255.0f) blue:(251.0f/255.0f) alpha:1.0f]];
        self.progressBar.emptyPartAlpha = 1.0f;
        self.progressBar.minimumValue = 0;
        self.progressBar.maximumValue =100;
        self.progressBar.progress = percentage;
//        self.progressBar.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleWidth;
        self.progressBar.layer.cornerRadius = 6.0f;
        self.progressBar.tag = 1000*i;
        
        UILabel *percentageLbl = [[UILabel alloc] initWithFrame:CGRectMake(option1.frame.origin.x+option1.frame.size.width+1, cursor.y, self.progressBar.frame.size.width-(option1.frame.origin.x+option1.frame.size.width), option1.frame.size.height)];
//        percentageLbl.text = [NSString stringWithFormat:@"%.02f%%",self.progressBar.progress];
        percentageLbl.text = [NSString stringWithFormat:@"%d%%",(int)ceilf(self.progressBar.progress)];
//        percentageLbl.text = [NSString stringWithFormat:@"%d%%",self.progressBar.progress];
        percentageLbl.tag = 10000*i;
        percentageLbl.textColor = UIColorFromRGB(0x5d5d5d);
        percentageLbl.textAlignment = NSTextAlignmentRight;
        percentageLbl.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        percentageLbl.font = [UIFont fontWithName:FONT_SEMIBOLD size:14.49];
        percentageLbl.backgroundColor  =[UIColor clearColor];
        percentageLbl.hidden = [pollQuestion.answerStatus isEqual:@"Yes"] ? NO:YES;
        
        [scrollView addSubview:optionButton1];
        [scrollView addSubview:self.progressBar];

        [scrollView addSubview:option1];
        [scrollView addSubview:percentageLbl];

        self.progressBar.hidden = [pollQuestion.answerStatus isEqual:@"Yes"] ? NO:YES;
        
        cursor.y +=optionButton1.frame.size.height+optionsGap;
        
        if (numberOfOptions == i) {
            scrollViewHeight = optionButton1.frame.origin.y +optionButton1.frame.size.height +90;
        }
    }
    scrollView.contentSize = CGSizeMake(totalFrame.size.width, scrollViewHeight);
    [pollQuestionView addSubview:scrollView];
    
    if (!submitButton)
        submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *btnBg = [UIImage imageNamed:@"RedBtn_Bg"];
    [submitButton setBackgroundImage:[btnBg stretchableImageWithLeftCapWidth:btnBg.size.width/2 topCapHeight:btnBg.size.height/2] forState:UIControlStateNormal];
    submitButton.contentMode = UIViewContentModeRedraw;
    [submitButton setTitle:@"Submit" forState:UIControlStateNormal];
    submitButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin |UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    submitButton.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.8];
    submitButton.frame = CGRectMake(cursor.x, pollQuestionView.frame.size.height-60.0f, self.view.frame.size.width-cursor.x-cursor.x, 40.0f);
    //NSLog(@"submitnbutton frame and view frame %@ %@",NSStringFromCGRect(submitButton.frame),NSStringFromCGRect(pollQuestionView.frame));
    [submitButton addTarget:self action:@selector(submitButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [pollQuestionView addSubview:submitButton];
//    submitButton.hidden = [pollQuestion.answerStatus isEqual:@"YES"] ? YES:NO;
    
    if (!nextButton)
        nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    UIImage *btnBg = [UIImage imageNamed:@"RedBtn_Bg"];
    [nextButton setBackgroundImage:[btnBg stretchableImageWithLeftCapWidth:btnBg.size.width/2 topCapHeight:btnBg.size.height/2] forState:UIControlStateNormal];
    nextButton.contentMode = UIViewContentModeRedraw;
    [nextButton setImage:[UIImage imageNamed:@"nextQuestion"] forState:UIControlStateNormal];
//    [nextButton setTitle:@"Next Question" forState:UIControlStateNormal];
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    nextButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin |UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
    nextButton.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.8];
    nextButton.frame = CGRectMake(cursor.x, pollQuestionView.frame.size.height-60.0f, self.view.frame.size.width-cursor.x-cursor.x, 40.0f);
    [nextButton addTarget:self action:@selector(nextButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//    nextButton.imageEdgeInsets = UIEdgeInsetsMake(0, nextButton.frame.size.width-100, 0, 0);
//    nextButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0,70);
    nextButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [pollQuestionView addSubview:nextButton];
//    nextButton.hidden = [pollQuestion.answerStatus isEqual:@"YES"] ? NO:YES;
    if (currentQuestionIndex  == QArray.count-1 && [pollQuestion.answerStatus isEqual:@"Yes"])
    {
        nextButton.hidden = YES;
        submitButton.hidden = YES;
    }else if (flipped || [pollQuestion.answerStatus isEqual:@"Yes"])
    {
        nextButton.hidden = NO;
        submitButton.hidden = YES;
    }else
    {
        nextButton.hidden = YES;
        submitButton.hidden = NO;
        submitButton.userInteractionEnabled = YES;
    }
    //    scrollViewHeight = submitButton.frame.origin.y+submitButton.frame.size.height+50;
    
    
}
#pragma mark - PollQuestions Delegate Methods
-(void)pollQuestionsSubmittedSuccessfully
{
    QArray = nil;
    QArray = [[PollQuestionsModel sharedDataSource] allObjects];
    [self flipAnimation];
    [self setApperience];

}
-(void)pollQuestionsSubmittedFailure
{
    
    //NSLog(@"Problem in Submitting the Poll Question");
}

#pragma mark - Animation Method
-(void)flipAnimation
{
    
    CATransition *applicationLoadViewIn = [CATransition animation];
    [applicationLoadViewIn setDelegate:self];
    [applicationLoadViewIn setDuration:1.5f];
    [applicationLoadViewIn setType:@"flip"];
    if(flipped)
    {
        [applicationLoadViewIn setSubtype:kCATransitionFromRight];
    }
    else
    {
        [applicationLoadViewIn setSubtype:kCATransitionFromLeft];
    }
    [applicationLoadViewIn setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    //    [self.layer addAnimation:applicationLoadViewIn forKey:NULL];
    [[pollQuestionView layer] addAnimation:applicationLoadViewIn forKey:kCATransitionReveal];
    
}
-(void) setApperience
{
    [[scrollView subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];

    if (flipped)
    {
        [self customIntializationWithPollQuestion:[QArray objectAtIndex:currentQuestionIndex]];
        if (submitSelected)
        {
            submitSelected = NO;
        }
    }
    else
    {
        if (currentQuestionIndex == QArray.count-1)
        {
//            submitButton.hidden = nextButton.hidden =  YES;
            return;
        }
        else
        {
            currentQuestionIndex++;
            [self customIntializationWithPollQuestion:[QArray objectAtIndex:currentQuestionIndex]];
        }
    }
}

//    [self customIntializationWithStr:@"Question1"];
-(IBAction)nextButtonTapped:(id)sender
{
//    for(UIView *subview in [scrollView subviews]) {
//        [subview removeFromSuperview];
//    }
    [[scrollView subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];

    flipped = NO;
    [self setApperience];
    [self slideAnimationIsLeft:NO];
}
-(IBAction)submitButtonTapped:(UIButton *)sender
{
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"POLL_TITLE", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    if (!optionSelcted)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"POLL_TITLE", @"") message:NSLocalizedString(@"POLL_ANSWER_CHOICE_ERROR", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    PollQuestions *question = [QArray objectAtIndex:currentQuestionIndex];
    
    [[PollQuestionsModel sharedDataSource]submitPollQuestionAnswerWithQuestionId:question.key AnswerId:answerNumber AndDeviceId:[[NSUserDefaults standardUserDefaults] valueForKey:IMMIGO_GUID]];
    flipped=!flipped;
    submitButton.userInteractionEnabled = NO;
    //    [self slideAnimationIsLeft:NO];
    
}
- (void) slideAnimationIsLeft:(BOOL)isleftDirection
{
    CATransition *applicationLoadViewIn = [CATransition animation];
    [applicationLoadViewIn setDuration:0.3f];
    [applicationLoadViewIn setType:kCATransitionPush];
    if(isleftDirection)
        [applicationLoadViewIn setSubtype:kCATransitionFromLeft];
    else
        [applicationLoadViewIn setSubtype:kCATransitionFromRight];
    flipped = NO;
    [[pollQuestionView layer] addAnimation:applicationLoadViewIn forKey:kCATransitionReveal];
    
}
-(IBAction)optionSelected:(id)sender
{
    optionSelcted = YES;
    UIButton *selectedButton = (UIButton *)sender;
    
    //    selectedButton.selected =!selectedButton.selected;
    
    for (UIView *view in [scrollView subviews])
    {
        if ([view isKindOfClass:[UIButton class]])
        {
            UIButton *button = (UIButton *)view;
            if (button.tag != selectedButton.tag && [button isSelected])
            {
                button.selected = NO;
            }
            else if (button.tag == selectedButton.tag && ![button isSelected])
            {
                button.selected = YES;
                answerNumber = (int)button.tag/100;
                //NSLog(@"button selected tag %ld",(long)button.tag);
            }
        }
    }
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self customizeNavigationBar];

    [[DataDownloader sharedDownloader] setDelegate:self];
    
    QArray = [[PollQuestionsModel sharedDataSource] allObjects];//[[NSMutableArray alloc] initWithObjects:@"Question1",@"Question2",@"Question3",@"Question4",@"Question5",@"Question6",@"Question7",@"Question8", nil];
    currentQuestionIndex = pollQuestionIndex;
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        //NSLog(@"runnig on < 7.0");
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"left arrow"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        UIImage *image = [UIImage imageNamed:@"question"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = rightItem;
        
    }
    
    {
        self.navigationController.navigationBarHidden = NO;
//        UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//        UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//        titlView.image = titleLogo;
//        self.navigationItem.titleView = titlView;
        //        pollsTable.rowHeight = 60.0f;
    }
    
    [self customIntializationWithPollQuestion:[QArray objectAtIndex:currentQuestionIndex]];

	// Do any additional setup after loading the view.
}
#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    [self resetTheProgressbarFrame];

    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = pollQuestionView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        viewFrame.size.height = UIInterfaceOrientationIsPortrait(orientation) ? 504:256;
        pollQuestionView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = pollQuestionView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        pollQuestionView.frame = viewFrame;
    }
    
    //    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];
}

-(void)resetTheProgressbarFrame
{
    PollQuestions *pollQuestion = [QArray objectAtIndex:currentQuestionIndex];
    
    UIButton *button;
    for (UIView *view in [scrollView subviews])
    {
        if ([view isKindOfClass:[UIButton class]] && button == nil)
        {
            button = (UIButton *)view;
        }
        if ([view isKindOfClass:[AMGProgressView class]])
        {
            //                [view removeFromSuperview];
            AMGProgressView *progress = (AMGProgressView *)view;
            
            CGRect frame = progress.frame;
            frame.size.width = button.frame.size.width;
            progress.frame = frame;
            
            int tag = progress.tag/1000;
            CGFloat percentage = 0.0;
            if (tag==1)
            {
                percentage = pollQuestion.answerOnePercentage;
            }else if (tag==2)
            {
                percentage = pollQuestion.answerTwoPercentage;
            }else if (tag==3)
            {
                percentage = pollQuestion.answerThreePercentage;
            }else if (tag==4)
            {
                percentage = pollQuestion.answerFourPercentage;
            }
            
            progress.gradientColors = @[[UIColor colorWithRed:(193.0f/255.0f) green:(245.0f/255.0f) blue:(251.0f/255.0f) alpha:1.0f]];
            progress.emptyPartAlpha = 1.0f;
            progress.minimumValue = 0.0f;
            progress.maximumValue =100.0f;
            progress.progress = percentage;
            //        self.progressBar.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleWidth;
            progress.layer.cornerRadius = 6.0f;
            
        }
    }

}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
@end
