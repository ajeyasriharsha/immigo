//
//  TrainingContactUrlVC.h
//  IMMIGO
//
//  Created by pradeep ISPV on 4/25/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrainingContactUrlVC : UIViewController
@property (nonatomic,strong) NSString *urlAddress;

@end
