//
//  AdvanedSearchMultiSelectVC.m
//  IMMIGO
//
//  Created by Ajeya Sriharsha on 5/14/15.
//  Copyright (c) 2015 pradeep ISPV. All rights reserved.
//

#import "AdvanedSearchMultiSelectVC.h"
#import "Common.h"
@interface AdvanedSearchMultiSelectVC (){
    
    
    IBOutlet UITableView *menuTable;
    BOOL isSerachEnabled;
    NSString *_searchString;
    IBOutlet UIView *childView;
    IBOutlet UIButton *tilteBtn;
    BOOL closeButtonTapped;
    UILabel *infoTextLabel;
    
}

@end

@implementation AdvanedSearchMultiSelectVC
@synthesize optionsArr =_optionsArr;
@synthesize selectedIndexes =_selectedIndexes;
@synthesize selectedValues =_selectedValues;
@synthesize popUpTitle =_popUpTitle;
@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    tilteBtn.backgroundColor=UIColorFromRGB(0Xc51039);
    
    // Do any additional setup after loading the view.
    
    {
        self.navigationController.navigationBarHidden = NO;
//        UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//        UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//        titlView.image = titleLogo;
//        self.navigationItem.titleView = titlView;
        
    }
    isSerachEnabled =NO;
    
  
    if(!_selectedValues)
        _selectedValues = [NSMutableArray array];
    if (!_selectedIndexes) {
            _selectedIndexes = [NSMutableArray array];
    }
    
    
    menuTable.layer.cornerRadius = 15;
    menuTable.layer.masksToBounds = YES;
    
    menuTable.layer.borderWidth = 2;
    menuTable.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    [childView setBackgroundColor:[UIColor colorWithRed:231.0f/255.0 green:231.0/255.0f blue:231.0/255.0f alpha:1.0]];
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        //NSLog(@"runnig on < 7.0");
    
        
        UIImage *image = [UIImage imageNamed:@"popUp_close"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = rightItem;
        [button setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];
        
    }
    
    [menuTable registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];

    
}

- (void)viewWillAppear:(BOOL)animated
{
    //[super viewWillAppear:animated];
    [self customizeNavigationBar];
    tilteBtn.titleLabel.text =_popUpTitle;
    
    if ([_popUpTitle isEqualToString:NSLocalizedString(@"LANGUAGESSPOKEN_TITLE", @"")]) {
        langsearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, 320, 45)];
        //temp.tintColor = [UIColor redColor];
       // searchBar.backgroundImage = [[UIImage alloc] init];
       // searchBar.tintColor = [UIColor blackColor];
        langsearchBar.showsCancelButton=NO;
        langsearchBar.autocorrectionType=UITextAutocorrectionTypeNo;
        langsearchBar.autocapitalizationType=UITextAutocapitalizationTypeNone;
        langsearchBar.delegate=self;
        langsearchBar.placeholder = NSLocalizedString(@"SEARCHBAR_LANGUAGE_PLACEHOLDER", @"");
        menuTable.tableHeaderView=langsearchBar;
    }
    
    
    if ([menuTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [menuTable setSeparatorInset:UIEdgeInsetsZero];
    }
    
    NSLog(@"Selected values:%@",_selectedValues);
    
    NSArray *array =[NSArray arrayWithArray:self.selectedValues];
    
    if ([array count]>0)
    {
        [_selectedIndexes removeAllObjects];
        [_selectedValues removeAllObjects];
        for (int i=0; i<array.count; i++)
        {
            NSUInteger index= [_optionsArr indexOfObject:[array objectAtIndex:i]];
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:0];
            [_selectedIndexes addObject:indexpath];
            [_selectedValues addObject:[_optionsArr objectAtIndex:index]];
        }
    }
   [menuTable reloadData];
    
}
-(CGFloat)heightForLabel:(UILabel *)label WithText:(NSString *)text
{
    CGSize maximumLabelSize = CGSizeMake(label.frame.size.width, FLT_MAX);
    CGRect textRect = [text boundingRectWithSize:maximumLabelSize
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName:label.font}
                                         context:nil];
    CGSize expectedLabelSize = textRect.size;
    return expectedLabelSize.height<40.0f ? 40.0f :expectedLabelSize.height;
}
-(void)viewDidAppear:(BOOL)animated{
    
    [self customizeNavigationBar];
}

-(void)customizeNavigationBar{
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    navigationBar.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed: @"Nav_Background"]];

//    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
//    [backView setBackgroundColor:[UIColor clearColor]];
//    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_Background"]];
//    titleImageView.frame = backView.frame; // Here I am
//    [backView addSubview:titleImageView];
//    titleImageView.contentMode = UIViewContentModeCenter;
//    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = childView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        childView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = childView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        childView.frame = viewFrame;
        
    }
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)close:(id)sender
{
    
    closeButtonTapped =YES;
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        NSLog(@"Dismiss completed");
        if (delegate && [delegate respondsToSelector:@selector(passValuesToField:)])
        {
            [delegate passValuesToField:_selectedValues];
        }
       // [delegate closePopUP];
    }];
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@". "]) {
        return NO;
    }
    return YES;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if([searchText length] == 0 && searchBar.text.length == 0)
    {
        isSerachEnabled =NO;
        [searchBar performSelector: @selector(resignFirstResponder)
                        withObject: nil
                        afterDelay: 0.1];
    }

}

//search button was tapped
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self handleSearch:searchBar];
}

//user finished editing the search text
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [self handleSearch:searchBar];
}



//do our search
- (void)handleSearch:(UISearchBar *)searchBar {
    
    _searchString = [searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([_searchString isEqualToString:@""]) {
        [searchBar resignFirstResponder];
    }

    if (_searchString.length >0) {
            isSerachEnabled=true;
    }
    //check what was passed as the query String and get rid of the keyboard
    NSLog(@"User searched for %@", searchBar.text);
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self contains[cd] %@", _searchString];
    NSArray *foundPersonArray = [_optionsArr filteredArrayUsingPredicate:predicate];
    _searchFilteredArray=[foundPersonArray mutableCopy];
    
    if ([_searchString isEqualToString:@""]) {
        isSerachEnabled=false;
        
    }
    [menuTable reloadData];
    [searchBar resignFirstResponder];
    
}
 


#pragma mark UITableview Delegates

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    static NSString *CellIdentifier =@"Cell";
//    
//    UITableViewCell *cell = [menuTable dequeueReusableCellWithIdentifier:CellIdentifier];
//    
//    
//    NSString *str = isSerachEnabled? [_searchFilteredArray objectAtIndex:indexPath.row]:[_optionsArr objectAtIndex:indexPath.row];
//    CGFloat textHeight = [self heightForLabel:cell.textLabel WithText:str];
//    return textHeight+10;
    if ([_popUpTitle isEqualToString:NSLocalizedString(@"LANGUAGESSPOKEN_TITLE", @"")]){
        
        return 40;
    }
    
    
    return 60;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSUInteger count =isSerachEnabled? [_searchFilteredArray count]:[_optionsArr count];//[optionsArr count];
    
    if (count<=0)
    {
        if (!infoTextLabel)
            infoTextLabel = [[UILabel alloc] init];
        infoTextLabel.frame = CGRectMake(tableView.frame.origin.x, 190, tableView.frame.size.width, 40);//childView.frame;
        infoTextLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        infoTextLabel.backgroundColor = [UIColor clearColor];
        infoTextLabel.textAlignment = NSTextAlignmentCenter;
        infoTextLabel.textColor = UIColorFromRGB(0x70706f);
        infoTextLabel.font = [UIFont fontWithName:FONT_EXTRA_BOLD size:14.32f]; //[UIFont fontWithName:FONT_ITALIC size:14.32f];  // VAMSHI //[UIFont boldSystemFontOfSize:16.0];
        infoTextLabel.text = NSLocalizedString(@"NORESULTS_FOUND_TITLE", @"");
        [self.view addSubview:infoTextLabel];
    }
    else{
        [infoTextLabel removeFromSuperview];
        infoTextLabel = nil;
    }
    return count;
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{

    if ([tableView isEqual:menuTable] ) {
            [cell setBackgroundColor:[UIColor clearColor]];
    }
    

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString *title =  isSerachEnabled? [_searchFilteredArray objectAtIndex:indexPath.row]:[_optionsArr objectAtIndex:indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text =title;
    cell.contentView.backgroundColor = [UIColor whiteColor];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.textLabel.numberOfLines = 3;
    cell.textLabel.font = [UIFont fontWithName:@"OpenSans" size:14.0f];
    
    
    
//    if([_selectedIndexes containsObject:indexPath])
//    {
//        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
//    } else {
//        [cell setAccessoryType:UITableViewCellAccessoryNone];
//    }
    if([_selectedValues containsObject:title])
    {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    } else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    return cell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    if (tableView.tableHeaderView && [langsearchBar isFirstResponder]) {
//        [langsearchBar resignFirstResponder];
//    }
    
    
    if([_selectedIndexes containsObject:indexPath])
    {
        [_selectedIndexes removeObject:indexPath];
        [_selectedValues removeObject: isSerachEnabled? [_searchFilteredArray objectAtIndex:indexPath.row]:[_optionsArr objectAtIndex:indexPath.row]];
    } else
    {
        [_selectedIndexes addObject:indexPath];
        [_selectedValues addObject:isSerachEnabled? [_searchFilteredArray objectAtIndex:indexPath.row]:[_optionsArr objectAtIndex:indexPath.row]];
    }
    [tableView reloadData];
    
}

/*
-(IBAction)close:(id)sender
{
    if (delegate && [delegate respondsToSelector:@selector(passValuesToField:)])
    {
        [delegate passValuesToField:selectedValues];
    }
    [delegate closePopUP];
}
-(void)closePopUpIfTouchOutSide
{
    if (delegate && [delegate respondsToSelector:@selector(passValuesToField:)])
    {
        [delegate passValuesToField:selectedValues];
    }
    [delegate closePopUP];
}

*/
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    
    
    
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = childView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        childView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = childView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        childView.frame = viewFrame;
    }
    
    
    
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

#pragma mark nav
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

@end
