//
//  ImmigrationBasicsVC.m
//  Immigo
//
//  Created by pradeep ISPV on 3/4/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "ImmigrationBasicsVC.h"
#import "ImmigrationSubTopicsVC.h"
#import "AboutUsVC.h"
#import "Common.h"
#import "ImmigrationBasicsModel.h"
#import "AppDelegate.h"
#import "DataDownloader.h"
@interface ImmigrationBasicsVC () <DataDownloaderDelegate>
{
    NSArray *topicsArray;
    IBOutlet UITableView *topicsTbl;
    __weak IBOutlet UIView *bgView;
}
@end

@implementation ImmigrationBasicsVC
@synthesize headerTitle;
@synthesize headerImage;
@synthesize selTopicType;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [Common logEventToGoogleAnalytics:@"Immigo ImmigrationBasics Screen"];
//    [Common logMainEventsToGoogleAnalytics:@"Immigo ImmigrationBasics Screen" withAction:@"Immigo ImmigrationBasics Screen" andLabel:@"" and:0];
    
    self.view.autoresizesSubviews = UIViewAutoresizingFlexibleHeight; 
    [[DataDownloader sharedDownloader] setDelegate:self];
    if (self.selTopicType == TP_CIVIL_RIGHTS) {
        [[ImmigrationBasicsModel sharedDataSource] fetchCivilRights];
        
    }else if(self.selTopicType ==TP_CIVIC_ENGAGEMENT){
        [[ImmigrationBasicsModel sharedDataSource] fetchCivicEngagement];
        
    }else{
        [[ImmigrationBasicsModel sharedDataSource] fetchTopics];
    }
  
    UIButton *button1;
    UIButton *button;
    {
        if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
        {            
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            UIImage *image1 = [UIImage imageNamed:@"left arrow"];
            button1 = [UIButton buttonWithType:UIButtonTypeCustom];
            [button1 setImage:image1 forState:UIControlStateNormal];
            [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
            
            // Create a custom UIBarButtonItem with the button
            UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
            self.navigationItem.leftBarButtonItem = leftItem;
            
            UIImage *image = [UIImage imageNamed:@"question"];
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setImage:image forState:UIControlStateNormal];
            [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
            button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
            
            // Create a custom UIBarButtonItem with the button
            UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
            self.navigationItem.rightBarButtonItem = rightItem;
            
            [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
            [button setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];
            
        }
    }
    
    self.navigationController.navigationBarHidden = NO;
    [heading setTitle:self.headerTitle forState:UIControlStateNormal];
    heading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
    heading.backgroundColor=UIColorFromRGB(0Xc51039);//RedColor ThroughtAPP//JUHI
    if (headerImage!=nil && ![headerImage isKindOfClass:[NSNull class]]) {
        [heading setImage:headerImage forState:UIControlStateNormal];
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    //NSLog(@"Height:%f Width:%f",[self window_height],[self window_width]);
    //NSLog(@"Current Version :%f",[[UIDevice currentDevice].systemVersion floatValue]);
//    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
//    {
//        [self customizeNavigationBar];
//    }
    
    [self customizeNavigationBar];

    [super viewWillAppear:animated];
    
}

- (CGFloat) window_height   {
    return [UIScreen mainScreen].applicationFrame.size.height;
}

- (CGFloat) window_width   {
    return [UIScreen mainScreen].applicationFrame.size.width;
}
-(void)customizeNavigationBar{
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    [navigationBar setBackgroundColor:[UIColor clearColor]]; // VAMSHI // [UIColor yellowColor]];
//    UIImage *navBarImg = [UIImage imageNamed:@"navbar_bg"];//TopTabTitleBg//Nav_Background.png
//    [navigationBar setBackgroundImage:navBarImg forBarMetrics:UIBarMetricsDefault];
    
    
//    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
//    [backView setBackgroundColor:[UIColor clearColor]];
//    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
//    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
//    [backView addSubview:titleImageView];
//    titleImageView.contentMode = UIViewContentModeCenter;
//    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        bgView.frame = viewFrame;
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Data Downloader Delegate methods
-(void)immigrationBasicsLoadedFailure
{
    topicsArray = [[ImmigrationBasicsModel sharedDataSource] allItems];
    [topicsTbl reloadData];
}
-(void)immigrationBasicsDownloadedSuccessfully
{
    topicsArray = [[ImmigrationBasicsModel sharedDataSource] allItems];
    [topicsTbl reloadData];
}
#pragma mark - Tableview delegate and Data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return topicsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Topic *topic =[topicsArray objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"TopicCell_BG"]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    cell.textLabel.text = topic.topicTitle;//[NSString stringWithFormat:@"TopicTopicTopicTopicTopicTopic %ld",indexPath.row+1];
    cell.textLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:14.5];
    cell.textLabel.textColor = UIColorFromRGB(0X124A7F);  //VAMSHI
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];
//    cell.accessoryView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"TopicCell_BG"]];
    return cell;
}
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"TopicCell_BG"]];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    ImmigrationSubTopicsVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"ImmigrationSubTopicsVC"];
    toVC.topic = [topicsArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:toVC animated:YES];
    
}
-(void)pushToViewControllerWithIdentifier:(NSString *)identifier //Pass a identifier Used in Storyboard for viewController
{
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    UIViewController* toVC = [storyboard instantiateViewControllerWithIdentifier:identifier];
    [self.navigationController pushViewController:toVC animated:YES];
    
}
#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        bgView.frame = viewFrame;
    }
    
    //    }
}
- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
@end
