//
//  ImmigrationLinkDetailVC.h
//  IMMIGO
//
//  Created by pradeep ISPV on 5/3/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImmigrationLinkDetailVC : UIViewController
@property (nonatomic,strong) NSString *linkUrl;
@end
