//
//  SplashViewController.m
//  Immigo
//
//  Created by pradeep ISPV on 2/27/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "SplashViewController.h"
#import "Common.h"
#import "HomeViewController.h"
#import "MyNavigationController.h"
#import "AppDelegate.h"
@interface SplashViewController ()
{
    IBOutlet UIImageView *splash;
}
@end

@implementation SplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization //
    }
    return self;
}
//-(UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    [[[UIApplication sharedApplication] windows] objectAtIndex:0]
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    UIImage *image;

    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        image = (iOSDeviceScreenSize.height == 480)? [UIImage imageNamed:@"LaunchPortrait"] :[UIImage imageNamed:@"LaunchPortrait-5"];
    }
    else
    {
        image = (iOSDeviceScreenSize.height == 480)? [UIImage imageNamed:@"landscape_480"] :[UIImage imageNamed:@"landscape_568"];
    }
    splash.image = image;

//    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)
//    {
//        [self setNeedsStatusBarAppearanceUpdate];
//    }
    //[self createPullableView];

    BOOL myBool = YES;
    NSNumber *passedValue = [NSNumber numberWithBool:myBool];

    [self performSelector:@selector(pushToHome) withObject:passedValue afterDelay:PARTNERS_DELAY_TIME];

}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    
    self.navigationController.navigationBarHidden = YES;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait ||interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration
{
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    UIImage *image;

    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        image = (iOSDeviceScreenSize.height == 480)? [UIImage imageNamed:@"Default"] :[UIImage imageNamed:@"Default-1"];
    }
    else
    {
        image = (iOSDeviceScreenSize.height == 480)? [UIImage imageNamed:@"landscape_480"] :[UIImage imageNamed:@"landscape_568"];
    }
    splash.image = image;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)createPullableView
{
    CGFloat xOffset = 0;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        xOffset = 224;
    }
    
    UIImage *parternsImg = [UIImage imageNamed:@"partners"];
    UIImageView *parternsImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, parternsImg.size.width, parternsImg.size.height)];
    parternsImageView.image = parternsImg;
    
    pullUpView = [[PartnersPullableView alloc] initWithFrame:CGRectMake(xOffset, 0, self.view.frame.size.width, self.view.frame.size.height-22)];
    pullUpView.openedCenter = CGPointMake(160 + xOffset,(self.view.frame.size.height+22)/2);
    pullUpView.closedCenter = CGPointMake(160 + xOffset, (self.view.frame.size.height+(self.view.frame.size.height-22)/2)-parternsImg.size.height);
    pullUpView.center = pullUpView.closedCenter;
    
    
    [pullUpView.handleView addSubview:parternsImageView];
    pullUpView.handleView.frame = CGRectMake(0, 0, parternsImageView.frame.size.width, parternsImageView.frame.size.height);
    pullUpView.delegate = self;
//    [self.view addSubview:pullUpView];
}
-(void)openPullableView:(NSNumber *)openedValue
{
    BOOL opened = [openedValue boolValue];
    [pullUpView setOpened:opened animated:opened]; //To set to open or close
}
- (void)pullableView:(PullableView *)pView didChangeState:(BOOL)opened
{
    if (opened)
    {
        //NSLog(@"Now I'm open!");
    }
    else
    {
        //NSLog(@"Now I'm closed, pull me up again!");
    }
}

-(void)pushToHome
{
//    [self pushToViewControllerWithIdentifier:@"HomeViewController"];
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    HomeViewController* toVC = [storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
//    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
//    delegate.window.rootViewController = toVC;
    toVC.fromSplash = YES;
    
    [self.navigationController pushViewController:toVC animated:YES];

}

-(void)pushToViewControllerWithIdentifier:(NSString *)identifier //Pass a identifier Used in Storyboard for viewController
{
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    UIViewController* toVC = [storyboard instantiateViewControllerWithIdentifier:identifier];
    [self.navigationController pushViewController:toVC animated:YES];
    
}

@end
