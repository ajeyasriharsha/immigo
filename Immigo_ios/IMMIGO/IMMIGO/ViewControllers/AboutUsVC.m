//
//  AboutUsVC.m
//  Immigo
//
//  Created by pradeep ISPV on 3/13/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "AboutUsVC.h"
#import "HomeViewController.h"
#import "Common.h"

@interface AboutUsVC ()
{
    IBOutlet UIButton *feedbackBtn;
    IBOutlet UIButton *desclimerBtn;
    IBOutlet UIButton *aboutImmigoBtn;
    IBOutlet UIButton *privacyBtn;
    IBOutlet UIButton *settingsBtn;
    __weak IBOutlet UIView *bgView;
}
@end

@implementation AboutUsVC
@synthesize fromHome;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [Common logEventToGoogleAnalytics:@"Immigo AboutUS Screen"];
    feedbackBtn.titleLabel.font = desclimerBtn.titleLabel.font = aboutImmigoBtn.titleLabel.font = privacyBtn.titleLabel.font = settingsBtn.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
    
    //Button color and Seperator ui fix //Juhi
    feedbackBtn.backgroundColor = UIColorFromRGB(0Xc51039);
    desclimerBtn.backgroundColor =  UIColorFromRGB(0Xc51039);
    aboutImmigoBtn.backgroundColor =  UIColorFromRGB(0Xc51039);
    privacyBtn.backgroundColor =  UIColorFromRGB(0Xc51039);
    settingsBtn.backgroundColor = UIColorFromRGB(0Xc51039);
    UIView *seperatorView1=[[UIView alloc]initWithFrame:CGRectMake(16, desclimerBtn.frame.origin.y-2, bgView.frame.size.width-25, 2)];
    seperatorView1.backgroundColor=UIColorFromRGB(0Xad082e);
    [bgView addSubview:seperatorView1];
    UIView *seperatorView2=[[UIView alloc]initWithFrame:CGRectMake(16, aboutImmigoBtn.frame.origin.y-2, bgView.frame.size.width-25, 2)];
    seperatorView2.backgroundColor=UIColorFromRGB(0Xad082e);
    [bgView addSubview:seperatorView2];
    UIView *seperatorView3=[[UIView alloc]initWithFrame:CGRectMake(16, privacyBtn.frame.origin.y-2, bgView.frame.size.width-25, 2)];
    seperatorView3.backgroundColor=UIColorFromRGB(0Xad082e);
    [bgView addSubview:seperatorView3];
    UIView *seperatorView4=[[UIView alloc]initWithFrame:CGRectMake(16, settingsBtn.frame.origin.y-2, bgView.frame.size.width-25, 2)];
    seperatorView4.backgroundColor=UIColorFromRGB(0Xad082e);
    [bgView addSubview:seperatorView4];
    
    
    [aboutImmigoBtn setTitle:NSLocalizedString(@"ABOUT OPORTUNIDAD",@"") forState:UIControlStateNormal];
    
    {
        if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 && fromHome)
        {
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            UIImage *image1 = [UIImage imageNamed:@"home"];
            UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
            button1.tintColor = [UIColor whiteColor];
            [button1 setImage:image1 forState:UIControlStateNormal];
            [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
            
            // Create a custom UIBarButtonItem with the button
            UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
            self.navigationItem.leftBarButtonItem = leftItem;
            
            [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
            
            self.navigationItem.rightBarButtonItem = nil;
        }
        else if([[UIDevice currentDevice].systemVersion floatValue] < 7.0 && !fromHome)
        {
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            UIImage *rightImage = [UIImage imageNamed:@"home"];
            UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
            rightButton.tintColor = [UIColor whiteColor];
            [rightButton setImage:rightImage forState:UIControlStateNormal];
            [rightButton addTarget:self action:@selector(dismissVC:) forControlEvents:UIControlEventTouchUpInside];
            rightButton.frame = CGRectMake(0.0, 0.0, rightImage.size.width, rightImage.size.height);
            
            // Create a custom UIBarButtonItem with the button
            UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
            self.navigationItem.rightBarButtonItem = rightItem;
            
            UIImage *image1 = [UIImage imageNamed:@"left arrow"];
            UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
            button1.tintColor = [UIColor whiteColor];
            [button1 setImage:image1 forState:UIControlStateNormal];
            [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
            
            // Create a custom UIBarButtonItem with the button
            UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
            self.navigationItem.leftBarButtonItem = leftItem;
            
            [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
            [rightButton setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];

        }
        else if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0 && fromHome)
        {
            UIImage *image1 = [UIImage imageNamed:@"home"];
            UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
            button1.tintColor = [UIColor whiteColor];
            [button1 setImage:image1 forState:UIControlStateNormal];
            [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
            
            // Create a custom UIBarButtonItem with the button
            UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
            self.navigationItem.leftBarButtonItem = leftItem;
            
            self.navigationItem.rightBarButtonItem = nil;
            
            [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 0, 0, 0)];
        }
        else if([[UIDevice currentDevice].systemVersion floatValue] >= 7.0 && !fromHome)
        {
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            UIImage *rightImage = [UIImage imageNamed:@"home"];
            UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
            //rightButton.tintColor = [UIColor whiteColor];
            [rightButton setTintColor:[UIColor whiteColor]];
            [rightButton setImage:rightImage forState:UIControlStateNormal];
            [rightButton addTarget:self action:@selector(homeBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            rightButton.frame = CGRectMake(0.0, 0.0, rightImage.size.width, rightImage.size.height);
            
            // Create a custom UIBarButtonItem with the button
            UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
            self.navigationItem.rightBarButtonItem = rightItem;
            
            UIImage *image1 = [UIImage imageNamed:@"left arrow"];
            UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
            button1.tintColor = [UIColor whiteColor];
            [button1 setImage:image1 forState:UIControlStateNormal];
            [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
            
            // Create a custom UIBarButtonItem with the button
            UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
            self.navigationItem.leftBarButtonItem = leftItem;
            [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 0, 0, 0)];
            [rightButton setImageEdgeInsets:UIEdgeInsetsMake(-14, 0, 0, 0)];
        }

    }

    {
        self.navigationController.navigationBarHidden = NO;
//        UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//        UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//        titlView.image = titleLogo;
//        self.navigationItem.titleView = titlView;
        //        pollsTable.rowHeight = 60.0f;
    }

    feedbackBtn.titleLabel.font = desclimerBtn.titleLabel.font = aboutImmigoBtn.titleLabel.font = privacyBtn.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.91];

    fromHome = false;
	// Do any additional setup after loading the view.
}
- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [self customizeNavigationBar];
    [super viewWillAppear:animated];
}
-(void)customizeNavigationBar{
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    
//    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
//    [backView setBackgroundColor:[UIColor clearColor]];
//    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
//    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
//    [backView addSubview:titleImageView];
//    titleImageView.contentMode = UIViewContentModeCenter;
//    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        bgView.frame = viewFrame;
        
    }
    
    
}
#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        bgView.frame = viewFrame;
    }
    
    //    }
}


- (IBAction)dismissVC:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)homeBtnPressed:(id)sender
{
    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:1] animated:YES];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
@end
