//
//  LegalSearchListVC.m
//  Immigo
//
//  Created by pradeep ISPV on 3/11/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "LegalSearchListVC.h"
#import "Common.h"
#import "SearchListCell.h"
#import "LegalFinderDetailVC.h"
#import "AboutUsVC.h"
#import "DataDownloader.h"
#import "LegalFinderModel.h"
#import "LegalFinder.h"
#import "Categories.h"

@interface LegalSearchListVC () <DataDownloaderDelegate,MKMapViewDelegate>
{
    MKMapView *legalMapView;
    IBOutlet UIButton *heading;
    
    NSMutableArray *legalFindersArray;
    IBOutlet UIView *childView;
    __weak IBOutlet UIView *bgView;
    
    IBOutlet UILabel *backgroundLabel;
//    CLLocationManager *locationManager;
//    NSString *currentLattitude;
//    NSString *currentLongitude;
    IBOutlet UILabel *sepratorLine;

}
@end

@implementation LegalSearchListVC
@synthesize zipCode;
@synthesize coordinates;
@synthesize isFromAdvancedSearch;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
//    if ([zipCode length]>0)
//    {
//        [self trackLocationForFindindDistacnce];
//        //NSLog(@"Tracking for zipcodes");
//    }
    [self customizeNavigationBar];

}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
//    if ([zipCode length]>0)
//    {
//        // Turn off the location manager to save power.
//        [locationManager stopUpdatingLocation];
//    }
}


-(void)customizeNavigationBar{
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    
//    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
//    [backView setBackgroundColor:[UIColor clearColor]];
//    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
//    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
//    [backView addSubview:titleImageView];
//    titleImageView.contentMode = UIViewContentModeCenter;
//    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        bgView.frame = viewFrame;
        
    }
    
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [DataDownloader sharedDownloader].delegate = self;
//    [[LegalFinderModel sharedDataSource] fetchFinders];

    searchList.rowHeight = 57.0f;
    heading.backgroundColor=UIColorFromRGB(0Xc51039);
    sepratorLine.backgroundColor=UIColorFromRGB(0Xc51039);
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        //NSLog(@"runnig on < 7.0");
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"home"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        UIImage *image = [UIImage imageNamed:@"question"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = rightItem;
        
        [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];

        
    }

    {
        self.navigationController.navigationBarHidden = NO;

    }
    heading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
    
    resultsNearLabel.textColor = [UIColor blackColor]; // VAMSHI // UIColorFromRGB(UI_LIST_TEXT_COLOR);
    resultsNearLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:14.5f]; // VAMSHI //[UIFont fontWithName:FONT_SEMIBOLD_ITALIC size:14.5f];
    
    if (isFromAdvancedSearch) {
        resultsNearLabel.text =NSLocalizedString(@"Advanced Search Results", @"Advanced Search Results") ;
        [resultsNearLabel setBackgroundColor:[UIColor clearColor]];
        CGRect frame =resultsNearLabel.frame;
        frame.size.width=frame.size.width+100;
        frame.size.height =backgroundLabel.frame.size.height-15;//-
        [resultsNearLabel setFrame:frame];
        [searchAreaName setHidden:YES];
    }else{
        [searchAreaName setHidden:NO];
    }
    
    searchAreaName.font = [UIFont fontWithName:FONT_SEMIBOLD size:12.5f]; // VAMSHI // [UIFont fontWithName:FONT_REGULAR size:12.5f];
    searchAreaName.textColor = UIColorFromRGB(0Xc51039);
    searchAreaName.text = [zipCode length]>0 ? zipCode :NSLocalizedString(@"Current Location", @"Current Location") ;//@"500 Streetname Granville MI";
    
//    searchList.rowHeight = 54.0f;
//    UIImage *mapBg = [UIImage imageNamed:@"list_bg"];
    mapButton.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:13.7f]; // VAMSHI //[UIFont fontWithName:FONT_REGULAR size:13.7f]; //
    mapButton.backgroundColor = [UIColor clearColor]; // VAMSHI
    //[mapButton setBackgroundImage:[mapBg stretchableImageWithLeftCapWidth:mapBg.size.width/2 topCapHeight:mapBg.size.height/2] forState:UIControlStateNormal];
    //[mapButton setBackgroundImage:[mapBg stretchableImageWithLeftCapWidth:mapBg.size.width/2 topCapHeight:mapBg.size.height/2] forState:UIControlStateSelected];
    
    {
        legalMapView = [[MKMapView alloc] initWithFrame:CGRectMake(searchList.frame.origin.x, searchList.frame.origin.y, searchList.frame.size.width, self.view.frame.size.height-searchList.frame.origin.y)];
        legalMapView.delegate = self;
        legalMapView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin;
        [childView addSubview:legalMapView];
        legalMapView.hidden = YES;
    }
    mapButton.userInteractionEnabled = NO;
	// Do any additional setup after loading the view.
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)legalFindersDownloadedSuccessfully
{
    
    //No results are available based on your search criteria.
    NSArray *array = [[LegalFinderModel sharedDataSource] allItems];
    if ([array count] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"LEGAL_HELP_FINDER_TITLE", @"") message:NSLocalizedString(@"NOSEARCH_RESULTS_CRITERIA", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
        [alert show];
        return;
    }
    mapButton.userInteractionEnabled = YES;
    legalFindersArray = [NSMutableArray arrayWithArray:array];
    [searchList reloadData];
}
-(void)legalFindersLoadedFailure
{
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"LEGAL_HELP_FINDER_TITLE", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
        [alertView show];
    }
}
- (IBAction)mapButtonTapped:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if ([sender isSelected])
    {
        [sender setTitle:NSLocalizedString(@"List", @"List") forState:UIControlStateNormal];
        [sender setTitleColor:UIColorFromRGB(0X104170) forState:UIControlStateNormal]; // VAMSHI
        [sender setImage:[UIImage imageNamed:@"listIcon"] forState:UIControlStateNormal];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.6];
        
        [UIView setAnimationDelegate: self];
        //    [UIView setAnimationDidStopSelector:
        //     @selector(animationFinished:finished:context:)];
        searchList.hidden = YES;
        legalMapView.hidden = NO;
        
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                               forView:legalMapView cache:YES];
        [UIView commitAnimations];
        
        [self dropPins];
    }
    else
    {
        [sender setTitle:NSLocalizedString(@"Map", @"")  forState:UIControlStateNormal];
        [sender setTitleColor:UIColorFromRGB(0X104170) forState:UIControlStateNormal]; // VAMSHI
        [sender setImage:[UIImage imageNamed:@"map_icon"] forState:UIControlStateNormal];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.6];
        
        [UIView setAnimationDelegate: self];
        //    [UIView setAnimationDidStopSelector:
        //     @selector(animationFinished:finished:context:)];
        searchList.hidden = NO;
        legalMapView.hidden = YES;
        
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                               forView:searchList cache:YES];
        [UIView commitAnimations];

    }

}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return legalFindersArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LegalFinder *finder = (LegalFinder *)[legalFindersArray objectAtIndex:indexPath.row];
    //NSLog(@"adder %@ %@",finder.finderAddress.latitude,finder.finderAddress.longitude);
    
    NSString *address = @"";
    if ([finder.finderAddress.street length]>0)
    {
        address = finder.finderAddress.street;
        address = [address stringByAppendingString:@","];
    }
    if ([finder.finderAddress.city length]>0)
    {
        address = [address stringByAppendingString:finder.finderAddress.city];
        address = [address stringByAppendingString:@","];
    }
    if ([finder.finderAddress.state length]>0)
    {
        address = [address stringByAppendingString:finder.finderAddress.state];
        address = [address stringByAppendingString:@" "];
    }
    NSString *string = [NSString stringWithFormat:@"%@",finder.finderAddress.zipcode];
    if ([string length]>0)
    {
        address = [address stringByAppendingString:string];
//        address = [address stringByAppendingString:@"."];
    }

    SearchListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell)
    {
        cell = [[SearchListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    cell.name.text = [finder.finderName trimmedString];//@"ABS Immigration";
    cell.address.text = [address trimmedString];
    cell.distance.text = @"5.99 Mi";
    cell.address.numberOfLines = 2;
    cell.name.font = [UIFont fontWithName:FONT_SEMIBOLD size:11.5];//[UIFont fontWithName:@"OpenSans-Semibold" size:16.91f];
    cell.address.font = [UIFont fontWithName:FONT_SEMIBOLD size:9.5];
    cell.distance.font = [UIFont fontWithName:FONT_REGULAR size:13.7f];
//    cell.name.textColor = cell.address.textColor = cell.distance.textColor = [UIColor colorWithRed:(76.0f/255.0f) green:(76.0f/255.0f) blue:(76.0f/255.0f) alpha:1.0];
    cell.name.textColor = UIColorFromRGB(0X124A7F); // VAMSHI
    cell.address.textColor = UIColorFromRGB(0X6A6A6A); // VAMSHI
    
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];

    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        [_objects removeObjectAtIndex:indexPath.row];
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
//    }
//}

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[LegalFinderModel sharedDataSource] selectFinderAtindex:indexPath.row];
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    LegalFinderDetailVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"LegalFinderDetailVC"];
//    toVC.isCurrentLocationEnabled = [zipCode length]>0 ? NO:YES;
//    if ([zipCode length]>0)
//    {
//        toVC.currentCoordinates = CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude);
//    }else
//    {
//        toVC.currentCoordinates = coordinates;
//    }
    [self.navigationController pushViewController:toVC animated:YES];
}

////*********************************Locatation Related*******************************//
//-(void)trackLocationForFindindDistacnce
//{
//        //NSLog(@" lat: %f",locationManager.location.coordinate.latitude);
//        //NSLog(@" lon: %f",locationManager.location.coordinate.longitude);
//    
//        if (locationManager == nil)
//        {
//            locationManager = [[CLLocationManager alloc] init];
//            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
//            locationManager.delegate = self;
//        }
//        [locationManager startUpdatingLocation];
//}
//#pragma mark - CLLocationManagerDelegate
//
//- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
//{
//    //NSLog(@"didFailWithError: %@", error);
//    UIAlertView *errorAlert = [[UIAlertView alloc]
//                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [errorAlert show];
//    [locationManager stopUpdatingLocation];
//}
//
//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
//    //NSLog(@"didUpdateToLocation: %@", newLocation);
//    CLLocation *currentLocation = newLocation;
//    
//    if (currentLocation != nil)
//    {
//        currentLattitude = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
//        currentLongitude = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
//    }
//    [locationManager stopUpdatingLocation];
//    
//}
////*********************************Locatation Related *****END*******************************//


- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];
}


#pragma mark - mapview Delegates
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    // here we illustrate how to detect which annotation type was clicked on for its callout
    id <MKAnnotation> annotation = [view annotation];
    
   // NSUInteger index= [_optionsArr indexOfObject:[array objectAtIndex:i]];

    //NSLog(@"annotation %d",[[mapView annotations] indexOfObject:annotation]);
    NSInteger index = 0;
    for (LegalFinder *finder in legalFindersArray)
    {
        
       // index =[legalFindersArray indexOfObject:finder.finderName];
        if ([finder.finderName isEqualToString:[annotation title]])
        {
            index = [legalFindersArray indexOfObject:finder];
            NSLog(@"Selected Index:%d",index);
            break;
        }
    }
    
    [[LegalFinderModel sharedDataSource] selectFinderAtindex:index];
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    LegalFinderDetailVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"LegalFinderDetailVC"];
//    toVC.isCurrentLocationEnabled = [zipCode length]>0 ? NO:YES;
//    if ([zipCode length]>0)
//    {
//        toVC.currentCoordinates = CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude);
//    }else
//    {
//        toVC.currentCoordinates = coordinates;
//    }
    [self.navigationController pushViewController:toVC animated:YES];
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *SFAnnotationIdentifier = @"mapView";
    MKAnnotationView *flagAnnotationView = [legalMapView dequeueReusableAnnotationViewWithIdentifier:SFAnnotationIdentifier];

    if (flagAnnotationView == nil)
    {
        MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                                        reuseIdentifier:SFAnnotationIdentifier];
        
        
        annotationView.canShowCallout = YES;
        annotationView.backgroundColor = [UIColor clearColor];
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        [rightButton setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
        [rightButton addTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
        annotationView.rightCalloutAccessoryView = rightButton;
        UIImage *flagImage = [UIImage imageNamed:@"pin.png"];
        annotationView.image = flagImage;

        annotationView.rightCalloutAccessoryView.tintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"arrow"]];
        annotationView.opaque = NO;
        

        
        //change her
        return annotationView;
    }
    else
    {
        flagAnnotationView.annotation = annotation;
    }
    return flagAnnotationView;
}
- (void)showAnnotations:(NSArray *)annotations animated:(BOOL)animated
{
    
}
-(void)dropPins
{
    int count = legalFindersArray.count;
    NSMutableArray *arr = [NSMutableArray array];
    for (int i=0; i<count; i++)
    {
        LegalFinder *finder = [legalFindersArray objectAtIndex:i];
        
        NSLog(@"Finder Title:%@",finder.finderName);
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        // Set your annotation to point at your coordinate
        point.coordinate = finder.finderAddress.coordinate;
        
        NSString *displayTitle =finder.finderName;
//        if (displayTitle.length >= 32) {
//            displayTitle =[displayTitle substringToIndex:31];
//        }
        point.title = displayTitle;
        //Drop pin on map
        [legalMapView addAnnotation:point];
        [arr addObject:point];
    }
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)
    {
        [legalMapView showAnnotations:arr animated:YES];
    }
    else
    {
        [self zoomToFitMapAnnotations:legalMapView];
    }
}


#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        bgView.frame = viewFrame;
    }
    
    //    }
}

- (void)zoomToFitMapAnnotations:(MKMapView *)mapView {
    if ([mapView.annotations count] == 0) return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    for(id<MKAnnotation> annotation in mapView.annotations) {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
    
    // Add a little extra space on the sides
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.1;
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.1;
    
    region = [mapView regionThatFits:region];
    [mapView setRegion:region animated:YES];
}
//#pragma mark CLLocationManager Delegate Methods
//
//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
//    NSDate* eventDate = newLocation.timestamp;
//    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
//    if (abs(howRecent) < 15.0)
//    {
//        //NSLog(@"New Latitude %+.6f, Longitude %+.6f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
//    }
//}
-(IBAction)mapTappes:(id)sender
{
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
@end
