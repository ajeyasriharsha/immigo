//
//  AppDelegate.h
//  Immigo
//
//  Created by pradeep ISPV on 2/27/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
//    UIView *splashChildView;
//    UIImageView *foregroundSplashView;
//
//    BOOL isInForeground;
//    
//    UIInterfaceOrientation currentOrientation;
}
@property (strong, nonatomic) UIWindow *window;
@property(nonatomic, strong) id<GAITracker> tracker;
-(void) customiseNavigationBar :(UINavigationController *)navCtrlr;

-(void)refreshFakeViewController;
@end
