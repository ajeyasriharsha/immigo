//
//  ImmigrationCheckListVC.m
//  IMMIGO
//
//  Created by pradeep ISPV on 5/2/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "ImmigrationCheckListVC.h"
#import "Topic.h"
#import "SubTopic.h"
#import "CheckListModel.h"
#import "Common.h"
#import "UITextViewDisableSelection.h"
#import "FaceBookActivityProvider.h"
#import "LinkedInActivity.h"

@interface ImmigrationCheckListVC ()
{
    IBOutlet UIScrollView *scrollView;
    
    IBOutlet UIView *bgView;
    IBOutlet UITextViewDisableSelection *subTopicDesc;
    IBOutlet UIButton *topHeading;
    
    NSArray *checkLists;
    IBOutlet UILabel *headingBg;
}
@end

@implementation ImmigrationCheckListVC
@synthesize subtopic = _subtopic;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)init
{
    self=[super init];
    return self;
}
-(CGRect)frameForLabel:(UILabel *)label WithText:(NSString *)text
{
    CGSize maximumLabelSize = CGSizeMake(label.frame.size.width, FLT_MAX);
    CGSize expectedLabelSize = [text sizeWithFont:label.font constrainedToSize:maximumLabelSize lineBreakMode:label.lineBreakMode];
    //adjust the label the the new height.
    CGRect newFrame = label.frame;
    newFrame.size.height = expectedLabelSize.height<44.0f ? 44.0f :expectedLabelSize.height;
    //    label.frame = newFrame;
    
    return newFrame;
}


- (void)customIntialization
{
    float optionsGap = 5.0f;
    
    CGRect totalFrame = bgView.frame;
    CGPoint cursor = CGPointMake(totalFrame.origin.x, headingBg.frame.origin.y+headingBg.frame.size.height);
    CGSize size = CGSizeMake(totalFrame.size.width, totalFrame.size.height-cursor.y);
    
    
//    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, size.height)];
    scrollView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    scrollView.backgroundColor = [UIColor clearColor];

    cursor.y = 5.0f;
    if ([_subtopic.heading length]>0)
    {
        UILabel * questionText=nil;
        
        if (!questionText)
            questionText = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, self.view.frame.size.width-40, 20)];
        questionText.lineBreakMode = NSLineBreakByWordWrapping;
        questionText.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.91]; //[UIFont fontWithName:FONT_SEMIBOLD_ITALIC size:16.91]; // VAMSHI
        questionText.frame = [self frameForLabel:questionText WithText:_subtopic.heading];
        questionText.numberOfLines =0;
        questionText.textColor = UIColorFromRGB(0x212121); // VAMSHI //UIColorFromRGB(0x4c4c4c);
        questionText.backgroundColor  = [UIColor clearColor];
        questionText.text = _subtopic.heading;
        questionText.autoresizingMask =  UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
        questionText.textAlignment = NSTextAlignmentLeft;
        
        UILabel *questionTextBG;
        if (!questionTextBG)
            questionTextBG= [[UILabel alloc] initWithFrame:CGRectMake(0, 0, size.width, questionText.frame.size.height+20)];
        questionTextBG.backgroundColor = [UIColor lightGrayColor];
        questionTextBG.alpha = 0.3f;
        
//        [scrollView addSubview:questionTextBG];
        [scrollView addSubview:questionText];
        cursor.y =questionTextBG.frame.size.height+optionsGap;
    }
    
    //UIImage *optionBG = [UIImage imageNamed:@"pollOptionBG"] ; // VAMSHI
    UIImage *offIcon = [UIImage imageNamed:@"unselected_icon"];
    UIImage *onIcon = [UIImage imageNamed:@"tick_icon"];
    cursor.x = 4;
        
    NSUInteger  numberOfOptions = checkLists.count;
    CGFloat scrollViewHeight= scrollView.contentSize.height;
    for (int i=1; i<=numberOfOptions; i++)
    {
        CheckListItems *checkListItem = [checkLists objectAtIndex:i-1];
        
        
        //        CGFloat buttonHeight = option1.frame.size.height<44.0 ? 44.0f:option1.frame.size.height;
        UILabel *option1;
        if (!option1)
            option1 = [[UILabel alloc] initWithFrame:CGRectMake(20+offIcon.size.width, cursor.y+3, size.width*0.8, size.height*0.25)];
        option1.lineBreakMode = NSLineBreakByWordWrapping;
        option1.font = [UIFont fontWithName:FONT_SEMIBOLD size:14.49];
        option1.frame = [self frameForLabel:option1 WithText:checkListItem.title];
        option1.textColor = UIColorFromRGB(0x5d5d5d);
        option1.numberOfLines =0;
        option1.autoresizingMask =  UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
        option1.backgroundColor  = [UIColor clearColor];
        option1.text = checkListItem.title;
        option1.tag = 10*i;
        option1.textAlignment = NSTextAlignmentLeft;

        
        UIButton *optionButton1= [UIButton buttonWithType:UIButtonTypeCustom];
        optionButton1.frame = CGRectMake(10, cursor.y, self.view.frame.size.width-20, option1.frame.size.height+6);
        optionButton1.imageEdgeInsets = UIEdgeInsetsMake(0,+05, 0, 0);
        [optionButton1 addTarget:self action:@selector(checkListSelected:) forControlEvents:UIControlEventTouchUpInside];
       // [optionButton1 setBackgroundImage:[optionBG stretchableImageWithLeftCapWidth:optionBG.size.width/2 topCapHeight:optionBG.size.height/2] forState:UIControlStateNormal];  // VAMSHI
        [optionButton1 setImage:offIcon forState:UIControlStateNormal];
        [optionButton1 setImage:onIcon forState:UIControlStateHighlighted];
        [optionButton1 setImage:onIcon forState:UIControlStateSelected];
        optionButton1.titleLabel.backgroundColor = [UIColor clearColor];
        optionButton1.tag = checkListItem.key;
        optionButton1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        optionButton1.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
        optionButton1.imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [optionButton1 setSelected:checkListItem.isChecked] ;
        

        [scrollView addSubview:optionButton1];
        [scrollView addSubview:option1];

        cursor.y +=optionButton1.frame.size.height+optionsGap;
        
        if (numberOfOptions == i)
        {
            scrollViewHeight = optionButton1.frame.origin.y +optionButton1.frame.size.height +90;
        }
    }
//    [bgView addSubview:scrollView];
//    [bgView bringSubviewToFront:scrollView];
    scrollView.contentSize = CGSizeMake(totalFrame.size.width, scrollViewHeight);

}


-(IBAction)print:(id)sender{
    
    
    
}
-(IBAction)checkListSelected:(UIButton *)sender
{
    sender.selected = !sender.selected;

    [[CheckListModel sharedDataSource] selectTheCheckListItemWithKey:sender.tag AndSubTopicID:_subtopic.subTopicId IfSelected:sender.selected];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    subTopicDesc.text= @"AJ";
    checkLists = [[CheckListModel sharedDataSource] fetchCheckListItemsForSubtopic:_subtopic];
    
    titleheading.backgroundColor=UIColorFromRGB(0Xc51039);
    titleheading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];

    sepratorLine.backgroundColor=UIColorFromRGB(0Xc51039);
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        //NSLog(@"runnig on < 7.0");
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"left arrow"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        UIImage *image = [UIImage imageNamed:@"question"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = rightItem;
        
        [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    topHeading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.91];
    //    topicHeading.text = @"TOPIC:";
    topHeading.backgroundColor = [UIColor clearColor];
    
    subTopicDesc.backgroundColor = [UIColor clearColor];
    subTopicDesc.font = [UIFont fontWithName:FONT_SEMIBOLD size:16]; // VAMSHI //[UIFont fontWithName:FONT_SEMIBOLD_ITALIC size:16.91];
    subTopicDesc.textColor = UIColorFromRGB(0XC51039);  //VAMSHI //[UIColor darkGrayColor];
    subTopicDesc.text = [NSString stringWithFormat:@"%@ / %@",_subtopic.topic.topicTitle,_subtopic.subtopicTitle];
    
    self.navigationController.navigationBarHidden = NO;
    //UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];  // VAMSHI
    //UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)]; // VAMSHI
    //titlView.image = titleLogo; // VAMSHI
    //self.navigationItem.titleView = titlView; // VAMSHI
    
   
    
    
    [self customIntialization];
    
    
}


-(NSString*)getCheckListHtmlisFromPrint:(BOOL)fromPrint{
   
    
    NSMutableString *bodyHtml=[NSMutableString string];
    NSUInteger  numberOfOptions = checkLists.count;
    for (int i=1; i<=numberOfOptions; i++)
    {
        CheckListItems *checkListItem = [checkLists objectAtIndex:i-1];
        
        NSString *linkText =checkListItem.title;
        NSString *titleHtml =[NSString stringWithFormat:@"<p>%@</p>",_subtopic.heading.length ?_subtopic.heading:@""];
        NSString *linkHtml =[NSString stringWithFormat:@"<p><input type=\"checkbox\" %@ > %@<br></p>",checkListItem.isChecked ? @"checked=\"checked\")" :@"",linkText];
        if (i==1) {
            linkHtml =[titleHtml stringByAppendingString:linkHtml];
        }
        [bodyHtml appendString:linkHtml];
        
    }
    if (!fromPrint) {
        NSURL *appstoreUrl = [NSURL URLWithString:APP_STORE_URL];
        NSString *mailSignature =[NSString stringWithFormat:@"<p> %@ <a href=\"%@\">%@</a></p>",NSLocalizedString(@"SENT_EMAIL_SIGNATURE", @""),appstoreUrl,[Common deviceName]];
        [bodyHtml appendString:mailSignature];
    }
    
    NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html><html><body> %@</body></html>",bodyHtml];
    return html;
}
-(IBAction)share:(id)sender{
    
    NSString *sharingContentForSocialMedia =[NSString stringWithFormat:@"%@/%@:",_subtopic.topic.topicTitle,_subtopic.subtopicTitle];
    
    
    NSString *textToShare = [self getCheckListHtmlisFromPrint:NO];
    NSURL *myWebsite = [NSURL URLWithString:APP_STORE_COMMON_LINK];
    
    NSString *printContent =[self getCheckListHtmlisFromPrint:YES];
    UIMarkupTextPrintFormatter *htmlFormatter = [[UIMarkupTextPrintFormatter alloc]
                                                 initWithMarkupText:printContent];
    htmlFormatter.startPage = 0;
    htmlFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
    
    
    NSArray *applicationActivities=nil;
    
//    if ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] integerValue] >= 6 && [[[[UIDevice currentDevice] systemVersion] substringToIndex:1] integerValue] <= 7) {
//        LinkedInActivity *activity =[[LinkedInActivity alloc] init];
//        activity.postUrl =[myWebsite absoluteString];
//        activity.postCommentText =sharingContentForSocialMedia;
//        applicationActivities =
//        @[
//          activity
//          ];
//        
//    }else if ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] integerValue] >= 8){
//        
//        applicationActivities =nil;
//    }
    
    FaceBookActivityProvider *activityItemProvider =
    [[FaceBookActivityProvider alloc] initWithPlaceholderItem:textToShare
                                                  twitterItem:sharingContentForSocialMedia andUrlForStore:[myWebsite absoluteString]];
    
    NSArray *objectsToShare = @[activityItemProvider,htmlFormatter,myWebsite];

    
    UIActivityViewController *controller =
    [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:applicationActivities];
    
    NSString *appname =NSLocalizedString(@"APP_NAME", "");
    NSString *subject =[NSString stringWithFormat:@"%@ - %@ - %@",appname,_subtopic.topic.topicTitle,_subtopic.subtopicTitle];
    
    [controller setValue:subject forKey:@"subject"];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,//@"com.linkedin.LinkedIn.ShareExtension",
                                   UIActivityTypePostToVimeo,
                                   UIActivityTypePostToTencentWeibo,
                                   UIActivityTypeMessage,
                                   UIActivityTypeCopyToPasteboard];
    
    controller.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:controller animated:YES completion:nil];
    
    [controller setCompletionHandler:^(NSString *activityType, BOOL completed)
     {
         if (completed)
         {
//             UIAlertView *objalert = [[UIAlertView alloc]initWithTitle:@"Immigo" message:@"Posting was successful" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//             [objalert show];
//             objalert = nil;
         }else
         {
//             UIAlertView *objalert = [[UIAlertView alloc]initWithTitle:@"Immigo" message:@"Posting was not successful" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//             [objalert show];
//             objalert = nil;
         }
     }];
    
    
}
-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController setToolbarHidden:YES];
    [subTopicDesc enableObserver:NO onObject:subTopicDesc selector:@selector(contentSize)];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
}
- (void)dealloc
{
    //NSLog(@"links dealloc");
    //[subTopicDesc disableAlginment];
}
-(void)viewWillAppear:(BOOL)animated
{

    [super viewWillAppear:animated];
    [self customizeNavigationBar];
    [self.navigationController setToolbarHidden:YES];
    [self.navigationController.toolbar setBarStyle:UIBarStyleDefault];  //for example
    
    //set the toolbar buttons
    
    
    // Create a custom UIBarButtonItem with the button
    UIBarButtonItem *share = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(share:)];
    
    [self setToolbarItems:[NSArray arrayWithObjects:share,nil]];
    
    //[subTopicDesc alignToTop];
    //new changes for textview center text
    [subTopicDesc enableObserver:YES onObject:subTopicDesc selector:@selector(contentSize)];
}
-(void)customizeNavigationBar{
    
    
    // Get the navigation controller of this view controller with:
    
//    UINavigationBar *navigationBar = [self.navigationController navigationBar];
//    CGRect frame = [navigationBar frame];
//    frame.size.height = 58.0f;
//    [navigationBar setFrame:frame];
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    [navigationBar setBackgroundColor:[UIColor clearColor]]; // VAMSHI // [UIColor yellowColor]];
//    UIImage *navBarImg = [UIImage imageNamed:@"navbar_bg"];//TopTabTitleBg//Nav_Background.png
//    [navigationBar setBackgroundImage:navBarImg forBarMetrics:UIBarMetricsDefault];
    
    
//    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
//    [backView setBackgroundColor:[UIColor clearColor]];
//    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
//    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
//    [backView addSubview:titleImageView];
//    titleImageView.contentMode = UIViewContentModeCenter;
//    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        bgView.frame = viewFrame;
        
    }
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        bgView.frame = viewFrame;
    }
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;

    scrollView.contentSize = CGSizeMake(iOSDeviceScreenSize.width, scrollView.contentSize.height);
    
    //    }
}

- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
@end
