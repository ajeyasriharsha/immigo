//
//  AdvancedSearchVC.m
//  Immigo
//
//  Created by pradeep ISPV on 3/6/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "AdvancedSearchVC.h"
#import "AboutUsVC.h"
#import "OptionsView.h"
#import "LegalSearchListVC.h"
#import "Common.h"
#import "AOLModel.h"
#import "LegalAssistanceModel.h"
#import "LanguageModel.h"
#import "Categories.h"
#import "DataDownloader.h"
#import "LegalFinderModel.h"
#import "DCRoundSwitch.h"
#import "FTLocationManager.h"
#import "INTULocationManager.h"
#import "RCSwitchOnOff.h"


#import "AdvanedSearchMultiSelectVC.h"
#define SCROLLVIEW_CONTENT_HEIGHT 460
#define SCROLLVIEW_CONTENT_WIDTH  320

@interface AdvancedSearchVC () <optionsViewDelegate,UIGestureRecognizerDelegate,DataDownloaderDelegate,multiselectDelegate>
{
    IBOutlet UIButton *heading;
    OptionsView *optionsPopUpView;
    IBOutlet UITextField *areaOfLawTF;
    IBOutlet UITextField *legalAssistTF;
    IBOutlet UITextField *languageTF;
    IBOutlet UIButton *searchButton;
    IBOutlet UIImageView *tfBG;
    IBOutlet UIImageView *tfBG1;
    IBOutlet UIImageView *tfBG2;
    IBOutlet UIImageView *tfBG3;
    IBOutlet UIImageView *tfBg4;
    IBOutlet UITextField *zipCodeTF;
    IBOutlet UITextField *currentLocationTF;
    //ON/OFF Switch Implemtation//JUHI
    //    DCRoundSwitch *switchControl;
    RCSwitchOnOff *switchControl;
    
    NSString *currentLattitude;
    NSString *currentLongitude;
    
    CGRect scrollView_OriginalFrame;
    
    NSArray *areaOfLaws;
    AdvanedSearchMultiSelectVC * multiSelectVC;
}
@property (assign, nonatomic) INTULocationRequestID locationRequestID;
@end

@implementation AdvancedSearchVC
@synthesize zipCode,delegate,activeField,coordinates,isSwitchOn,selectedLocation;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[DataDownloader sharedDownloader] setDelegate:self];
    heading.backgroundColor=UIColorFromRGB(0Xc51039);

    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        //NSLog(@"runnig on < 7.0");
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"left arrow"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        UIImage *image = [UIImage imageNamed:@"question"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = rightItem;
        
        [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];
        
    }
    
    {
        self.navigationController.navigationBarHidden = NO;
//        UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//        UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//        titlView.image = titleLogo;
//        self.navigationItem.titleView = titlView;
        //        pollsTable.rowHeight = 60.0f;
    }
    {
        UIImage *bgIm = [UIImage imageNamed:@"TextField_BG"];
        tfBG.image = tfBG1.image = tfBG2.image = tfBG3.image = tfBg4.image = [bgIm stretchableImageWithLeftCapWidth:bgIm.size.width/2 topCapHeight:bgIm.size.height/2];
    }
    areaOfLawTF.font = languageTF.font= legalAssistTF.font = zipCodeTF.font =currentLocationTF.font= [UIFont fontWithName:FONT_SEMIBOLD_ITALIC size:14.32f];
    
    heading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
    heading.backgroundColor=UIColorFromRGB(0Xc51039);//RedColor ThroughtAPP//JUHI
//    UIImage *btnBg = [UIImage imageNamed:@"RedBtn_Bg"];
//    [searchButton setBackgroundImage:[btnBg stretchableImageWithLeftCapWidth:btnBg.size.width/2 topCapHeight:btnBg.size.height/2] forState:UIControlStateNormal];
    [searchButton setBackgroundColor:UIColorFromRGB(0X114171)];  //VAMSHI
    searchButton.contentMode = UIViewContentModeRedraw;
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOutside:)];
    
    [recognizer setNumberOfTapsRequired:1];
    recognizer.cancelsTouchesInView = NO; //So the user can still interact with controls in the modal view
    [self.view addGestureRecognizer:recognizer];
    
    if ([zipCode length]>0)
    {
        zipCodeTF.text = zipCode;
    }
    //ON/OFF Switch Implemtation//JUHI
//    switchControl = [[DCRoundSwitch alloc] initWithFrame:CGRectMake(tfBg4.frame.origin.x+tfBg4.frame.size.width-78, currentLocationTF.frame.origin.y, 70, 30)];
//    [switchControl addTarget:self action:@selector(tappedOnSwitch:) forControlEvents:UIControlEventValueChanged];
//    switchControl.on = NO;
//    switchControl.autoresizingMask= UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin;// |
//    switchControl.backgroundColor = [UIColor clearColor];
    
    switchControl = [[RCSwitchOnOff alloc] initWithFrame:CGRectMake(currentLocationTF.frame.origin.x+currentLocationTF.frame.size.width-83, currentLocationTF.frame.origin.y-3, 81, 36)];//(tfBg4.frame.origin.x+tfBg4.frame.size.width-78, currentLocationTF.frame.origin.y-3, 90, 40)];
    [switchControl setOn:YES];
    [switchControl addTarget:self action:@selector(tappedOnSwitch:) forControlEvents:UIControlEventValueChanged];
    switchControl.autoresizingMask= UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin;
    [optionsScrollView  addSubview:switchControl];
    
    //selectedLocation =nil;
    
	// Do any additional setup after loading the view.
}

-(void)tappedOutside:(UITapGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateEnded)
    {
        CGPoint location = [gesture locationInView:nil]; //Passing nil gives us coordinates in the window
        
        //Then we convert the tap's location into the local view's coordinate system, and test to see if it's in or outside. If outside, dismiss the view.
        
        if (CGRectContainsPoint(optionsPopUpView.frame, location))
        {
            return;
        }
        else
        {
            self.delegate = optionsPopUpView;
            
            if (delegate && [delegate respondsToSelector:@selector(closePopUpIfTouchOutSide)]) {
                [delegate closePopUpIfTouchOutSide];
            }
            
        }
    }
}

//ON/OFF Switch Implemtation//JUHI
-(IBAction)tappedOnSwitch:(RCSwitchOnOff*)sender
{
    if ([sender isOn])
    {
        
        zipCodeTF.text = @"";
        zipCodeTF.userInteractionEnabled = NO;
        self.isSwitchOn =sender.isOn;
        
        __weak __typeof(self) weakSelf = self;
        __block NSString *outputText = nil;
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        self.locationRequestID  =[locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyNeighborhood
                                           timeout:10
                              delayUntilAuthorized:YES
                                             block:
         ^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
             __typeof(weakSelf) strongSelf = weakSelf;
             
             if (status == INTULocationStatusSuccess) {
                 // achievedAccuracy is at least the desired accuracy (potentially better)
                 
                 strongSelf.selectedLocation =currentLocation;
                 currentLattitude = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
                 currentLongitude = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
                 
             }
             else if (status == INTULocationStatusServicesDisabled) {
                 //  User has turned off location services device-wide (for all apps) from the system Settings app.
                 outputText = [NSString stringWithFormat:NSLocalizedString(@"LOCATION_FAILURE_ERROR", @"")];
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"APP_NAME", @"")
                                                                     message:outputText
                                                                    delegate:nil
                                                           cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                                           otherButtonTitles:nil];
                     [alert show];
                     
                     [switchControl setOn:NO];
                 });
                 
                 
                 
             }else if (status == INTULocationStatusServicesDenied){
                 /** User has explicitly denied this app permission to access location services. */
                 outputText = [NSString stringWithFormat:NSLocalizedString (@"LOCATION_PERMISSIONS_DENIED_ERROR",@"")];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"APP_NAME", @"")
                                                                     message:outputText
                                                                    delegate:nil
                                                           cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                                           otherButtonTitles:nil];
                     [alert show];
                     
                     [switchControl setOn:NO];
                 });
                 
             }
             else if (status == INTULocationStatusTimedOut) {
                 // You may wish to inspect achievedAccuracy here to see if it is acceptable, if you plan to use currentLocation
                 [switchControl setOn:NO];
             }
             else {
                 // An error occurred
                 [switchControl setOn:NO];
                 
             }
             
         }];
        
    }
    else
    {
        self.isSwitchOn =sender.isOn;
        zipCodeTF.userInteractionEnabled = YES;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];
}

- (IBAction)tappedOnAreaOfLaw:(id)sender
{
    [self resignTextFieldKeyBoard];
    areaOfLaws = [[AOLModel sharedDataSource] allAreas];
    
    NSArray *existingValues;
    if ([areaOfLawTF.text length]>0)
    {
        existingValues = [areaOfLawTF.text componentsSeparatedByString:@","];
    }else
    {
        existingValues = nil;
    }
    
    
    NSString * storyboardName = @"Main_iPhone";
    NSString * viewControllerID = @"AdvanedSearchMultiSelectVC";
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    multiSelectVC = (AdvanedSearchMultiSelectVC *)[storyboard instantiateViewControllerWithIdentifier:viewControllerID];
    multiSelectVC.optionsArr =[areaOfLaws mutableCopy];
    multiSelectVC.selectedValues =[existingValues mutableCopy];
    multiSelectVC.delegate =self;
    multiSelectVC.view.tag =1;
    multiSelectVC.popUpTitle =NSLocalizedString (@"ARESOFLAW_TITLE",@"");
    
    UINavigationController *nav =[[UINavigationController alloc] initWithRootViewController:multiSelectVC];
    [self presentViewController:nav animated:YES completion:nil];
    
}


- (void)animateViewHeight:(UIView*)animateView withAnimationType:(NSString*)animType {
    CATransition *animation = [CATransition animation];
    [animation setType:kCATransitionPush];
    [animation setSubtype:animType];
    
    [animation setDuration:0.5];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [[animateView layer] addAnimation:animation forKey:kCATransition];

    
}
- (void) slideAnimationIsLeft:(BOOL)isleftDirection
{
    CATransition *applicationLoadViewIn = [CATransition animation];
    [applicationLoadViewIn setDuration:0.3f];
    [applicationLoadViewIn setType:kCATransitionPush];
    if(isleftDirection)
        [applicationLoadViewIn setSubtype:kCATransitionFromLeft];
    else
        [applicationLoadViewIn setSubtype:kCATransitionFromRight];
    [[optionsPopUpView layer] addAnimation:applicationLoadViewIn forKey:kCATransitionReveal];
    
}
- (IBAction)tappedOnLegalAssistance:(id)sender
{
    [self resignTextFieldKeyBoard];
    NSArray *existingValues;
    if ([legalAssistTF.text length]>0)
    {
        existingValues = [legalAssistTF.text componentsSeparatedByString:@","];
    }else
    {
        existingValues = nil;
    }
    
    NSArray *options = [[LegalAssistanceModel sharedDataSource] allObjects];//[[NSArray alloc] initWithObjects:@"option1",@"option2",@"option3",@"option4",@"option5",@"option6",@"option7",@"option8",@"option9",@"option10",@"option11",@"option12",@"option13",@"option14", nil];
    
    /*
    optionsPopUpView = [[OptionsView alloc] initWithFrame:CGRectMake(20, 84, self.view.frame.size.width-40, self.view.frame.size.height-160) withOptions:options withTitle:@"LEGAL(S) ASSISTANCE" withValues:existingValues];
    //        optionsPopUpView = [[OptionsView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) withOptions:options];
    optionsPopUpView.delegate = self;
    optionsPopUpView.tag = 2;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6];
	
    [UIView setAnimationDelegate: self];
    //    [UIView setAnimationDidStopSelector:
    //     @selector(animationFinished:finished:context:)];
    [self.view addSubview:optionsPopUpView];
	
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp
                           forView:optionsPopUpView cache:YES];
    [UIView commitAnimations];
     */
     
    NSString * storyboardName = @"Main_iPhone";
    NSString * viewControllerID = @"AdvanedSearchMultiSelectVC";
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    multiSelectVC = (AdvanedSearchMultiSelectVC *)[storyboard instantiateViewControllerWithIdentifier:viewControllerID];
    multiSelectVC.optionsArr =[options mutableCopy];
    multiSelectVC.selectedValues =[existingValues mutableCopy];
    multiSelectVC.delegate =self;
    multiSelectVC.view.tag =2;
    multiSelectVC.popUpTitle =@"TYPES OF LEGAL ASSISTANCE";
    UINavigationController *nav =[[UINavigationController alloc] initWithRootViewController:multiSelectVC];
    [self presentViewController:nav animated:YES completion:nil];
    
    
}

- (IBAction)tappedOnLanguage:(id)sender
{
    [self resignTextFieldKeyBoard];
    NSArray *options = [[LanguageModel sharedDataSource] allObjects];//[[NSArray alloc] initWithObjects:@"option1",@"option2",@"option3",@"option4",@"option5",@"option6",@"option7",@"option8",@"option9",@"option10",@"option11",@"option12",@"option13",@"option14", nil];
    
    NSArray *existingValues;
    if ([languageTF.text length]>0)
    {
        existingValues = [languageTF.text componentsSeparatedByString:@","];
    }else
    {
        existingValues = nil;
    }
    
    /*
    optionsPopUpView = [[OptionsView alloc] initWithFrame:CGRectMake(20, 84, self.view.frame.size.width-40, self.view.frame.size.height-160) withOptions:options withTitle:@"LANGUAGE(S)" withValues:existingValues];
    //        optionsPopUpView = [[OptionsView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) withOptions:options];
    optionsPopUpView.delegate = self;
    optionsPopUpView.tag = 3;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6];
	
    [UIView setAnimationDelegate: self];
    //    [UIView setAnimationDidStopSelector:
    //     @selector(animationFinished:finished:context:)];
    [self.view addSubview:optionsPopUpView];
	
    [UIView setAnimationTransition:UIViewAnimationTransitionNone
                           forView:optionsPopUpView cache:YES];
    [UIView commitAnimations];
     */
    
    NSString * storyboardName = @"Main_iPhone";
    NSString * viewControllerID = @"AdvanedSearchMultiSelectVC";
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    multiSelectVC = (AdvanedSearchMultiSelectVC *)[storyboard instantiateViewControllerWithIdentifier:viewControllerID];
    multiSelectVC.optionsArr =[options mutableCopy];
    multiSelectVC.selectedValues =[existingValues mutableCopy];
    multiSelectVC.delegate =self;
    multiSelectVC.view.tag =3;
    multiSelectVC.popUpTitle =NSLocalizedString(@"LANGUAGESSPOKEN_TITLE",@"");
    UINavigationController *nav =[[UINavigationController alloc] initWithRootViewController:multiSelectVC];
    [self presentViewController:nav animated:YES completion:nil];
}

- (IBAction)advanceSearchTapped:(id)sender
{
    // get languageIDs
    
    NSArray *languageIds;
    NSString *languageString;
    if ([languageTF.text length]==1)
    {
        // code to write for when field exists single value
    }else
    {
        if (languageTF.text && languageTF.text.length>0) {
            languageIds = [languageTF.text componentsSeparatedByString:@","];
            
            for (int i=0; i<languageIds.count; i++)
            {
                if (!languageString)
                {
                    languageString = [[LanguageModel sharedDataSource] getIdForValue:[languageIds objectAtIndex:0]];
                }
                else
                {
                    NSString *string1 = [NSString stringWithFormat:@",%@",[[LanguageModel sharedDataSource] getIdForValue:[languageIds objectAtIndex:i]]];
                    languageString = [languageString stringByAppendingString:string1];
                }
            }
        }
        
    }
    

    
    // get Area of laws
    
    NSArray *aolIds;
    NSString *aolString;
    if ([areaOfLawTF.text length]==1)
    {
        // code to write for when field exists single value
    }else
    {
        
        if (areaOfLawTF.text && areaOfLawTF.text.length>0) {
        aolIds = [areaOfLawTF.text componentsSeparatedByString:@","];
        
        for (int i=0; i<aolIds.count; i++)
        {
            if (!aolString)
            {
                aolString = [[AOLModel sharedDataSource] getLegalIdForValue:[aolIds objectAtIndex:0]];
            }
            else
            {
                NSString *string1 = [NSString stringWithFormat:@",%@",[[AOLModel sharedDataSource] getLegalIdForValue:[aolIds objectAtIndex:i]]];
                aolString = [aolString stringByAppendingString:string1];
            }
        }
      }
    }

    
    // get legal assiatance
    
    NSArray *legalAssitanceIds;
    NSString *legalAssistanceString;
    if ([legalAssistTF.text length]==1)
    {
        // code to write for when field exists single value
    }else
    {
        
        if (legalAssistTF.text && legalAssistTF.text.length>0) {
            legalAssitanceIds = [legalAssistTF.text componentsSeparatedByString:@","];
            
            for (int i=0; i<legalAssitanceIds.count; i++)
            {
                if (!legalAssistanceString)
                {
                    legalAssistanceString = [[LegalAssistanceModel sharedDataSource] getIdForValue:[legalAssitanceIds objectAtIndex:i]];
                }
                else
                {
                    NSString *string1 = [NSString stringWithFormat:@",%@",[[LegalAssistanceModel sharedDataSource] getIdForValue:[legalAssitanceIds objectAtIndex:i]]];
                    legalAssistanceString = [legalAssistanceString stringByAppendingString:string1];
                }
            }
        }
        
    }
    
    
    [Common logMainEventsToGoogleAnalytics:@"LegalHelpFinder Advanced Search" withAction:@"LegalHelpFinder Advanced Search tapped" andLabel:@"" and:0];
    
    
    if ([zipCodeTF.text length]==0 && ![switchControl isOn] )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString (@"LEGAL_HELP_FINDER_TITLE",@"") message:NSLocalizedString (@"ADVANCEDSEARCH_ERROR",@"") delegate:self cancelButtonTitle:NSLocalizedString (@"ALERT_OK_BUTTON_TITLE",@"") otherButtonTitles:nil];
        [alert show];
    }else if (([zipCodeTF.text length]<5 || [zipCodeTF.text  isEqual:@"00000"]) && ![switchControl isOn])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString (@"LEGAL_HELP_FINDER_TITLE",@"") message:NSLocalizedString (@"ADVANCEDSEARCH_ERROR",@"") delegate:self cancelButtonTitle:NSLocalizedString (@"ALERT_OK_BUTTON_TITLE",@"") otherButtonTitles:nil];
        [alert show];
        
    }
    else if (languageTF.text.length == 0 && areaOfLawTF.text.length == 0 && legalAssistTF.text.length == 0){
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString (@"LEGAL_HELP_FINDER_TITLE",@"") message:NSLocalizedString (@"ADVANCEDSEARCH_VALIDATION_ERROR",@"")  delegate:self cancelButtonTitle:NSLocalizedString (@"ALERT_OK_BUTTON_TITLE",@"") otherButtonTitles:nil];//
        [alert show];
        
    }
    
    else
    {
        if ([switchControl isOn]) {
            
            
            
//            __weak __typeof(self) weakSelf = self;
//            __block NSString *outputText = nil;
//            INTULocationManager *locMgr = [INTULocationManager sharedInstance];
//            self.locationRequestID= [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyNeighborhood
//                                               timeout:10
//                                  delayUntilAuthorized:NO
//                                                 block:
//             ^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
//                 __typeof(weakSelf) strongSelf = weakSelf;
//                 
//                 if (status == INTULocationStatusSuccess) {
//                     // achievedAccuracy is at least the desired accuracy (potentially better)
//                     
//                     strongSelf.selectedLocation =currentLocation;
//                     currentLattitude = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
//                     currentLongitude = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
//                     
//                 }
//                 else if (status == INTULocationStatusServicesDisabled) {
//                     //  User has turned off location services device-wide (for all apps) from the system Settings app.
//                     outputText = [NSString stringWithFormat:@"Failed to get your location as location services are disabled for IMMIGO application."];
//                     
//                     dispatch_async(dispatch_get_main_queue(), ^{
//                         
//                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Immigo"
//                                                                         message:outputText
//                                                                        delegate:nil
//                                                               cancelButtonTitle:@"OK"
//                                                               otherButtonTitles:nil];
//                         [alert show];
//                         
//                         [switchControl setOn:NO];
//                     });
//                     
//                     
//                     
//                 }else if (status == INTULocationStatusServicesDenied){
//                     /** User has explicitly denied this app permission to access location services. */
//                     outputText = [NSString stringWithFormat:@"Failed to get your location as user denied the permissions for the Immigo application."];
//                     dispatch_async(dispatch_get_main_queue(), ^{
//                         
//                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Immigo"
//                                                                         message:outputText
//                                                                        delegate:nil
//                                                               cancelButtonTitle:@"OK"
//                                                               otherButtonTitles:nil];
//                         [alert show];
//                         
//                         [switchControl setOn:NO];
//                     });
//                     
//                 }
//                 else if (status == INTULocationStatusTimedOut) {
//                     // You may wish to inspect achievedAccuracy here to see if it is acceptable, if you plan to use currentLocation
//                     [switchControl setOn:NO];
//                 }
//                 else {
//                     // An error occurred
//                     [switchControl setOn:NO];
//                     
//                 }
//                 
//             }];
            

            
            
        }
        
        [self makeServiceCallwithAol:aolString withLegalAssistance:legalAssistanceString withLanguage:languageString];
//        double delayInSeconds = 10.0;
//        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//            [self makeServiceCallwithAol:aolString withLegalAssistance:legalAssistanceString withLanguage:languageString];
//        });
        
       
    }
}


-(void)makeServiceCallwithAol:(NSString *)aolString withLegalAssistance:(NSString *)legalAssistanceString withLanguage:(NSString *)languageString
{
    
    NSString *latitide =[NSString stringWithFormat:@"%f",self.selectedLocation.coordinate.latitude];
    NSString *longitude =[NSString stringWithFormat:@"%f",self.selectedLocation.coordinate.longitude];
    [[LegalFinderModel sharedDataSource] fetchLegalFindersWithZipCode:zipCodeTF.text OrWithCurrentLocationLattitude:latitide AndLongitude:longitude withLanguages:languageString withAreasOfLaws:aolString withLegalAssistance:legalAssistanceString];
    
    
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    LegalSearchListVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"LegalSearchListVC"];
    toVC.zipCode = zipCodeTF.text;
    toVC.coordinates =selectedLocation.coordinate;
    toVC.isFromAdvancedSearch =YES;
    [DataDownloader sharedDownloader].delegate = (id)toVC;
    [self.navigationController pushViewController:toVC animated:YES];
}

-(void)closePopUP
{
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.6];
//    [UIView setAnimationDelegate: self];
//    optionsPopUpView.hidden = YES;
//    [UIView setAnimationTransition:UIViewAnimationTransitionNone
//                           forView:optionsPopUpView cache:NO];
//    [UIView commitAnimations];
    
    
    [self animateViewHeight:optionsPopUpView withAnimationType:kCATransitionFromBottom];
    optionsPopUpView.hidden =YES;
    
    
    //    [UIView animateWithDuration:0.3 animations:^{
//        optionsPopUpView.frame = CGRectMake(0, self.view.frame.size.height, optionsPopUpView.frame.size.width, 180);
//    } completion:^(BOOL finished) {
//        [UIView animateWithDuration:0.3 animations:^{
//            optionsPopUpView.hidden =YES;
//            
//            
//        }];
//    }];
    
}
-(void)hidepopUp
{
    optionsPopUpView.hidden = YES;
}
-(void)passValuesToField:(NSArray *)values
{
    
    NSSet* mySetWithUniqueItems= [NSSet setWithArray: values];
    NSString *string=@"";
    values =[[mySetWithUniqueItems allObjects]  sortedArrayUsingSelector:@selector(compare:)];
    string = [values componentsJoinedByString:@","];
    values =[values  sortedArrayUsingSelector:@selector(compare:)];
   // NSString *string = [values componentsJoinedByString:@","];
    
    //NSLog(@"array %@",values);
    switch (multiSelectVC.view.tag) {
        case 1:
        {
            areaOfLawTF.text = @"";
            //            //NSLog(@"values %@",[[AOLModel sharedDataSource] getLegalIdForValue:string]);
            areaOfLawTF.text = string;
        }
            break;
        case 2:
        {
            legalAssistTF.text = @"";
            //            //NSLog(@"values %@",[[LegalAssistanceModel sharedDataSource] getIdForValue:string]);
            legalAssistTF.text = string;
        }
            break;
        case 3:
        {
            languageTF.text = @"";
            //            //NSLog(@"values %@",[[LanguageModel sharedDataSource] getIdForValue:string]);
            languageTF.text = string;
        }
            break;
            
        default:
            break;
    }
    
}
//-(void)passValueToField:(NSString *)string
//{
//    switch (optionsPopUpView.tag) {
//        case 1:
//        {
//            areaOfLawTF.text = @"";
//            //NSLog(@"values %@",[[AOLModel sharedDataSource] getLegalIdForValue:string]);
//            areaOfLawTF.text = string;
//        }
//            break;
//        case 2:
//        {
//            legalAssistTF.text = @"";
//            //NSLog(@"values %@",[[LegalAssistanceModel sharedDataSource] getIdForValue:string]);
//            legalAssistTF.text = string;
//        }
//            break;
//        case 3:
//        {
//            languageTF.text = @"";
//            //NSLog(@"values %@",[[LanguageModel sharedDataSource] getIdForValue:string]);
//            languageTF.text = string;
//        }
//            break;
//            
//        default:
//            break;
//    }
//}
#pragma mark - UItextfield delegates


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
   //UITextField *p=(PlaceHolderField *)UITextField
    self.activeField    =textField;
    if (textField == zipCodeTF) {
        return YES;
    }
    return NO;  // Hide both keyboard and blinking cursor.
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
//    [textField resignFirstResponder];
    self.activeField =nil;
}

#define kOFFSET_FOR_KEYBOARD 80.0

-(void)keyboardWillShow:(NSNotification *)note
{
    
    // If keyboard is visible, return
	if (keyboardVisible) {
		//NSLog(@"Keyboard is already visible. Ignore notification.");
		return;
	}
	
    NSDictionary *userInfo = [note userInfo];
    
    CGRect keyboardFrameInWindow;
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardFrameInWindow];
    
    // the keyboard frame is specified in window-level coordinates. this calculates the frame as if it were a subview of our view, making it a sibling of the scroll view
    CGRect keyboardFrameInView = [self.view convertRect:keyboardFrameInWindow fromView:nil];
    
    CGRect scrollViewKeyboardIntersection = CGRectIntersection(optionsScrollView.frame, keyboardFrameInView);
    UIEdgeInsets newContentInsets = UIEdgeInsetsMake(0, 0, scrollViewKeyboardIntersection.size.height, 0);
    
    // this is an old animation method, but the only one that retains compaitiblity between parameters (duration, curve) and the values contained in the userInfo-Dictionary.
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue]];
    
    optionsScrollView.contentInset = newContentInsets;
    optionsScrollView.scrollIndicatorInsets = newContentInsets;
    
    /*
     * Depending on visual layout, _focusedControl should either be the input field (UITextField,..) or another element
     * that should be visible, e.g. a purchase button below an amount text field
     * it makes sense to set _focusedControl in delegates like -textFieldShouldBeginEditing: if you have multiple input fields
     */
    if (activeField) {
        CGRect controlFrameInScrollView = [optionsScrollView convertRect:activeField.bounds fromView:activeField]; // if the control is a deep in the hierarchy below the scroll view, this will calculate the frame as if it were a direct subview
        controlFrameInScrollView = CGRectInset(controlFrameInScrollView, 0, -10); // replace 10 with any nice visual offset between control and keyboard or control and top of the scroll view.
        
        CGFloat controlVisualOffsetToTopOfScrollview = controlFrameInScrollView.origin.y - optionsScrollView.contentOffset.y;
        CGFloat controlVisualBottom = controlVisualOffsetToTopOfScrollview + controlFrameInScrollView.size.height;
        
        // this is the visible part of the scroll view that is not hidden by the keyboard
        CGFloat scrollViewVisibleHeight = optionsScrollView.frame.size.height - scrollViewKeyboardIntersection.size.height;
        
        if (controlVisualBottom > scrollViewVisibleHeight) { // check if the keyboard will hide the control in question
            // scroll up until the control is in place
            CGPoint newContentOffset = optionsScrollView.contentOffset;
            newContentOffset.y += (controlVisualBottom - scrollViewVisibleHeight);
            
            // make sure we don't set an impossible offset caused by the "nice visual offset"
            // if a control is at the bottom of the scroll view, it will end up just above the keyboard to eliminate scrolling inconsistencies
            newContentOffset.y = MIN(newContentOffset.y, optionsScrollView.contentSize.height - scrollViewVisibleHeight);
            
            [optionsScrollView setContentOffset:newContentOffset animated:NO]; // animated:NO because we have created our own animation context around this code
        } else if (controlFrameInScrollView.origin.y < optionsScrollView.contentOffset.y) {
            // if the control is not fully visible, make it so (useful if the user taps on a partially visible input field
            CGPoint newContentOffset = optionsScrollView.contentOffset;
            newContentOffset.y = controlFrameInScrollView.origin.y;
            
            [optionsScrollView setContentOffset:newContentOffset animated:NO]; // animated:NO because we have created our own animation context around this code
        }
    }
    
    [UIView commitAnimations];
	keyboardVisible = YES;
}
-(IBAction)doneButton:(id)sender{
    [self.view endEditing:YES];
}
-(void)keyboardWillHide:(NSNotification *)notification
{
    if (!keyboardVisible) {
		//NSLog(@"Keyboard is already hidden. Ignore notification.");
		return;
	}
	
	NSDictionary *userInfo = notification.userInfo;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[[userInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[[userInfo valueForKey:UIKeyboardAnimationCurveUserInfoKey] intValue]];
    
    // undo all that keyboardWillShow-magic
    // the scroll view will adjust its contentOffset apropriately
    optionsScrollView.contentInset = UIEdgeInsetsZero;
    optionsScrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
    
    [UIView commitAnimations];
	// Keyboard is no longer visible
	keyboardVisible = NO;
}





- (void)viewWillAppear:(BOOL)animated
{
    // Setup content size
	optionsScrollView.contentSize = CGSizeMake(SCROLLVIEW_CONTENT_WIDTH,
										SCROLLVIEW_CONTENT_HEIGHT);
	
	//Initially the keyboard is hidden
	keyboardVisible = NO;
    scrollView_OriginalFrame = optionsScrollView.frame;
    [optionsScrollView scrollsToTop];
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [self customizeNavigationBar];
    [switchControl setOn:self.isSwitchOn];
    if (self.isSwitchOn) {
        [zipCodeTF setUserInteractionEnabled:NO];
    }else{
        
        [zipCodeTF setUserInteractionEnabled:YES];
    }
}

-(void)customizeNavigationBar{
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    
//    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
//    [backView setBackgroundColor:[UIColor clearColor]];
//    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
//    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
//    [backView addSubview:titleImageView];
//    titleImageView.contentMode = UIViewContentModeCenter;
//    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = childView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        childView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = childView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        childView.frame = viewFrame;
        
    }
    
}
#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}



- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = childView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        childView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = childView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        childView.frame = viewFrame;
    }
    
    
//    CAGradientLayer *gradient = [CAGradientLayer layer];
//    gradient.frame = optionsPopUpView.bounds;
//    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor lightGrayColor] CGColor], (id)[[UIColor darkGrayColor] CGColor], nil];
//    [optionsPopUpView.layer insertSublayer:gradient atIndex:0];
    
    
     [[[optionsPopUpView.layer sublayers] objectAtIndex:0] setFrame:optionsPopUpView.bounds];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
     
                                                  object:nil];
    
    [zipCodeTF resignFirstResponder];
    //    [self.view endEditing:YES];
    
}

-(void)resignTextFieldKeyBoard
{
    if ([zipCodeTF isFirstResponder])
    {
        [zipCodeTF resignFirstResponder];
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    if ([[string trimmedString] isEqual:@"0"] && [[textField.text trimmedString] length]==0)
//    {
//        return NO;
//    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 5 || ![self isNumeric:string]) ? NO : YES;
}

//-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    if([self isNumeric:string])
//        return TRUE;
//    else
//        return FALSE;
//}

-(BOOL)isNumeric:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

@end
