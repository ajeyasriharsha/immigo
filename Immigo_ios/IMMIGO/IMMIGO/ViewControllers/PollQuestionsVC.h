//
//  PollQuestionsVC.h
//  Immigo
//
//  Created by pradeep ISPV on 3/10/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMGProgressView.h"
#import "PollQuestions.h"

@interface PollQuestionsVC : UIViewController
{
    IBOutlet UIView *pollQuestionView;
    IBOutlet UIButton *heading;
    
}
//@property (nonatomic,strong) PollQuestions *selectedPoll
@property (nonatomic)int pollQuestionIndex;
@end
