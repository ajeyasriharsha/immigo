//
//  CommunityPollVC.m
//  Immigo
//
//  Created by pradeep ISPV on 3/6/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "CommunityPollVC.h"
#import "PollQuestionsVC.h"
#import "AboutUsVC.h"
#import "Common.h"
#import "AppDelegate.h"
@interface CommunityPollVC ()
{
    
    IBOutlet UIButton *heading;
    
    NSArray *pollQuestions;
    __weak IBOutlet UIView *bgView;
}
@end

@implementation CommunityPollVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [[DataDownloader sharedDownloader] setDelegate:self];
    [[PollQuestionsModel sharedDataSource] fetchPollQuestionsForDevice:[[NSUserDefaults standardUserDefaults] valueForKey:IMMIGO_GUID]];

    [self customizeNavigationBar];
    [super viewWillAppear:animated];
}
-(void)customizeNavigationBar{
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    
    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
    [backView setBackgroundColor:[UIColor clearColor]];
    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
    [backView addSubview:titleImageView];
    titleImageView.contentMode = UIViewContentModeCenter;
    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        bgView.frame = viewFrame;
        
    }
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    pollsTable.rowHeight = 60.0f;
    [Common logEventToGoogleAnalytics:@"Immigo CommunityPoll Screen"];
//    [Common logMainEventsToGoogleAnalytics:@"Immigo CommunityPoll Screen" withAction:@"Immigo CommunityPoll Screen" andLabel:@"" and:0];

    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        //NSLog(@"runnig on < 7.0");
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"left arrow"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        UIImage *image = [UIImage imageNamed:@"question"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = rightItem;
        
        [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];
        
    }

    heading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
    {
        self.navigationController.navigationBarHidden = NO;
//        UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//        UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//        titlView.image = titleLogo;
//        self.navigationItem.titleView = titlView;
//        pollsTable.rowHeight = 60.0f;
    }

	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        bgView.frame = viewFrame;
    }
    
    //    }
}

#pragma mark - DataDownloader delegate
- (void) pollQuestionsDownloadedSuccessfully
{
    pollQuestions = [[PollQuestionsModel sharedDataSource] allObjects];
    [pollsTable reloadData];
}
-(void) pollQuestionsLoadedFailure
{
    pollQuestions = [[PollQuestionsModel sharedDataSource] allObjects];
    [pollsTable reloadData];
}
#pragma mark - TableView delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return pollQuestions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PollQuestions *pollQuestion = [pollQuestions objectAtIndex:indexPath.row];
    
    PollCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
//    if (!cell)
//    {
//        cell = [[PollCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
//    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.pollQuestion.font = [UIFont fontWithName:FONT_SEMIBOLD size:14.29];
    cell.pollQuestion.textColor = UIColorFromRGB(0x4c4c4c);
    cell.pollQuestion.numberOfLines = 2;
    cell.pollQuestion.text = pollQuestion.question;//[NSString stringWithFormat:@"This is poll question area to display the question in two lines #%ld",indexPath.row+1];
    cell.votedLbl.text = [pollQuestion.answerStatus isEqual:@"Yes"]?@"Voted":@"";
    cell.votedLbl.font = [UIFont fontWithName:FONT_SEMIBOLD size:8.29];
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        [_objects removeObjectAtIndex:indexPath.row];
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
//    }
//}

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    [PollQuestionsModel sharedDataSource].selectedQuestion = [pollQuestions objectAtIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    PollQuestionsVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"PollQuestionsVC"];
    toVC.pollQuestionIndex = indexPath.row;
    [self.navigationController pushViewController:toVC animated:YES];
}
-(void)pushToViewControllerWithIdentifier:(NSString *)identifier //Pass a identifier Used in Storyboard for viewController
{
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    UIViewController* toVC = [storyboard instantiateViewControllerWithIdentifier:identifier];
    [self.navigationController pushViewController:toVC animated:YES];
    
}

- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
@end
