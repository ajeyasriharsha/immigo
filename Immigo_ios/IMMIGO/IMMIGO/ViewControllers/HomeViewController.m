//
//  ViewController.m
//  Immigo
//
//  Created by pradeep ISPV on 2/27/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "HomeViewController.h"
#import "Common.h"
#import "AboutUsVC.h"
#import "DataDownloader.h"
#import "Alerts.h"
#import "AlertsModel.h"
#import "MTPopupWindow.h"
#import "AlertView.h"
#import "AppDelegate.h"
#import "AlertsDAO.h"
#import "Alerts.h"
#import "ImmigrationSubTopicsVC.h"
#import "ImmigrationBasicsVC.h"
#import "PartnersLandscapeView.h"
#import "UIImage+Utilities.h"

@interface HomeViewController () <DataDownloaderDelegate,UIWebViewDelegate,PartnersPullableViewDelegate,MTPopupWindowDelegate>
{
    NSArray *alertsArray;
    NSTimer* myTimer;
    
    UIWebView *alertsWebview;
    UIActivityIndicatorView *activityView;
    __weak IBOutlet UIScrollView *buttonsScroll;
    NSMutableArray *views;
    int currentPage;
    IBOutlet UIView *alertsView;

}
@end

@implementation HomeViewController

-(void)setFromSplash:(BOOL)fromSplash
{
    _fromSplash = fromSplash;
}
-(BOOL)fromSplash
{
    return _fromSplash;
}
- (IBAction)pushToAbout:(id)sender
{
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    AboutUsVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"AboutUsVC"];
    toVC.fromHome = YES;
    [self.navigationController pushViewController:toVC animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    alertsScrollView.clipsToBounds = YES;
    buttonsScroll.contentSize =CGSizeMake(320, 420);
      [Common logEventToGoogleAnalytics:@"Immigo Home Screen"];
//    [Common logMainEventsToGoogleAnalytics:@"Immigo Home Screen" withAction:@"Immigo Home Screen" andLabel:@"" and:0];

    {
        basicsBtn.titleLabel.font = finderBtn.titleLabel.font = newsBtn.titleLabel.font= civicEngagementBtn.titleLabel.font = govtresourceBtn.titleLabel.font = aboutBtn.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
        [basicsBtn setTitleColor:UIColorFromRGB(0X124A7F) forState:UIControlStateNormal]; // VAMSHI
        [finderBtn setTitleColor:UIColorFromRGB(0X124A7F) forState:UIControlStateNormal]; // VAMSHI
        [civicEngagementBtn setTitleColor:UIColorFromRGB(0X124A7F) forState:UIControlStateNormal]; // VAMSHI
        [newsBtn setTitleColor:UIColorFromRGB(0X124A7F) forState:UIControlStateNormal]; // VAMSHI
        [govtresourceBtn setTitleColor:UIColorFromRGB(0X124A7F) forState:UIControlStateNormal]; // VAMSHI
        [aboutBtn setTitleColor:UIColorFromRGB(0X124A7F) forState:UIControlStateNormal]; // VAMSHI
        
    }
    
    basicsBtn.exclusiveTouch = finderBtn.exclusiveTouch = newsBtn.exclusiveTouch = civicEngagementBtn.exclusiveTouch = govtresourceBtn.exclusiveTouch = aboutBtn.exclusiveTouch = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(languageChanged) name:@"LanguageChanged" object:nil];
    
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"isAppFirstLaunch"]) {
        [self showLanguageSelectionAlert];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isAppFirstLaunch"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

}

-(void)showLanguageSelectionAlert{
    
    if ([UIAlertController class])
    {
        // use UIAlertController
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:NSLocalizedString(@"Select Language", @"")
                                   message:nil
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"English", @"") style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       //Do Some action here
                                                       //[self pushToHome];
                                                       [[BundleLocalization sharedInstance] setLanguage:@"en"];
//                                                       [[NSNotificationCenter defaultCenter] postNotificationName:@"LanguageChanged" object:nil];
                                                       [self refreshUI];
                                                       [pullUpView removeFromSuperview];
                                                       [self createPullableView];

                                                       
                                                   }];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Spanish", @"") style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           //[self pushToHome];
                                                           [[BundleLocalization sharedInstance] setLanguage:@"es"];
//                                                           [[NSNotificationCenter defaultCenter] postNotificationName:@"LanguageChanged" object:nil];
                                                           [self refreshUI];
                                                           [pullUpView removeFromSuperview];
                                                           [self createPullableView];
                                                           
                                                           
                                                       }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        // use UIAlertView
        UIAlertView* dialog = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Select Language", @"")
                                                         message:nil
                                                        delegate:self
                                               cancelButtonTitle:nil
                                               otherButtonTitles:NSLocalizedString(@"English", @""), NSLocalizedString(@"Spanish", @""),nil];
        
        dialog.tag = 400;
        [dialog show];
        
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag == 400){
        if (buttonIndex == 0) {
            //Do something
            [[BundleLocalization sharedInstance] setLanguage:@"en"];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"LanguageChanged" object:nil];
            [self refreshUI];
            [pullUpView removeFromSuperview];
            [self createPullableView];
            
        } else if (buttonIndex == 1) {
            //Do something else
            [[BundleLocalization sharedInstance] setLanguage:@"es"];
            [self refreshUI];
            [pullUpView removeFromSuperview];
            [self createPullableView];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"LanguageChanged" object:nil];
        }
    }
}

-(void)refreshUI{
    
//    IBOutlet UIButton *basicsBtn;
//    IBOutlet UIButton *finderBtn;
//    IBOutlet UIButton *newsBtn;
//    IBOutlet UIButton *civicEngagementBtn;
//    IBOutlet UIButton *govtresourceBtn;
//    IBOutlet UIButton *aboutBtn;
    /*
    "HOME_IMMIGRATIONBASICS_TITLE" ="INMIGRACIÓN";
    "HOME_CIVICENGAGEMENT_TITLE" ="COMPROMISO CÍVICO";
    "HOME_CIVILRIGHTS_TITLE" ="CIVIL RIGHTS";
    "HOME_LEGALHELP_TITLE" ="ENCONTRAR AYUDA";
    "HOME_IMMIGRATIONNEWS_TITLE" ="NOTICIAS";
    "HOME_ABOUTIMMIGO_TITLE" ="SOBRE NOSOTROS";
     */
    [basicsBtn setTitle:NSLocalizedString(@"HOME_IMMIGRATIONBASICS_TITLE", nil) forState:UIControlStateNormal];
    [finderBtn setTitle:NSLocalizedString(@"HOME_LEGALHELP_TITLE", nil) forState:UIControlStateNormal];
    [newsBtn setTitle:NSLocalizedString(@"HOME_IMMIGRATIONNEWS_TITLE", nil) forState:UIControlStateNormal];\
    [civicEngagementBtn setTitle:NSLocalizedString(@"HOME_CIVICENGAGEMENT_TITLE", nil) forState:UIControlStateNormal];
    [govtresourceBtn setTitle:NSLocalizedString(@"HOME_CIVILRIGHTS_TITLE", nil) forState:UIControlStateNormal];
    [aboutBtn setTitle:NSLocalizedString(@"HOME_ABOUTIMMIGO_TITLE", nil) forState:UIControlStateNormal];
    
}

-(void)languageChanged{
    
    // Reload our root view controller
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    NSString *storyboardName = @"Main_iPhone"; // Your storyboard name
    UIStoryboard *storybaord = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    delegate.window.rootViewController = [storybaord instantiateInitialViewController];
//        delegate.window.rootViewController = [storybaord instantiateViewControllerWithIdentifier:@"HomeViewController"];
//    [basicsBtn setTitle:NSLocalizedString(@"HOME_IMMIGRATIONBASICS_TITLE", @"") forState:UIControlStateNormal];
//    [finderBtn setTitle:NSLocalizedString(@"HOME_LEGALHELP_TITLE", @"") forState:UIControlStateNormal];
//    [newsBtn setTitle:NSLocalizedString(@"HOME_IMMIGRATIONNEWS_TITLE", @"") forState:UIControlStateNormal];
//    [civicEngagementBtn setTitle:NSLocalizedString(@"HOME_CIVICENGAGEMENT_TITLE", @"") forState:UIControlStateNormal];
//    [govtresourceBtn setTitle:NSLocalizedString(@"HOME_GOVERNMENTRESOURCE_TITLE", @"") forState:UIControlStateNormal];
//    [aboutBtn setTitle:NSLocalizedString(@"HOME_ABOUTIMMIGO_TITLE", @"") forState:UIControlStateNormal];
}
-(void)Alertsloaded
{
    if (alertsArray.count>0)
    {
        alertsArray =nil;
    }
    alertsArray = [[AlertsModel sharedDataSource] allAlerts];
    [self createAlertsPageControl];
    [self numberOfPagesInPagingView:alertsArray];

}
-(void)AlertsLoadFailure
{
    if (alertsArray.count>0)
    {
        alertsArray =nil;
    }
    alertsArray = [[AlertsModel sharedDataSource] allAlerts];
    [self createAlertsPageControl];
    [self numberOfPagesInPagingView:alertsArray];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [myTimer invalidate];
    myTimer=nil;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [[DataDownloader sharedDownloader] setDelegate:self];
    [[AlertsModel sharedDataSource] fetchAlerts];
    
    [self scrollToFirstAlert];	// Do any additional setup after loading the view, typically from a nib.

    self.navigationController.navigationBarHidden = YES;
    [self createPullableView];
    pullUpView.animationDuration = 0.9f;
    [pullUpView setOpened:_fromSplash animated:YES];
    if (_fromSplash)
    {
        [self performSelector:@selector(closePartner) withObject:self afterDelay:5.0f];
    }
    myTimer = [NSTimer scheduledTimerWithTimeInterval:10.0 target: self
                                             selector: @selector(scrollToNextAlert) userInfo: nil repeats: YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    _fromSplash = NO;
    [pullUpView removeFromSuperview];

}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)closePartner
{
    [pullUpView setOpened:NO animated:YES];
//    myTimer = [NSTimer scheduledTimerWithTimeInterval:10.0 target: self
//                                             selector: @selector(scrollToNextAlert) userInfo: nil repeats: YES];
}

-(void)viewWillLayoutSubviews{
    
    [super viewWillLayoutSubviews];
     [alertsView setFrame:CGRectMake(0, buttonsScroll.contentSize.height-alertsView.frame.size.height-10, alertsView.frame.size.width, alertsView.frame.size.height)];
    NSLog(@"Scrollview content size:%f",buttonsScroll.contentSize.height);
}
-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    //[alertsView setBackgroundColor:[UIColor redColor]];
    //[buttonsScroll setBackgroundColor:[UIColor grayColor]];
    
    return;
}
#pragma mark - Pullableview methods
-(void)createPullableView
{
    CGFloat xOffset = 0;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        xOffset = 224;
    }
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];

    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
        {
            
            //For iOs 6
            CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
            //Partner UI Fix//JUHI
            UIView *pullableTopView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, iOSDeviceScreenSize.width, 25)];
            
            UILabel *partnerLabel = [[UILabel alloc]initWithFrame:pullableTopView.bounds];
            
            partnerLabel.backgroundColor = UIColorFromRGB(0Xc51039);
            partnerLabel.textAlignment = NSTextAlignmentCenter;
            partnerLabel.text       = NSLocalizedString(@"PARTNERS", @"") ;
            partnerLabel.textColor  = [UIColor whiteColor];
            [partnerLabel setFont:[UIFont fontWithName:FONT_REGULAR size:10.0]];
            
            [pullableTopView addSubview:partnerLabel];
            
            CGRect frame = (iOSDeviceScreenSize.height == 480)? CGRectMake(0, 0, 320, 460) :CGRectMake(0, 0, 320, 548);
            
            pullUpView = [[PartnersPullableView alloc] initWithFrame:CGRectMake(xOffset, 0, frame.size.width, frame.size.height)];
            pullUpView.partnersDelegate = self;
            pullUpView.openedCenter = CGPointMake(frame.size.width/2,(frame.size.height/2));
            pullUpView.closedCenter = CGPointMake(frame.size.width/2, (frame.size.height+(frame.size.height/2)-pullableTopView.frame.size.height));
            pullUpView.center = pullUpView.closedCenter;
            
            
            [pullUpView.handleView addSubview:pullableTopView];//parternsImageView];
            pullUpView.handleView.frame = CGRectMake(0, 0, pullableTopView.frame.size.width, pullableTopView.frame.size.height);
            pullUpView.delegate = self;
            [self.view addSubview:pullUpView];
        }
        else
        {
            CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
            NSLog(@"Size from bounds in portrait%@",NSStringFromCGSize(iOSDeviceScreenSize));
            UIView *pullableTopView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, iOSDeviceScreenSize.width, 30)];
            
            UILabel *partnerLabel = [[UILabel alloc]initWithFrame:pullableTopView.bounds];
            
            partnerLabel.backgroundColor = UIColorFromRGB(0Xc51039);
            
            partnerLabel.textAlignment = NSTextAlignmentCenter;
            
            partnerLabel.text       = NSLocalizedString(@"PARTNERS", @"");
            
            partnerLabel.textColor  = [UIColor whiteColor];
            
            [partnerLabel setFont:[UIFont fontWithName:FONT_REGULAR size:10.0f]];
            
            [pullableTopView addSubview:partnerLabel];
            
            if (SYSTEM_VERSION_LESS_THAN(@"8")) {
                //ios 7
                            pullUpView = [[PartnersPullableView alloc] initWithFrame:CGRectMake(xOffset, 0, self.view.frame.size.width, self.view.frame.size.height-22)];
                            pullUpView.partnersDelegate = self;
                
                            pullUpView.openedCenter = CGPointMake(self.view.frame.size.width/2,(self.view.frame.size.height/2)+pullableTopView.frame.size.height);
                            pullUpView.closedCenter = CGPointMake(self.view.frame.size.width/2, (self.view.frame.size.height+(self.view.frame.size.height/2)-pullableTopView.frame.size.height)-10);
                            pullUpView.center = pullUpView.closedCenter;

            }else{
                //ios 8 and Above
                pullUpView = [[PartnersPullableView alloc] initWithFrame:CGRectMake(xOffset, 0, iOSDeviceScreenSize.width, iOSDeviceScreenSize.height-22)];
                pullUpView.partnersDelegate = self;
                pullUpView.openedCenter = CGPointMake(iOSDeviceScreenSize.width/2,(iOSDeviceScreenSize.height/2)+pullableTopView.frame.size.height);
                pullUpView.closedCenter = CGPointMake(iOSDeviceScreenSize.width/2, (iOSDeviceScreenSize.height+(iOSDeviceScreenSize.height/2)-pullableTopView.frame.size.height)-10);
                pullUpView.center = pullUpView.closedCenter;
            }
            
            NSLog(@"Pull Up View Center in portrait %@",NSStringFromCGPoint(pullUpView.center));
            [pullUpView.handleView addSubview:pullableTopView];//parternsImageView];
            pullUpView.handleView.frame = CGRectMake(0, 0, pullableTopView.frame.size.width, pullableTopView.frame.size.height);
            pullUpView.delegate = self;
            [self.view addSubview:pullUpView];
            
        }
        pullUpView.partnerLandscapeView.hidden = YES;
    }else
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
        {
            CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
            //Partner UI Fix//JUHI
            UIView *pullableTopView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, iOSDeviceScreenSize.width, 30)];
            
            UILabel *partnerLabel = [[UILabel alloc]initWithFrame:pullableTopView.bounds];
            
            partnerLabel.backgroundColor = UIColorFromRGB(0Xc51039);
            
            partnerLabel.textAlignment = NSTextAlignmentCenter;
            
            partnerLabel.text       = NSLocalizedString(@"PARTNERS", @"");
            
            partnerLabel.textColor  = [UIColor whiteColor];
            
            [partnerLabel setFont:[UIFont fontWithName:FONT_REGULAR size:10.0]];
            
            [pullableTopView addSubview:partnerLabel];
            
            CGRect frame = (iOSDeviceScreenSize.height == 480)? CGRectMake(0, 0, 480, 320) :CGRectMake(0, 0, 568, 320);
            pullUpView = [[PartnersPullableView alloc] initWithFrame:CGRectMake(xOffset, 0, frame.size.width, frame.size.height-10)];
            pullUpView.partnersDelegate = self;
            pullUpView.openedCenter = CGPointMake(frame.size.width/2,(frame.size.height/2));
            pullUpView.closedCenter = CGPointMake(frame.size.width/2, (frame.size.height+(frame.size.height/2)-pullUpView.frame.size.height)-20);
            pullUpView.center = pullUpView.closedCenter;
            NSLog(@"Pull Up View Center in landscape %@",NSStringFromCGPoint(pullUpView.center));
            [pullUpView.handleView addSubview:pullableTopView];//parternsImageView];
            pullUpView.handleView.frame = CGRectMake(0, 0, pullUpView.frame.size.width, pullUpView.frame.size.height);
            pullUpView.delegate = self;
            [self.view addSubview:pullUpView];
        }
        else
        {

                NSLog(@"Size from bounds in landscape%@",NSStringFromCGSize(iOSDeviceScreenSize));
            
            UIView *pullableTopView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, iOSDeviceScreenSize.width, 30)];
            
            UILabel *partnerLabel = [[UILabel alloc]initWithFrame:pullableTopView.bounds];
            
            partnerLabel.backgroundColor = UIColorFromRGB(0Xc51039);
            
            partnerLabel.textAlignment = NSTextAlignmentCenter;
            
            partnerLabel.text       = NSLocalizedString(@"PARTNERS", @"");
            
            partnerLabel.textColor  = [UIColor whiteColor];
            
            [partnerLabel setFont:[UIFont fontWithName:FONT_REGULAR size:10.0]];
            [pullableTopView addSubview:partnerLabel];
            
            if (SYSTEM_VERSION_LESS_THAN(@"8")){
                //ios 7
                            pullUpView = [[PartnersPullableView alloc] initWithFrame:CGRectMake(xOffset, 0, self.view.frame.size.width, self.view.frame.size.height-22)];
                            pullUpView.partnersDelegate = self;
                            pullUpView.openedCenter = CGPointMake(self.view.frame.size.width/2,(self.view.frame.size.height/2)+pullUpView.frame.size.height);
                            pullUpView.closedCenter = CGPointMake(self.view.frame.size.width/2, (self.view.frame.size.height+(self.view.frame.size.height/2)-pullUpView.frame.size.height)-10);
                            pullUpView.center = pullUpView.closedCenter;
                
            }else{
                
                //ios 8 and Above
                pullUpView = [[PartnersPullableView alloc] initWithFrame:CGRectMake(xOffset, 0, iOSDeviceScreenSize.width, iOSDeviceScreenSize.height-22)];
                pullUpView.partnersDelegate = self;
                pullUpView.openedCenter = CGPointMake(iOSDeviceScreenSize.width/2,(iOSDeviceScreenSize.height/2)+pullableTopView.frame.size.height);
                pullUpView.closedCenter = CGPointMake(iOSDeviceScreenSize.width/2, (iOSDeviceScreenSize.height+(iOSDeviceScreenSize.height/2)-pullableTopView.frame.size.height)-10);
                pullUpView.center = pullUpView.closedCenter;

            }
            NSLog(@"Pull Up View Center in landscape %@",NSStringFromCGPoint(pullUpView.center));
            
            [pullUpView.handleView addSubview:pullableTopView];//parternsImageView];
            pullUpView.handleView.frame = CGRectMake(0, 0, pullableTopView.frame.size.width, pullableTopView.frame.size.height);
            pullUpView.delegate = self;
            [self.view addSubview:pullUpView];
            
        }

        pullUpView.partnerLandscapeView.hidden = NO;
    }
    
    
}
- (void)pullableView:(PullableView *)pView didChangeState:(BOOL)opened
{
    if (opened)
    {
        //NSLog(@"Now I'm open!");
    }
    else
    {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(closePartner) object:self];
        //NSLog(@"Now I'm closed, pull me up again!");
    }
}
#pragma mark - AlertsPagecontrll methods
-(void)createAlertsPageControl
{
    
    for (UIView * view in alertsScrollView.subviews) {
        if (view.tag == 10) {
            [view removeFromSuperview];
        }
    }
    //NSLog(@"alert scrollview size %f",alertsScrollView.frame.size.height);
    views = [[NSMutableArray alloc] initWithCapacity:[alertsArray count]];
    for (int i = 0; i < [alertsArray count]; i++)
    {
        Alerts *alert = [alertsArray objectAtIndex:i];
        UIView *scrollInnerView = [[UIView alloc] initWithFrame:alertsScrollView.bounds];
        scrollInnerView.tag =10;
        scrollInnerView.autoresizingMask =UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
        [scrollInnerView setBackgroundColor:[UIColor clearColor]];
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(3, 1, alertsScrollView.frame.size.width, 30)];
      //  title.text = @"This is a very simple ios app using a UICollectionView to view all the photos in your iPad (or iPhones) library. As far as I know it's the only full working project on the internet. Every tutorial I could find did not have a ";//alert.alertTitle;
        title.text =alert.alertTitle;
        title.autoresizingMask =UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
        title.backgroundColor = [UIColor clearColor];
        title.textColor = UIColorFromRGB(0X124A7F);  //VAMSHI //UIColorFromRGB(UI_TITLE_HEADER_COLOR);
        title.font = [UIFont fontWithName:FONT_SEMIBOLD size:14.85f];//Size Fix //juhi
        
        [scrollInnerView addSubview:title];
        
        UITextView *descTextView =[[UITextView alloc] initWithFrame:CGRectMake(-2,title.frame.size.height-7, alertsScrollView.frame.size.width+4, alertsScrollView.frame.size.height-(title.frame.size.height+title.frame.origin.y)+4)];
        descTextView.text = alert.alertDiscription;
        [descTextView setEditable:NO];
        descTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleBottomMargin |UIViewAutoresizingFlexibleHeight;
        descTextView.textColor = UIColorFromRGB(0X737373F);//color Fix //juhi//UIColorFromRGB(UI_ALERT_DESCRIPTION_COLOR);
        descTextView.font = [UIFont fontWithName:FONT_SEMIBOLD size:13.145f];//Size Fix //juhi
        descTextView.backgroundColor = [UIColor clearColor];
        
        // descTitle.numberOfLines =3;
        // [descTitle sizeToFit];
        [scrollInnerView addSubview:descTextView];

        if ([alert.link length]>0)
        {
            UIButton *linkButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [linkButton addTarget:self action:@selector(linkTapped:) forControlEvents:UIControlEventTouchUpInside];
            linkButton.frame = CGRectMake(title.frame.origin.x, 0, alertsScrollView.frame.size.width, alertsScrollView.frame.size.height);
            linkButton.backgroundColor = [UIColor clearColor];
            linkButton.autoresizingMask =UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
            linkButton.tag = 1*(i+1);
            [scrollInnerView addSubview:linkButton];
        }


        /*
        UILabel *descTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,title.frame.size.height, alertsScrollView.frame.size.width, alertsScrollView.frame.size.height*0.6)];
        descTitle.text = alert.alertDiscription;
        descTitle.autoresizingMask =UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
        descTitle.textColor = UIColorFromRGB(UI_ALERT_DESCRIPTION_COLOR);
        descTitle.font = [UIFont fontWithName:@"OpenSans-Semibold" size:11.145f];
        descTitle.backgroundColor = [UIColor clearColor];
        descTitle.numberOfLines =3;
        [descTitle sizeToFit];
        [scrollInnerView addSubview:descTitle];
         
         */
        [alertsScrollView addSubview:scrollInnerView];
        [views addObject:scrollInnerView];
        [alertsScrollView flashScrollIndicators];

    }
    
    [self alignSubviews];
    
    /*
    //Set the content size of our scrollview according to the total width of our imageView objects.
    alertsScrollView.contentSize = CGSizeMake(alertsScrollView.frame.size.width * [alertsArray count], alertsScrollView.frame.size.height);
    */
    
}



- (void)alignSubviews
{
	// Position all the content views at their respective page positions
	alertsScrollView.contentSize = CGSizeMake([alertsArray count]*alertsScrollView.bounds.size.width,alertsScrollView.bounds.size.height);
	NSUInteger i = 0;
    
	for (UIView *v in views)
    {
		v.frame = CGRectMake(i * alertsScrollView.bounds.size.width, 0,
							 alertsScrollView.bounds.size.width, alertsScrollView.bounds.size.height);
        
        for (UIView*view in [v subviews])
        {
            if ([view isKindOfClass:[UITextView class]])
            {
                UITextView *textView =(UITextView *)view;
                //            [textView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                [textView scrollRangeToVisible:NSMakeRange(0, 0)];
            }
        }
		i++;
	}
    
    
    
}

-(void)scrollToFirstAlert
{
    CGPoint newOffset = CGPointMake(0, alertsScrollView.contentOffset.y);
    [alertsScrollView setContentOffset:newOffset animated:YES];
    alertsPageControl.currentPage = 0;
}
-(void)scrollToNextAlert
{

    NSUInteger page = alertsPageControl.currentPage + 1;
    if (page == [alertsArray count])
    {
        [self scrollToFirstAlert];
    }else
    {
        [UIView animateWithDuration:1.0 animations:^(void)
         {
             
             alertsPageControl.currentPage = alertsPageControl.currentPage+1;
             CGPoint newOffset = CGPointMake(alertsScrollView.bounds.size.width*alertsPageControl.currentPage, alertsScrollView.contentOffset.y);
             [alertsScrollView setContentOffset:newOffset animated:NO];
             
         }];
        
    }
    
}
#pragma mark -Rotation Delegates

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
								duration:(NSTimeInterval)duration {
	currentPage = alertsScrollView.contentOffset.x / alertsScrollView.bounds.size.width;
    
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
										 duration:(NSTimeInterval)duration {
    
	[pullUpView removeFromSuperview];
    [self createPullableView];
    if(interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
       interfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        /* handle resizing here*/
       // buttonsScroll.contentSize =CGSizeMake(buttonsScroll.bounds.size.width, buttonsScroll.bounds.size.height);
    }
    else if(interfaceOrientation == UIInterfaceOrientationPortrait ||
            interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        
     //   buttonsScroll.contentSize =CGSizeMake(buttonsScroll.bounds.size.width, buttonsScroll.bounds.size.height);
    }
    
    for (UIView *mine in [alertsScrollView subviews])
    {
        CGRect viewRect = mine.frame;
        CGRect mainRect = self.view.frame;
        if(CGRectIntersectsRect(mainRect, viewRect))
        {
            //NSLog(@"Current View Shown is not hidden");
            //view is visible
        }else{
            [mine setHidden:YES];
            
        }
        
        
    }
    [self alignSubviews];
    alertsScrollView.contentOffset = CGPointMake(alertsScrollView.bounds.size.width * currentPage, 0);
    
    
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    
    //scrollView is the main scroll view
    //mainview is scrollview.superview
    //view is the view inside the scroll view
    

    for (UIView *mine in [alertsScrollView subviews]) {
        [mine setHidden:NO];
        CGRect viewRect = mine.frame;
        CGRect mainRect = self.view.frame;
        if(CGRectIntersectsRect(mainRect, viewRect))
        {
            //view is visible
        }else{
            // [mine setHidden:NO];
            
        }
        
        
    }
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = alertsScrollView.frame.size.width;
    int page = floor((alertsScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    alertsPageControl.currentPage = page;
    
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self stopAnimatingAlerts];
    CGFloat pageWidth = alertsScrollView.frame.size.width;
    int page = floor((alertsScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    //    alertsPageControl.currentPage = page;
    
    if (0 == page%2)
    {
        //NSLog(@"page number %d %ld",page,(long)alertsPageControl.currentPage);
//        alertsPageControl.currentPage = alertsPageControl.currentPage+1;
    }

}
- (CGSize)sizeForNumberOfPages:(NSInteger)pageCount
{
    return CGSizeMake(2, 2);
}
- (void)numberOfPagesInPagingView:(NSArray *)objectsArray
{
    int noPages = (int)[objectsArray count];
    if(1>=noPages)
    {
        alertsPageControl.hidden=true;
        alertsPageControl.tag=1;
    }
    else
    {
        alertsPageControl.hidden=false;
        int pageShareRatio=1;
        if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && noPages>18)
        {
            if(0==noPages%18)
            {
                pageShareRatio=noPages/18;
            }
            else
                pageShareRatio=(noPages-(noPages%18))/18+1;
        }
        alertsPageControl.numberOfPages=noPages/pageShareRatio;
        alertsPageControl.tag=pageShareRatio;
    }
}
-(IBAction)linkTapped:(UIButton *)sender
{
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERTS_TITLE", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    [self stopAnimatingAlerts];
    NSInteger index = sender.tag-1;
    Alerts *alert = [alertsArray objectAtIndex:index];


    alertsWebview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-50, self.view.frame.size.height-80)];
    alertsWebview.delegate = self;
    
    activityView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, alertsWebview.frame.size.width, alertsWebview.frame.size.height)];
    activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
//    activityView.tintColor = [UIColor blueColor];
    alertsWebview.scalesPageToFit = YES;
    [alertsWebview addSubview:activityView];
    activityView.hidden = YES;
//    
//    NSString *linkUrl =alert.link;
//    NSURL *myURL;
//    if ([linkUrl hasPrefix:@"http://"]) {
//        myURL = [NSURL URLWithString:linkUrl];
//    } else {
//        myURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",linkUrl]];
//    }
//    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:myURL];
//    [alertsWebview loadRequest:urlRequest];
//    [alertsWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://www.google.co.in"]]];
    NSString *linkUrl =alert.link;
//    NSURL *myURL;
    if ([linkUrl hasPrefix:@"http://"] || [linkUrl hasPrefix:@"https://"])
    {
        
    }
    else
    {
        linkUrl = [NSString stringWithFormat:@"http://%@",linkUrl];
    }

    MTPopupWindow *popup = [[MTPopupWindow alloc] init];
    popup.delegate = self;
    popup.fileName = linkUrl;
    //    [popup.webView loadHTMLString:@"<!DOCTYPE html><html><body>    <h1>My First Heading</h1><p>My first paragraph.</p>    </body>    </html>" baseURL:nil];
    [popup show];
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    activityView.hidden = NO;
    [activityView startAnimating];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [activityView stopAnimating];
    activityView.hidden = YES;
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [activityView stopAnimating];
    activityView.hidden = YES;
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked){
        if (![Common currentNetworkStatus])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERTS_TITLE", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
            [alertView show];
            return NO;
        }
        
    }
    return YES;
    
    
}
-(void)stopAnimatingPartnersView
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(closePartner) object:self];
}
-(void)stopAnimatingAlerts
{
    [myTimer invalidate];
    myTimer = nil;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
//    if ([segue.identifier isEqualToString:@"SegueIdentifier"]) {
//        
//    }
    ImmigrationBasicsVC *vc = (ImmigrationBasicsVC *)segue.destinationViewController;
    if ([vc isKindOfClass:[ImmigrationBasicsVC class]]) {
        UIButton *butt =(UIButton *)sender;
        NSString *title =butt.titleLabel.text;
        UIImage *titleImg;
        if ([title isEqualToString:@"CIVIC ENGAGEMENT"]) {
            titleImg=[[UIImage imageNamed:@"CivicEngagement"] imageWithColor:[UIColor whiteColor]];
            vc.selTopicType =TP_CIVIC_ENGAGEMENT;
            
           
        }
        else if ([title isEqualToString:@"CIVIL RIGHTS"]){
            titleImg=[[UIImage imageNamed:@"CivilRights"] imageWithColor:[UIColor whiteColor]];
            vc.selTopicType =TP_CIVIL_RIGHTS;
        }
        else{
            titleImg=[[UIImage imageNamed:@"White_immigration"] imageWithColor:[UIColor whiteColor]];
            
        }
        
        vc.headerTitle =title;
        vc.headerImage=titleImg;

    }
    
    
    
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

@end
