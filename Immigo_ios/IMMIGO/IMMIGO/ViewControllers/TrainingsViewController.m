//
//  TrainingsViewController.m
//  Immigo
//
//  Created by pradeep ISPV on 3/6/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "TrainingsViewController.h"
#import "TrainingsDetailVC.h"
#import "Common.h"
#import "AboutUsVC.h"
#import "EventsModel.h"
#import "AppDelegate.h"
#import "Categories.h"
#import "NSDate+Helper.h"

@interface TrainingsViewController () <DataDownloaderDelegate>
{
    NSArray *trainings;
    __weak IBOutlet UIView *bgView;
}
@end

@implementation TrainingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [Common logEventToGoogleAnalytics:@"Immigo Trainings & Events Screen"];
//    [Common logMainEventsToGoogleAnalytics:@"Immigo Trainings & Events Screen" withAction:@"Immigo Trainings & Events Screen" andLabel:@"" and:0];

    shareObj = [[ShareToSocialNetworkingSites alloc] init];
    
    [[DataDownloader sharedDownloader] setDelegate:self];
    [[DataDownloader sharedDownloader] cancelAllRequests];
    
//    if (![Common currentNetworkStatus])
//    {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Trainings & Events" message:@"You are not connected to the Internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alertView show];
//    }
//    else
//    {
        [[EventsModel sharedDataSource] fetchTrainings];
//    }

    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        //NSLog(@"runnig on < 7.0");
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"left arrow"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        UIImage *image = [UIImage imageNamed:@"question"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = rightItem;
        
        [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];
        
    }

    heading.titleLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:16.785f];
    
    self.navigationController.navigationBarHidden = NO;
//    UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//    UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//    titlView.image = titleLogo;
//    self.navigationItem.titleView = titlView;
    trainingsTable.rowHeight = 60.0f;
    
	// Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [self customizeNavigationBar];
    [super viewWillAppear:animated];
}
-(void)customizeNavigationBar{
    
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    
    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 58)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
    [backView setBackgroundColor:[UIColor clearColor]];
    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav_immigo"]];
    titleImageView.frame = CGRectMake(0, 5,titleImageView.frame.size.width , titleImageView.frame.size.height); // Here I am passing origin as (45,5) but can pass them as your requirement.
    [backView addSubview:titleImageView];
    titleImageView.contentMode = UIViewContentModeCenter;
    self.navigationItem.titleView = backView;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        bgView.frame = viewFrame;
        
    }
    
}

#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        bgView.frame = viewFrame;
    }
    
    //    }
}



-(void)TrainingsLoaded
{
    trainings = [[EventsModel sharedDataSource] allTrainings];
    [trainingsTable reloadData];
}
-(void)TrainingsLoadFailure
{
    trainings = [[EventsModel sharedDataSource] allTrainings];
    [trainingsTable reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];
}
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UILabel *lbl = [[UILabel alloc] init];
//    lbl.textAlignment = NSTextAlignmentLeft;
//    lbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
//    lbl.textColor = [UIColor grayColor];
//    lbl.text = @"  TOPIC: Asylum";
//    return lbl;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 40.0f;
//}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [trainings count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Event *event = [trainings objectAtIndex:indexPath.row];
    
    NSString *address = @"";
//    if ([event.locationStreetAddress length]>0)
//    {
//        address = event.locationStreetAddress;
//        address = [address stringByAppendingString:@","];
//    }
//    if ([event.locationStreetAddress2 length]>0)
//    {
//        address = [address stringByAppendingString:event.locationStreetAddress2];
//        address = [address stringByAppendingString:@","];
//    }
    if ([event.locationCity length]>0)
    {
        address = event.locationCity;
        address = [address stringByAppendingString:@","];
    }
    if ([event.locationState length]>0) {
        address = [address stringByAppendingString:event.locationState];
//        address = [address stringByAppendingString:@","];
    }
//    if ([event.locationCountry length]>0) {
//        address = [address stringByAppendingString:event.locationCountry];
//    }

    address = [address trimmedString];
    
    
    SWTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];//[requestsTbl dequeueReusableCellWithIdentifier:@"requestCell" forIndexPath:indexPath];
    [cell setCellHeight:cell.frame.size.height];
    cell.containingTableView = tableView;
    cell.rightUtilityButtons = [self rightButtons];
    cell.delegate = self;
        
    NSRange range = [event.title rangeOfString:@"^\\s*" options:NSRegularExpressionSearch];
    NSString *trimmedTitle = [event.title stringByReplacingCharactersInRange:range withString:@""];

    NSString *title = [address length] > 35 ? [[address substringToIndex:35] stringByAppendingString:@"..."] :address;
    
    NSDate *displayDate =[self getDateFromString:[event.startDate displayDateFormat] withTime:event.dailyStartTime];
    NSString *displayDateString = [NSDate stringFromDate:displayDate withFormat:@"YYYY-MM-dd"];
    cell.topicTitle.text = [NSString stringWithFormat:@"%@ | %@",title,[displayDateString displayDateFormat]];
 
    cell.topicSubTitle.text = [NSString stringWithFormat:@"%@",trimmedTitle];
    cell.topicSubTitle.numberOfLines = 2;
    
    cell.topicTitle.font = [UIFont fontWithName:FONT_SEMIBOLD size:9.7f];
    cell.topicSubTitle.font = [UIFont fontWithName:FONT_SEMIBOLD size:11.5];
    
    cell.topicTitle.textColor = UIColorFromRGB(UI_TRAINING_HEADER_TEXT_COLOR);
    cell.topicSubTitle.textColor = UIColorFromRGB(UI_TRAINING_DESC_TEXT_COLOR);
    [cell.accView setImage:[UIImage imageNamed:@"share_icon"] forState:UIControlStateNormal];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}
- (NSDate*)getDateFromString:(NSString *)dateString withTime:(NSString *)time {
    
    NSDateFormatter *reDateFormatter = [[NSDateFormatter alloc] init];
    [reDateFormatter setDateFormat:@"MMM d, yyyy"];
    NSDate *date = [reDateFormatter dateFromString:dateString];
    NSInteger hourVal;
    NSInteger minuteVal;
    NSCalendar *calendar=[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components=[calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    NSArray* stringComponents=[time componentsSeparatedByString:@":"];
    NSString *eventTme =[stringComponents objectAtIndex:1];
    
    if (eventTme!=nil) {
        
        if ([eventTme rangeOfString:@"pm" options:NSCaseInsensitiveSearch].location == NSNotFound)
        {
            
            hourVal=[[stringComponents objectAtIndex:0] integerValue];
            minuteVal=[[stringComponents objectAtIndex:1] integerValue];
        }
        else
        {
            hourVal=[[stringComponents objectAtIndex:0] integerValue]+12;
            minuteVal=[[stringComponents objectAtIndex:1] integerValue];
           // //NSLog(@"Event time is Night");
        }
    }
    else{
        hourVal=9;
        minuteVal=0;

    }
    
    
    [components setHour:hourVal];
    [components setMinute:minuteVal];
    NSDate *finalDate = [calendar dateFromComponents:components];
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:finalDate];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:finalDate];
    NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:finalDate];
    
    return destinationDate;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [[EventsModel sharedDataSource] selectEventAtindex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    TrainingsDetailVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"TrainingsDetailVC"];
    toVC.selectedEvent = [trainings objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:toVC animated:YES];
    
}
- (NSArray *)rightButtons
{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[UIImage imageNamed:@"linkedin_icon"]];
    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[UIImage imageNamed:@"twitter_icon"]];
    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor clearColor] icon:[UIImage imageNamed:@"facebook_icon"]];

    return leftUtilityButtons;
}
-(NSArray *)getDatesBetweenTwoDates:(NSDate *)startDate andEndDate:(NSDate *)endDate{
    
    NSMutableArray *dates = [NSMutableArray array];
    NSDate *curDate = startDate;
    while([curDate timeIntervalSince1970] <= [endDate timeIntervalSince1970]) //you can also use the earlier-method
    {
        [dates addObject:curDate];
        curDate = [NSDate dateWithTimeInterval:86400 sinceDate:curDate];//Adding one day to the Current Date
        
    }
    ////NSLog(@"Dates Between %@ and %@ are %@",startDate,endDate,dates);
    return dates;
}

#pragma mark - SWTableViewDelegate


- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"TRAININGS_AND_EVENTS_TITLE", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
        [alertView show];
        return ;
    }

    NSIndexPath *indexPath = [trainingsTable indexPathForCell:cell];
    Event *selectedEvent = [trainings objectAtIndex:indexPath.row];
    
    NSString *date;
    
    if ([selectedEvent.dailyStartTime displayDateFormat] && [selectedEvent.dailyEndTime displayDateFormat])
    {
        NSString *datesString;
        if ([[selectedEvent.startDate displayDateFormat] isEqual:[selectedEvent.endDate displayDateFormat]])
        {
            datesString = [NSString stringWithFormat:@"%@",[selectedEvent.startDate displayDateFormat]];
        }
        else
        {
            datesString = [NSString stringWithFormat:@"%@ - %@",[selectedEvent.startDate displayDateFormat],[selectedEvent.endDate displayDateFormat]];
        }
        
        date = [NSString stringWithFormat:@"%@",datesString];
    }
    else
    {
        if ([[selectedEvent.startDate displayDateFormat] isEqual:[selectedEvent.endDate displayDateFormat]])
        {
            date = [NSString stringWithFormat:@"%@",[selectedEvent.startDate displayDateFormat]];
        }
        else
        {
            date = [NSString stringWithFormat:@"%@ - %@",[selectedEvent.startDate displayDateFormat],[selectedEvent.endDate displayDateFormat]];
        }
    }
//    NSString *sharingText = [NSString stringWithFormat:@"%@ \n %@",[selectedEvent.title trimmedString],date];
//    date =[date stringByAppendingFormat:@"(UTC)"];
    NSString *sharingText = [NSString stringWithFormat:@"%@ \n %@",[selectedEvent.title trimmedString],date];


    switch (index)
    {
        case 0:
        {
            //NSLog(@"linkedin was pressed");
            
//            UrlShortener *shortener = [[UrlShortener alloc] init];
//            [shortener shortenUrl:selectedEvent.link withService:UrlShortenerServiceBitly completion:^(NSString *shortUrl) {
//                //NSLog(@"Shortened Url:%@",shortUrl);
//                
//            } error:^(NSError *error) {
//                // Handle the error.
//                //NSLog(@"Error: %@", [error localizedDescription]);
//            }];
            [shareObj linkedInLogIn:selectedEvent.link andText:sharingText fromViewContoller:self];

            //[userRequests objectAtIndex:indexPath.row];
            //            [[ContactsModel sharedDataSource] processFriendRequest:indexedReq withStatus:CRS_ACCEPTED]; 9538883912
            
        }
            break;
        case 1:
        {
            //NSLog(@"twitter was pressed");
            [shareObj tweetTapped:selectedEvent.link andText:sharingText fromViewContoller:self];
            //            [[ContactsModel sharedDataSource] processFriendRequest:indexedReq withStatus:CRS_DENIED];
        }
            break;
        case 2:
        {
            //NSLog(@"fb was pressed");
            [shareObj fbShareBtnTapped:selectedEvent.link andText:sharingText fromViewContoller:self];
            //            [[ContactsModel sharedDataSource] processFriendRequest:indexedReq withStatus:CRS_BLOCKED];
            
        }
            break;
        default:
            break;
    }
    //    if (indexPath)
    //    {
    //        [userRequests removeObjectAtIndex:indexPath.row];
    //        [requestsTbl deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    //    }
    //
}
/*
 - (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
 switch (index) {
 case 0:
 {
 //NSLog(@"More button was pressed");
 UIAlertView *alertTest = [[UIAlertView alloc] initWithTitle:@"Hello" message:@"More more more" delegate:nil cancelButtonTitle:@"cancel" otherButtonTitles: nil];
 [alertTest show];
 
 [cell hideUtilityButtonsAnimated:YES];
 break;
 }
 case 1:
 {
 // Delete button was pressed
 NSIndexPath *cellIndexPath = [requestsTbl indexPathForCell:cell];
 
 [userRequests[cellIndexPath.section] removeObjectAtIndex:cellIndexPath.row];
 [requestsTbl deleteRowsAtIndexPaths:@[cellIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
 break;
 }
 default:
 break;
 }
 }
 */
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
@end
