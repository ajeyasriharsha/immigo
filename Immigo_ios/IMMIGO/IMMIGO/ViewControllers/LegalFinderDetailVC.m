//
//  LegalFinderDetailVC.m
//  Immigo
//
//  Created by pradeep ISPV on 3/11/14.
//  Copyright (c) 2014 pradeep ISPV. All rights reserved.
//

#import "LegalFinderDetailVC.h"
#import "AboutUsVC.h"
#import "LegalFinder.h"
#import "LegalFinderModel.h"
#import "Categories.h"
#import <MessageUI/MessageUI.h>
#import "LegalFinderUrlVC.h"
#import "Common.h"
#import "UIImage+Utilities.h"

#define finderTitle @"A_finderTitle"
#define addressMacro @"B_Address1"
#define urlMacro @"C_Url:"
#define emailMacro @"D_email:"
#define mPhoneMacro @"E_Mobile:"
#define EMAIL_TAG 22222
#define URL_TAG 333333
#define PHONE_TAG 444444

@interface LegalFinderDetailVC () <MFMailComposeViewControllerDelegate>
{
    IBOutlet UIScrollView *detailScrollView;
    LegalFinder *selectedFinder;
    NSMutableArray *sortedKeys;

    __weak IBOutlet UIView *bgView;
    IBOutlet UIButton *mapButton;
    IBOutlet MKMapView *detailMapView;
    IBOutlet UIButton *heading;
    __weak IBOutlet UITableView *dataTable;
}
@property (nonatomic,strong)    NSMutableDictionary *dataDict;
- (IBAction)mapIconTapped:(id)sender;
@end

@implementation LegalFinderDetailVC
//@synthesize isCurrentLocationEnabled,currentCoordinates;
@synthesize dataDict;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    selectedFinder = [[LegalFinderModel sharedDataSource] selectedFinder];
    
    detailMapView.hidden = YES;
    heading.backgroundColor=UIColorFromRGB(0Xc51039);
    [mapButton setImage:[[UIImage imageNamed:@"map_icon"] imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        //NSLog(@"runnig on < 7.0");
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIImage *image1 = [UIImage imageNamed:@"left arrow"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setImage:image1 forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button1.frame = CGRectMake(0.0, 0.0, image1.size.width, image1.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        UIImage *image = [UIImage imageNamed:@"question"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(questionMarkTapped:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        
        // Create a custom UIBarButtonItem with the button
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = rightItem;
        
        
        [button1 setImageEdgeInsets:UIEdgeInsetsMake(-14, 9, 0, -9)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(-14, -9, 0, 9)];

        
    }

//        heading.titleLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:16.785f];
    {
        self.navigationController.navigationBarHidden = NO;
//        UIImage *titleLogo =[UIImage imageNamed:@"Nav_immigo"];
//        UIImageView *titlView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLogo.size.width, titleLogo.size.height)];
//        titlView.image = titleLogo;
//        self.navigationItem.titleView = titlView;
        //        pollsTable.rowHeight = 60.0f;
    }

    //*********** Start Detail creation *************//
    
    CGFloat headingsGap = 8.0f;
    CGPoint cursor = CGPointMake(20,7);
    CGSize size = CGSizeMake(self.view.frame.size.width-cursor.x-cursor.x, self.view.frame.size.height);
    
    {
        UILabel *newsTitle = [[UILabel alloc] initWithFrame:CGRectMake(cursor.x, cursor.y, size.width, 40.0f)];
        [newsTitle setAutoresizingMask:UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin];
        newsTitle.text = [selectedFinder.finderName trimmedString];
        newsTitle.font = [UIFont fontWithName:FONT_REGULAR size:14.9f];
        newsTitle.numberOfLines = 0;
        
        CGSize newsTitleSize = [newsTitle sizeOfMultiLineLabel];
        CGRect newsTitleFrame = newsTitle.frame;
        newsTitleFrame.size.height = newsTitleSize.height;
        newsTitle.frame = newsTitleFrame;
        
        [detailScrollView addSubview:newsTitle];
        
        cursor.y +=newsTitle.frame.size.height+headingsGap;
    }

    NSString *address = @"";
    if ([selectedFinder.finderAddress.street length]>0)
    {
        address = selectedFinder.finderAddress.street;
        address = [address stringByAppendingString:@","];
    }
    if ([selectedFinder.finderAddress.city length]>0)
    {
        address = [address stringByAppendingString:selectedFinder.finderAddress.city];
        address = [address stringByAppendingString:@","];
    }
    if ([selectedFinder.finderAddress.state length]>0)
    {
        address = [address stringByAppendingString:selectedFinder.finderAddress.state];
        address = [address stringByAppendingString:@" "];
    }
    if (selectedFinder.finderAddress.zipcode >0)
    {
        address = [address stringByAppendingString:selectedFinder.finderAddress.zipcode];
    }

    self.dataDict = [[NSMutableDictionary alloc] init];

    // address label
    // contacts label
    // email Button
    // phone Button
    if (selectedFinder.finderName.length >0) {
        [self.dataDict setObject:[selectedFinder.finderName trimmedString] forKey:finderTitle];
    }
    if (address.length >0)
    { // address label
        [self.dataDict setObject:address forKey:addressMacro];
    }
    if ([selectedFinder.contact.website length]>0)
    { // Website Button
        [self.dataDict setObject:[selectedFinder.contact.website trimmedString] forKey:urlMacro];
    }
    if ([selectedFinder.contact.email length]>0)
    { // email Button
        [self.dataDict setObject:[selectedFinder.contact.email trimmedString] forKey:emailMacro];
        
    }
    if ([selectedFinder.contact.phone length]>0)
    { // phone Button
        [self.dataDict setObject:[selectedFinder.contact.phone trimmedString] forKey:mPhoneMacro];
        
    }

}


-(void)viewWillAppear:(BOOL)animated
{
    
    [self customizeNavigationBar];
    [super viewWillAppear:animated];
}
-(void)customizeNavigationBar{
    
    
    // Get the navigation controller of this view controller with:
    
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    frame.size.height = 58.0f;
    [navigationBar setFrame:frame];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0 )
    {
        //fix
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = (viewFrame.size.width == 320) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }else {
        
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =  navigationBar.frame.origin.y+frame.size.height;
        bgView.frame = viewFrame;
        
    }
    
    
    
}

#pragma mark tableview data source and Delegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.dataDict allKeys] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0 && [[self.dataDict allKeys]containsObject:finderTitle])
    {
        static NSString *CellIdentifier = @"eGViewContactAddressCell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.textLabel.text =[self.dataDict valueForKey:finderTitle];
        [cell.textLabel setFont:[UIFont fontWithName:FONT_BOLD size:16.5]];
        [cell.textLabel setNumberOfLines:0];
        [cell.textLabel setLineBreakMode:NSLineBreakByWordWrapping];
        cell.textLabel.textColor = UIColorFromRGB(0X124A7F);
        return cell;
        
    }
    else{
        
        
        static NSString *CellIdentifier = @"eGViewContactOtherCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
            
            sortedKeys = [[[self.dataDict allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)] mutableCopy];
            
            NSInteger currentIndex = 0;
            
            if([[self.dataDict allKeys]containsObject:finderTitle]){
                [sortedKeys removeObjectAtIndex:[sortedKeys indexOfObject:finderTitle]];
                currentIndex = indexPath.row-1;
            }
            else{
                currentIndex = indexPath.row;
            }
        NSString *headerLabelText = [sortedKeys objectAtIndex:currentIndex];
        [cell.textLabel setNumberOfLines:0];
        [cell.textLabel setLineBreakMode:NSLineBreakByWordWrapping];
        cell.textLabel.text =[self.dataDict valueForKey:headerLabelText];
        cell.textLabel.textColor=UIColorFromRGB(0Xc51039);
        
        if([[sortedKeys objectAtIndex:currentIndex] isEqualToString:emailMacro])
        {
            cell.imageView.image =[UIImage imageNamed:@"mail_icon"];
            [cell.textLabel setFont:[UIFont fontWithName:FONT_LIGHT size:14.5]];
            cell.tag = EMAIL_TAG;
        }
        else if([[sortedKeys objectAtIndex:currentIndex] isEqualToString:addressMacro])
        {
            //cell.imageView.image =[UIImage imageNamed:@"location_icon"];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"location_icon"]];
            cell.accessoryView = imageView;
            [cell.textLabel setFont:[UIFont fontWithName:FONT_EXTRA_BOLD size:13.5]];
            //[cell.textLabel setFont:[UIFont fontWithName:FONT_BOLD size:16.5]]; // VAMSHI
        }
        else if([[sortedKeys objectAtIndex:currentIndex] isEqualToString:urlMacro])
        {
            cell.imageView.image =[UIImage imageNamed:@"url_icon"];
            [cell.textLabel setFont:[UIFont fontWithName:FONT_LIGHT size:14.5]];
            cell.tag = URL_TAG;
        }//mPhoneMacro
        else if([[sortedKeys objectAtIndex:currentIndex] isEqualToString:mPhoneMacro])
        {
            cell.imageView.image =[UIImage imageNamed:@"phone_icon"];
            [cell.textLabel setFont:[UIFont fontWithName:FONT_LIGHT size:14.5]];
            cell.tag = PHONE_TAG;
        }
        return cell;

    }
    return nil;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell =(UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.tag == EMAIL_TAG && ([sortedKeys containsObject:emailMacro])) {
        // MAIL
        NSLog(@"MAIL");
        
        if ([MFMailComposeViewController canSendMail])
        {
            // set the sendTo address
            NSMutableArray *recipients = [[NSMutableArray alloc] initWithCapacity:1];
            [recipients addObject:selectedFinder.contact.email];
            
            MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
            controller.mailComposeDelegate = self;
            //		[controller setSubject:@""];
            
            [controller setToRecipients:recipients];
            
            if ([self respondsToSelector:@selector(presentViewController:animated:completion:)])
            {
                
                [[controller navigationBar] setTintColor:[UIColor blueColor]];
                [self presentViewController:controller animated:YES completion:nil];
                
                
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERT_TITLE", @"") message:NSLocalizedString(@"NOEMAIL_ACCOUNT_FOUND", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles: nil];
            [alert show];
            
        }

    }
    else if (cell.tag == PHONE_TAG && [sortedKeys containsObject:mPhoneMacro])
    {
        NSLog(@"PHONE");
        if ([[selectedFinder.contact.phone trimmedString] length]>0)
        {
            
            if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
            {
                
                NSString *fianlphoneNumber = [[selectedFinder.contact.phone trimmedString] frameContactNumber];
                NSString *phoneNumDecimalsOnly = [[fianlphoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
                NSString *phoneNumber = [@"telprompt://" stringByAppendingString:phoneNumDecimalsOnly];
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                
            } else {
                
                UIAlertView *warning =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERT_TITLE", @"") message:NSLocalizedString(@"DEVICE_NOT_SUPPORTED_ERROR", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
                
                [warning show];
            }
        }

    }
    else if (cell.tag == URL_TAG && [sortedKeys containsObject:urlMacro])
    {
        NSLog(@"URL");
        
        if (![Common currentNetworkStatus])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"LEGAL_HELP_FINDER_TITLE", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
            [alertView show];
            return;
        }
        UIStoryboard *storyboard;
        if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        else
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
        LegalFinderUrlVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"LegalFinderUrlVC"];
        toVC.urlAddress = [selectedFinder.contact.website trimmedString];
        [self.navigationController pushViewController:toVC animated:YES];
    }
    
}

-(IBAction)websiteTapped:(id)sender
{
    if (![Common currentNetworkStatus])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"LEGAL_HELP_FINDER_TITLE", @"") message:NSLocalizedString(@"NOINTERNET_ERROR_MESSAGE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    UIStoryboard *storyboard;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    LegalFinderUrlVC* toVC = [storyboard instantiateViewControllerWithIdentifier:@"LegalFinderUrlVC"];
    toVC.urlAddress = [selectedFinder.contact.website trimmedString];
    [self.navigationController pushViewController:toVC animated:YES];

}
-(IBAction)emailBtnTapped:(id)sender
{

    if ([MFMailComposeViewController canSendMail])
	{
		// set the sendTo address
		NSMutableArray *recipients = [[NSMutableArray alloc] initWithCapacity:1];
		[recipients addObject:selectedFinder.contact.email];
		
		MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
		controller.mailComposeDelegate = self;
//		[controller setSubject:@""];
		
		[controller setToRecipients:recipients];
		
		if ([self respondsToSelector:@selector(presentViewController:animated:completion:)])
        {
            
            [[controller navigationBar] setTintColor:[UIColor blueColor]];
            [self presentViewController:controller animated:YES completion:nil];
            
            
        }
	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERT_TITLE", @"") message:NSLocalizedString(@"NOEMAIL_ACCOUNT_FOUND", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles: nil];
		[alert show];
		
	}

}
-(IBAction)phoneBtnTapped:(id)sender
{
    //NSLog(@"Phone Tapped");
    if ([[selectedFinder.contact.phone trimmedString] length]>0)
    {

        if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        {
            
            NSString *fianlphoneNumber = [[selectedFinder.contact.phone trimmedString] frameContactNumber];
            NSString *phoneNumDecimalsOnly = [[fianlphoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
            NSString *phoneNumber = [@"telprompt://" stringByAppendingString:phoneNumDecimalsOnly];

            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
            
        } else {
            
            UIAlertView *warning =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERT_TITLE", @"") message:NSLocalizedString(@"DEVICE_NOT_SUPPORTED_ERROR", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles:nil];
            
            [warning show];
        }
    }
}
-(IBAction)tollFreeBtnTapped:(id)sender
{
    
}
#pragma mark Orientation handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    //[super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    UINavigationBar *navigationBar = [self.navigationController navigationBar];
    CGRect frame = [navigationBar frame];
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        frame.size.height = 58;
        
    } else
    {
        frame.size.height = 58.0f;    //fix
    }
    navigationBar.frame = frame;
    //fix
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0)
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y = UIInterfaceOrientationIsPortrait(orientation) ? navigationBar.frame.origin.y-5 : navigationBar.frame.origin.y+5;
        bgView.frame = viewFrame;
    }
    else
    {
        CGRect viewFrame = bgView.frame;
        viewFrame.origin.y =navigationBar.frame.size.height+20;
        bgView.frame = viewFrame;
    }
    
    //    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)questionMarkTapped:(id)sender
{
    [self performSegueWithIdentifier: @"question" sender: self];
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	switch (result)
	{
		case MFMailComposeResultCancelled:
		{
			break;
		}
		case MFMailComposeResultSaved:
		{
			break;
		}
		case MFMailComposeResultSent:
		{
			break;
		}
		case MFMailComposeResultFailed:
		{
			break;
		}
		default:
		{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"EMAIL_TITLE", @"") message:NSLocalizedString(@"EMAIL_FAILED", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_OK_BUTTON_TITLE", @"") otherButtonTitles: nil];
			[alert show];
            
		}
			break;
	}
	if ([self respondsToSelector:@selector(dismissModalViewControllerAnimated:)]) {
        [self performSelector:@selector(dismissModalViewControllerAnimated:) withObject:[NSNumber numberWithBool:YES]];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
	
	
}


- (IBAction)mapIconTapped:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if ([sender isSelected])
    {
        [sender setImage:[[UIImage imageNamed:@"listIcon"] imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.6];
        
        [UIView setAnimationDelegate: self];
        dataTable.hidden = YES;
        detailMapView.hidden = NO;
        
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                               forView:detailMapView cache:YES];
        [UIView commitAnimations];
        
        [self dropPins];
    }
    else
    {
        
        [sender setImage:[[UIImage imageNamed:@"map_icon"] imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.6];
        
        [UIView setAnimationDelegate: self];
        //    [UIView setAnimationDidStopSelector:
        //     @selector(animationFinished:finished:context:)];
        dataTable.hidden = NO;
        detailMapView.hidden = YES;
        
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                               forView:dataTable cache:YES];
        [UIView commitAnimations];
        
    }
}
#pragma mark - mapview Delegates
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    mapButton.selected = NO;
    [mapButton setImage:[UIImage imageNamed:@"map_icon"] forState:UIControlStateNormal];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6];
    
    [UIView setAnimationDelegate: self];
    //    [UIView setAnimationDidStopSelector:
    //     @selector(animationFinished:finished:context:)];
    detailScrollView.hidden = NO;
    detailMapView.hidden = YES;
    
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                           forView:detailScrollView cache:YES];
    [UIView commitAnimations];
}
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *SFAnnotationIdentifier = @"mapView";
    MKAnnotationView *flagAnnotationView = [detailMapView dequeueReusableAnnotationViewWithIdentifier:SFAnnotationIdentifier];
    
    if (flagAnnotationView == nil)
    {
        MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                                        reuseIdentifier:SFAnnotationIdentifier];
        annotationView.canShowCallout = YES;
        annotationView.backgroundColor = [UIColor clearColor];
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        [rightButton setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
        [rightButton addTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
        annotationView.rightCalloutAccessoryView = rightButton;
        UIImage *flagImage = [UIImage imageNamed:@"pin.png"];
        annotationView.image = flagImage;
        //        if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)
        //        {
        annotationView.rightCalloutAccessoryView.tintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"arrow"]];
        //        }
        annotationView.opaque = NO;
        
        
        
        //change her
        return annotationView;
    }
    else
    {
        flagAnnotationView.annotation = annotation;
    }
    return flagAnnotationView;
}
- (void)showAnnotations:(NSArray *)annotations animated:(BOOL)animated
{
    
}
-(void)dropPins
{

        //Create your annotation
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        // Set your annotation to point at your coordinate
        point.coordinate = selectedFinder.finderAddress.coordinate;
        point.title = selectedFinder.finderName;
        //Drop pin on map
        [detailMapView addAnnotation:point];
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)
    {
        [detailMapView showAnnotations:[NSArray arrayWithObject:point] animated:YES];
    }
    else
    {
        [self zoomToFitMapAnnotations:detailMapView];
    }
}
- (void)zoomToFitMapAnnotations:(MKMapView *)mapView {
    if ([mapView.annotations count] == 0) return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    for(id<MKAnnotation> annotation in mapView.annotations) {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
    
    // Add a little extra space on the sides
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.1;
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.1;
    
    region = [mapView regionThatFits:region];
    [mapView setRegion:region animated:YES];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
@end
