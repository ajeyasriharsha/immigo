//
//  NSDictionary+JSON.h
//  ComplianceWire
//
//  Created by Prasad Babu KN on 3/14/14.
//  Copyright (c) 2014 EduNeering Holdings, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (JSON)

- (BOOL)hasValidJSONKey:(NSString*) key;
- (id)getObjectForKey:(NSString*) key;
- (int)getIntForKey:(NSString*) key;
- (NSInteger)getIntegerForKey:(NSString*) key;
- (NSNumber*)getNSNumberForKey:(NSString*) key;
- (BOOL)getBoolForKey:(NSString*) key;
- (NSString*)getStringForKey:(NSString*) key;
- (NSString*)getURLStringForKey:(NSString*) key;
- (NSData*)getNSDataForKey:(NSString*) key;
- (NSDate *)getDateForKey:(NSString*) key;
- (NSDate*)getTimeForKey:(NSString*)key;
- (NSTimeInterval) getNSTimeIntervalForKey:(NSString*) key;
- (float)getFloatForKey:(NSString*) key;

@end
