//
//  SubTopic.h
//  LinkedInClientLibrary
//
//  Created by pradeep ISPV on 4/18/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CheckListItems, Links, Topic;

@interface SubTopic : NSManagedObject

@property (nonatomic) int32_t subTopicId;
@property (nonatomic, retain) NSString * subtopicTitle;
@property (nonatomic, retain) NSString * textContent;
@property (nonatomic, retain) Topic *topic;
@property (nonatomic, retain) NSSet *links;
@property (nonatomic, retain) NSSet *checkListItems;
@end

@interface SubTopic (CoreDataGeneratedAccessors)

- (void)addLinksObject:(Links *)value;
- (void)removeLinksObject:(Links *)value;
- (void)addLinks:(NSSet *)values;
- (void)removeLinks:(NSSet *)values;

- (void)addCheckListItemsObject:(CheckListItems *)value;
- (void)removeCheckListItemsObject:(CheckListItems *)value;
- (void)addCheckListItems:(NSSet *)values;
- (void)removeCheckListItems:(NSSet *)values;

@end
