//
//  CheckListItems.m
//  LinkedInClientLibrary
//
//  Created by pradeep ISPV on 4/18/14.
//
//

#import "CheckListItems.h"
#import "Items.h"
#import "SubTopic.h"


@implementation CheckListItems

@dynamic questionText;
@dynamic checkListItems;
@dynamic items;

@end
