//
//  Links.h
//  LinkedInClientLibrary
//
//  Created by pradeep ISPV on 4/18/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SubTopic;

@interface Links : NSManagedObject

@property (nonatomic) int32_t id;
@property (nonatomic, retain) NSString * linkUrl;
@property (nonatomic, retain) SubTopic *links;

@end
