//
//  Topic.h
//  LinkedInClientLibrary
//
//  Created by pradeep ISPV on 4/18/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SubTopic;

@interface Topic : NSManagedObject

@property (nonatomic, retain) NSString * topicDescription;
@property (nonatomic) int32_t topicId;
@property (nonatomic, retain) NSString * topicTitle;
@property (nonatomic, retain) NSSet *subTopics;
@end

@interface Topic (CoreDataGeneratedAccessors)

- (void)addSubTopicsObject:(SubTopic *)value;
- (void)removeSubTopicsObject:(SubTopic *)value;
- (void)addSubTopics:(NSSet *)values;
- (void)removeSubTopics:(NSSet *)values;

@end
