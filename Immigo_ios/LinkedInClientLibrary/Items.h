//
//  Items.h
//  LinkedInClientLibrary
//
//  Created by pradeep ISPV on 4/18/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CheckListItems;

@interface Items : NSManagedObject

@property (nonatomic) int32_t id;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) CheckListItems *items;

@end
