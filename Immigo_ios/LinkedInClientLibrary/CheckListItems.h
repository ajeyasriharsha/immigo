//
//  CheckListItems.h
//  LinkedInClientLibrary
//
//  Created by pradeep ISPV on 4/18/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Items, SubTopic;

@interface CheckListItems : NSManagedObject

@property (nonatomic, retain) NSString * questionText;
@property (nonatomic, retain) SubTopic *checkListItems;
@property (nonatomic, retain) NSSet *items;
@end

@interface CheckListItems (CoreDataGeneratedAccessors)

- (void)addItemsObject:(Items *)value;
- (void)removeItemsObject:(Items *)value;
- (void)addItems:(NSSet *)values;
- (void)removeItems:(NSSet *)values;

@end
