//
//  Links.m
//  LinkedInClientLibrary
//
//  Created by pradeep ISPV on 4/18/14.
//
//

#import "Links.h"
#import "SubTopic.h"


@implementation Links

@dynamic id;
@dynamic linkUrl;
@dynamic links;

@end
