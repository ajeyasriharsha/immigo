//
//  Disclaimer.h
//  LinkedInClientLibrary
//
//  Created by pradeep ISPV on 4/21/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Disclaimer : NSManagedObject

@property (nonatomic, retain) NSString * content;
@property (nonatomic) int32_t key;

@end
