//
//  SubTopic.m
//  LinkedInClientLibrary
//
//  Created by pradeep ISPV on 4/18/14.
//
//

#import "SubTopic.h"
#import "CheckListItems.h"
#import "Links.h"
#import "Topic.h"


@implementation SubTopic

@dynamic subTopicId;
@dynamic subtopicTitle;
@dynamic textContent;
@dynamic topic;
@dynamic links;
@dynamic checkListItems;

@end
