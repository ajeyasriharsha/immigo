//
//  Topic.m
//  LinkedInClientLibrary
//
//  Created by pradeep ISPV on 4/18/14.
//
//

#import "Topic.h"
#import "SubTopic.h"


@implementation Topic

@dynamic topicDescription;
@dynamic topicId;
@dynamic topicTitle;
@dynamic subTopics;

@end
