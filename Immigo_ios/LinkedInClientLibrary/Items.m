//
//  Items.m
//  LinkedInClientLibrary
//
//  Created by pradeep ISPV on 4/18/14.
//
//

#import "Items.h"
#import "CheckListItems.h"


@implementation Items

@dynamic id;
@dynamic text;
@dynamic items;

@end
